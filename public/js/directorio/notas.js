$(document).ready(function() {	
	// Inicialización de variable
	var field = $("#datepicker")[0];

	$(".tooltips").tooltip();
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/directorio/notas");

	// Función para obtener fecha actual
	toString = function(date) {
		const dia = (date.getDate() < 10 ? "0" : "") + date.getDate();
		const mes = ((date.getMonth() + 1) < 10 ? "0" : "") + (date.getMonth() + 1);
		const ano = date.getFullYear();
		return ano + "-" + mes + "-" + dia;
	}

	// Obtener nota del día
	$.post("../directorio/getnota", { fecha: toString(new Date()) }, function(data) {
		$("#summernote").summernote("code", data);
	});

	// Obtención de todas las notas para el usuario
	$.post("../directorio/getnotas", function(data) {
		picker = new Pikaday({
			keyboardInput: false,
			firstDay: 1,
			minDate: new Date("2018-01-01"),
			bound: false,
			container: $("#container")[0],
			format: "DD/MM/YYYY",

			onSelect: function(date) {
				var summernote = $("#summernote");

				summernote.summernote("code", "");
				summernote.summernote("disable");
				field.value = toString(date);

	    		$.post("../directorio/getnota", { fecha: field.value }, function(data) {
					summernote.summernote("enable");
					summernote.summernote("code", data);
	    		});
			},

			events: data
	    });

		field.parentNode.insertBefore(picker.el, field.nextSibling);

        $("#preloader").fadeOut("slow");
        $("body").css({ "overflow": "visible" });
	});

	// Inicialización de editor de texto
	$("#summernote").summernote({
		height: 300,
		lang: "es-ES"
	});

	$("#regNota").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#inbox-compose-form");
        var loading = $("#loaNota");

        if($.trim($("#summernote").val()) != "") {
            loading.show().removeClass("hide");
            
            var str = $f.serializeArray();
				str.push({ name: "fecha", value: field.value });

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../directorio/regnota",
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }
	});

	$("#eliNota").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#inbox-compose-form");
        var loading = $("#loaNota");

        loading.show().removeClass("hide");
        
        // Bloqueo de peticiones repetidas
        if($f.data("locked") == undefined || !$f.data("locked")) {
            $.ajax({  
                type: "POST",
                url: "../directorio/elinota",
                dataType: "json",

                data: {
                	fecha: field.value
                },

                beforeSend: function() { 
                    $f.data("locked", true);
                },

                success: function(respuesta) {
                    loading.hide();
					$("#summernote").summernote("code", "");

                    notif({
                        msg: respuesta.text,
                        type: respuesta.type,
                        position: "center",
                        multiline: true
                    });
                },

                error: function () {
                    loading.hide();

                    notif({
                        msg: "Sucedió un error, contacte con el administrador del sistema.",
                        type: "error",
                        position: "center",
                        multiline: true
                    });
                },

                complete: function() { 
                    $f.data("locked", false);
                }               
            });

            return false;
        }
	});
});