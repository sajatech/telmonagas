$(document).ready(function() {
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/directorio/contactos");
    $("#telefono1, #telefono2").inputmask();

    $.get("../directorio/getcontactos", function(data) {
        var data = JSON.parse(data);

        $("#ajaxSpinnerContactos").hide();
        $("#panContactos").removeClass("hide").show();

        // Tabla de los clientes
        oTable = $("#contactos").DataTable({
            "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-12'p>>",
            "aaData": data.aaData,
            "bServerSide": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "lengthChange":false,
            "aaSorting": [],
            "info": false,
            "iDisplayLength": 8,

            "aoColumns": [
                { "mData": "codigo", "visible": false },
                { "mData": "nombre" },
                { "mData": "telefono1" },
                { "mData": "telefono2" }
            ],

            "oTableTools": {
                "sRowSelect": "single",
                "aButtons": false         
        	},

            "language": {
                "lengthMenu": "_MENU_ &nbsp;&nbsp;"
            }
        });

        oTable.on("draw", function() {
            var body = $(oTable.table().body());

            body.unhighlight();
            body.highlight( oTable.search() );
        });

        $("#contactos_filter input").addClass("form-control").attr("placeholder", "Buscar Contacto...");

        // Se añaden los elementos de la tabla al panel
        $("#panContactos .panel-ctrls").append($("#contactos_filter").addClass("pull-right"));

        $("#panContactos .panel-footer").append($("#contactos+.row"));
        $("#contactos_paginate>ul.pagination").addClass("pull-right");
        $("#contactos_wrapper").find("div").remove();

        new $.fn.dataTable.FixedHeader(oTable);

        // ******************************** //

        $("#contactos tbody").on("click", "tr", function() {
            var $this = $(this);
            var data = oTable.row($this).data();
            var regContacto = $("#regContacto");
            var eliContacto = $("#eliContacto");

            if(($this.attr("class")).indexOf("active") > 0) {
                $("#nombre").val(data.nombre);
                $("#telefono1").val(data.telefono1);
                $("#telefono2").val(data.telefono2);
                eliContacto.removeClass("hide");
                regContacto.attr("data-contacto-accion", "actualizar");
                regContacto.attr("data-contacto-codigo", data.codigo);
            } else {
                $("#nombre, #telefono1, #telefono2").val("");
                eliContacto.addClass("hide");
                regContacto.attr("data-contacto-accion", "registrar");
            }
        });   
    });

    // Registro de contactos
    $("#regContacto").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forContacto");
        var loading = $("#loaContacto");

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var $this = $(this);
            var accion = $this.attr("data-contacto-accion");

            var str = $f.serializeArray();
                str.push({ name: "accion", value: accion });

            if(accion == "actualizar")
                str.push({ name: "codigo", value: $this.attr("data-contacto-codigo") });

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../directorio/regcontacto",
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });

                        if(respuesta.type != "error") {
                            $("#canContacto").click();

                            if(respuesta.add) {
                                oTable.row.add({
                                    "codigo": respuesta.codigo,
                                    "nombre": respuesta.nombre,
                                    "telefono1": respuesta.telefono1,
                                    "telefono2": respuesta.telefono2
                                }).draw(false);
                            } else {
                                oTable.ajax.url("../directorio/getcontactos").load();
                                $this.removeAttr("data-contacto-codigo").attr("data-contacto-accion", "registrar");
                            }
                        }
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }
    });

    // Resetear formulario
    $("#canContacto").on("click", function () {
        $("#forContacto").each(function() {
            this.reset();
        });

        $("#contactos tbody tr.active").click();
        $("#eliContacto").addClass("hide");
        $("#forContacto").parsley().destroy();
        $("#nombre").focus();

        return false;
    });

    // Preparación de modal para más información
    $("#masInformacion").on("click", function() {
        var ajaSpinner = $("#ajaxSpinner");

        ajaSpinner.show();
        $("#modInformacion").modal("show");

        $("#resultado").empty().load("../directorio/masinformacion", function(response, status) {
            ajaSpinner.hide();

            if(status == "error") {
                notif({
                    msg: "Sucedió un error, contacte con el administrador del sistema.",
                    type: "error",
                    position: "center",
                    multiline: true
                });
            }

            // Tabla de los contactos por usuario
            table = $("#tabMasInformacion").dataTable({
                "searching": false,
                "paging": true, 
                "info": false,         
                "lengthChange": false,
                "iDisplayLength": 5
            });
        });
    });

    $("#eliContacto").on("click", function() {
        var eliConCallback = function(text) {
            if(text !== false && text == "LINPIESA") {
                // Se guarda la referencia al elemento
                var $f = $(this);
                var loading = $("#loaContacto");
                var regContacto = $("#regContacto");
                
                loading.show().removeClass("hide");

                $.ajax({  
                    type: "POST",
                    url: "../directorio/elicontacto",
                    dataType: "json",

                    data: { 
                        codigo: regContacto.attr("data-contacto-codigo") 
                    },

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });

                        if(respuesta.type != "error") {
                            $("#canContacto").click();
                            oTable.ajax.url("../directorio/getcontactos").load();
                            regContacto.removeAttr("data-contacto-codigo").attr("data-contacto-accion", "registrar");
                        }                
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }

        notif_prompt({
            "message": "Por favor, ingrese la clave máster para continuar.",
            "textaccept": "¡Deseo continuar!",
            "textcancel": "Me equivoqué",
            "fullscreen": true,
            "callback": eliConCallback
        });

        $(".notifit_prompt_input").removeAttr("type").attr("type", "password");
    });
});