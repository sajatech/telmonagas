$(document).ready(function() {

	$("#login-form").hide();

	$("body").addClass("focused-form");
	$(".login-logo").removeClass("hide");

	$("#entrar").on("click", function () {
			$("#login-form").show();
			$("#entrar").hide();
		});

	setTimeout(function() {
		$("input#nombreUsuario").focus();
	}, 1000);

	$("#acceder").on("click", function () {
		if($("#login").parsley().validate()) {
			$("#loading").show().removeClass("hide");

	        var str = $("#login").serialize();
	        // Se guarda la referencia al formulario
	       	var $f = $(this);

		  	// Bloqueo de peticiones repetidas
		  	if($f.data("locked") == undefined || !$f.data("locked")) {
		  		$.ajax({
		            type: "POST",
		            url: "inicio/login",
		            data: str,

		            beforeSend: function() {
		            	$f.data("locked", true);
		            },

		            success: function(respuesta) { 
		            	if(respuesta == 0)
							$.redirect("/" + (window.location.pathname).split("/")[1] + "/articulos/lista");
						else {
							$("#loading").hide();

		                    notif({
		                     	msg: respuesta,
		                    	type: "error",
		                     	position: "center",
                            	multiline: true
		                    });
						}
					},

					complete: function() {
						$f.data("locked", false);
					}
	        	});
		  	}
		}
	});
});