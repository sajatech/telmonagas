$(document).ready(function() {
    var salSopCallback = function(text) {
    if(text !== false && text == 'LINPIESA')
        {

    	$("#catGasto").select2({width: "100%"});

        $(".input-small").datepicker({
            todayHighlight: true,
            startView: 1,
            autoclose: true
        });

        // Consulta de gastos
        $("#consultar").on("click", function () {
        	$(this).blur();

            // Inicialización de variables
            var loading = $("#loaGasto");
            var desde = $("#desde").val();
            var hasta = $("#hasta").val();
            var catGasto = $("#catGasto").val();
            var resultado = $("#resultado");

            loading.show().removeClass("hide");

         	resultado.empty().removeClass("p-20").load("../reportes/getgastos", { "desde": desde, "hasta": hasta, "concepto": catGasto }, function(response, status) {
         		var gengastospdf = $("#gengastospdf");

                loading.hide();

         		if(status == "error") {
                    notif({
                        msg: "Sucedió un error, contacte con el administrador del sistema.",
                        type: "error",
                        position: "center",
                        multiline: true
                    });
         		}

                if(response == "") {
                    gengastospdf.addClass("disabled");
                    resultado.addClass("p-20").html("<div class='alert alert-info mb-n'><i class='ti ti-info-alt'></i>&nbsp; No se encontraron gastos para los parámetros específicados. Ingrese nuevamente los datos.</div>");
                } else {
    				var str = $("#forGasto").serialize();
                    
                    gengastospdf.removeClass("disabled").attr("onclick", "window.open('/" + (window.location.pathname).split("/")[1] + "/reportes/gastos_emitidos?" + str + "', 'Gastos Emitidos', 'width=240, height=600')");
    				loading.hide();

    			    // Tabla de los gastos
    			    table = $("#gastos").dataTable({
    			        "searching": false,
    			        "paging": true, 
    			        "info": false,         
    			        "lengthChange":false,
    			        "iDisplayLength": 50,
    			        "aaSorting": [0, "asc"],
                        "bAutoWidth": false
    			    });

                    $("#totImportes").text($("#total").text());
                    $("#totDivisas").text($("#spaTotDivisas").text());
                    $("#totEuro").text($("#spaTotEuro").text());
    			}
         	});

            return false;
        });

    }
            else{

                notif({
                    'type': 'error',
                    'msg': 'Zona Prohibida :(',
                    position: "center"
                })
                $.redirect("/" + (window.location.pathname).split("/")[1] + "/articulos/lista");

            }
        }
        
        notif_prompt({
            "message": "Introduzca Clave de Acceso",
            "textaccept": "Entrar",
            "textcancel": "No poseo clave",
            "fullscreen": true,
            "callback": salSopCallback
        })

        $(".notifit_prompt_input").removeAttr("type","text");
        $(".notifit_prompt_input").attr("type","password");
});