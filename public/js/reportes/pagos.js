$(document).ready(function() {
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/reportes/pagos");
	$("#cuenta, #modCuenta, #moneda").select2({width: "100%"});

    // Calendario de fechas
    $(".input-small").datepicker({
        todayHighlight: true,
        startView: 1,
        autoclose: true
    });

    // Formatear campos de monto
    $("#importe").priceFormat({
        prefix: "",
        centsSeparator: ",",
        thousandsSeparator: "."
    });

    // Validar campo de monto
    window.ParsleyValidator.addValidator("importe", function(value) {
        var response = value == "0,00" ? false : true;
        return response;
    });

    // Consulta de pagos
    $("#consultar").on("click", function () {
        $(this).blur();

        // Inicialización de variables
        var loading = $("#loaPago");
        var desde = $("#desde").val();
        var hasta = $("#hasta").val();
        var cuenta = $("#cuenta").val();
        var descripcion = $("#descripcion").val();
        var resultado = $("#resultado");

        loading.show().removeClass("hide");

        resultado.empty().removeClass("p-20").load("../reportes/getpagos", { "desde": desde, "hasta": hasta, "cuenta": cuenta, "descripcion": descripcion }, function(response, status) {
            var genpagospdf = $("#genpagospdf");

            loading.hide();

            if(status == "error") {
                notif({
                    msg: "Sucedió un error, contacte con el administrador del sistema.",
                    type: "error",
                    position: "center",
                    multiline: true
                });
            }

            if(response == "") {
                genpagospdf.addClass("disabled");
                resultado.addClass("p-20").html("<div class='alert alert-info mb-n'><i class='ti ti-info-alt'></i>&nbsp; No se encontraron pagos para los parámetros específicados. Ingrese nuevamente los datos.</div>");
            } else {
                var str = $("#forPago").serialize();
                
                genpagospdf.removeClass("disabled").attr("onclick", "window.open('/" + (window.location.pathname).split("/")[1] + "/reportes/pagos_bancarios?" + str + "', 'Pagos Bancarios', 'width=240, height=600')");

				loading.hide();

			    // Tabla de los pagos
			    table = $("#pagos").dataTable({
			        "searching": false,
			        "paging": true, 
			        "info": false,         
			        "lengthChange":false,
			        "iDisplayLength": 50,
			        "aaSorting": [0, "desc"]
			    });

                table.next().find("div").eq(0).html("<table><tr><td class='pr-md text-right'><h3 class='mt-n total-table mb-n' style='font-weight: bold; color: #8bc34a'>INGRESOS:</h3></td><td class='p-xs'><h3 class='mt-n total-table mb-n' style='font-weight: bold; color: #8bc34a'>" + $("#ingresosdol").text() + "</h3></td><td class='p-xs'><h3 class='mt-n total-table mb-n' style='font-weight: bold; color: #8bc34a'>/</h3></td><td class='p-xs'><h3 class='mt-n total-table mb-n' style='font-weight: bold; color: #8bc34a'>" + $("#ingresosbs").text() + "</h3></td></tr><td class='pr-md text-right'><h3 class='mt-n total-table mb-n' style='font-weight: bold; color: #e51c23'>EGRESOS:</h3></td><td class='p-xs'><h3 class='mt-n total-table mb-n' style='font-weight: bold; color: #e51c23'>" + $("#egresosdol").text() + "</h3></td><td class='p-xs'><h3 class='mt-n total-table mb-n' style='font-weight: bold; color: #e51c23'>/</h3></td><td class='p-xs'><h3 class='mt-n total-table mb-n' style='font-weight: bold; color: #e51c23'>" + $("#egresosbs").text() + "</h3></td></tr><tr><td class='pr-md text-right'><h3 class='mb-n mt-n total-table' style='font-weight: bold; color: #8bc34a'>SALDO DISPONIBLE:</h3></td><td class='p-xs'><h3 class='mt-n total-table mb-n' style='font-weight: bold; color: #8bc34a'>" + $("#totaldol").text() + "</h3></td><td class='p-xs'><h3 class='mt-n total-table mb-n' style='font-weight: bold; color: #8bc34a''>/</h3></td><td class='p-xs'><h3 class='mt-n total-table mb-n' style='font-weight: bold; color: #8bc34a''>" + $("#totalbs").text() + "</h3></td></tr></table>");

                new $.fn.dataTable.FixedHeader(table);
			}
     	});

        return false;
    });

    // Preparación de modal para agregar nuevo(s) débito(s)
    $("#agrEgreso").on("click", function() {
        $("#modCuenta").val(null).trigger("change");
        $("#importe, #descripcion").val("");
        $("#modEgresos").modal("show");
    });

    // Registro de egresos
    $("#regEgreso").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forEgreso");

        if($f.parsley().validate()) {
            var str = $f.serialize();
            var loading = $("#loaEgreso");

            loading.show().removeClass("hide");

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../egresos/registrobd",
                    dataType: "json",
                    data: str,

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();
                        //$("#resultado").empty();
                        //$("#modEgresos").modal("hide");
                        //$("#cuenta").val(respuesta.cuenta).trigger("change");
                        //$("#consultar").click();
                        $("#importe, #descripcion").val("");
                        $("#moneda").val("BOLÍVARES").trigger("change");

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });                        
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }
    });    
});