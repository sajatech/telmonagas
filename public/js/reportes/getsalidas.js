$(document).ready(function() {

    //accion al hacer click en detalle
    $(".detalle").on("click", function() {

        $('.detalle').attr("disabled", true);

        $("#modArt").on("hidden.bs.modal", function () {
            
             $('.detalle').attr("disabled", false);

        });

                $.ajax({  
                    type: "POST",
                    url: "../reportes/listArticulos?id=" + $(this).attr("data-salida-id"),
                    dataType: "json",

                    success: function(respuesta) {

                        verArticulo(JSON.stringify(respuesta));                       
                    },

                    error: function () {

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    }           
                });

                return false;
            

    });

    // Preparación de modal para ver detalle de salida en reporte
    verArticulo = function(articulo) {

        var json = JSON.parse(articulo);

        json.forEach( function(valor) {
            var row  = "<tr>";
                row += "<td class='codigo'>" + valor.art_descripcion + "</td>";
                row += "<td class='codigo'>" + valor.mov_cantidad + "</td>";
                row += "</tr>";
                $(row).appendTo("#tabla_modal");
        });

        $("#modArt").modal("show");
       
    };

});