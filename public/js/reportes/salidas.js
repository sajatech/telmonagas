$(document).ready(function() {
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/reportes/salidas");

    var desde = new Pikaday({ field: $("#desde")[0], format: "DD/MM/YYYY", minDate: new Date("2018-01-01") });
    var hasta = new Pikaday({ field: $("#hasta")[0], format: "DD/MM/YYYY", minDate: new Date("2018-01-01") });

    $("#usuario, #codigo, #descripcion").select2({width: "100%"});

    // Consulta de sal. de artículos
    $("#consultar").on("click", function () {
        $(this).blur();

        // Se guarda la referencia al formulario
        var loading = $("#loaSalida");
        var desde = $("#desde").val();
        var hasta = $("#hasta").val();
        var usuario = $("#usuario").val();
        var codigo = $("#codigo").val();
        var resultado = $("#resultado");

        loading.show().removeClass("hide");

        resultado.empty().removeClass("p-20").load("../reportes/getsalidas", { "desde": desde, "hasta": hasta, "usuario": usuario, "codigo": codigo }, function(response, status) {
            var getsalidaspdf = $("#getsalidaspdf");

            loading.hide();

            if(status == "error") {
                notif({
                    msg: "Sucedió un error, contacte con el administrador del sistema.",
                    type: "error",
                    position: "center",
                    multiline: true
                });
            }
            console.log(response);
            if(response == "") {
                getsalidaspdf.addClass("disabled");
                resultado.addClass("p-20").html("<div class='alert alert-info mb-n'><i class='ti ti-info-alt'></i>&nbsp; No se encontraron movimientos para los parámetros específicados. Ingrese nuevamente los datos.</div>");
            } else {
                var str = $("#forMovimiento").serialize();
                
                getsalidaspdf.removeClass("disabled").attr("onclick", "window.open('/" + (window.location.pathname).split("/")[1] + "/reportes/pdf_salidas?" + str + "', 'Reporte de Salidas', 'width=240, height=600')");
                loading.hide();


                $("#salidas").fixedHeader.enable();

			    // Tabla 
			    table = $("#salidas").dataTable({
			        "searching": false,
			        "paging": true, 
			        "info": false,         
			        "lengthChange":false,
			        "iDisplayLength": 50,
			        "aaSorting": [0, "desc"],
                    "bAutoWidth": false,

                    "columnDefs": [
                          { "width": "6.79%", "targets": 0 },
                          { "width": "7.74%", "targets": 1 },
                          { "width": "17.62%", "targets": 2 },
                          { "width": "11.34%", "targets": 3 },
                          { "width": "17.76%", "targets": 4 },
                          { "width": "17.76%", "targets": 5 },
                          { "width": "16.41%", "targets": 6 },
                          { "width": "4.58%", "targets": 7 }
                        ]                    
			    });

                $("#totCostos1").html($("#spaTotCostos1").text());
                $("#totCostos2").html($("#spaTotCostos2").text());
                $("#totCostos3").html($("#spaTotCostos3").text());
                $("#totVentas1").html($("#spaTotVentas1").text());
                $("#totVentas2").html($("#spaTotVentas2").text());
                $("#totVentas3").html($("#spaTotVentas3").text());
                $("#totPagado1").html($("#spaTotPagado1").text());
                $("#totPagado2").html($("#spaTotPagado2").text());
                $("#totPagado3").html($("#spaTotPagado3").text());
                $("#totGanancias1").html($("#spaTotGanancias1").text());
                $("#totGanancias2").html($("#spaTotGanancias2").text());
                $("#totGanancias3").html($("#spaTotGanancias3").text());
                $("#pagDivisas").html("<span class='red'>" + $("#spaPagDivisas").text() + "</span> (efectivo y/o transferencias)");
                $("#ivaAcumulado").html($("#spaIvaAcumulado").text());

                new $.fn.dataTable.FixedHeader(table);
                //table.fixedHeader.adjust();
            }
        });

        return false;
    });

    $(".articulo").select2({width: "100%"}).on("select2:select", function(e) {
        var selector = $(this).attr("id") == "codigo" ? "descripcion" : "codigo";
        var id = e.params.data.id;
        $("#" + selector).val(id).trigger("change").trigger("select2:select");
    });
});