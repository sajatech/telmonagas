$(document).ready(function() {

    $(".input-small").datepicker({
        todayHighlight: true,
        startView: 1
    }).change(function() {
        $(".datepicker").hide();
    });

    // Consulta de sal. de artículos
    $("#consultar").on("click", function () {
        $(this).blur();

        // Se guarda la referencia al formulario
        var loading = $("#loaProductos");
        var ventas = $("#ventas").val();
        var desde = $("#desde").val();
        var hasta = $("#hasta").val();
        var resultado = $("#resultado");

        loading.show().removeClass("hide");

        resultado.empty().removeClass("p-20").load("../reportes/getproductos", { "ventas": ventas, "desde": desde, "hasta": hasta }, function(response, status) {
        var getproductospdf = $("#getproductospdf");

            loading.hide();

            if(status == "error") {
                notif({
                    msg: "Sucedió un error, contacte con el administrador del sistema.",
                    type: "error",
                    position: "center",
                    multiline: true
                });
            }

            if(response == "") {
                resultado.addClass("p-20").html("<div class='alert alert-info mb-n'><i class='ti ti-info-alt'></i>&nbsp; No se encontraron productos para los parámetros específicados. Ingrese nuevamente los datos.</div>");
            } else {
                var str = $("#forProducto").serialize();

                getproductospdf.removeClass("disabled").attr("onclick", "window.open('/" + (window.location.pathname).split("/")[1] + "/reportes/pdf_productos?" + str + "', 'Reporte de Productos', 'width=240, height=600')");
                loading.hide();
                
                // Tabla 
                table = $("#productos").dataTable({
                    "searching": false,
                    "paging": true, 
                    "info": false,         
                    "lengthChange":false,
                    "iDisplayLength": 50,
                    "aaSorting": [4, "desc"]
                });
            }
        });

        return false;
    });

});