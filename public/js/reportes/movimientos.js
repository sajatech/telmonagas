$(document).ready(function() {
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/reportes/movimientos");
    $("#usuario").select2({width: "100%"});
    $("#articulo").select2({width: "100%"});

    $(".input-small").datepicker({
        todayHighlight: true,
        startView: 1
    }).change(function() {
        $(".datepicker").hide();
    });

    // Consulta de mov. de artículos
    $("#consultar").on("click", function () {
        $(this).blur();

        // Se guarda la referencia al formulario
        var loading = $("#loaMovimiento");
        var desde = $("#desde").val();
        var hasta = $("#hasta").val();
        var usuario = $("#usuario").val();
        var articulo = $("#articulo").val();
        var tipMovimiento = Array();
        var resultado = $("#resultado");

        $("input:checkbox:checked").each(function() {
            tipMovimiento.push($(this).val());
        });

        loading.show().removeClass("hide");

        resultado.empty().removeClass("p-20").load("../reportes/getmovimientos", { "desde": desde, "hasta": hasta, "usuario": usuario, "articulo": articulo, "tipMovimiento": tipMovimiento }, function(response, status) {
            var genmovimientospdf = $("#genmovimientospdf");

            loading.hide();

            if(status == "error") {
                notif({
                    msg: "Sucedió un error, contacte con el administrador del sistema.",
                    type: "error",
                    position: "center",
                    multiline: true
                });
            }

            if(response == "") {
                genmovimientospdf.addClass("disabled");
                resultado.addClass("p-20").html("<div class='alert alert-info mb-n'><i class='ti ti-info-alt'></i>&nbsp; No se encontraron movimientos para los parámetros específicados. Ingrese nuevamente los datos.</div>");
            } else {
                var str = $("#forMovimiento").serialize();
                
                genmovimientospdf.removeClass("disabled").attr("onclick", "window.open('/" + (window.location.pathname).split("/")[1] + "/reportes/movimientos_articulos?" + str + "', 'Movimientos de Artículos', 'width=240, height=600')");
                loading.hide();

			    // Tabla de los movimientos
			    table = $("#movimientos").dataTable({
			        "searching": false,
			        "paging": true, 
			        "info": false,         
			        "lengthChange": false,
			        "iDisplayLength": 50,
			        "aaSorting": [0, "desc"]

			    });
			}
     	});

        return false;
    });

    // Checkboxes personalizados
    $(".icheck input").iCheck({
        checkboxClass: "icheckbox_flat-blue"
    });
});