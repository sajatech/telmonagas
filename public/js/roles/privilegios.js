$(document).ready(function() {
    // Checkboxes personalizados
    $(".icheck input").iCheck({
        checkboxClass: "icheckbox_flat-blue"
    });

    // Registro de privilegios
    $("#regPrivilegios").on("click", function() {

    var questionSopCallback = function(text) {

    if(text !== false && text == 'LINPIESA')
        { 
        // Se guarda la referencia al formulario
        var $f = $("#forPrivilegios");
        var loading = $("#loaPrivilegios");
        var str = $f.serialize();

        if(str != "") {
            loading.show().removeClass("hide");

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../roles/privilegiosbd?rol=" + $("#regPrivilegios").attr("data-rol-id"),
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });                        
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        } else {
            notif({
                msg: "Por favor, seleccione al menos un privilegio.",
                type: "error",
                position: "center",
                multiline: true
            });
        }

        }
            else{

                notif({
                    'type': 'error',
                    'msg': 'Proceso Cancelado :(',
                    position: "center"
                })

            }
        }
        
        notif_prompt({
            "message": "¿Desa modificar privilegios?",
            "textaccept": "Sí, ¡deseo modificar!",
            "textcancel": "No, me equivoqué",
            "fullscreen": true,
            "callback": questionSopCallback
        })

        $(".notifit_prompt_input").removeAttr("type","text");
        $(".notifit_prompt_input").attr("type","password");
    });


    // Resetear formulario
    $("#canPrivilegios").on("click", function () { 
        $(".icheck input").iCheck('uncheck');
        return false;
    });
});