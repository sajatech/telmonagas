$(document).ready(function() {
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/gastos/lista");

    var salSopCallback = function(text) {
    if(text !== false && text == 'LINPIESA')
        {
            var footer = $("footer");
            footer.hide();

            $.get("../gastos/getgastos", function(data) {
                var data = JSON.parse(data);

                $("#ajaxSpinner").hide();
                $("#panGastos").removeClass("hide").show();
                footer.show();

                // Tabla de los gastos
                oTable = $("#gastos").DataTable({
                    "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
                    "aaData": data.aaData,
                    "bServerSide": false,
                    "bAutoWidth": false,
                    "bDestroy": true,
                    "aaSorting": [],

                    "aoColumns": [
                        { "mData": "codigo" },
                        { "mData": "descripcion" },
                        { "mData": "fecha" },
                        { "mData": "importe" },
                        { "mData": "tipCambio" },
                        { "mData": "categoria" },
                        { "mData": "estatus" }
                    ],

                    "oTableTools": {
                        "sRowSelect": "single",

                        "aButtons": [
                            { "sExtends": "editor_create", "sButtonText": "Agregar",

                                "fnInit": function(button) {
                                    $(button).addClass("hide");
                                    
                                    if(data.privilegios.includes(69))
                                        $(button).removeClass("hide");
                                },

                                "fnClick": function() {
                                    $.redirect("/" + (window.location.pathname).split("/")[1] + "/gastos/registro");
                                }

                        	},
                            
                            { "sExtends": "editor_edit", "sButtonText": "Actualizar/Ver",

                                "fnInit": function(button) {
                                    $(button).addClass("hide disabled");
                                    
                                    if(data.privilegios.includes(71) || data.privilegios.includes(72))
                                        $(button).removeClass("hide");
                                },

                            	"fnClick": function(button, conf) {
                            		var node = this.fnGetSelectedData();
                                    $.redirect("/" + (window.location.pathname).split("/")[1] + "/gastos/actualizacion", { "codigo": node[0].codigo });
                            	}

                            },

                            { "sExtends": "editor_remove", "sButtonText": "Cancelar",

                                "fnInit": function(button) {
                                    $(button).addClass("hide disabled");
                                    
                                    if(data.privilegios.includes(73))
                                        $(button).removeClass("hide");
                                },

                            	"fnClick": function() {
                                    var node = this.fnGetSelectedData();

                                    canGasCallback = function(choice) {
                                        if(choice)
                                            canGasto(node);
                                    }

                                    notif_confirm({
                                        "message": "¿Está seguro de cancelar el gasto con código #" + node[0].codigo + "?",
                                        "textaccept": "Sí, ¡deseo cancelarlo!",
                                        "textcancel": "Cerrar",
                                        "fullscreen": true,
                                        "callback": canGasCallback
                                    });
                            	}
                            }
                        ]            
                	},

                    "language": {
                        "lengthMenu": "_MENU_ &nbsp;&nbsp;"
                    },

                    "iDisplayLength": 50,    
                });

                $("#gastos_filter input").addClass("form-control").attr("placeholder", "Buscar Gasto...");
                $("#clientes_length select").addClass("form-control");

                // Se añaden los íconos
                $("#ToolTables_gastos_0").prepend("<i class='ti ti-plus'/> ");
                $("#ToolTables_gastos_1").prepend("<i class='ti ti-pencil'/> ");
                $("#ToolTables_gastos_2").prepend("<i class='ti ti-na'/> ");

                // Se añaden los elementos de la tabla al panel
                $(".panel-ctrls").append($("#gastos_filter").addClass("pull-right"));
                $(".panel-ctrls").append("<i class='separator hidden-xs'></i>");
                $('.panel-ctrls').append($('#gastos_length').addClass("pull-left mt-14").find("label").addClass("panel-ctrls-center hidden-xs"));
                $(".panel-ctrls").append("<i class='separator hidden-xs'></i>");
                $(".panel-ctrls").append($(".DTTT.btn-group").addClass("pull-left mt-10"));

                $(".panel-footer").append($("#gastos+.row"));
                $("#gastos_paginate>ul.pagination").addClass("pull-right pagination-lg");

                $("#gastos_wrapper").find("div").remove();

                new $.fn.dataTable.FixedHeader(oTable);

                // ******************************** //
            });

            // Cancelación de gasto
            canGasto = function(node) {
                var tooGas2 = $("#ToolTables_gastos_2");
                tooGas2.addClass("disabled");

                $.ajax({  
                    type: "POST",
                    url: "../gastos/cangasto",
                    dataType: "json",

        	        data: { 
        	        	codigo: node[0].codigo
        	        },

                    success: function(respuesta) {                
                        oTable.api().ajax.url("../gastos/getgastos").load();
        			    $("#ToolTables_gastos_1, #ToolTables_gastos_2").addClass("disabled");

        				notif({
        				    msg: respuesta.text,
        				    type: respuesta.type,
        				    position: "center",
                            multiline: true
        				});
                    },

                    error: function () {
                        tooGas2.removeClass("disabled");

        				notif({
        				    msg: "Sucedió un error, contacte con el administrador del sistema.",
        				    type: "error",
        				    position: "center",
                            multiline: true
        				});
                    }
                });                
        		
        		return false; 
        	}

        }
            else{

                notif({
                    'type': 'error',
                    'msg': 'Zona Prohibida :(',
                    position: "center"
                })
                $.redirect("/" + (window.location.pathname).split("/")[1] + "/articulos/lista");

            }
        }
        
        notif_prompt({
            "message": "Introduzca Clave de Acceso",
            "textaccept": "Entrar",
            "textcancel": "No poseo clave",
            "fullscreen": true,
            "callback": salSopCallback
        })

        $(".notifit_prompt_input").removeAttr("type","text");
        $(".notifit_prompt_input").attr("type","password");
});