$(document).ready(function() {
    $("#categoria").select2({width: "100%"});
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/gastos/lista");

    $("#fecGasto").datepicker({
        todayHighlight: true,
        startView: 1,
        autoclose: true
    });

    // Formatear campo de costo
    $("#costo").priceFormat({
        prefix: "",
        centsSeparator: ",",
        thousandsSeparator: "."
    });

    window.ParsleyValidator.addValidator("costo", function(value) {
        var response = value == "0,00" ? false : true;
        return response;
    });

    $("#cantidad, #costo").on("keyup", function() {
        $("#importe").val(number_format(Number($("#cantidad").val()) * Number(replace_number($("#costo").val()))));
    });
    
	// Actualización de gasto
	$("#actGasto").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forGasto");
        var loading = $("#loaGasto");

        $f.parsley().destroy();

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var str = $f.serialize();

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../gastos/actualizacionbd?id=" + $(this).attr("data-gasto-id"),
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });                        
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }
	});
});