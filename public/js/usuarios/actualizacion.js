$(document).ready(function() {
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/usuarios/lista");

	// Actualización de usuario
	$("#actUsuario").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forUsuario");
        var loading = $("#loaUsuario");

        if($("#contrasena").val() == "") {
            $f.parsley().destroy();
            $("#contrasena, #repContrasena").removeAttr("required");
        }

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var str = $f.serialize();

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../usuarios/actualizacionbd?id=" + $(this).attr("data-usuario-id"),
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });

                        if(respuesta.type != "error")
                            $("#contrasena, #repContrasena").attr("required", "required")
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }
	});
});