$(document).ready(function() {
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/usuarios/lista");

    var salSopCallback = function(text) {
        if(text !== false && text == 'LINPIESA')
        {
            $.get("../usuarios/getusuarios", function(data) {
                var data = JSON.parse(data);

        $("#ajaxSpinner").hide();
        $("#panUsuarios").removeClass("hide").show();

        // Tabla de los usuarios
        oTable = $("#usuarios").DataTable({
            "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "aaData": data.aaData,
            "bServerSide": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "aaSorting": [],
            responsive : true,

            "aoColumns": [
            { "mData": "codigo" },
            { "mData": "cedIdentidad" },
            { "mData": "nombre" },
            { "mData": "nomUsuario" },
            { "mData": "tipUsuario" },
            { "mData": "estatus" }
            ],

            "oTableTools": {
                "sRowSelect": "single",

                "aButtons": [
                { "sExtends": "editor_create", "sButtonText": "Agregar",

                "fnInit": function(button) {
                    $(button).addClass("hide");
                    
                    if(data.privilegios.includes(26))
                        $(button).removeClass("hide");
                },


                "fnClick": function() {
                    $.redirect("/" + (window.location.pathname).split("/")[1] + "/usuarios/registro");
                }

            },
            
            { "sExtends": "editor_edit", "sButtonText": "Actualizar/Ver",

            "fnInit": function(button) {
                $(button).addClass("hide disabled");
                
                if(data.privilegios.includes(28) || data.privilegios.includes(29))
                    $(button).removeClass("hide");
            },

            "fnClick": function(button, conf) {
              var node = this.fnGetSelectedData();
              $.redirect("/" + (window.location.pathname).split("/")[1] + "/usuarios/actualizacion", { "codigo": node[0].cedIdentidad });
          }

      },

      { "sExtends": "editor_remove", "sButtonText": "Activar/Desactivar",

      "fnInit": function(button) {
        $(button).addClass("hide disabled");
        
        if(data.privilegios.includes(30))
            $(button).removeClass("hide");
    },

    "fnClick": function() { 
      var node = this.fnGetSelectedData();  
      desUsuario(node);
  }
  
}
]
},

"language": {
    "lengthMenu": "_MENU_ &nbsp;&nbsp;"
},

"iDisplayLength": 50,    
});

        $("#usuarios_filter input").addClass("form-control").attr("placeholder", "Buscar Usuario...");
        $("#usuarios_length select").addClass("form-control");

        // Se añaden los íconos
        $("#ToolTables_usuarios_0").prepend("<i class='ti ti-plus'/> ");
        $("#ToolTables_usuarios_1").prepend("<i class='ti ti-pencil'/> ");
        $("#ToolTables_usuarios_2").prepend("<i class='ti ti-exchange-vertical'/> ");

        // Se añaden los elementos de la tabla al panel
        $(".panel-ctrls").append($("#usuarios_filter").addClass("pull-right"));
        $(".panel-ctrls").append("<i class='separator hidden-xs'></i>");
        $('.panel-ctrls').append($('#usuarios_length').addClass("pull-left mt-14").find("label").addClass("panel-ctrls-center hidden-xs"));
        $(".panel-ctrls").append("<i class='separator hidden-xs'></i>");
        $(".panel-ctrls").append($(".DTTT.btn-group").addClass("pull-left mt-10"));

        $(".panel-footer").append($("#usuarios+.row"));
        $("#usuarios_paginate>ul.pagination").addClass("pull-right pagination-lg");

        new $.fn.dataTable.FixedHeader(oTable);

        // ******************************** //
    });

    // Desactivación/Activación de cliente
    desUsuario = function(node) {
        $("#ToolTables_usuarios_2").addClass("disabled");

        var url = (node[0].estatus.replace(/<[^>]*>?/g, "") == "ACTIVO") ? "../usuarios/desusuario" : "../usuarios/activusuario";

        $.ajax({  
            type: "POST",
            url: url,
            dataType: "json",

            data: { 
              codigo: node[0].cedIdentidad
          },

          success: function(respuesta) {                
            oTable.api().ajax.url("../usuarios/getusuarios").load();
            $("#ToolTables_usuarios_1").addClass("disabled");
            $("#ToolTables_usuarios_2").addClass("disabled");

            notif({
                msg: respuesta.text,
                type: respuesta.type,
                position: "center",
                multiline: true
            });
        },

        error: function () {
            $("#ToolTables_usuarios_2").removeClass("disabled");

            notif({
                msg: "Sucedió un error, contacte con el administrador del sistema.",
                type: "error",
                position: "center",
                multiline: true
            });
        }
    });                
        
        return false; 
    }
}
else{

    notif({
        'type': 'error',
        'msg': 'Zona Prohibida :(',
        position: "center"
    })
    $.redirect("/" + (window.location.pathname).split("/")[1] + "/articulos/lista");

}
}

notif_prompt({
    "message": "Introduzca Clave de Acceso",
    "textaccept": "Entrar",
    "textcancel": "No poseo clave",
    "fullscreen": true,
    "callback": salSopCallback
})

$(".notifit_prompt_input").removeAttr("type","text");
$(".notifit_prompt_input").attr("type","password");
});