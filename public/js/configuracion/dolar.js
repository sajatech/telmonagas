$(document).ready(function() {
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/configuracion/dolar");

    $.get("../dolares/getdolares", function(data) {
        var data = JSON.parse(data);

        $("#ajaxSpinner").hide();
        $("#panDolar").removeClass("hide").show();

        // Tabla de los artículos
        oTable = $("#dolares").dataTable({
            "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "aaData": data.aaData,
            "bServerSide": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "aaSorting": [0, "desc"],

            "aoColumns": [
                { "mData": "codigo" },
                { "mData": "fecha" },
                { "mData": "monto" },
                { "mData": "usuario" }
            ],

            "oTableTools": {
                "sRowSelect": "single",

                "aButtons": [
                    { "sExtends": "editor_create", "sButtonText": "Agregar", 

                        "fnInit": function(button) {
                            $(button).addClass("hide");
                            
                            if(data.privilegios.includes(31))
                                $(button).removeClass("hide");
                        },

                    	"fnClick": function() {
                            accionesDolar.registrar();
                    	}

                	},
                    
                    { "sExtends": "editor_edit", "sButtonText": "Actualizar",

                        "fnInit": function(button) {
                            $(button).addClass("hide disabled");
                            
                            if(data.privilegios.includes(33))
                                $(button).removeClass("hide");
                        },

                    	"fnClick": function(button, conf) {
                            var node = this.fnGetSelectedData(); 
                            accionesDolar.actualizar(node);
                    	}

                    }
                ]
        	},

            "language": {
                "lengthMenu": "_MENU_ &nbsp;&nbsp;"
            },

            "iDisplayLength": 50,    
        });

        $("#dolares_filter input").addClass("form-control").attr("placeholder", "Buscar Precio...");
        $("#dolares_length select").addClass("form-control");

        // Se añaden los íconos
        $("#ToolTables_dolares_0").prepend("<i class='ti ti-plus'/> ");
        $("#ToolTables_dolares_1").prepend("<i class='ti ti-pencil'/> ");

        // Se añaden los elementos de la tabla al panel
        $(".panel-ctrls").append($("#dolares_filter").addClass("pull-right"));
        $(".panel-ctrls").append("<i class='separator'></i>");
        $('.panel-ctrls').append($('#dolares_length').addClass("pull-left mt-14").find("label").addClass("panel-ctrls-center"));
        $(".panel-ctrls").append("<i class='separator'></i>");
        $(".panel-ctrls").append($(".DTTT.btn-group").addClass("pull-left mt-10"));

        $(".panel-footer").append($("#dolares+.row"));
        $("#dolares_paginate>ul.pagination").addClass("pull-right pagination-lg");

        // ******************************** //
    });

    $(".accion").on("click", function() {
        $(this).attr("data-dolar-accion") == "registrar" ? regdolar() : actdolar();
    });

    var accionesDolar = {};

    accionesDolar.registrar = function() {
        $("#modDolares .modal-title").html("Registro de precio");
        $("#monto").val("");
        $(".accion").attr("data-dolar-accion", "registrar").text("Registrar");
        $("#modDolares").modal("show");

        setTimeout(function() {
            $("input#monto").focus();
        }, 1000);
    }

    accionesDolar.actualizar = function(node) {
        monto = node[0].monto;
        codigo = node[0].codigo;
        $("#modDolares .modal-title").html("Actualización de precio");
        $("#monto").val(monto);
        $(".accion").attr({ "data-dolar-codigo": node[0].codigo, "data-dolar-accion": "actualizar" }).text("Actualizar");
        $("#modDolares").modal("show");
    }

    window.ParsleyValidator.addValidator("monto", function(value) {
        var response = value == "0,00" ? false : true;
        return response;
    }).addMessage("en", "monto", "Ingrese monto.");

    regdolar = function() {
        // Se guarda la referencia al formulario
        var $f = $("#dolar");

        if($f.parsley().validate()) {
            $("#loading").show().removeClass("hide");

            var str = $f.serialize();

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../configuracion/regdolar",
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        $("#loading").hide();

                        if(respuesta.type != "error") {
                            $("#cerrar").click();
                            oTable.api().ajax.url("../dolares/getdolares").load();
                            $("#ToolTables_dolares_1").addClass("disabled");
                        }

                        notif({
                          msg: respuesta.text,
                          type: respuesta.type,
                          position: "center"
                        });
                    },

                    error: function () {
                        $("#loading").hide();

                        notif({
                          msg: "Sucedió un error, contacte con el administrador del sistema.",
                          type: "error",
                          position: "center"
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });                
                
                return false;       
            }
        } else
            return false;
    }

    actdolar = function() {
        // Se guarda la referencia al formulario
        var $f = $("#dolar");

        if($f.parsley().validate()) {
            $("#loading").show().removeClass("hide");

            var str = $f.serialize();

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../configuracion/actdolar?codigo=" + $(".accion").attr("data-dolar-codigo"),
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        $("#loading").hide();
                        
                        if(respuesta.type != "error") {
                            $("#cerrar").click();
                            oTable.api().ajax.url("../dolares/getdolares").load();
                            $("#ToolTables_marcas_1").addClass("disabled");
                        }

                        notif({
                          msg: respuesta.text,
                          type: respuesta.type,
                          position: "center"
                        });
                    },

                    error: function () {
                        $("#loading").hide();
                        
                        notif({
                          msg: "Sucedió un error, contacte con el administrador del sistema.",
                          type: "error",
                          position: "center"
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });                
                
                return false;       
            }
        }
    }

    // Formatear campo de monto
    $("#monto").priceFormat({
        prefix: "",
        centsSeparator: ",",
        thousandsSeparator: "."
    });
});