$(document).ready(function() {
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/configuracion/calculos");

    // Registro de valores en porcentaje para cálculos de precio
	$("#actCalculo").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forCalculo");
        var loading = $("#loaCalculo");

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var str = $f.serialize();

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../configuracion/regcalculo",
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });
                        
                        if(respuesta.type != "error") {
                            if(flag)
                                $("#canCliente").click();
                            else {
                                var newOption = new Option(respuesta.cliente, respuesta.id, true, true);

                                $("#cliente").append(newOption).trigger({ type: "select2:select", params: { data: { id: respuesta.id, text: respuesta.cliente } } }).next().next().remove();
                                $("#modCliente").modal("hide");
                            }
                        }
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }
	});

    // Formatear campos de monto
    $("#preCifVenezuela, #preMayor, #preDetal, #merLibre").priceFormat({
        prefix: "",
        centsSeparator: ",",
        thousandsSeparator: "."
    });

    window.ParsleyValidator.addValidator("valor", function(value) {
        var response = value == "0,00" ? false : true;
        return response;
    }).addMessage("en", "valor", "Ingrese % Precio.");

    // Resetear formulario
    $("#canCalculo").on("click", function () {
        $(".form-control").val("");
        $("#preCifVenezuela").focus();

        return false;
    });
});