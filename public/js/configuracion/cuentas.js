$(document).ready(function() {
    $("#banco, #tipCuenta").select2({width: "100%"});
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/configuracion/cuentas");

    var banco = $("#banco");
    banco.select2({width: "100%"}).on("select2:select", function(e) {
        $("#numCuenta").val(($("option:selected", banco).attr("data-banco-codigo"))).focus();
    });

    $.get("../cuentas/getcuentas", function(data) {
        var data = JSON.parse(data);

        $("#ajaxSpinner").hide();
        $("#panCuentas").removeClass("hide").show();

        // Tabla de los artículos
        oTable = $("#cuentas").dataTable({
            "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "aaData": data.aaData,
            "bServerSide": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "aaSorting": [0, "desc"],

            "aoColumns": [
                { "mData": "codigo" },
                { "mData": "codBanco", "visible": false },
                { "mData": "banco" },
                { "mData": "numero" },
                { "mData": "tipo" },
                { "mData": "estatus" }
            ],

            "oTableTools": {
                "sRowSelect": "single",

                "aButtons": [
                    { "sExtends": "editor_create", "sButtonText": "Agregar", 

                        "fnInit": function(button) {
                            $(button).addClass("hide");
                            
                            //if(data.privilegios.includes(31))
                                $(button).removeClass("hide");
                        },

                    	"fnClick": function() {
                            accionesCuenta.registrar();
                    	}

                	},
                    
                    { "sExtends": "editor_edit", "sButtonText": "Actualizar",

                        "fnInit": function(button) {
                            $(button).addClass("hide disabled");
                            
                            //if(data.privilegios.includes(33))
                                $(button).removeClass("hide");
                        },

                    	"fnClick": function(button, conf) {
                            var node = this.fnGetSelectedData(); 
                            accionesCuenta.actualizar(node);
                    	}

                    }
                ]
        	},

            "language": {
                "lengthMenu": "_MENU_ &nbsp;&nbsp;"
            },

            "iDisplayLength": 50,    
        });

        $("#cuentas_filter input").addClass("form-control").attr("placeholder", "Buscar Cuenta...");
        $("#cuentas_length select").addClass("form-control");

        // Se añaden los íconos
        $("#ToolTables_cuentas_0").prepend("<i class='ti ti-plus'/> ");
        $("#ToolTables_cuentas_1").prepend("<i class='ti ti-pencil'/> ");

        // Se añaden los elementos de la tabla al panel
        $(".panel-ctrls").append($("#cuentas_filter").addClass("pull-right"));
        $(".panel-ctrls").append("<i class='separator'></i>");
        $('.panel-ctrls').append($('#cuentas_length').addClass("pull-left mt-14").find("label").addClass("panel-ctrls-center"));
        $(".panel-ctrls").append("<i class='separator'></i>");
        $(".panel-ctrls").append($(".DTTT.btn-group").addClass("pull-left mt-10"));

        $(".panel-footer").append($("#cuentas+.row"));
        $("#cuentas_paginate>ul.pagination").addClass("pull-right pagination-lg");

        // ******************************** //
    });

    $(".accion").on("click", function() {
        $(this).attr("data-cuenta-accion") == "registrar" ? regcuenta() : actcuenta();
    });

    var accionesCuenta = {};

    accionesCuenta.registrar = function() {
        $("#modCuentas .modal-title").html("Registro de cuenta bancaria");
        $("#banco, #tipCuenta").val(null).trigger("change");
        $("#numCuenta").val("");
        $(".accion").attr("data-cuenta-accion", "registrar").text("Registrar");
        $("#modCuentas").modal("show");
    }

    accionesCuenta.actualizar = function(node) {
        $("#modCuentas .modal-title").html("Actualización de cuenta bancaria");
        $("#banco").val(node[0].codBanco).trigger("change");
        $("#tipCuenta").val(node[0].tipo).trigger("change");
        $("#numCuenta").val(node[0].numero);
        $(".accion").attr({ "data-cuenta-codigo": node[0].codigo, "data-cuenta-accion": "actualizar" }).text("Actualizar");
        $("#modCuentas").modal("show");
    }

    regcuenta = function() {
        // Se guarda la referencia al formulario
        var $f = $("#cuenta");

        if($f.parsley().validate()) {
            var str = $f.serialize();
            var loaCuenta = $("#loaCuenta");

            loaCuenta.show().removeClass("hide");

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../configuracion/regcuenta",
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loaCuenta.hide();

                        if(respuesta.type != "error") {
                            $("#cerrar").click();
                            oTable.api().ajax.url("../cuentas/getcuentas").load();
                            $("#ToolTables_cuentas_1").addClass("disabled");
                        }

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });
                    },

                    error: function () {
                        loaCuenta.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });                
                
                return false;       
            }
        } else
            return false;
    }

    actcuenta = function() {
        // Se guarda la referencia al formulario
        var $f = $("#cuenta");

        if($f.parsley().validate()) {
            var str = $f.serialize();
            var loaCuenta = $("#loaCuenta");

            loaCuenta.show().removeClass("hide");

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../configuracion/actcuenta?codigo=" + $(".accion").attr("data-cuenta-codigo"),
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loaCuenta.hide();
                        
                        if(respuesta.type != "error") {
                            $("#cerrar").click();
                            oTable.api().ajax.url("../cuentas/getcuentas").load();
                            $("#ToolTables_cuentas_1").addClass("disabled");
                        }

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });
                    },

                    error: function () {
                        loaCuenta.hide();
                        
                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });                
                
                return false;       
            }
        }
    }
});