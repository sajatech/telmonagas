$(document).ready(function() {
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/configuracion/gastos");
    
    $.get("../gastos/getcategorias", function(data) {
        var data = JSON.parse(data);

        $("#ajaxSpinner").hide();
        $("#panGastos").removeClass("hide").show();

        // Tabla de los artículos
        oTable = $("#categorias").dataTable({
            "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "aaData": data.aaData,
            "bServerSide": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "aaSorting": [],

            "aoColumns": [
                { "mData": "codigo" },
                { "mData": "fecha" },
                { "mData": "nombre" },
                { "mData": "usuario" },
                { "mData": "estatus" }
            ],

            "oTableTools": {
                "sRowSelect": "single",

                "aButtons": [
                    { "sExtends": "editor_create", "sButtonText": "Agregar", 

                        "fnInit": function(button) {
                            $(button).addClass("hide");
                            
                            if(data.privilegios.includes(66))
                                $(button).removeClass("hide");
                        },

                    	"fnClick": function() {
                            accionesCategorias.registrar();
                    	}

                	},
                    
                    { "sExtends": "editor_edit", "sButtonText": "Actualizar",

                        "fnInit": function(button) {
                            $(button).addClass("hide disabled");
                            
                            if(data.privilegios.includes(67))
                                $(button).removeClass("hide");
                        },

                    	"fnClick": function(button, conf) {
                            var node = this.fnGetSelectedData(); 
                            accionesCategorias.actualizar(node);
                    	}

                    },

                    { "sExtends": "editor_remove", "sButtonText": "Sumar a Gastos/Sumar Separadamente",

                        "fnInit": function(button) {
                            $(button).addClass("hide disabled");
                            
                            if(data.privilegios.includes(68))
                                $(button).removeClass("hide");
                        },

                        "fnClick": function() { 
                            var node = this.fnGetSelectedData();  
                            accionesCategorias.descontar(node);
                        }
                        
                    }
                ]
        	},

            "language": {
                "lengthMenu": "_MENU_ &nbsp;&nbsp;"
            },

            "iDisplayLength": 50,    
        });

        $("#categorias_filter input").addClass("form-control").attr("placeholder", "Buscar Concepto...");
        $("#categorias_length select").addClass("form-control");

        // Se añaden los íconos
        $("#ToolTables_categorias_0").prepend("<i class='ti ti-plus'/> ");
        $("#ToolTables_categorias_1").prepend("<i class='ti ti-pencil'/> ");
        $("#ToolTables_categorias_2").prepend("<i class='ti ti-exchange-vertical'/> ");

        // Se añaden los elementos de la tabla al panel
        $(".panel-ctrls").append($("#categorias_filter").addClass("pull-right"));
        $(".panel-ctrls").append("<i class='separator'></i>");
        $('.panel-ctrls').append($("#categorias_length").addClass("pull-left mt-14").find("label").addClass("panel-ctrls-center"));
        $(".panel-ctrls").append("<i class='separator'></i>");
        $(".panel-ctrls").append($(".DTTT.btn-group").addClass("pull-left mt-10"));

        $(".panel-footer").append($("#categorias+.row"));
        $("#categorias_paginate>ul.pagination").addClass("pull-right pagination-lg");

        // ******************************** //
    });

    $(".accion").on("click", function() {
        $(this).attr("data-categoria-accion") == "registrar" ? regcategoria() : actcategoria();
    });

    var accionesCategorias = {};

    accionesCategorias.registrar = function() {
        $("#modCategorias .modal-title").html("Registro de concepto");
        $("#nombre").val("");
        $(".accion").attr("data-categoria-accion", "registrar").text("Registrar");
        $("#modCategorias").modal("show");

        setTimeout(function() {
            $("input#nombre").focus();
        }, 1000);
    }

    accionesCategorias.actualizar = function(node) {
        nombre = node[0].nombre;
        codigo = node[0].codigo;
        $("#modCategorias .modal-title").html("Actualización de concepto");
        $("#nombre").val(nombre);
        $(".accion").attr({ "data-categoria-codigo": node[0].codigo, "data-categoria-accion": "actualizar" }).text("Actualizar");
        $("#modCategorias").modal("show");
    }

    accionesCategorias.descontar = function(node) {
        $("#ToolTables_categorias_2").addClass("disabled");

        var url = (node[0].estatus.replace(/<[^>]*>?/g, "") == "SUMAR A GASTOS") ? "../configuracion/desgasto" : "../configuracion/totgasto";

        $.ajax({  
            type: "POST",
            url: url,
            dataType: "json",

            data: { 
                codigo: node[0].codigo
            },

            success: function(respuesta) {                
                oTable.api().ajax.url("../gastos/getcategorias").load();
                $("#ToolTables_categorias_1").addClass("disabled");
                $("#ToolTables_categorias_2").addClass("disabled");

                notif({
                    msg: respuesta.text,
                    type: respuesta.type,
                    position: "center",
                    multiline: true
                });
            },

            error: function () {
                $("#ToolTables_categorias_2").removeClass("disabled");

                notif({
                    msg: "Sucedió un error, contacte con el administrador del sistema.",
                    type: "error",
                    position: "center",
                    multiline: true
                });
            }
        });                
        
        return false; 
    }

    regcategoria = function() {
        // Se guarda la referencia al formulario
        var $f = $("#categoria");

        if($f.parsley().validate()) {
            $("#loading").show().removeClass("hide");

            var str = $f.serialize();

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../configuracion/reggasto",
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        $("#loading").hide();

                        if(respuesta.type != "error") {
                            $("#cerrar").click();
                            oTable.api().ajax.url("../gastos/getcategorias").load();
                            $("#ToolTables_categorias_1").addClass("disabled");
                            $("#ToolTables_categorias_2").addClass("disabled");
                        }

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });
                    },

                    error: function () {
                        $("#loading").hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });                
                
                return false;       
            }
        } else
            return false;
    }

    actcategoria = function() {
        // Se guarda la referencia al formulario
        var $f = $("#categoria");

        if($f.parsley().validate()) {
            $("#loading").show().removeClass("hide");

            var str = $f.serialize();

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../configuracion/actgasto?codigo=" + $(".accion").attr("data-categoria-codigo"),
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        $("#loading").hide();
                        
                        if(respuesta.type != "error") {
                            $("#cerrar").click();
                            oTable.api().ajax.url("../gastos/getcategorias").load();
                            $("#ToolTables_categorias_1").addClass("disabled");
                        }

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });
                    },

                    error: function () {
                        $("#loading").hide();
                        
                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });                
                
                return false;       
            }
        }
    }
});