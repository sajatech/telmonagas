$(document).ready(function() {
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/configuracion/roles");

    $.get("../roles/getroles", function(data) {
        var data = JSON.parse(data);

        $("#ajaxSpinner").hide();
        $("#panRoles").removeClass("hide").show();

        // Tabla de los roles de usuario
        oTable = $("#roles").dataTable({
            "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "aaData": data.aaData,
            "bServerSide": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "searching": false,
            "paging": false, 
            "aaSorting": [0, "desc"],

            "aoColumns": [
                { "mData": "codigo" },
                { "mData": "nombre" },
                { "mData": "usuario" },
                { "mData": "estatus" }
            ],

            "oTableTools": {
                "sRowSelect": "single",

                "aButtons": [
                    { "sExtends": "editor_edit", "sButtonText": "Asignar/Ver Privilegios",

                        "fnInit": function(button) {
                            $(button).addClass("hide disabled");
                            
                            if(data.privilegios.includes(37) || data.privilegios.includes(38))
                                $(button).removeClass("hide");
                        },

                        "fnClick": function(button, conf) {
                            var node = this.fnGetSelectedData();
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/roles/privilegios", { "rol": node[0].codigo });
                        }

                    },
                ]
        	},

            "language": {
                "lengthMenu": "_MENU_ &nbsp;&nbsp;"
            },

            "iDisplayLength": 50,    
        });

        $("#roles_filter input").addClass("form-control").attr("placeholder", "Buscar Rol...");
        $("#roles_length select").addClass("form-control");

        // Se añaden los íconos
        $("#ToolTables_roles_0").prepend("<i class='ti ti-check-box'/> ");

        // Se añaden los elementos de la tabla al panel
        $(".panel-ctrls").append($(".DTTT.btn-group").addClass("pull-left mt-10"));
        $(".panel-footer").append($("#roles+.row"));

        // ******************************** //
    });
});