$(document).ready(function() {
	$("#usuario").select2({width: "100%"});
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/configuracion/accesos");

    $(".input-small").datepicker({
        todayHighlight: true,
        startView: 1
    }).change(function() {
    	$(".datepicker").hide();
    });

    // Consulta de mov. de artículos
    $("#consultar").on("click", function () {
    	$(this).blur();

        // Se guarda la referencia al formulario
        var loading = $("#loaAcceso");
        var desde = $("#desde").val();
        var hasta = $("#hasta").val();
        var usuario = $("#usuario").val();
        var resultado = $("#resultado");

        loading.show().removeClass("hide");

     	resultado.empty().removeClass("p-20").load("../configuracion/getaccesos", { "desde": desde, "hasta": hasta, "usuario": usuario }, function(response, status) {
     		//var getenviospdf = $("#getenviospdf");
            //console.log(response); 
            loading.hide();

     		if(status == "error") {
                notif({
                    msg: "Sucedió un error, contacte con el administrador del sistema.",
                    type: "error",
                    position: "center",
                    multiline: true
                });
     		}

            if(response == "") {
        //      getenviospdf.addClass("disabled");
                resultado.addClass("p-20").html("<div class='alert alert-info mb-n'><i class='ti ti-info-alt'></i>&nbsp; No se encontraron movimientos para los parámetros específicados. Ingrese nuevamente los datos.</div>");
            } else {
				var str = $("#forMovimiento").serialize();
                
        //      getenviospdf.removeClass("disabled").attr("onclick", "window.open('/" + (window.location.pathname).split("/")[1] + "/reportes/pdf_envios?" + str + "', 'Reporte de Envios', 'width=240, height=600')");
				loading.hide();

			    // Tabla 
			    table = $("#accesos").DataTable({
			        "searching": true,
			        "paging": true, 
			        "info": false,         
			        "lengthChange":false,
			        "iDisplayLength": 50,
			        "aaSorting": [0, "desc"]
			    });

                new $.fn.dataTable.FixedHeader(table);
			}
     	});

        return false;
    });

});