$(document).ready(function() {
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/envios/lista");

	$(".input-small").datepicker({
        todayHighlight: true,
        startView: 1
    }).change(function() {
    	$(".datepicker").hide();
    });
    
	$("#cliente").select2({width: "100%"});

	// Habilitar botón "precargar" detalle del cliente a enviar
	$("#cliente").select2({width: "100%"}).on("select2:select", function(e) {
		var verCliente = $("#verCliente");
		var id = e.params.data.id;

		if(id != "") {
		    $.ajax({
		        type: "POST",
		        url: "../clientes/getcliente",
		        dataType: "json",

		        data: {
		        	cliente: $("option:selected", "#cliente").val(),
		        },

		        success: function(respuesta) {
					verCliente.removeClass("disabled").attr("onclick", "verCliente('" + JSON.stringify(respuesta) + "')");
		        }
		    });
		} else
			verCliente.addClass("disabled");
	});

		$("#registrar").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forEnvio");
        var loading = $("#loading");

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var str = $f.serialize();

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../envios/registrobd",
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();
                        
                        if(respuesta.type != "error") {
                            $("#cancelar").click();
                            $("#verCliente").addClass("disabled");

							notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        	});

                        } else {
	                        notif({
	                            msg: respuesta.text,
	                            type: "error",
	                            position: "center",
	                            multiline: true
	                        });
                        }
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }
	});
   

	 // Formatear campos de monto
    $("#EnvMonto").priceFormat({
        prefix: "",
        centsSeparator: ",",
        thousandsSeparator: "."
    });

    //  // Formatear campos de monto
    // $("#EnvMontoSalida").priceFormat({
    //     prefix: "",
    //     centsSeparator: ",",
    //     thousandsSeparator: "."
    // });

    // Función para aplicar máscara a campo de cédula o rif
    mascara = function(str) { 
        return (str == "v" || str == "V" || str == "e" || str == "E") ? "a-999999[99]" : "a-99999999-9";
    }

    var cedIdentidad = $("#cedIdentidad1, #cedIdentidad2");
    cedIdentidad.keyup(function() {
	    // Máscara a campo de cédula
	    cedIdentidad.inputmask({ 
	        mask: mascara($(this).val().substring(0, 1)),
	        greedy: false, 
	        clearIncomplete: true,
	        definitions: { 
	            "a": { 
	                validator: "[vVeEjJgG]" 
	            } 
	        } 
	    });
	});


	// Actualización de envio
	$("#actualizar").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#salida");
        var loading = $("#loading");

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var str = $f.serialize();

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../salidas/actualizacionbd?id=" + $(this).attr("data-salida-id"),
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });                        
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }
	});

	// Precarga de detalle del cliente
	verCliente = function(cliente) {
		var json = JSON.parse(cliente);

		$("#modCliente .modal-title").text("Cliente: " + json.cli_nombre);
    	$("#codCliente").val(json.cli_codigo);
    	$("#nombre").val(json.cli_nombre);
    	$("#corElectronico").val(json.cli_cor_electronico);
	    $("#regCliente").hide();
	    $("#canCliente").hide();
	}

    // Función para aplicar máscara a campo de cédula o rif
    mascara = function(str) { 
        return (str == "v" || str == "V" || str == "e" || str == "E") ? "a-999999[99]" : "a-99999999-9";
    }

    var codCliente = $("#codCliente");
    codCliente.keyup(function() {
        var cedula = $(this).val();

        codCliente.inputmask({ 
            mask: mascara($(this).val().substring(0, 1)), 
            greedy: false, 
            clearIncomplete: true,
            definitions: { 
                "a": { 
                    validator: "[vVeEjJgG]" 
                } 
            } 
        }); 
    });
    
    // Resetar formulario de datos de envío
    $("#cancelar").on("click", function () {
        $("#forEnvio").each(function() {
            this.reset();
        });

        $("#codCliente").focus();

        return false;
    });
});