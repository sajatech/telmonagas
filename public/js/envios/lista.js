$(document).ready(function() {
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/envios/lista");

    $.get("../envios/getenvios", function(data) {
        var data = JSON.parse(data);

        $("#ajaxSpinner").hide();
        $("#panEnvios").removeClass("hide").show();

        // Tabla de los envios
        oTable = $("#envios").dataTable({
            "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "aaData": data.aaData,
            "bServerSide": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "aaSorting": [],

            "aoColumns": [
                { "mData": "codigo" },
                { "mData": "rif" },
                { "mData": "EnvNombre" },
                { "mData": "EnvFecha" },
                { "mData": "EnvEmpresa" },
                { "mData": "EnvGuia" },
                { "mData": "UsuId" },
                { "mData": "estatus" }
            ],

            "oTableTools": {
                "sRowSelect": "single",

                "aButtons": [
                    { "sExtends": "editor_create", "sButtonText": "Agregar",

                        "fnInit": function(button) {
                            $(button).addClass("hide");
                            
                            if(data.privilegios.includes(50))
                                $(button).removeClass("hide");
                        },

                        "fnClick": function() {
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/envios/registro");
                        }

                	},
                    
                    { "sExtends": "editor_edit", "sButtonText": "Reenviar",

                        "fnInit": function(button) {
                            $(button).addClass("hide");
                            
                            if(data.privilegios.includes(51))
                                $(button).removeClass("hide");
                        },

                    	"fnClick": function(button, conf) {
                    		var node = this.fnGetSelectedData();
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/envios/actualizacion", { "ide": node[0].codigo });
                    	}

                    }

                ]            
        	},

            "language": {
                "lengthMenu": "_MENU_ &nbsp;&nbsp;"
            },

            "iDisplayLength": 50,    
        });

        $("#envios_filter input").addClass("form-control").attr("placeholder", "Buscar Envíos...");
        $("#envios_length select").addClass("form-control");

        // Se añaden los íconos
        $("#ToolTables_envios_0").prepend("<i class='ti ti-plus'/> ");
        $("#ToolTables_envios_1").prepend("<i class='ti ti-pencil'/> ");
        $("#ToolTables_envios_2").prepend("<i class='ti ti-exchange-vertical'/> ");

        // Se añaden los elementos de la tabla al panel
        $(".panel-ctrls").append($("#envios_filter").addClass("pull-right"));
        $(".panel-ctrls").append("<i class='separator'></i>");
        $('.panel-ctrls').append($('#envios_length').addClass("pull-left mt-14").find("label").addClass("panel-ctrls-center"));
        $(".panel-ctrls").append("<i class='separator'></i>");
        $(".panel-ctrls").append($(".DTTT.btn-group").addClass("pull-left mt-10"));

        $(".panel-footer").append($("#envios+.row"));
        $("#envios_paginate>ul.pagination").addClass("pull-right pagination-lg");

        // ******************************** //
    });

});