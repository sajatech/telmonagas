$(document).ready(function() {

    //iniciar summernote
    $(document).ready(function() {
        $('#summernote').summernote();
    });

    //MULTISELECT2

    // $('#multi-select2').multiSelect({
    // selectableHeader: "<div class='custom-header'>Seleccione Destinatarios</div>",
    // selectionHeader: "<div class='custom-header'>Seleccionados</div>"
    // });
 $('#multi-select2').multiSelect({
    selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Buscar'>",
    selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Buscar'>",
  afterInit: function(ms){
    var that = this,
        $selectableSearch = that.$selectableUl.prev(),
        $selectionSearch = that.$selectionUl.prev(),
        selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
        selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
    .on('keydown', function(e){
      if (e.which === 40){
        that.$selectableUl.focus();
        return false;
      }
    });

    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
    .on('keydown', function(e){
      if (e.which == 40){
        that.$selectionUl.focus();
        return false;
      }
    });
  },
  afterSelect: function(){
    this.qs1.cache();
    this.qs2.cache();
  },
  afterDeselect: function(){
    this.qs1.cache();
    this.qs2.cache();
  }
});

    // $('#select-100').click(function(){
    // $('#multi-select2').multiSelect('select', ['3a.rentalvideo@gmail.com']);
    //   return false;
    // });

    $('#deselect-all').click(function(){
    $('#multi-select2').multiSelect('deselect_all');
    return false;
    });
   

    // Checkboxes personalizados
    $(".icheck input").iCheck({
        checkboxClass: "icheckbox_flat-blue"
    });

    //desactivo el multiselect si selecciono la opcion "enviar a todos"

    $("#todos").on("ifChecked", function() {
        var preAlt = $("#preloader-alt");
        preAlt.removeClass("hide").show();
        preAlt.fadeOut("slow");
        $('#multi-select2').multiSelect('select_all');
        $("#lote").prop('disabled', true);

    });

    $("#todos").on("ifUnchecked", function() {
        $('#multi-select2').multiSelect('deselect_all');
        $("#lote").prop('disabled', false);
    });



    $("#registrar").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forEnvio");
        var loading = $("#loading");

        if($f.parsley().validate()) {
            loading.show().removeClass("hide");
            var asunto = $("#asunto").val(); 
            var cuerpo = $("#summernote").val(); 
            var emails = JSON.stringify($("#multi-select2").val()); 
            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({
                    type: "POST",
                    url: "../envios/promocionbd",
                    //url: "../salidas/registrobd?articulos=" + JSON.stringify(objArticulo) + "&pagos=" + JSON.stringify(objPago) + "&iva=" + $("#iva").val(),
                    data: {asunto : asunto, cuerpo : cuerpo, emails: emails},
                    dataType: "json",

                    beforeSend: function() {
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        if(respuesta.type != "error") {
                            $("#cancelar").click();

                            notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                            });
                            if(respuesta!="")
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/envios/promocionesLista");  

                        } else {
                            notif({
                                msg: respuesta.text,
                                type: "error",
                                position: "center",
                                multiline: true
                            });
                        }
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() {
                        $f.data("locked", false);
                    }
                });

                return false;
            }
        }
    });

    $("#continuar").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forEnvio");
        var loading = $("#loading");

        if($f.parsley().validate()) {
            loading.show().removeClass("hide");
            var asunto = $("#asunto").val(); 
            var cuerpo = $("#summernote").val(); 
            var emails = JSON.stringify($("#multi-select2").val()); 
            var id = $("#id_promo").val();
            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({
                    type: "POST",
                    url: "../envios/continuar_promocionbd",
                    data: {asunto : asunto, cuerpo : cuerpo, emails: emails, id: id},
                    dataType: "json",

                    beforeSend: function() {
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        if(respuesta.type != "error") {
                            $("#cancelar").click();

                            notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                            });
                            if(respuesta!="")
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/envios/promocionesLista");  

                        } else {
                            notif({
                                msg: respuesta.text,
                                type: "error",
                                position: "center",
                                multiline: true
                            });
                        }
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() {
                        $f.data("locked", false);
                    }
                });

                return false;
            }
        }
    });

    //funcion para seleccionar envios por lotes

    $("#lote").change(function(){

        var preAlt = $("#preloader-alt");

        preAlt.removeClass("hide").show();

        $('#multi-select2').multiSelect('deselect_all');
        
        var lote = $("#lote").val();
        var id = $("#id_promo").val();
        if(lote)
        {           
         $.ajax({
            type: "POST",
            url: "../envios/select_lotes?lote="+ lote +"&ide=" + id,
                //data: {asunto : asunto, cuerpo : cuerpo, emails: emails},
                dataType: "json",

                success: function(respuesta) {
                    preAlt.fadeOut("slow");
                    //console.log(respuesta);
                    //console.log(JSON.parse(JSON.stringify(respuesta)));
                    var obj = JSON.parse(JSON.stringify(respuesta));                       
                    //alert(obj[0].cli_cor_electronico);
                    
                    var correos = new Array();
                    var i = 0;
                    while (i < lote) {
                        if(id<1)
                            correos.push(obj[i].cli_cor_electronico);
                        else
                            correos.push(obj[i]);
                        i++;
                    }
                    //console.log(correos);
                    $('#multi-select2').multiSelect('select', correos );
                    
                },

                error: function () {
                    return false;
                }
            });

     }
     // else
     // {

     //    notif({
     //            msg: "Seleccione la cantidad a enviar del lote.",
     //            type: "error",
     //            position: "center",
     //            multiline: true
     //        });

     // }
    
    });


    $("#select-lotes").on("ifUnchecked", function() {
        $('#multi-select2').multiSelect('deselect_all');
    });
  
});