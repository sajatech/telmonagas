$(document).ready(function() {
    $.get("../envios/getpromociones", function(data) {
        var data = JSON.parse(data);

        // Tabla de los artículos
        oTable = $("#proveedores").dataTable({
            "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "aaData": data.aaData,
            "bServerSide": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "aaSorting": [],

            "aoColumns": [
                { "mData": "codigo" },
                { "mData": "asunto" },
                { "mData": "fecha" },
                { "mData": "timer" },
                { "mData": "totales" },
                { "mData": "estatus" }
            ],

            "oTableTools": {
                "sRowSelect": "single",

                "aButtons": [
                    { "sExtends": "editor_create", "sButtonText": "Agregar",

                        "fnInit": function(button) {
                            // $(button).addClass("hide");

                            // if(data.privilegios.includes(6))
                            //     $(button).removeClass("hide");
                        },

                        "fnClick": function() {
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/envios/promociones");
                        }

                    },

                    { "sExtends": "editor_create", "sButtonText": "Reenviar",

                        "fnInit": function(button) {
                            // $(button).addClass("hide");

                            // if(data.privilegios.includes(6))
                            //     $(button).removeClass("hide");
                        },

                        "fnClick": function() {
                            var node = this.fnGetSelectedData();
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/envios/promociones?id="+ node[0].codigo);
                        }

                    },

                    { "sExtends": "editor_create", "sButtonText": "Continuar Envio",

                        "fnInit": function(button) {
                            // $(button).addClass("hide");

                            // if(data.privilegios.includes(6))
                            //     $(button).removeClass("hide");
                        },

                        "fnClick": function() {
                            var node = this.fnGetSelectedData();
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/envios/promociones?id="+ node[0].codigo +"&continuar=1");
                        }

                    },

                    { "sExtends": "editor_remove", "sButtonText": "Activar/Desactivar",

                        "fnInit": function(button) {
                            // $(button).addClass("hide disabled");

                            // if(data.privilegios.includes(10))
                            //     $(button).removeClass("hide");
                        },

                        "fnClick": function() {
                            var node = this.fnGetSelectedData();
                            desPromocion(node);
                        }

                    },

                    { "sExtends": "editor_create", "sButtonText": "Detalle",

                        "fnInit": function(button) {
                            // $(button).addClass("hide disabled");

                            // if(data.privilegios.includes(10))
                            //     $(button).removeClass("hide");
                        },

                        "fnClick": function() {
                            var node = this.fnGetSelectedData();
                            verPromocion(node);
                        }

                    }

                ]
            },

            "language": {
                "lengthMenu": "_MENU_ &nbsp;&nbsp;"
            },

            "iDisplayLength": 50,
        });

        $("#proveedores_filter input").addClass("form-control").attr("placeholder", "Buscar Promocion...");
        $("#proveedores_length select").addClass("form-control");

        // Se añaden los íconos
        $("#ToolTables_proveedores_0").prepend("<i class='ti ti-plus'/> ");
        $("#ToolTables_proveedores_1").prepend("<i class='ti ti-reload'/> ");
        $("#ToolTables_proveedores_2").prepend("<i class='ti ti-angle-double-right'/> ");
        $("#ToolTables_proveedores_3").prepend("<i class='ti ti-exchange-vertical'/> ");
        $("#ToolTables_proveedores_4").prepend("<i class='ti ti-zoom-in'/> ");

        // Se añaden los elementos de la tabla al panel
        $(".panel-ctrls").append($("#proveedores_filter").addClass("pull-right"));
        $(".panel-ctrls").append("<i class='separator'></i>");
        $('.panel-ctrls').append($('#proveedores_length').addClass("pull-left mt-14").find("label").addClass("panel-ctrls-center"));
        $(".panel-ctrls").append("<i class='separator'></i>");
        $(".panel-ctrls").append($(".DTTT.btn-group").addClass("pull-left mt-10"));

        $(".panel-footer").append($("#proveedores+.row"));
        $("#proveedores_paginate>ul.pagination").addClass("pull-right pagination-lg");

        // ******************************** //

    });

    // Desactivación/Activación de promociones
    desPromocion = function(node) {
        $("#ToolTables_proveedores_2").addClass("disabled");

        var estatus = node[0].estatus.replace(/<[^>]*>?/g, "");
        if (estatus != "TERMINADO")
            {

            var url = (node[0].estatus.replace(/<[^>]*>?/g, "") == "PENDIENTE") ? "../envios/desPromocion" : "../envios/activaPromocion";

            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",

                data: {
                    codigo: node[0].codigo
                },

                success: function(respuesta) {
                    oTable.api().ajax.url("../envios/getpromociones").load();
                    $("#ToolTables_proveedores_2").addClass("disabled");

                    notif({
                        msg: respuesta.text,
                        type: respuesta.type,
                        position: "center",
                        multiline: true
                    });
                },

                error: function () {
                    $("#ToolTables_proveedores_2").removeClass("disabled");

                    notif({
                        msg: "Sucedió un error, contacte con el administrador del sistema.",
                        type: "error",
                        position: "center",
                        multiline: true
                    });
                }
            });

            return false;
            } 
        else
            {
                $("#ToolTables_proveedores_1").addClass("disabled");
                return false;
            }

     
    }


        //accion al hacer click en detalle
    verPromocion = function(node) {
        //alert(node[0].codigo);

        $('.detalle').attr("disabled", true);

        $("#modPromo").on("hidden.bs.modal", function () {
            
             $(".asunto").remove();
             $(".codigo").remove();
             $('.detalle').attr("disabled", false);

        });

                $.ajax({  
                    type: "POST",
                    url: "../envios/listPromo?id=" + node[0].codigo,
                    dataType: "json",

                    success: function(respuesta) {

                        verPromo(JSON.stringify(respuesta));                       
                    },

                    error: function () {

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    }           
                });

                return false;
            
    };

    // Preparación de modal para ver detalle de promocion en modal
    verPromo = function(promo) {

        var json = JSON.parse(promo);

        var titulo = "<div align='center' class='asunto'>" + json.pro_asunto + "<div>";
            $(titulo).appendTo(".modal-title");
        
        var row  = "<tr>";
            row += "<td class='codigo'>" + json.pro_cuerpo + "</td>";
            row += "</tr>";
            $(row).appendTo("#tabla_modal");
        
        $("#modPromo").modal("show");
       
    };
});