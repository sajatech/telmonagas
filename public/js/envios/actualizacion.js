$(document).ready(function() {
    $("#cliente").select2({width: "100%"});
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/envios/lista");

    $(".input-small").datepicker({
        todayHighlight: true,
        startView: 1
    }).change(function() {
        $(".datepicker").hide();
    });


    // Habilitar botón "precargar" detalle del cliente a enviar
    $("#cliente").select2({width: "100%"}).on("select2:select", function(e) {
        var verCliente = $("#verCliente");
        var id = e.params.data.id;

        if(id != "") {
            $.ajax({
                type: "POST",
                url: "../clientes/getcliente",
                dataType: "json",

                data: {
                    cliente: $("option:selected", "#cliente").val(),
                },

                success: function(respuesta) {
                    verCliente.removeClass("disabled").attr("onclick", "verCliente('" + JSON.stringify(respuesta) + "')");
                }
            });
        } else
            verCliente.addClass("disabled");
    });

         // Formatear campos de monto
    $("#EnvMonto").priceFormat({
        prefix: "",
        centsSeparator: ",",
        thousandsSeparator: "."
    });

     // Formatear campos de monto
    $("#EnvMontoSalida").priceFormat({
        prefix: "",
        centsSeparator: ",",
        thousandsSeparator: "."
    });

	// Actualización de Envios
	$("#actualizar").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forEnvio");
        var loading = $("#loaCliente");

        $f.parsley().destroy();

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var str = $f.serialize();

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../envios/actualizacionbd?id=" + $(this).attr("data-envio-id"),
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });                        
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }
	});

    // Eliminación de envios
    $("#elmEnvio").on("click", function() {

        var salSopCallback = function(text) {
            if(text !== false && text == 'LINPIESA')
            {
                        // Se guarda la referencia al formulario
                        var $f = $("#forEnvio");
                        var loading = $("#loaCliente");

                        $f.parsley().destroy();

                        if($f.parsley().validate()) { 
                            loading.show().removeClass("hide");
                            var formData = new FormData($f[0]);

                            // Bloqueo de peticiones repetidas
                            if($f.data("locked") == undefined || !$f.data("locked")) {
                                $.ajax({  
                                    type: "POST",
                                    url: "../envios/eliminacionbd?id=" + $("#elmEnvio").attr("data-envio-id"),
                                    data: formData,
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    dataType: "json",

                                    beforeSend: function() { 
                                        $f.data("locked", true);
                                    },

                                    success: function(respuesta) {
                                        loading.hide();

                                        if(respuesta.type!='error')
                                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/envios/lista");

                                        notif({
                                            msg: respuesta.text,
                                            type: respuesta.type,
                                            position: "center",
                                            multiline: true
                                        });                        
                                    },

                                    error: function () {
                                        loading.hide();

                                        notif({
                                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                                            type: "error",
                                            position: "center",
                                            multiline: true
                                        });
                                    },

                                    complete: function() { 
                                        $f.data("locked", false);
                                    }               
                                });

                                return false;
                            }
                        }
                    }
                else{

                notif({
                    'type': 'error',
                    'msg': 'Proceso Cancelado :('
                })

            }
        }
        
        notif_prompt({
            "message": "Desa Eliminar este Envio?",
            "textaccept": "Sí, ¡deseo eliminarlo!",
            "textcancel": "No, me equivoque",
            "fullscreen": true,
            "callback": salSopCallback
        })

    $(".notifit_prompt_input").removeAttr("type","text");
    $(".notifit_prompt_input").attr("type","password");

    });
});