$(document).ready(function() {
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/inventario/ajustes");

    $.get("../ajustes/getajustes", function(data) {
        var data = JSON.parse(data);

        $("#ajaxSpinner").hide();
        $("#panAjustes").removeClass("hide").show();

        // Tabla de las ajustes de mercancía
        oTable = $("#ajustes").dataTable({
            "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "aaData": data.aaData,
            "bServerSide": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "aaSorting": [],

            "aoColumns": [
                { "mData": "codigo" },
                { "mData": "motAjuste" },
                { "mData": "tipMovimiento" },
                { "mData": "fecha" },
                { "mData": "usuario" }
            ],

            "oTableTools": {
                "sRowSelect": "single",

                "aButtons": [
                    { "sExtends": "editor_create", "sButtonText": "Agregar", 

                        "fnInit": function(button) {
                            $(button).addClass("hide");
                            
                            if(data.privilegios.includes(47))
                                $(button).removeClass("hide");
                        },

                    	"fnClick": function() {
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/inventario/ajuste", "", "GET");
                    	}

                	},
                    
                    { "sExtends": "editor_edit", "sButtonText": "Ver",

                        "fnInit": function(button) {
                            $(button).addClass("hide disabled");
                            
                            if(data.privilegios.includes(49))
                                $(button).removeClass("hide");
                        },

                        "fnClick": function(button, conf) {
                            var node = this.fnGetSelectedData();
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/inventario/ajuste?token=true", { "codigo": node[0].codigo });
                        }

                    }
                ]
        	},

            "language": {
                "lengthMenu": "_MENU_ &nbsp;&nbsp;"
            },

            "iDisplayLength": 50,    
        });

        $("#ajustes_filter input").addClass("form-control").attr("placeholder", "Buscar Ajuste...");
        $("#ajustes_length select").addClass("form-control");

        // Se añaden los íconos
        $("#ToolTables_ajustes_0").prepend("<i class='ti ti-plus'/> ");
        $("#ToolTables_ajustes_1").prepend("<i class='ti ti-search'/> ");

        // Se añaden los elementos de la tabla al panel
        $(".panel-ctrls").append($("#ajustes_filter").addClass("pull-right"));
        $(".panel-ctrls").append("<i class='separator'></i>");
        $('.panel-ctrls').append($('#ajustes_length').addClass("pull-left mt-14").find("label").addClass("panel-ctrls-center"));
        $(".panel-ctrls").append("<i class='separator'></i>");
        $(".panel-ctrls").append($(".DTTT.btn-group").addClass("pull-left mt-10"));

        $(".panel-footer").append($("#ajustes+.row"));
        $("#ajustes_paginate>ul.pagination").addClass("pull-right pagination-lg");

        // ******************************** //
    });
});