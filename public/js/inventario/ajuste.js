$(document).ready(function() {
	$("#motAjuste, #tipMovimiento, #articulo").select2({width: "100%"});
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/inventario/ajustes");

    // Checkboxes personalizados
    $(".icheck input").iCheck({
        radioClass: "iradio_flat-blue"
    });

	var arrArticulo = [];
    // Registro de ajuste de inventario
	$("#regAjuste").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#ajuste");
        var loading = $("#loaAjuste");

        $f.parsley().destroy();

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var str = $f.serialize();

			var objArticulo = {};
			for(i in arrArticulo)
				objArticulo[i] = arrArticulo[i];

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../inventario/ajustebd?articulos=" + JSON.stringify(objArticulo),
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();
                        
                        if(respuesta.type != "error")
                            $("#canAjuste").click();
                        
                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }
	});

	// Agregación de nuevo artículo a lista de ajustes
	$("#articulo").select2({width: "100%"}).on("select2:select", function(e) {
		var id = e.params.data.id;

		if($.inArray(id, arrArticulo) < 0) {
			arrArticulo.push(id);

		    $.ajax({
		        type: "POST",
		        url: "../articulos/urlgetarticulo",
		        dataType: "json",

		        data: {
		        	articulo: id,
		        	tipo: "id"
		        },

		        success: function(respuesta) {
		        	if(respuesta.false == undefined) {
						$("#lisAjustes").show().removeClass("hide");

			    		var trs = $("#tabLisAjustes tbody tr").length;

			    		if(trs == 0) {
			    			var row  = "<tr id='ajuste" + respuesta.articulo.id + "'>";
			    				row += "<td class='codigo'>" + respuesta.articulo.art_codigo + "</td>";
			    				row += "<td><input type='text' name='cantidad[]' class='cantidad form-control' required='required' placeholder='0' data-parsley-type='number' data-parsley-min='1'/></td>";
			    				row += "<td class='cosUnitario'>" + "$ <span class='spaCosUnitario'>" + number_format(respuesta.articulo.art_cos_unitario) + "</span> / Bs. " + number_format(respuesta.articulo.art_pre_unitario1)  + "</td>";
			    				row += "<td class='cosTotal'>$ <span class='spaCosTotal'>0,00</span> / Bs. 0,00</td>";
			    				row += "<td><a class='btn btn-md btn-primary btn-label eliAjuste mr-xs' onclick='eliAjuste(" + respuesta.articulo.id + ")'><i class='ti ti-trash'></i> Eliminar</a><a class='btn btn-md btn-primary btn-label verArticulo'><i class='ti ti-search'></i> Ver Artículo</a></td>";
			    				row += "</tr>";
			    			
			    			$(row).appendTo("#tabLisAjustes tbody");			    			
			    			$(".verArticulo").attr("onclick", "verArticulo('" + JSON.stringify(respuesta.articulo) + "', '" + respuesta.dolar + "')");
			    			$(".cantidad").focus();

			    			if($("#descarga").is(":checked"))
			    				$(".cantidad").attr("data-parsley-max", respuesta.existencia);
			    		} else {
			    			var tabLisAjustes = $("#tabLisAjustes tr:last");
							var clone = tabLisAjustes.clone(true);
							var cantidad = $(".cantidad", clone);

							clone.attr("id", "ajuste" + respuesta.articulo.id);
							$(".codigo", clone).text(respuesta.articulo.art_codigo);
							cantidad.val("").removeAttr("data-parsley-max");

			    			if($("#descarga").is(":checked"))
			    				cantidad.attr("data-parsley-max", respuesta.existencia);
							
							$(".cosUnitario", clone).html("$ <span class='spaCosUnitario'>" + number_format(respuesta.articulo.art_cos_unitario) + "</span> / Bs. " + number_format(respuesta.articulo.art_cos_unitario * respuesta.dolar));
							$(".cosTotal", clone).html("$ <span class='spaCosTotal'>0,00</span> / Bs. 0,00");
							$(".eliAjuste", clone).attr("onclick", "eliAjuste(" + respuesta.articulo.id + ")");
							$(".verArticulo", clone).attr("onclick", "verArticulo('" + JSON.stringify(respuesta.articulo) + "', '" + respuesta.dolar + "')");
							$(".help-block", clone).removeClass("filled").empty();
							$(clone).insertAfter("#tabLisAjustes tr:last");
							cantidad.focus();
			    		}
					}
		        }
		    });
		} else {
	        notif({
	        	msg: "Ya se encuentra ese artículo en la lista.",
	        	type: "error",
	        	position: "center",
                multiline: true
	        });
		}
	});

	// Cálculo de costo total
	$("#lisAjustes").on("keyup", ".cantidad", function() {
		var cantidad = $(this).val();
		var parent = $(this).parent().next();
		var cosUnitario = replace_number(parent.find(".spaCosUnitario").text());
		var cosTotal = Number(cantidad) * Number(cosUnitario);

	    $.ajax({
	        type: "POST",
	        url: "../dolares/urlgetdolar",

	        success: function(respuesta) {
				parent.next().html("$ <span class='spaCosTotal'>" + number_format(cosTotal) + "</span> / Bs. " + number_format(cosTotal * respuesta));

				var sumCosTotal = 0;
				$(".spaCosTotal").each(function() {
					sumCosTotal = Number(sumCosTotal) + Number(replace_number($(this).text()));
				});

        		$("#sumCosTotal").text("$ " + number_format(sumCosTotal) + " / Bs. " + number_format(sumCosTotal * respuesta));
	        }
	    });
	});

	// Eliminación de ajuste
    $("#elmAjuste").on("click", function() {

     	var salSopCallback = function(text) {
     		if(text !== false && text == 'LINPIESA')
     		{
                                    // Se guarda la referencia al formulario
                                    var $f = $("#ajuste");
        							var loading = $("#loaAjuste");

                                    $f.parsley().destroy();

                                    if($f.parsley().validate()) { 
                                        loading.show().removeClass("hide");
                                        var formData = new FormData($f[0]);

                                        // Bloqueo de peticiones repetidas
                                        if($f.data("locked") == undefined || !$f.data("locked")) {
                                            $.ajax({  
                                                type: "POST",
                                                url: "../inventario/eliminacionbd?id=" + $("#elmAjuste").attr("data-ajuste-id"),
                                                data: formData,
                                                cache: false,
                                                contentType: false,
                                                processData: false,
                                                dataType: "json",

                                                beforeSend: function() { 
                                                    $f.data("locked", true);
                                                },

                                                success: function(respuesta) {
                                                    loading.hide();

                                                    if(respuesta.type!='error')
                                                        $.redirect("/" + (window.location.pathname).split("/")[1] + "/inventario/ajustes");

                                                    notif({
                                                        msg: respuesta.text,
                                                        type: respuesta.type,
                                                        position: "center",
                                                        multiline: true
                                                    });                        
                                                },

                                                error: function () {
                                                    loading.hide();

                                                    notif({
                                                        msg: "Sucedió un error, contacte con el administrador del sistema.",
                                                        type: "error",
                                                        position: "center",
                                                        multiline: true
                                                    });
                                                },

                                                complete: function() { 
                                                    $f.data("locked", false);
                                                }               
                                            });

                                            return false;
                                        }
                                    }
                                }
                            else{

            	notif({
            		'type': 'error',
            		'msg': 'Proceso Cancelado :('
            	})

            }
        }
        
        notif_prompt({
        	"message": "Desa Eliminar este Ajuste?",
        	"textaccept": "Sí, ¡deseo eliminarlo!",
        	"textcancel": "No, me equivoque",
        	"fullscreen": true,
        	"callback": salSopCallback
        })

        $(".notifit_prompt_input").removeAttr("type","text");
     	$(".notifit_prompt_input").attr("type","password");
    });

    // Resetear formulario de ajuste
    $("#canAjuste").on("click", function () {
        $("#ajuste").each(function() {
            this.reset();
			$("#motAjuste, #articulo").val(null).trigger("change");
			$("#ajuste").parsley().destroy();
            $("#tabLisAjustes tbody").empty();
            $("#lisAjustes").hide();
            arrArticulo = [];
        });

        return false;
    });

	// Asignar cantidad máxima de artículos que se puede despachar
	$("#descarga").on("ifChecked", function() {
		for(i in arrArticulo) {
		    $.ajax({
		        type: "POST",
		        url: "../articulos/urlgetarticulo",
		        dataType: "json",

		        data: {
		        	articulo: arrArticulo[i],
		        	tipo: "id"
		        },

		        success: function(respuesta) {
		        	if(respuesta.false == undefined) {
		        		$("#ajuste" + respuesta.articulo.id).find(".cantidad").attr("data-parsley-max", respuesta.existencia);
		        	}
		        }
		    });
		}
	});

	$("#carga").on("ifChecked", function() {
		$(".cantidad").removeAttr("data-parsley-max");
	});

	// Función para eliminar un ajuste de la lista
	eliAjuste = function(ajuste) {
		$("#ajuste" + ajuste).remove();
		delete arrArticulo[$.inArray(ajuste.toString(), arrArticulo)];

		if($("#tabLisAjustes tbody tr").length == 0) {
			$("#lisAjustes").hide();
			$("#articulo").val(null).trigger("change");
		}
	}

	// Preparación de modal para ver detalle de artículo
	verArticulo = function(articulo, dolar) {
		var json = JSON.parse(articulo);

		$("#modArticulo .modal-title").text("Artículo: " + json.art_codigo);
		$("#codArticulo").val(json.art_codigo).attr("readonly", "readonly");
		$("#descripcion").val(json.art_descripcion).attr("readonly", "readonly");
		$("#cosUnitario").val(number_format(json.art_cos_unitario)).attr("readonly", "readonly");
		$("#preUnitario1").val(number_format(json.art_pre_unitario1)).attr("readonly", "readonly");
		$("#preUnitario2").val(number_format(json.art_pre_unitario2)).attr("readonly", "readonly");
		$("#preUnitario3").val(number_format(json.art_pre_unitario3)).attr("readonly", "readonly");
		$("#cosUniBs").val(number_format(json.art_cos_unitario * dolar)).attr("readonly", "readonly");
		$("#preUni1Bs").val(number_format(json.art_pre_unitario1 * dolar)).attr("readonly", "readonly");
		$("#preUni2Bs").val(number_format(json.art_pre_unitario2 * dolar)).attr("readonly", "readonly");
		$("#preUni3Bs").val(number_format(json.art_pre_unitario3 * dolar)).attr("readonly", "readonly");
		$("#invInicial").val(json.art_inv_inicial).attr("readonly", "readonly");
		$("#fecCreacion").val(json.art_fec_creacion).attr("readonly", "readonly");
		$("#stoMinimo").val(json.art_sto_minimo).attr("readonly", "readonly");
		$("#ubicacion").val(json.art_ubicacion).attr("readonly", "readonly");
		
		// Evitar carga de foto referencial en esta pantalla
		$("#canFotReferencial").click();

		if(json.art_fot_referencial != null) {
			$(".fileinput-filename").html(json.art_fot_referencial);
			$("#fileinput").removeClass("fileinput-new").addClass("fileinput-exists").find(".uneditable-input").addClass("disabled");
			$("#canFotReferencial, #btn-file").addClass("disabled");
			$("#divFotReferencial").show().removeClass("hide");
			$("#divFotReferencial img").attr("src", "/" + (window.location.pathname).split("/")[1] + "/" + (json.art_fot_referencial).replace("files/", "") + "?" + Math.random());
		} else {
			$("#fileinput").find(".uneditable-input").addClass("disabled");
			$("#btn-file").addClass("disabled");
			$("#divFotReferencial").hide();
		}

		$("#selFoto").on("click", function(e) {
			e.preventDefault();
		})

		// ******************************** //

		$("#regArticulo").hide();
		$("#canArticulo").hide();
    	$("#modArticulo").modal("show");
	}
});