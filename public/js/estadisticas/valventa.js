$(document).ready(function() {
    // Checkboxes personalizados
    $(".icheck input").iCheck({
        radioClass: "iradio_flat-blue"
    });

    var fecInicio = new Pikaday({ field: $("#fecInicio")[0], format: "DD/MM/YYYY", minDate: new Date("2018-01-01") });
    var fecFin = new Pikaday({ field: $("#fecFin")[0], format: "DD/MM/YYYY", minDate: new Date("2018-01-01") });

	chaCreate = function(button) {
		$.post("../estadisticas/getdata1", { seleccione: $("#masValor").is(":checked"), fecInicio: $("#fecInicio").val(), fecFin: $("#fecFin").val(), offset: $("#" + button).attr("data-articulos-offset") }, function(data) {
			$("#preloader").fadeOut("slow");

			var data = JSON.parse(data);

			if(data.length > 0) {
				var chart = am4core.create("chartdiv", am4charts.PieChart);

				if(button == "procesar") {
					$("#anterior").attr("data-articulos-offset", 0);
					$("#siguiente").attr("data-articulos-offset", 0);		
				}

				$("svg").find("g[visibility=hidden]").next().remove();

				var siguiente = $("#siguiente").attr("data-articulos-offset");

				if(button == "procesar" || button == "siguiente") {
					$("#anterior").attr("data-articulos-offset", Number(siguiente) - 10);
					$("#siguiente").attr("data-articulos-offset", Number(siguiente) + 10);
				} else if(button == "anterior") {
					$("#anterior").attr("data-articulos-offset", Number(siguiente) - 30);
					$("#siguiente").attr("data-articulos-offset", Number(siguiente) - 10);
				}

				$("#siguiente").attr("data-articulos-offset") >= 20 ? $("#anterior").removeClass("disabled") : $("#anterior").addClass("disabled");

				chart.data = data;
				chart.numberFormatter.numberFormat = "#.###,##";

				var pieSeries = chart.series.push(new am4charts.PieSeries());
					pieSeries.dataFields.value = "valor";
					pieSeries.dataFields.category = "articulo";
					pieSeries.labels.template.text = "{category}: [bold]{value.formatNumber('#.00')}%[/] \nCompras: {compras} \nVentas: {ventas}";
					pieSeries.slices.template.tooltipText = "{descripcion} \n *** {value.percent.formatNumber('#.00')} de 100 por ciento";
					pieSeries.slices.template.configField = "config";			
					pieSeries.slices.template.stroke = am4core.color("#fff");
					pieSeries.slices.template.strokeWidth = 2;
					pieSeries.slices.template.strokeOpacity = 1;
					pieSeries.hiddenState.properties.opacity = 1;
					pieSeries.hiddenState.properties.endAngle = -45;
					pieSeries.hiddenState.properties.startAngle = -45;
					pieSeries.labels.template.paddingTop = 0;
					pieSeries.labels.template.paddingLeft = 0;
					pieSeries.labels.template.paddingBottom = 0;
					pieSeries.tooltip.label.maxWidth = 300;
					pieSeries.tooltip.label.wrap = true;
					pieSeries.tooltip.getFillFromObject = false;
					pieSeries.tooltip.label.background.fill = am4core.color("#FFFFFF");
					pieSeries.tooltip.label.fill = am4core.color("#000000");
			} else {
				notif({
				    msg: "No existen más artículos.",
				    type: "error",
				    position: "center",
                    multiline: true
				});				
			}
		});
	}

	am4core.ready(function() { $("svg").find("g[visibility=hidden]").next().remove();
		am4core.useTheme(am4themes_kelly);
		am4core.useTheme(am4themes_animated);

		chaCreate("procesar");
	});

	$("#procesar, #anterior, #siguiente").on("click", function() {  
		chaCreate($(this).attr("id"));
	});
});