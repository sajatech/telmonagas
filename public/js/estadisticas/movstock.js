$(document).ready(function() {
    // Checkboxes personalizados
    $(".icheck input").iCheck({
        radioClass: "iradio_flat-blue"
    });

    var fecInicio = new Pikaday({ field: $("#fecInicio")[0], format: "DD/MM/YYYY", minDate: new Date("2018-01-01") });
    var fecFin = new Pikaday({ field: $("#fecFin")[0], format: "DD/MM/YYYY", minDate: new Date("2018-01-01") });

	chaCreate = function(button) {
		$.post("../estadisticas/getdata2", { seleccione: $("#opcion1").is(":checked"), fecInicio: $("#fecInicio").val(), fecFin: $("#fecFin").val(), offset: $("#" + button).attr("data-articulos-offset") }, function(data) {
			$("#preloader").fadeOut("slow");

			var data = JSON.parse(data);

			if(data.length > 0) {
				var chart = am4core.create("chartdiv", am4charts.XYChart);

				if(button == "procesar") {
					$("#anterior").attr("data-articulos-offset", 0);
					$("#siguiente").attr("data-articulos-offset", 0);		
				}

				$("svg").find("g[visibility=hidden]").next().remove();

				var siguiente = $("#siguiente").attr("data-articulos-offset");

				if(button == "procesar" || button == "siguiente") {
					$("#anterior").attr("data-articulos-offset", Number(siguiente) - 10);
					$("#siguiente").attr("data-articulos-offset", Number(siguiente) + 10);
				} else if(button == "anterior") {
					$("#anterior").attr("data-articulos-offset", Number(siguiente) - 30);
					$("#siguiente").attr("data-articulos-offset", Number(siguiente) - 10);
				}

				$("#siguiente").attr("data-articulos-offset") >= 20 ? $("#anterior").removeClass("disabled") : $("#anterior").addClass("disabled");

				chart.data = data;
				chart.numberFormatter.numberFormat = "#.###,##";

				var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
					categoryAxis.dataFields.category = "articulo";
					categoryAxis.renderer.grid.template.location = 0;
					categoryAxis.renderer.minGridDistance = 30;

					categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
	  					if(target.dataItem && target.dataItem.index & 2 == 2)
	    					return dy + 25;
	  					
	  					return dy;
					});

				var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

				var series = chart.series.push(new am4charts.ColumnSeries());
					series.dataFields.valueY = "valor";
					series.dataFields.categoryX = "articulo";
					series.columns.template.tooltipPosition = "fixed";
					series.columns.template.tooltipText = "{descripcion} \n *** Ventas: [bold]{valueY}[/] \n *** Existencia: {existencia}";
					series.columns.template.fillOpacity = .8;
					series.tooltip.label.maxWidth = 300;
					series.tooltip.label.wrap = true;
					series.tooltip.getFillFromObject = false;
					series.tooltip.label.background.fill = am4core.color("#FFFFFF");
					series.tooltip.label.fill = am4core.color("#000000");

				var labelBullet = series.bullets.push(new am4charts.LabelBullet());
					labelBullet.label.text = "{valueY}";

				var columnTemplate = series.columns.template;
					columnTemplate.strokeWidth = 2;
					columnTemplate.strokeOpacity = 1;
			} else {
				notif({
				    msg: "No existen más movimientos.",
				    type: "error",
				    position: "center",
                    multiline: true
				});				
			}
		});
	}

	am4core.ready(function() { 
		$("svg").find("g[visibility=hidden]").next().remove();
		am4core.useTheme(am4themes_kelly);
		am4core.useTheme(am4themes_animated);
		chaCreate("procesar");
	});

	$("#procesar, #anterior, #siguiente").on("click", function() {  
		chaCreate($(this).attr("id"));
	});
});