$(document).ready(function() {
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/importacion/precios");
    	
        $(".input-small").datepicker({
            todayHighlight: true,
            startView: 1
        }).change(function() {
        	$(".datepicker").hide();
        });

    	 // Formatear campos de monto
        $("#fac_monto").priceFormat({
            prefix: "",
            centsSeparator: ",",
            thousandsSeparator: "."
        });

        $("#fac_monto_flete").priceFormat({
            prefix: "",
            centsSeparator: ",",
            thousandsSeparator: "."
        });

        $("#fac_aduana_bs").priceFormat({
            prefix: "",
            centsSeparator: ",",
            thousandsSeparator: "."
        });

        $("#fac_transporte_nac").priceFormat({
            prefix: "",
            centsSeparator: ",",
            thousandsSeparator: "."
        });

        $("#fac_tasa_dolar").priceFormat({
            prefix: "",
            centsSeparator: ",",
            thousandsSeparator: "."
        });

        $("#fac_gastos_ope").priceFormat({
            prefix: "",
            centsSeparator: ",",
            thousandsSeparator: "."
        });

        $("#fac_imp1").priceFormat({
            prefix: "",
            centsSeparator: ",",
            thousandsSeparator: "."
        });

        $("#fac_imp2").priceFormat({
            prefix: "",
            centsSeparator: ",",
            thousandsSeparator: "."
        });

        $("#fac_imp3").priceFormat({
            prefix: "",
            centsSeparator: ",",
            thousandsSeparator: "."
        });

        $("#gastos_garantia").priceFormat({
            prefix: "",
            centsSeparator: ",",
            thousandsSeparator: "."
        });
          
        // Formatear campos de peso
        $("#fac_peso").priceFormat({
            prefix: "",
            centsSeparator: ",",
            thousandsSeparator: "."
        });

        $("#item_peso").priceFormat({
            prefix: "",
            centsSeparator: ",",
            thousandsSeparator: "."
        });

        $("#item_costo").priceFormat({
            prefix: "",
            centsSeparator: ",",
            thousandsSeparator: "."
        });

         $("#fac_peso_porcentaje").priceFormat({
            prefix: "",
            centsSeparator: ",",
            thousandsSeparator: "."
        });

         $('#item_cantidad').on('input', function () { 
                this.value = this.value.replace(/[^0-9]/g,'');
                });
        //bloqueo el campo aduana impuesto si costo de flete es mayor a 0.
        
        $("#fac_monto_flete").change(function(){
            
            var pp = $("#fac_monto_flete").val();
            
            if(pp!='0,00')
                $('#fac_aduana_bs').attr("readonly","readonly");
            else
                $('#fac_aduana_bs').removeAttr("readonly");

        });

        $("#fac_aduana_bs").change(function(){
            
            var abs = $("#fac_aduana_bs").val();
            
            if(abs!='0,00')
                $('#fac_monto_flete').attr("readonly","readonly");
            else
                $('#fac_monto_flete').removeAttr("readonly");

        });

        // Resetear formulario
        $("#cancelar").on("click", function () { 
            $("#forFactura").each(function() {
                this.reset();
            });

            $("#item").each(function() {
                this.reset();
            });

            $("#forFactura").parsley().destroy();
            $("#item").parsley().destroy();
            
            $("#tabLisItems tbody").empty();
            $("#lisItems").hide();
            arrItem = [];

            return false;
        });

        // Función para eliminar un item de la lista
    	eliItem = function(item,cantidad,costo,peso) {
            
            var total_factura = parseFloat($("#fac_monto").val()) - parseFloat(costo * cantidad);
            $("#fac_monto").val(number_format(total_factura)); 

            var total_cantidad = replace_number($("#fac_bultos").val()) - parseFloat(cantidad);
            $("#fac_bultos").val(total_cantidad); 

            var peso_real = replace_number($("#fac_peso_porcentaje").val()) - parseFloat(parseFloat(cantidad * peso)/1000);

            $("#fac_peso_porcentaje").val(number_format(peso_real));  

    		$("#item" + item).remove();   	

    		arrItem.splice(arrItem.findIndex(x => x.id == item));

    		if($("#tabLisItems tbody tr").length == 0)
    			{
                    $("#lisItems").hide();
                    $("#fac_monto").val(0); 
                    $("#fac_bultos").val(0); 
                    $("#fac_peso_porcentaje").val(0); 
                    $("#fac_peso_embalaje").val(0); 
                }
    	}

        //funcion para agregar items a la factura
        var arrItem = [];
        var index = 1;
        $("#addItem").on("click", function() {
            // Se guarda la referencia al formulario
            var $f = $("#item");
            //var loading = $("#loading");

            $f.parsley().destroy();

            if($f.parsley().validate()) {
            	//variable de la factura
            	var fac_tasa_dolar = replace_number($("#fac_tasa_dolar").val());  
            	var fac_monto = replace_number($("#fac_monto").val()); 
            	var fac_monto_flete = replace_number($("#fac_monto_flete").val()); 
            	var fac_pesoneto = replace_number($("#fac_peso").val()); 
                var fac_peso_porcentaje = replace_number($("#fac_peso_porcentaje").val());
                var fac_peso = parseFloat(fac_pesoneto) - parseFloat(fac_peso_porcentaje);
            	var fac_aduana_bs = replace_number($("#fac_aduana_bs").val()); 
            	var fac_transporte_nac = replace_number($("#fac_transporte_nac").val());
                var fac_transporte_nac_dolar = parseFloat(fac_transporte_nac) / parseFloat(fac_tasa_dolar);
            	var fac_total_art = $("#fac_bultos").val(); 
            	var gasto_operativo = replace_number($("#fac_gastos_ope").val());
            	var imp1 = replace_number($("#fac_imp1").val());
            	var imp2 = replace_number($("#fac_imp2").val());
            	var imp3 = replace_number($("#fac_imp3").val());

            	//varibles especificas del item
             	var item_codigo = $("#item_codigo").val();
            	var item_descripcion = $("#item_descripcion").val();
            	var item_cantidad = $("#item_cantidad").val();
            	var item_peso = replace_number($("#item_peso").val());
            	var item_costo = replace_number($("#item_costo").val());
            	var gastos_garantia = replace_number($("#gastos_garantia").val());
            	var trs = $("#tabLisItems tbody tr").length;

            	var x = (parseFloat(fac_monto_flete)/parseFloat(fac_pesoneto))/1000;
            	var y = (parseFloat(fac_transporte_nac_dolar)/parseFloat(fac_pesoneto))/1000;
            	var aduana_dolar = parseFloat(fac_aduana_bs) / parseFloat(fac_tasa_dolar);

                // var total_factura = (parseFloat(fac_monto) + parseFloat(item_cantidad * item_costo));
                // var tota_art = (parseFloat(fac_total_art) + parseFloat(item_cantidad));
                // $("#fac_monto").val(number_format(total_factura)); 
                // $("#fac_bultos").val(tota_art); 

                // var total_peso = (parseFloat(fac_peso_porcentaje) + parseFloat(parseFloat((item_cantidad * item_peso)/1000)));
                // $("#fac_peso_porcentaje").val(number_format(total_peso)); 
    			//alert(parseFloat(fac_peso)); alert(y);

    			$("#lisItems").show().removeClass("hide");

                //calculos de precio rela X item

                var costo_gpg = parseFloat(parseFloat(item_costo)*(parseFloat(gastos_garantia)/100));

                var valor1 = parseFloat(parseFloat(x)*parseFloat(item_peso));

                var valor2 = parseFloat(parseFloat(y)*parseFloat(item_peso));

                var valor3 = ((parseFloat(parseFloat(aduana_dolar)/parseFloat(fac_pesoneto)))/1000) * parseFloat(item_peso);

                var valor4 = ((parseFloat((parseFloat(gasto_operativo)/parseFloat(fac_tasa_dolar))/parseFloat(fac_pesoneto)))/1000) * parseFloat(item_peso);

                var valor5 = ((parseFloat(imp1)/parseFloat(fac_pesoneto))/1000) * parseFloat(item_peso);

                var valor6 = ((parseFloat(parseFloat(parseFloat(imp2)/parseFloat(fac_tasa_dolar))/parseFloat(fac_pesoneto)))/1000) * parseFloat(item_peso);

                var valor7 = ((parseFloat(parseFloat(parseFloat(imp3)/parseFloat(fac_tasa_dolar))/parseFloat(fac_pesoneto)))/1000) * parseFloat(item_peso);

                var precio_real = parseFloat(parseFloat(item_costo) + parseFloat(valor1) + parseFloat(valor2) + parseFloat(valor3) + parseFloat(costo_gpg) + parseFloat(valor4) + parseFloat(valor5) + parseFloat(valor6) + parseFloat(valor7));

                var porcentaje_precio = ((parseFloat(precio_real) - parseFloat(item_costo)) * 100 ) / parseFloat(item_costo);

             	var item = {
             		index: index,
             		item_codigo: item_codigo,
             		item_descripcion: item_descripcion,
             		item_cantidad: item_cantidad,
             		item_peso: item_peso,
             		item_costo: item_costo,
             		costo_gpg: gastos_garantia,
                    precio_real: number_format(precio_real)
           
             	};

             	arrItem.push(item);

                var factura = $('#fac_numero').val();
             	//alert(precio_real);
                if(fac_pesoneto!="" && factura!="")
                {
                    var total_factura = (parseFloat(fac_monto) + parseFloat(item_cantidad * item_costo));
                    var tota_art = (parseFloat(fac_total_art) + parseFloat(item_cantidad));
                    $("#fac_monto").val(number_format(total_factura)); 
                    $("#fac_bultos").val(tota_art); 

                    var total_peso = (parseFloat(fac_peso_porcentaje) + parseFloat(parseFloat((item_cantidad * item_peso)/1000)));
                    $("#fac_peso_porcentaje").val(number_format(total_peso)); 
                    $("#fac_peso_embalaje").val(number_format(fac_pesoneto - total_peso)); 

            		if(trs == 0) {

            			var row  = "<tr id='item" + index + "'>";
            				row += "<td class='codigo'>" + item_codigo + "</td>";
            				row += "<td class='descripcion'>" + item_descripcion + "</td>";
            				row += "<td class='cantidad'>" + item_cantidad + "</td>";
            				row += "<td class='peso'>" + number_format(item_peso) + "</td>";
            				row += "<td class='costo'>" + number_format(item_costo) + "</td>";
            				row += "<td class='costo_gpg'>" + number_format(costo_gpg) + "</td>";
            				row += "<td class='resultado_costo' style='color:green'>" + number_format(precio_real) + "</td>";
                            row += "<td class='incremento' style='color:green'>" + number_format(porcentaje_precio) + " % </td>";
            				row += "<td class='text-center'><a class='btn btn-md btn-primary btn-label eliItem mr-xs tooltips' onclick='eliItem(" + index + "," + item_cantidad + "," + parseFloat(item_costo) + "," + parseFloat(item_peso) + ")' data-trigger='hover' data-original-title='Eliminar'><i class='ti ti-trash btn-label-custom'></i></a></td>";
            				row += "</tr>";
            			
            			$(row).appendTo("#tabLisItems tbody");
            		} else {
            			var tabLisItems = $("#tabLisItems tr:last");
        				var clone = tabLisItems.clone();

        				clone.attr("id", "item" + index);
        				$(".codigo", clone).text(item_codigo);
        				$(".descripcion", clone).text(item_descripcion);
        				$(".cantidad", clone).text(item_cantidad);
        				$(".peso", clone).text(number_format(item_peso));
        				$(".costo", clone).text(number_format(item_costo));
        				$(".costo_gpg", clone).text(number_format(costo_gpg));
                        $(".resultado_costo", clone).text(number_format(precio_real));
                        $(".incremento", clone).text(number_format(porcentaje_precio) + " %");
        				$(".eliItem", clone).attr("onclick", "eliItem(" + index + "," + item_cantidad + "," + parseFloat(item_costo) + "," + parseFloat(item_peso) + ")");
        				$(clone).insertAfter("#tabLisItems tr:last");
            		}

        			$(".tooltips").tooltip();
        			//$("#sumPagTotal").html("$ <span id='dolares'>" + number_format(total$) + "</span> - Bs. <span id='bolivares'>" + number_format(totalbs) + "</span>");
        			//$("#cuenta, #moneda").val(null).trigger("change");
                	index++;

                }
                else
                    {
                        $("#lisItems").hide().addClass("hide");

                        notif({
                            msg: "Por favor cargue todos los datos obligatorios de la factura.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                        return false;

                    }
            	$("#item").each(function() {
    	            this.reset();
    	        });
            }

            return false;
        });


        //registro de factura
    	$("#registrar").on("click", function() {

            var salSopCallback = function(text) {

            if(text !== false && text == 'LINPIESA')
            {

            // Se guarda la referencia al formulario
            var $f = $("#forFactura");
            var loading = $("#loading");

            $f.parsley().destroy();

            if($f.parsley().validate()) {
                $(".selPreVenta").next(".help-block").remove();

            	if($("#tabLisItems tbody tr").length > 0 ) {
    	            loading.show().removeClass("hide");

    				var objItem = {};
    				for(i in arrItem)
    					objItem[i] = arrItem[i];

                    var str = $f.serializeArray();
                    str.push( {name:"items",value: JSON.stringify(objItem) });

    	            // Bloqueo de peticiones repetidas
    	            if($f.data("locked") == undefined || !$f.data("locked")) {
    	                $.ajax({  
    	                    type: "POST",
    	                    url: "../importacion/registrobd",
                            data: str,
    	                    //data: str + "items=" + JSON.stringify(objItem),
    	                    dataType: "json",

    	                    beforeSend: function() { 
    	                        $f.data("locked", true);
    	                    },

    	                    success: function(respuesta) {
    	                     
    	                        loading.hide();
    	                        
    	                        if(respuesta.type != "error") {
    	                            $("#cancelar").click();

    								var impSopCallback = function(choice) {
    									if(choice)
    										window.open("/" + (window.location.pathname).split("/")[1] + "/importacion/factura_pdf/" + respuesta.factura, "Calculador de Precios", "width=240, height=600");
    									else
    										$.redirect("/" + (window.location.pathname).split("/")[1] + "/importacion/precios?factura=" + respuesta.factura);
    								}
    								notif_confirm({
    									"message": respuesta.text,
    									"textaccept": "Sí, ¡deseo imprimirlo!",
    									"textcancel": "No, imprimir más tarde",
    									"fullscreen": true,
    									"callback": impSopCallback
    								});
    	                        } else {
    		                        notif({
    		                            msg: respuesta.text,
    		                            type: "error",
    		                            position: "center",
    		                            multiline: true
    		                        });
    	                        }
    	                    },

    	                    error: function () {
    	                        loading.hide();

    	                        notif({
    	                            msg: "Sucedió un error, contacte con el administrador del sistema.",
    	                            type: "error",
    	                            position: "center",
    	                            multiline: true
    	                        });
    	                    },

    	                    complete: function() { 
    	                        $f.data("locked", false);
    	                    }               
    	                });

    	                return false;
    	            }
    	        } else {
    	            notif({
    	                msg: "Debe registrar un Items al menos.",
    	                type: "error",
    	                position: "center",
    	                multiline: true
    	            });
    	        }
            }

             }
            else{

                notif({
                    'type': 'error',
                    'msg': 'Proceso Cancelado :(',
                    position: "center"
                })

            }
        }
        
        notif_prompt({
            "message": "Desea Procesar La Factura?",
            "textaccept": "Sí, ¡deseo Procesar!",
            "textcancel": "No, me equivoque",
            "fullscreen": true,
            "callback": salSopCallback
        })

        $(".notifit_prompt_input").removeAttr("type","text");
        $(".notifit_prompt_input").attr("type","password");

    	});




            // Eliminación de facturas
     $("#elmFactura").on("click", function() {

        var salSopCallback = function(text) {

            if(text !== false && text == 'LINPIESA')
                {
                // Se guarda la referencia al formulario
                var $f = $("#forFactura");
                var loading = $("#loading");

                $f.parsley().destroy();

                if($f.parsley().validate()) { 
                    loading.show().removeClass("hide");
                    var formData = new FormData($f[0]);

                    // Bloqueo de peticiones repetidas
                    if($f.data("locked") == undefined || !$f.data("locked")) {
                        $.ajax({  
                            type: "POST",
                            url: "../importacion/eliminacionbd?id=" + $("#elmFactura").attr("data-factura-id"),
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            dataType: "json",

                            beforeSend: function() { 
                                $f.data("locked", true);
                            },

                            success: function(respuesta) {
                                loading.hide();

                                if(respuesta.type!='error')
                                    $.redirect("/" + (window.location.pathname).split("/")[1] + "/importacion/precios");

                                notif({
                                    msg: respuesta.text,
                                    type: respuesta.type,
                                    position: "center",
                                    multiline: true
                                });                        
                            },

                            error: function () {
                                loading.hide();

                                notif({
                                    msg: "Sucedió un error, contacte con el administrador del sistema.",
                                    type: "error",
                                    position: "center",
                                    multiline: true
                                });
                            },

                            complete: function() { 
                                $f.data("locked", false);
                            }               
                        });

                        return false;
                    }
                }
            }
            else{

                notif({
                    'type': 'error',
                    'msg': 'Proceso Cancelado :(',
                    position: "center"
                })

            }
        }
        
        notif_prompt({
            "message": "Desa Eliminar esta Factura?",
            "textaccept": "Sí, ¡deseo eliminarlo!",
            "textcancel": "No, me equivoque",
            "fullscreen": true,
            "callback": salSopCallback
        })

        $(".notifit_prompt_input").removeAttr("type","text");
        $(".notifit_prompt_input").attr("type","password");
    });

});