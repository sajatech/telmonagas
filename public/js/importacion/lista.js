$(document).ready(function() {
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/importacion/lista");

    var salSopCallback = function(text) {
    if(text !== false && text == 'LINPIESA')
        {
        var footer = $("footer");
        footer.hide();

        $.get("../importacion/getfacturas", function(data) {
            var data = JSON.parse(data);

            $("#ajaxSpinner").hide();
            $("#panFacturas").removeClass("hide").show();
            footer.show();

            // Tabla de las salidas de mercancía
            oTable = $("#facturas").dataTable({
                "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
                "aaData": data.aaData,
                "bServerSide": false,
                "bAutoWidth": false,
                "bDestroy": true,
                "aaSorting": [],

                "aoColumns": [
                    { "mData": "ide" },
                    { "mData": "codigo" },
                    { "mData": "fecha" },
                    { "mData": "observacion" },
                    { "mData": "monto" },
                    { "mData": "peso" },
                ],

                "oTableTools": {
                    "sRowSelect": "single",

                    "aButtons": [
                     { "sExtends": "editor_create", "sButtonText": "Agregar", 

                            "fnInit": function(button) {
                                $(button).addClass("hide");
                                
                                if(data.privilegios.includes(74))
                                    $(button).removeClass("hide");
                            },

                            "fnClick": function() {
                                $.redirect("/" + (window.location.pathname).split("/")[1] + "/importacion/precios", "", "GET");
                            }

                    },

                    { "sExtends": "editor_edit", "sButtonText": "Ver Detalle",

                    "fnInit": function(button) { 
                        $(button).addClass("hide disabled");

                        if(data.privilegios.includes(74))
                            $(button).removeClass("hide");
                    },

                    "fnClick": function(button) {
                        var node = this.fnGetSelectedData(); 
                        $.redirect("/" + (window.location.pathname).split("/")[1] + "/importacion/precios", { "factura": node[0].ide });
                    }

                    }
                    
                      
                    ]
            	},

                "language": {
                    "lengthMenu": "_MENU_ &nbsp;&nbsp;"
                },

                "iDisplayLength": 10,    
            });

            $("#facturas_filter input").addClass("form-control").attr("placeholder", "Buscar factura...");
            $("#facturas_length select").addClass("form-control");

            // Se añaden los íconos
            $("#ToolTables_facturas_0").prepend("<i class='ti ti-plus'/> ");
            $("#ToolTables_facturas_1").prepend("<i class='ti ti-pencil'/> ");
            $("#ToolTables_facturas_2").prepend("<i class='ti ti-printer'/> ");

            // Se añaden los elementos de la tabla al panel
            $(".panel-ctrls").append($("#facturas_filter").addClass("pull-right"));
            $(".panel-ctrls").append("<i class='separator'></i>");
            $('.panel-ctrls').append($('#facturas_length').addClass("pull-left mt-14").find("label").addClass("panel-ctrls-center"));
            $(".panel-ctrls").append("<i class='separator'></i>");
            $(".panel-ctrls").append($(".DTTT.btn-group").addClass("pull-left mt-10"));

            $(".panel-footer").append($("#facturas+.row"));
            $("#facturas_paginate>ul.pagination").addClass("pull-right pagination-lg");

            // ******************************** //
        });

        }
            else{

                notif({
                    'type': 'error',
                    'msg': 'Zona Prohibida :(',
                    position: "center"
                })
                $.redirect("/" + (window.location.pathname).split("/")[1] + "/articulos/lista");

            }
        }
        
        notif_prompt({
            "message": "Introduzca Clave de Acceso",
            "textaccept": "Entrar",
            "textcancel": "No poseo clave",
            "fullscreen": true,
            "callback": salSopCallback
        })

        $(".notifit_prompt_input").removeAttr("type","text");
        $(".notifit_prompt_input").attr("type","password");

});