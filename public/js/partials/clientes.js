$(document).ready(function() {
    $("#telLocal, #telCelular").inputmask(); 

	// Registro de clientes
	$("#regCliente").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forCliente");
        var loading = $("#loaCliente");

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var str = $f.serialize();

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../clientes/registrobd",
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });
                        
                        if(respuesta.type != "error") {
                            if((respuesta.privilegios.includes(44) && $(".modal").length > 1) || (!respuesta.privilegios.includes(44) && $(".modal").length > 0)) {
                                var newOption = new Option(respuesta.cliente, respuesta.id, true, true);

                                $("#cliente").append(newOption).trigger({ type: "select2:select", params: { data: { id: respuesta.id, text: respuesta.cliente } } }).next().next().remove();
                                $("#modCliente").modal("hide");
                            } else
                                $("#canCliente").click();
                        }
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }
	});

    // Función para aplicar máscara a campo de cédula o rif
    /*mascara = function(str) { 
        return (str == "v" || str == "V" || str == "e" || str == "E") ? "a-999999[99]" : "a-99999999-9";
    }

    var codCliente = $("#codCliente");
    codCliente.keyup(function() {
        var cedula = $(this).val();

        codCliente.inputmask({ 
            mask: mascara($(this).val().substring(0, 1)), 
            greedy: false, 
            clearIncomplete: true,
            definitions: { 
                "a": { 
                    validator: "[vVeEjJgG]" 
                } 
            } 
        }); 
    });*/

    // Resetear formulario
    $("#canCliente").on("click", function () {
        $("#forCliente").each(function() {
            this.reset();
        });

        $("#codCliente").focus();

        return false;
    });
});