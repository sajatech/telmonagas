$(document).ready(function() {    
    // Tabla de los seriales
    table = $("#seriales").DataTable({
        "sDom": "<'row'<'col-sm-4'i><'col-sm-8'f>r>t<'row'<'col-sm-4'i><'col-sm-8'p>>",
        "paging": true, 
        "info": false,         
        "lengthChange":false,
        "iDisplayLength": 5,
        "aaSorting": [0, "desc"],

        "fnDrawCallback": function(settings, json) {
            $(".tooltips").tooltip();
        },

        "aoColumns": [
            { "mData": "codigo", "visible": false },
            { "mData": "serial" },
            { "mData": "estatus" },
            { "mData": "accion" }
        ],
    });

    $("#seriales_filter input").addClass("form-control").attr("placeholder", "Buscar Serial...");

    // Agregación de serial a la lista
    $("#agrSerial").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forSerial");
        var loading = $("#loaSerial");

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                var serial = $("#serial");
                var flag = false;
                table.data().each(function(d, index) {
                    if((d.serial).toUpperCase() == ((serial.val()).replace(/\s+/gi, " ")).toUpperCase())
                        flag = true;
                });

                if(!flag) {
                    var counter = Number(table.rows()[0].length) + 1;
                    var random = Math.floor(Math.random() * Math.floor(9223372036854775807));

                    table.row.add({
                        "codigo": counter,
                        "serial": (serial.val()).replace(/\s+/gi, " "),
                        "estatus": "<span class='label label-success'>ACTIVO</span>",
                        "accion": "<span><a id='serial" + random + "' class='btn btn-inverse-alt btn-label mr-xs desSerial tooltips' onclick='desSerial(" + random + ", false)' data-trigger='hover' data-original-title='Activar/Desactivar'><i class='ti ti-exchange-vertical btn-label-custom'></i></a><a class='btn btn-inverse-alt btn-label disabled btn-label-custom mr-xs' onclick='actSerial(false, \"" + serial.val() + "\", \"" + $("#agrSeriales").attr("data-articulo-id") + "\")'><i class='ti ti-pencil btn-label-custom'></i></a><a class='btn btn-inverse-alt btn-label eliSerial tooltips' onclick='eliSerial(" + random + ", false)' data-trigger='hover' data-original-title='Eliminar'><i class='ti ti-trash btn-label-custom'></i></a></span>"
                    }).draw(false);

                    loading.hide();
                    serial.val("");
                } else {
                    notif({
                        msg: "Ya existe un serial registrado con ese número.",
                        type: "error",
                        position: "center",
                        multiline: true
                    });

                    loading.hide();
                }

                return false;
            }
        }
    });

    // Registro de seriales
    $("#regSerial").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forSerial");
        var loading = $("#loaSerial");
        loading.show().removeClass("hide");

        // Bloqueo de peticiones repetidas
        if($f.data("locked") == undefined || !$f.data("locked")) {
            var objSerial = {};

            table.data().each(function(d, index) {
                if($(d.accion).find(".desSerial").attr("onclick").indexOf("false") > 0)
                    objSerial[index] = d.serial;
            });

            $.ajax({  
                type: "POST",
                url: "../seriales/registrobd?id=" + $(this).attr("data-articulo-id"),
                dataType: "json",

                data: { 
                    seriales: JSON.stringify(objSerial),
                },

                beforeSend: function() { 
                    $f.data("locked", true);
                },

                success: function(respuesta) {
                    loading.hide();
                    table.ajax.url("../seriales/getseriales?serial=" + $("#regSerial").attr("data-articulo-id")).load();

                    setTimeout(function() {
                        $("input#serial").val("").focus();
                    }, 1000);        

                    notif({
                        msg: respuesta.text,
                        type: respuesta.type,
                        position: "center",
                        multiline: true
                    });                        
                },

                error: function () {
                    loading.hide();

                    notif({
                        msg: "Sucedió un error, contacte con el administrador del sistema.",
                        type: "error",
                        position: "center",
                        multiline: true
                    });
                },

                complete: function() { 
                    $f.data("locked", false);
                }               
            });

            return false;
        }
    });

    agrSeriales = function(articulo) {
        $("#modSeriales .modal-title").html("Registro de seriales");
        $("#regSerial").attr("data-articulo-id", articulo);
        table.ajax.url("../seriales/getseriales?serial=" + articulo).load();
        $("#modSeriales").modal("show");

        setTimeout(function() {
            $("input#serial").val("").focus();
        }, 1000);
    }

    // Desactivación/Activación de seriales
    desSerial = function(codigo, flag) {
        var serial = $("#serial" + codigo);

        if(flag) {
            serial.addClass("disabled");

            $.ajax({
                type: "POST",
                url: "../seriales/desserial",
                dataType: "json",

                data: { 
                    codigo: codigo
                },

                success: function(respuesta) {
                    serial.removeClass("disabled").parent().parent().prev().html(respuesta.estatus);

                    setTimeout(function() {
                        $("input#serial").val("").focus();
                    }, 1000);        

                    notif({
                        msg: respuesta.text,
                        type: respuesta.type,
                        position: "center",
                        multiline: true
                    });
                },

                error: function () {
                    notif({
                        msg: "Sucedió un error, contacte con el administrador del sistema.",
                        type: "error",
                        position: "center"
                    });
                }
            });

            return false;
        } else
            table.row(serial.parents("tr")).remove().draw(false);
    }

    // Actualización de seriales
    actSerial = function(codigo, numero, articulo) {
        var modSeriales = $("#modSeriales");
            modSeriales.modal("hide");

        actModCallback = function(text) {
            if(text !== false && $.trim(text) != "") {
                $.ajax({  
                    type: "POST",
                    url: "../seriales/actserial",
                    dataType: "json",

                    data: {
                        codigo: codigo,
                        numero: $(".notifit_prompt_input").val(),
                        articulo: articulo
                    },

                    success: function(respuesta) {
                        modSeriales.modal("show");

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });

                        if(respuesta.type != "error")
                            table.ajax.url("../seriales/getseriales?serial=" + $("#regSerial").attr("data-articulo-id")).load();
                    },

                    error: function () {
                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center"
                        });
                    }
                });

                return false;               
            } else
                modSeriales.modal("show");
        }

        notif_prompt({
            "message": "Ingrese el nuevo número de serial",
            "textaccept": "Actualizar",
            "textcancel": "Cancelar",
            "fullscreen": true,
            "callback": actModCallback
        });

        $(".notifit_prompt_input").val(numero);
    }

    // Eliminación de seriales
    eliSerial = function(codigo, flag) {
        var serial = $("#serial" + codigo);

        if(flag) {
            var modSeriales = $("#modSeriales");
                modSeriales.modal("hide");

            eliSerCallback = function(choice) {
                if(choice) {
                    $.ajax({  
                        type: "POST",
                        url: "../seriales/eliserial",
                        dataType: "json",

                        data: {
                            codigo: codigo,
                        },

                        success: function(respuesta) {
                            modSeriales.modal("show");

                            notif({
                                msg: respuesta.text,
                                type: respuesta.type,
                                position: "center",
                                multiline: true
                            });

                            if(respuesta.type != "error")
                                table.ajax.url("../seriales/getseriales?serial=" + $("#regSerial").attr("data-articulo-id")).load();
                        },

                        error: function () {
                            notif({
                                msg: "Sucedió un error, contacte con el administrador del sistema.",
                                type: "error",
                                position: "center"
                            });
                        }
                    });

                    return false;               
                } else
                    modSeriales.modal("show");
            }

            notif_confirm({
                "message": "¿Realmente desea eliminar este número de serial?",
                "textaccept": "Sí, deseo eliminarlo",
                "textcancel": "Cancelar",
                "fullscreen": true,
                "callback": eliSerCallback
            });
        } else
            table.row(serial.parents("tr")).remove().draw(false);
    }
});