$(document).ready(function() {
    $("#selFoto").tooltip();
    
	// Registro de artículos
	$("#regArticulo").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forArticulo");
        var loading = $("#loaArticulo");

        $f.parsley().destroy();

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var formData = new FormData($f[0]);

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../articulos/registrobd?getCosUnitario=" + $("#cosUnitario").attr("data-articulo-value") + "&getPreUnitario1=" + $("#preUnitario1").attr("data-articulo-value") + "&getPreUnitario2=" + $("#preUnitario2").attr("data-articulo-value") + "&getPreUnitario3=" + $("#preUnitario3").attr("data-articulo-value") + "&getPreMerLibre=" + $("#preMerLibre").attr("data-articulo-value"),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });
                        
                        if(respuesta.type != "error") {
                            if((respuesta.privilegios.includes(44) && $(".modal").length > 1) || (!respuesta.privilegios.includes(44) && $(".modal").length > 0)) {
                                var newOption  = new Option(respuesta.articulo, respuesta.id, true, true);
                                
                                $("#articulo").append(newOption).trigger({ type: "select2:select", params: { data: { id: respuesta.id, text: respuesta.articulo } } }).next().next().remove();
                                $("#modArticulo").modal("hide");
                            } else
                                $("#canArticulo").click();
                        }
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        } else {
            $("#cosUniBs").parent().parent().removeClass("mb-md");
            $("#preUni1Bs").parent().parent().removeClass("mb-md");
        }
	});

    // Formatear campos de monto
    $("#cosUnitario, #preUnitario1, #preUnitario2, #preUnitario3, #cosUniBs, #preUni1Bs, #preUni2Bs, #preUni3Bs, #preUni1, #preUni2, #preUni3, #preMerLibre, #preMerLibreBs, #porML").priceFormat({
        prefix: "",
        centsSeparator: ",",
        thousandsSeparator: "."
    });

	// Obtención del precio del dólar
    $("#cosUnitario, #preUnitario1, #preUnitario2, #preUnitario3, #cosUniBs, #preUni1Bs, #preUni2Bs, #preUni3Bs, #preUni1, #preUni2, #preUni3,  #preMerLibre, #preMerLibreBs, #porML").on("keyup", function() {
        var precio = $(this);
        precio.attr("data-articulo-value", replace_number(precio.val()));

	    $.ajax({
	        type: "POST",
	        url: "../dolares/urlgetdolar",
            dataType: "json",

	        success: function(respuesta) {
                var monUnitario = precio.attr("data-articulo-value");
                var id, monto;

                // Montos en dólares
                var preFobPaiOrigen = $("#cosUnitario").attr("data-articulo-value");
                var preCifVenezuela = $("#preUnitario1").attr("data-articulo-value");
                var preMayor = $("#preUnitario2").attr("data-articulo-value");
                var preDetal = $("#preUnitario3").attr("data-articulo-value");
                var preMerLibre = $("#preMerLibre").attr("data-articulo-value");

                // Montos en bolívares
                var preFobPaiOriBs = $("#cosUniBs").attr("data-articulo-value");
                var preCifVenBs = $("#preUni1Bs").attr("data-articulo-value");
                var preMayBs = $("#preUni2Bs").attr("data-articulo-value");
                var preMerLibreBs = $("#preMerLibreBs").attr("data-articulo-value");

                // Valores en porcentajes
                var porCif = $("#preUni1").attr("data-articulo-value");
                var porMayor = $("#preUni2").attr("data-articulo-value");
                var porDetal = $("#preUni3").attr("data-articulo-value");
                var porML = $("#porML").attr("data-articulo-value");

                if(precio.attr("id") == "cosUnitario") {
                    id = "cosUniBs";
                    monto = monUnitario * respuesta;
                    preCifVenezuela = Number((preFobPaiOrigen * porCif / 100)) + Number(preFobPaiOrigen);
                    preMayor = Number((preCifVenezuela * porMayor / 100)) + Number(preCifVenezuela);
                    preDetal = Number((preMayor * porDetal / 100)) + Number(preMayor);
                    preMerLibre = Number((preMayor * porML / 100)) + Number(preMayor);
                } else if(precio.attr("id") == "preUnitario1") {
                    id = "preUni1Bs";
                    monto = monUnitario * respuesta;
                    preMayor = Number((preCifVenezuela * porMayor / 100)) + Number(preCifVenezuela);
                    preDetal = Number((preMayor * porDetal / 100)) + Number(preMayor);
                    porCif = ((monUnitario - preFobPaiOrigen) * 100) / preFobPaiOrigen;
                    preMerLibre = Number((preMayor * porML / 100)) + Number(preMayor);
                } else if(precio.attr("id") == "preUnitario2") {
                    id = "preUni2Bs";
                    monto = monUnitario * respuesta;
                    preDetal = Number((preMayor * porDetal / 100)) + Number(preMayor);
                    porMayor = ((monUnitario - preCifVenezuela) * 100) / preCifVenezuela;
                    preMerLibre = Number((preMayor * porML / 100)) + Number(preMayor);
                } else if(precio.attr("id") == "preUnitario3") {
                    id = "preUni3Bs";
                    monto = monUnitario * respuesta;
                    porDetal = ((monUnitario - preMayor) * 100) / preMayor;
                } else if(precio.attr("id") == "preMerLibre") {
                    id = "preMerLibreBs";
                    monto = monUnitario * respuesta;
                    porML = ((monUnitario - preMayor) * 100) / preMayor;
                } else if(precio.attr("id") == "cosUniBs") {
                    id = "cosUnitario";
                    monto = monUnitario / respuesta;
                    preFobPaiOrigen = monUnitario / respuesta;
                    preCifVenezuela = Number((preFobPaiOrigen * porCif / 100)) + Number(preFobPaiOrigen);
                    preMayor = Number((preCifVenezuela * porMayor / 100)) + Number(preCifVenezuela);
                    preDetal = Number((preMayor * porDetal / 100)) + Number(preMayor);
                    preMerLibre = Number((preMayor * porML / 100)) + Number(preMayor);
                } else if(precio.attr("id") == "preUni1Bs") {
                    id = "preUnitario1";
                    monto = monUnitario / respuesta; 
                    preCifVenezuela = monto;
                    preMayor = Number((preCifVenezuela * porMayor / 100)) + Number(preCifVenezuela);
                    preDetal = Number((preMayor * porDetal / 100)) + Number(preMayor);
                    porCif = ((monUnitario - preFobPaiOriBs) * 100) / preFobPaiOriBs;
                } else if(precio.attr("id") == "preUni2Bs") {
                    id = "preUnitario2";
                    monto = monUnitario / respuesta;
                    preMayor = monto;
                    preDetal = Number((preMayor * porDetal / 100)) + Number(preMayor);
                    porMayor = ((monUnitario - preCifVenBs) * 100) / preCifVenBs;
                } else if(precio.attr("id") == "preUni3Bs") {
                    id = "preUnitario3";
                    monto = monUnitario / respuesta;
                    preDetal = monto;
                    porDetal = ((monUnitario - preMayBs) * 100) / preMayBs;
                } else if(precio.attr("id") == "preMerLibreBs") {
                    id = "preMerLibre";
                    monto = monUnitario / respuesta;
                    preMerLibre = monto;
                    porML = ((monUnitario - preMayor) * 100) / preMayor;
                } else if(precio.attr("id") == "preUni1") {
                    preCifVenezuela = Number(preFobPaiOrigen * monUnitario / 100) + Number(preFobPaiOrigen);
                    preMayor = Number((preCifVenezuela * porMayor / 100)) + Number(preCifVenezuela);
                    preDetal = Number((preMayor * porDetal / 100)) + Number(preMayor);
                    preMerLibre = Number(preMayor * porML / 100) + Number(preMayor);
                } else if(precio.attr("id") == "preUni2") {
                    preMayor = Number((preCifVenezuela * porMayor / 100)) + Number(preCifVenezuela);
                    preDetal = Number((preMayor * porDetal / 100)) + Number(preMayor);
                    preMerLibre = Number(preMayor * porML / 100) + Number(preMayor);
                } else if(precio.attr("id") == "preUni3") 
                    preDetal = Number((preMayor * porDetal / 100)) + Number(preMayor);
                  else if(precio.attr("id") == "porML")                    
                    preMerLibre = Number(preMayor * monUnitario / 100) + Number(preMayor);

	        	if(precio.attr("id") != "preUni1" && precio.attr("id") != "preUni2" && precio.attr("id") != "preUni3" && precio.attr("id") != "porML") {
                    $("#" + id).val(number_format(monto));
                    $("#" + id).attr("data-articulo-value", monto);                    
                }

                $("#cosUnitario").val(number_format(preFobPaiOrigen)).attr("data-articulo-value", preFobPaiOrigen);

                $("#preUni1").val(number_format(porCif)).attr("data-articulo-value", porCif);
                $("#preUnitario1").val(number_format(preCifVenezuela)).attr("data-articulo-value", preCifVenezuela);
                $("#preUni1Bs").val(number_format(preCifVenezuela * respuesta)).attr("data-articulo-value", preCifVenezuela * respuesta);
                
                $("#preUni2").val(number_format(porMayor)).attr("data-articulo-value", porMayor);
                $("#preUnitario2").val(number_format(preMayor)).attr("data-articulo-value", preMayor);
                $("#preUni2Bs").val(number_format(preMayor * respuesta)).attr("data-articulo-value", preMayor * respuesta);

                $("#preUni3").val(number_format(porDetal)).attr("data-articulo-value", porDetal);
                $("#preUnitario3").val(number_format(preDetal)).attr("data-articulo-value", preDetal);
                $("#preUni3Bs").val(number_format(preDetal * respuesta)).attr("data-articulo-value", preDetal * respuesta);

                $("#porML").val(number_format(porML)).attr("data-articulo-value", porML);
                $("#preMerLibre").val(number_format(preMerLibre)).attr("data-articulo-value", preMerLibre);
                $("#preMerLibreBs").val(number_format(preMerLibre * respuesta)).attr("data-articulo-value", preMerLibre * respuesta);

	        }
	    });
	});

    // Resetear formulario
    $("#canArticulo").on("click", function () { 
        $("#forArticulo").each(function() {
            $("#forArticulo input, #forArticulo textarea").not("#fecCreacion, #fotReferencial").val("");
            $("#canFotReferencial").click();
            $("#divFotReferencial").hide();
        });

        $("#codArticulo").focus();

        return false;
    });

    // Thumbnail
    $("#ima-thumbnail").click(function(e) {
        e.preventDefault();
        var img = $(this).find("img").attr("src");
        var imgname = $(this).closest(".item-wrapper").attr("data-name");

        bootbox.dialog({
            message: "<center><img src='" + img + "' class='img-responsive'  width='100%'/></center>",
            buttons: {
                close: {
                    label: "Cerrar",
                    className: "btn-default"
                }
            }
        });

        $(".modal .bootbox-close-button").hide();
    });

    // Carga dinámica de formulario de artículo
    $("#codArticulo").blur(function() {
        if((window.location.pathname).split("/")[3] != "actualizacion") {
            var codArticulo = $(this).val();
            
            if(codArticulo != "") {
                var inputs = $("input[type=text], textarea");
                var fileinput = $("#fileinput");
                var btnFile = $("#btn-file");
                var registrar = $("#regArticulo");

                inputs.not("#codArticulo").attr("disabled", "disabled");
                fileinput.find(".uneditable-input").addClass("disabled");
                btnFile.addClass("disabled");
                registrar.addClass("disabled");
                $("#canFotReferencial").click();

                $.ajax({  
                    type: "POST",
                    url: "../articulos/urlgetarticulo",
                    dataType: "json",

                    data: { 
                        articulo: codArticulo,
                        tipo: "codigo"
                    },
                    
                    success: function(respuesta) { //alert(respuesta);
                        inputs.removeAttr("disabled");
                        fileinput.find(".uneditable-input").removeClass("disabled");
                        btnFile.removeClass("disabled");
                        registrar.removeClass("disabled");

                        var descripcion = $("#descripcion");

                        if(respuesta.articulo != undefined) {
                            descripcion.val(respuesta.articulo.art_descripcion);
                            $("#invInicial").val(respuesta.articulo.art_inv_inicial);
                            $("#stoMinimo").val(respuesta.articulo.art_sto_minimo);
                            $("#cosUnitario").val(number_format(respuesta.articulo.art_cos_unitario));
                            $("#cosUniBs").val(number_format(respuesta.articulo.art_cos_unitario * respuesta.dolar));
                            $("#preUnitario1").val(number_format(respuesta.articulo.art_pre_unitario1));
                            $("#preUni1Bs").val(number_format(respuesta.articulo.art_pre_unitario1 * respuesta.dolar));
                            $("#preUnitario2").val(number_format(respuesta.articulo.art_pre_unitario2));
                            $("#preUni2Bs").val(number_format(respuesta.articulo.art_pre_unitario2 * respuesta.dolar));
                            $("#preUnitario3").val(number_format(respuesta.articulo.art_pre_unitario3));
                            $("#preUni3Bs").val(number_format(respuesta.articulo.art_pre_unitario3 * respuesta.dolar));
                            $("#ubicacion").val(respuesta.articulo.art_ubicacion);

                            if(respuesta.articulo.art_fot_referencial != null) {
                                $(".fileinput-filename").html((respuesta.articulo.art_fot_referencial).replace("files/", ""));
                                fileinput.removeClass("fileinput-new").addClass("fileinput-exists");
                                $("#divFotReferencial").show().removeClass("hide");
                                $("#divFotReferencial img").attr("src", "/" + (window.location.pathname).split("/")[1] + "/" + respuesta.articulo.art_fot_referencial + "?" + Math.random());
                            }
                        } else {
                            inputs.not("#codArticulo, #fecCreacion").val("");
                            descripcion.focus();
                        }
                    },

                    error: function () {
                        inputs.removeAttr("disabled");
                        fileinput.find(".uneditable-input").removeClass("disabled");
                        btnFile.removeClass("disabled");
                        registrar.removeClass("disabled");

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    }
                });
            }
        }
    });

    window.ParsleyValidator.addValidator("monto", function(value) {
        var response = value == "0,00" ? false : true;
        return response;
    }).addMessage("en", "monto", "Ingrese monto.");
});