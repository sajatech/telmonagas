$(document).ready(function() {
    $("#telLocal, #telCelular").inputmask(); 
	// Registro de proveedores
	$("#regProveedor").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forProveedor");
        var loading = $("#loaProveedor");

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var str = $f.serialize();

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../proveedores/registrobd",
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();
                        
                        if(respuesta.type != "error") {
                            if((respuesta.privilegios.includes(44) && $(".modal").length > 1) || (!respuesta.privilegios.includes(44) && $(".modal").length > 0)) {
                                var newOption = new Option(respuesta.proveedor, respuesta.id, true, true);

                                $("#proveedor").append(newOption).trigger({ type: "select2:select", params: { data: { id: respuesta.id, text: respuesta.proveedor } } }).next().next().remove();
                                $("#modProveedor").modal("hide");                                
                            } else
                                $("#canProveedor").click();
                        }

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }
	});

    // Resetear formulario
    $("#canProveedor").on("click", function () {
        $("#forProveedor").each(function() {
            $("#forProveedor input, #forProveedor textarea").val("");
        });

        $("#codProveedor").focus();

        return false;
    });
});