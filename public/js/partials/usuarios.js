$(document).ready(function() {
	$("#tipUsuario").select2({width: "100%"});
    $("#telefono").inputmask(); 

	// Registro de usuarios
	$("#regUsuario").on("click", function() {

        var salSopCallback = function(text) {
        if(text !== false && text == 'LINPIESA')
        {
        // Se guarda la referencia al formulario
        var $f = $("#forUsuario");
        var loading = $("#loaUsuario");

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var str = $f.serialize();

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../usuarios/registrobd",
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });

                        if(respuesta.type != "error")
                            $("#canUsuario").click();
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }

        }
            else{

                notif({
                    'type': 'error',
                    'msg': 'Zona Prohibida :(',
                    position: "center"
                })

            }
        }
        
        notif_prompt({
            "message": "Introduzca Clave de Acceso",
            "textaccept": "Entrar",
            "textcancel": "No poseo clave",
            "fullscreen": true,
            "callback": salSopCallback
        })

        $(".notifit_prompt_input").removeAttr("type","text");
        $(".notifit_prompt_input").attr("type","password");
	});

    $("#codUsuario").inputmask({ 
        mask: "a-999999[99]", 
        greedy: false, 
        clearIncomplete: true,
        definitions: { 
            "a": { 
                validator: "[vVeEjJgG]" 
            } 
        } 
    }); 

    // Resetear formulario
    $("#canUsuario").on("click", function () {
        $("#forUsuario").each(function() {
            this.reset();
			$("#tipUsuario").val(null).trigger("change");
			$("#forUsuario").parsley().destroy();
        });

        $("#codUsuario").focus();

        return false;
    });

    $("#fecNacimiento").datepicker({
        todayHighlight: true,
        startView: 1
    }).change(function() {
        $(".datepicker").hide();
    });
});