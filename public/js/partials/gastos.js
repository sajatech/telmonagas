$(document).ready(function() {
    $("#categoria").select2({width: "100%"});

    $("#fecGasto").datepicker({
        todayHighlight: true,
        startView: 1,
        autoclose: true
    });

    // Formatear campo de costo
    $("#costo, #divisas").priceFormat({
        prefix: "",
        centsSeparator: ",",
        thousandsSeparator: "."
    });

    window.ParsleyValidator.addValidator("costo", function(value) {
        var response = value == "0,00" ? false : true;
        return response;
    });

    $("#cantidad, #costo").on("keyup", function() {
        $("#importe").val(number_format(Number($("#cantidad").val()) * Number(replace_number($("#costo").val()))));
    });

	// Registro de gastos
	$("#regGasto").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forGasto");
        var loading = $("#loaGasto");

        $f.parsley().destroy();

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var str = $f.serialize();

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../gastos/registrobd",
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });
                        
                        if(respuesta.type != "error") {
                            $("#canGasto").click();
                            $("#divisas").attr("readonly", "readonly").removeAttr("required");
                        }
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }
	});

    // Resetear formulario
    $("#canGasto").on("click", function () {
        $("#forGasto").each(function() {
            this.reset();
        });

        $("#categoria").val(null).trigger("change");

        return false;
    });

	$("#categoria").select2({width: "100%"}).on("select2:select", function(e) {
		var divisas = $("#divisas");
		e.params.data.id > 0 ? divisas.removeAttr("readonly").attr("required", "required") : divisas.attr("readonly", "readonly").removeAttr("required").val("");
	});
});