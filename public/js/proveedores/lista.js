$(document).ready(function() {
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/proveedores/lista");

    $.get("../proveedores/getproveedores", function(data) {
        var data = JSON.parse(data);

        $("#ajaxSpinner").hide();
        $("#panProveedores").removeClass("hide").show();

        // Tabla de los artículos
        oTable = $("#proveedores").dataTable({
            "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "aaData": data.aaData,
            "bServerSide": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "aaSorting": [],

            "aoColumns": [
                { "mData": "codigo" },
                { "mData": "nombre" },
                { "mData": "perContacto" },
                { "mData": "telCelular" },
                { "mData": "corElectronico" },
                { "mData": "estatus" }
            ],

            "oTableTools": {
                "sRowSelect": "single",

                "aButtons": [
                    { "sExtends": "editor_create", "sButtonText": "Agregar", 

                        "fnInit": function(button) {
                            $(button).addClass("hide");
                            
                            if(data.privilegios.includes(6))
                                $(button).removeClass("hide");
                        },

                    	"fnClick": function() {
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/proveedores/registro");
                    	}

                	},
                    
                    { "sExtends": "editor_edit", "sButtonText": "Actualizar/Ver",

                        "fnInit": function(button) {
                            $(button).addClass("hide disabled");
                            
                            if(data.privilegios.includes(8) || data.privilegios.includes(9))
                                $(button).removeClass("hide");
                        },

                    	"fnClick": function(button, conf) {
                    		var node = this.fnGetSelectedData(); 
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/proveedores/actualizacion", { "codigo": node[0].codigo });
                    	}

                    },

                    { "sExtends": "editor_remove", "sButtonText": "Activar/Desactivar",

                        "fnInit": function(button) {
                            $(button).addClass("hide disabled");
                            
                            if(data.privilegios.includes(10))
                                $(button).removeClass("hide");
                        },

                    	"fnClick": function() { 
                    		var node = this.fnGetSelectedData();  
                    		desProveedor(node);
                    	}
                    	
                    }
                ]
        	},

            "language": {
                "lengthMenu": "_MENU_ &nbsp;&nbsp;"
            },

            "iDisplayLength": 50,    
        });

        $("#proveedores_filter input").addClass("form-control").attr("placeholder", "Buscar Proveedor...");
        $("#proveedores_length select").addClass("form-control");

        // Se añaden los íconos
        $("#ToolTables_proveedores_0").prepend("<i class='ti ti-plus'/> ");
        $("#ToolTables_proveedores_1").prepend("<i class='ti ti-pencil'/> ");
        $("#ToolTables_proveedores_2").prepend("<i class='ti ti-exchange-vertical'/> ");

        // Se añaden los elementos de la tabla al panel
        $(".panel-ctrls").append($("#proveedores_filter").addClass("pull-right"));
        $(".panel-ctrls").append("<i class='separator'></i>");
        $('.panel-ctrls').append($('#proveedores_length').addClass("pull-left mt-14").find("label").addClass("panel-ctrls-center"));
        $(".panel-ctrls").append("<i class='separator'></i>");
        $(".panel-ctrls").append($(".DTTT.btn-group").addClass("pull-left mt-10"));

        $(".panel-footer").append($("#proveedores+.row"));
        $("#proveedores_paginate>ul.pagination").addClass("pull-right pagination-lg");

        // ******************************** //

    });

    // Desactivación/Activación de proveedor
    desProveedor = function(node) {
        $("#ToolTables_proveedores_2").addClass("disabled");

    	var url = (node[0].estatus.replace(/<[^>]*>?/g, "") == "ACTIVO") ? "../proveedores/desproveedor" : "../proveedores/activproveedor";

        $.ajax({  
            type: "POST",
            url: url,
            dataType: "json",

	        data: { 
	        	codigo: node[0].codigo
	        },

            success: function(respuesta) {                
                oTable.api().ajax.url("../proveedores/getproveedores").load();
			    $("#ToolTables_proveedores_1").addClass("disabled");
			    $("#ToolTables_proveedores_2").addClass("disabled");

				notif({
				    msg: respuesta.text,
				    type: respuesta.type,
				    position: "center",
                    multiline: true
				});
            },

            error: function () {
                $("#ToolTables_proveedores_2").removeClass("disabled");

				notif({
				    msg: "Sucedió un error, contacte con el administrador del sistema.",
				    type: "error",
				    position: "center",
                    multiline: true
				});
            }
        });                
		
		return false; 
	}
});