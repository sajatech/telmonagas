$(document).ready(function() {
    $.trim($(".fileinput-filename").text()) != "" ? $("#divFotReferencial").show().removeClass("hide") : false;
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/articulos/lista");

	// Actualización de artículos
	$("#actArticulo").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forArticulo");
        var loading = $("#loaArticulo");

        $f.parsley().destroy();

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var formData = new FormData($f[0]);

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../articulos/actualizacionbd?id=" + $(this).attr("data-articulo-id") + "&getCosUnitario=" + $("#cosUnitario").attr("data-articulo-value") + "&getPreUnitario1=" + $("#preUnitario1").attr("data-articulo-value") + "&getPreUnitario2=" + $("#preUnitario2").attr("data-articulo-value") + "&getPreUnitario3=" + $("#preUnitario3").attr("data-articulo-value") + "&getPreMerLibre=" + $("#preMerLibre").attr("data-articulo-value"),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });                        
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }
	});

    // Desactivación de artículos
    $("#desArticulo").on("click", function() {
        var anuSalCallback = function(text) {
            if(text !== false && text == 'LINPIESA') {
                // Se guarda la referencia al formulario
                var $f = $("#forArticulo");
                var loading = $("#loaArticulo");

                loading.show().removeClass("hide");

                // Bloqueo de peticiones repetidas
                if($f.data("locked") == undefined || !$f.data("locked")) {
                    $.ajax({  
                        type: "POST",
                        url: "../articulos/desactivacionbd",
                        dataType: "json",

                        data: {
                            id: $("#desArticulo").attr("data-articulo-id") 
                        },

                        beforeSend: function() { 
                            $f.data("locked", true);
                        },

                        success: function(respuesta) {
                            loading.hide();

                            if(respuesta.type != "error")
                                $.redirect("/" + (window.location.pathname).split("/")[1] + "/articulos/inactivos");

                            notif({
                                msg: respuesta.text,
                                type: respuesta.type,
                                position: "center",
                                multiline: true
                            });
                        },

                        error: function () {
                            loading.hide();

                            notif({
                                msg: "Sucedió un error, contacte con el administrador del sistema.",
                                type: "error",
                                position: "center",
                                multiline: true
                            });
                        },

                        complete: function() { 
                            $f.data("locked", false);
                        }               
                    });

                    return false;
                }
            } else {
                notif({
                    "type": "error",
                    "msg": "Proceso Cancelado :(",
                    "position": "center"
                });
            }
        }
        
        notif_prompt({
            "message": "¿Desa desactivar el artículo?",
            "textaccept": "Sí, ¡deseo desactivarlo!",
            "textcancel": "No, me equivoqué",
            "fullscreen": true,
            "callback": anuSalCallback
        })

        $(".notifit_prompt_input").removeAttr("type", "text");
        $(".notifit_prompt_input").attr("type", "password");
    });

    // Activación de artículos
    $("#activArticulo").on("click", function() {
        var anuSalCallback = function(text) {
            if(text !== false && text == 'LINPIESA') {
                // Se guarda la referencia al formulario
                var $f = $("#forArticulo");
                var loading = $("#loaArticulo");

                loading.show().removeClass("hide");

                // Bloqueo de peticiones repetidas
                if($f.data("locked") == undefined || !$f.data("locked")) {
                    $.ajax({  
                        type: "POST",
                        url: "../articulos/activacionbd",
                        dataType: "json",

                        data: {
                            id: $("#activArticulo").attr("data-articulo-id") 
                        },

                        beforeSend: function() { 
                            $f.data("locked", true);
                        },

                        success: function(respuesta) {
                            loading.hide();

                            if(respuesta.type != "error")
                                $.redirect("/" + (window.location.pathname).split("/")[1] + "/articulos/lista");

                            notif({
                                msg: respuesta.text,
                                type: respuesta.type,
                                position: "center",
                                multiline: true
                            });
                        },

                        error: function () {
                            loading.hide();

                            notif({
                                msg: "Sucedió un error, contacte con el administrador del sistema.",
                                type: "error",
                                position: "center",
                                multiline: true
                            });
                        },

                        complete: function() { 
                            $f.data("locked", false);
                        }               
                    });

                    return false;
                }
            } else {
                notif({
                    "type": "error",
                    "msg": "Proceso Cancelado :(",
                    "position": "center"
                });
            }
        }
        
        notif_prompt({
            "message": "¿Desa activar el artículo?",
            "textaccept": "Sí, ¡deseo activarlo!",
            "textcancel": "No, me equivoqué",
            "fullscreen": true,
            "callback": anuSalCallback
        })

        $(".notifit_prompt_input").removeAttr("type", "text");
        $(".notifit_prompt_input").attr("type", "password");
    });

    // Preparación de modal para agregar nuevo(s) serial(es)
    $("#agrSeriales").on("click", function() {
        agrSeriales($("#agrSeriales").attr("data-articulo-id"));
    });
});