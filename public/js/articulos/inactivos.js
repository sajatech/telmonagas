$(document).ready(function() {
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/articulos/lista");
    
    $.get("../articulos/getarticulos?estatus=INACTIVO", function(data) {
        var data = JSON.parse(data);

        $("#ajaxSpinner").hide();
        $("#panArticulos").removeClass("hide").show();

        // Tabla de los articulos
        oTable = $("#articulos").DataTable({
            "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "aaData": data.aaData,
            "bServerSide": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "aaSorting": [],

            "aoColumns": [
                { "mData": "codigo" },
                { "mData": "descripcion" },
                { "mData": "invInicial" },
                { "mData": "stoMinimo" },
                { "mData": "artPreMayor" },
                { "mData": "artPreDetal" },
                { "mData": "estatus" }
            ],

            "oTableTools": {
                "sRowSelect": "single",

                "aButtons": [
                    { "sExtends": "editor_create", "sButtonText": "Agregar", 

                        "fnInit": function(button) {
                            $(button).addClass("hide");
                            
                            if(data.privilegios.includes(1))
                                $(button).removeClass("hide");
                        },

                    	"fnClick": function() {
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/articulos/registro", "", "GET");
                    	}

                	},
                    
                    { "sExtends": "editor_edit", "sButtonText": "Ver",

                        "fnInit": function(button) {
                            $(button).addClass("hide disabled");
                            
                            if(data.privilegios.includes(4))
                                $(button).removeClass("hide");
                        },

                        "fnClick": function(button, conf) {
                            var node = this.fnGetSelectedData();
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/articulos/actualizacion", { "codigo": node[0].codigo, "anulado": true });
                        }

                    }
                ]
        	},

            "language": {
                "lengthMenu": "_MENU_ &nbsp;&nbsp;"
            },

            "iDisplayLength": 50,    
        });

        $("#articulos_filter input").addClass("form-control").attr("placeholder", "Buscar Artículo...");
        $("#articulos_length select").addClass("form-control");

        // Se añaden los íconos
        $("#ToolTables_articulos_0").prepend("<i class='ti ti-plus'/> ");
        $("#ToolTables_articulos_1").prepend("<i class='ti ti-search'/> ");

        // Se añaden los elementos de la tabla al panel
        $(".panel-ctrls").append($("#articulos_filter").addClass("pull-right"));
        $(".panel-ctrls").append("<i class='separator hidden-xs'></i>");
        $('.panel-ctrls').append($("#articulos_length").addClass("pull-left mt-14").find("label").addClass("panel-ctrls-center hidden-xs"));
        $(".panel-ctrls").append("<i class='separator hidden-xs'></i>");
        $(".panel-ctrls").append($(".DTTT.btn-group").addClass("pull-left mt-10"));

        $(".panel-footer").append($("#articulos+.row"));
        $("#articulos_paginate>ul.pagination").addClass("pull-right pagination-lg");

        new $.fn.dataTable.FixedHeader(oTable);

        // ******************************** //
    });
});