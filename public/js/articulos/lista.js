$(document).ready(function() {
    $.wijets.make();
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/articulos/lista");

    // Checkboxes personalizados
    $(".icheck input").iCheck({
        radioClass: "iradio_flat-blue"
    });

    $.get("../articulos/getarticulos?estatus=ACTIVO", function(data) {
        var data = JSON.parse(data);

        $("#ajaxSpinner").hide();
        $("#panArticulos").removeClass("hide").show();

        // Tabla de los artículos
        oTable = $("#articulos").DataTable({
            "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "aaData": data.aaData,
            "bServerSide": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "aaSorting": [],

            "aoColumns": [
                { "mData": "codigo" },
                { "mData": "descripcion" },
                { "mData": "invInicial" },
                { "mData": "stoMinimo" },
                { "mData": "artPreMayor" },
                { "mData": "artPreDetal" },
                { "mData": "estatus" }
            ],

            "oTableTools": {
                "sRowSelect": "single",

                "aButtons": [
                    { "sExtends": "editor_create", "sButtonText": "Agregar", 

                        "fnInit": function(button) {
                            $(button).addClass("hide");
                            
                            if(data.privilegios.includes(1))
                                $(button).removeClass("hide");
                        },

                        "fnClick": function() {
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/articulos/registro");
                        }

                    },
                    
                    { "sExtends": "editor_edit", "sButtonText": "Actualizar/Ver",

                        "fnInit": function(button) { 
                            $(button).addClass("hide disabled");

                            if(data.privilegios.includes(3) || data.privilegios.includes(4))
                                $(button).removeClass("hide");
                        },

                        "fnClick": function(button) {
                            var node = this.fnGetSelectedData(); 
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/articulos/actualizacion", { "codigo": node[0].codigo });
                        }

                    },

                    { "sExtends": "editor_remove", "sButtonText": "Activar/Desactivar",

                        "fnInit": function(button) { 
                            $(button).addClass("hide disabled");

                            if(data.privilegios.includes(5)) {
                                $(button).removeClass("hide");
                            }
                        },

                        "fnClick": function() { 
                            var node = this.fnGetSelectedData();  
                            desArticulo(node);
                        }
                        
                    },

                    { "sExtends": "editor_edit", "sButtonText": "Ver Descripción",

                        "fnClick": function() {
                            var node = this.fnGetSelectedData();

                            bootbox.dialog({
                                message: node[0].descripcion,
                                buttons: {
                                    close: {
                                        label: "Cerrar",
                                        className: "btn-default"
                                    }
                                }
                            });
                        }
                        
                    }
                ]
            },

            "language": {
                "lengthMenu": "_MENU_ &nbsp;&nbsp;"
            },

            "iDisplayLength": 50,    
        });

        oTable.on("draw", function() {
            var body = $(oTable.table().body());

            body.unhighlight();
            body.highlight( oTable.search() );
        });

        $("#articulos_filter input").on("keyup", function() {
            $("#codigo").is(":checked") ? oTable.columns(0).search("^" + $(this).val(), true, false).draw() : oTable.columns(1).search($(this).val()).draw();
        });

        $("#codigo, #descripcion").on("ifChecked", function() {
            $(this).attr("id") == "codigo" ? oTable.columns(1).search("").draw() : oTable.columns(0).search("").draw();
            $("#articulos_filter input").keyup();
        });

        $("#articulos_filter input").addClass("form-control").attr("placeholder", "Buscar Artículo...");
        $("#articulos_length select").addClass("form-control");

        // Se añaden los íconos
        $("#ToolTables_articulos_0").prepend("<i class='ti ti-plus'/> ");
        $("#ToolTables_articulos_1").prepend("<i class='ti ti-pencil'/> ");
        $("#ToolTables_articulos_2").prepend("<i class='ti ti-exchange-vertical'/> ");
        $("#ToolTables_articulos_3").prepend("<i class='ti ti-search'/> ");

        // Se añaden los elementos de la tabla al panel
        $("#filter").append($("#articulos_filter"));
        $("#panCtrls").append("<i class='separator hidden-xs'></i>");
        $('#panCtrls').append($('#articulos_length').addClass("pull-left mt-14").find("label").addClass("panel-ctrls-center hidden-xs"));
        $("#panCtrls").append($(".DTTT.btn-group").addClass("pull-left mt-10"));

        $(".panel-footer").append($("#articulos+.row"));
        $("#articulos_paginate>ul.pagination").addClass("pull-right pagination-lg");
        $("#articulos_wrapper").find("div").remove();

        new $.fn.dataTable.FixedHeader(oTable);

        // ******************************** //

        // Funcionalidad para redirigir a vista de actualización de artículos
        var codigo;            

        // Al hacer click y presionar luego la tecla enter
        $("#articulos tbody").on("click", "tr", function() {
            codigo = oTable.row(this).data()["codigo"];            
            var body = $("body");

            body.off("keyup");
            
            body.keyup(function(e) {
                if(e.which == 13)
                    $.redirect("/" + (window.location.pathname).split("/")[1] + "/articulos/actualizacion", { "codigo": codigo });
            });
        });

        // Al hacer doble click
        $("#articulos tbody").on("dblclick", "tr", function() {
            codigo = oTable.row(this).data()["codigo"];            
            $.redirect("/" + (window.location.pathname).split("/")[1] + "/articulos/actualizacion", { "codigo": codigo });
        });

        // ******************************** //
    });

    // Desactivación/Activación de artículo
    desArticulo = function(node) {
        $("#ToolTables_articulos_2").addClass("disabled");

    	var url = (node[0].estatus.replace(/<[^>]*>?/g, "") == "ACTIVO") ? "../articulos/desarticulo" : "../articulos/activarticulo";

        $.ajax({  
            type: "POST",
            url: url,
            dataType: "json",

	        data: { 
	        	codigo: node[0].codigo
	        },

            success: function(respuesta) {
                oTable.ajax.url("../articulos/getarticulos").load();
			    $("#ToolTables_articulos_1").addClass("disabled");
                $("#ToolTables_articulos_2").addClass("disabled");
                $("#ToolTables_articulos_3").addClass("disabled");

				notif({
				    msg: respuesta.text,
				    type: respuesta.type,
				    position: "center",
                    multiline: true
				});
            },

            error: function () {
                $("#ToolTables_articulos_2").removeClass("disabled");

				notif({
				    msg: "Sucedió un error, contacte con el administrador del sistema.",
				    type: "error",
				    position: "center",
                    multiline: true
				});
            }
        });                
		
		return false; 
	}
});