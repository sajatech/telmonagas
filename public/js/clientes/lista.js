$(document).ready(function() {
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/clientes/lista");

    $.get("../clientes/getclientes", function(data) {
        var data = JSON.parse(data);

        $("#ajaxSpinnerClientes").hide();
        $("#panClientes").removeClass("hide").show();

        // Tabla de los clientes
        oTable = $("#clientes").DataTable({
            "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "aaData": data.aaData,
            "bServerSide": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "aaSorting": [],

            "aoColumns": [
                { "mData": "codigo" },
                { "mData": "rif" },
                { "mData": "nombre" },
                { "mData": "direccion" },
                { "mData": "telCelular" },
                { "mData": "corElectronico" },
                { "mData": "estatus" }
            ],

            "oTableTools": {
                "sRowSelect": "single",

                "aButtons": [
                    { "sExtends": "editor_create", "sButtonText": "Agregar",

                        "fnInit": function(button) {
                            $(button).addClass("hide");
                            
                            if(data.privilegios.includes(11))
                                $(button).removeClass("hide");
                        },

                        "fnClick": function() {
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/clientes/registro");
                        }

                	},
                    
                    { "sExtends": "editor_edit", "sButtonText": "Actualizar/Ver",

                        "fnInit": function(button) {
                            $(button).addClass("hide disabled");
                            
                            if(data.privilegios.includes(13) || ata.privilegios.includes(14))
                                $(button).removeClass("hide");
                        },

                    	"fnClick": function(button, conf) {
                    		var node = this.fnGetSelectedData();
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/clientes/actualizacion", { "codigo": node[0].rif });
                    	}

                    },

                    { "sExtends": "editor_remove", "sButtonText": "Activar/Desactivar",

                        "fnInit": function(button) {
                            $(button).addClass("hide disabled");
                            
                            if(data.privilegios.includes(15))
                                $(button).removeClass("hide");
                        },

                    	"fnClick": function() { 
                    		var node = this.fnGetSelectedData();  
                    		desCliente(node);
                    	}
                    	
                    },

                    { "sExtends": "editor_remove", "sButtonText": "Ver Compras",

                        "fnInit": function(button) {
                            $(button).addClass("hide disabled");
                            
                            //if(data.privilegios.includes(15))
                                $(button).removeClass("hide");
                        },

                        "fnClick": function() { 
                            var node = this.fnGetSelectedData();

                            $("#ToolTables_clientes_3").addClass("disabled");
                            verCompras(node);
                        }
                        
                    }                    
                ]            
        	},

            "language": {
                "lengthMenu": "_MENU_ &nbsp;&nbsp;"
            },

            "iDisplayLength": 50,    
        });

        oTable.on("draw", function() {
            var body = $(oTable.table().body());

            body.unhighlight();
            body.highlight( oTable.search() );
        });

        $("#clientes_filter input").addClass("form-control").attr("placeholder", "Buscar Cliente...");
        $("#clientes_length select").addClass("form-control");

        // Se añaden los íconos
        $("#ToolTables_clientes_0").prepend("<i class='ti ti-plus'/> ");
        $("#ToolTables_clientes_1").prepend("<i class='ti ti-pencil'/> ");
        $("#ToolTables_clientes_2").prepend("<i class='ti ti-exchange-vertical'/> ");
        $("#ToolTables_clientes_3").prepend("<i class='ti ti-search'/> ");

        // Se añaden los elementos de la tabla al panel
        $(".panel-ctrls").append($("#clientes_filter").addClass("pull-right"));
        $(".panel-ctrls").append("<i class='separator hidden-xs'></i>");
        $('.panel-ctrls').append($('#clientes_length').addClass("pull-left mt-14").find("label").addClass("panel-ctrls-center hidden-xs"));
        $(".panel-ctrls").append("<i class='separator hidden-xs'></i>");
        $(".panel-ctrls").append($(".DTTT.btn-group").addClass("pull-left mt-10"));

        $(".panel-footer").append($("#clientes+.row"));
        $("#clientes_paginate>ul.pagination").addClass("pull-right pagination-lg");
        $("#clientes_wrapper").find("div").remove();

        new $.fn.dataTable.FixedHeader(oTable);

        // ******************************** //
    });

    // Desactivación/Activación de cliente
    desCliente = function(node) {
        $("#ToolTables_clientes_2").addClass("disabled");

    	var url = (node[0].estatus.replace(/<[^>]*>?/g, "") == "ACTIVO") ? "../clientes/descliente" : "../clientes/activcliente";

        $.ajax({  
            type: "POST",
            url: url,
            dataType: "json",

	        data: { 
	        	codigo: node[0].rif
	        },

            success: function(respuesta) {                
                oTable.api().ajax.url("../clientes/getclientes").load();
			    $("#ToolTables_clientes_1").addClass("disabled");
			    $("#ToolTables_clientes_2").addClass("disabled");

				notif({
				    msg: respuesta.text,
				    type: respuesta.type,
				    position: "center",
                    multiline: true
				});
            },

            error: function () {
                $("#ToolTables_clientes_2").removeClass("disabled");

				notif({
				    msg: "Sucedió un error, contacte con el administrador del sistema.",
				    type: "error",
				    position: "center",
                    multiline: true
				});
            }
        });                
		
		return false; 
	}

    // Ver compras registradas para el cliente
    var y = 1;
    verCompras = function(node) {
        $(".popover").remove();

        $.post("/" + (window.location.pathname).split("/")[1] + "/clientes/getcompras", { cliente: node[0].codigo }, function(data) {
            var compras = $("#compras");
            var modCompras = $("#modCompras");

            if(y == 1) {
                // Tabla de las compras
                tabCompras = compras.DataTable({
                    "sDom": "<'row'<'col-sm-3'l><'col-sm-6'><'col-sm-3'f>r>t<'row'<'col-sm-5'i><'col-sm-7'p>>",
                    "paging": true, 
                    "info": false,         
                    "lengthChange":false,
                    "iDisplayLength": 5,
                    "aaSorting": [0, "desc"],
                    "searching": false,

                    "aoColumns": [
                        { "mData": "codigo" },
                        { "mData": "fecha" },
                        { "mData": "prePagado" },
                        { "mData": "articulos" }
                    ],

                    drawCallback: function() {
                        $(".popover").remove();
                        $("[data-toggle='popover']").popover({ html: true });

                        $(".detalle").on("click", function() {
                           var popover = $("[data-toggle='popover']");

                           $(this).attr("data-popover", "true");
                           popover.not("[data-popover='true']").popover("hide");
                           popover.removeAttr("data-popover");
                        });
                    }
                });
            }

            Object.keys(data.salidas).length > 0 ? tabCompras.clear().rows.add(data.salidas).draw() : tabCompras.clear().rows.add([]).draw();
            compras.next().find("div").eq(0).html("<h4 class='mt-sm text-danger mb-n'>Total: Bs. " + data.totBolivares + " / $ " + data.totDolares + "</h4>");

            modCompras.on("shown.bs.modal", function () {
                $("#ToolTables_clientes_3").removeClass("disabled");
                $("#ajaxSpinner").hide();
                $("[data-toggle='popover']").popover({ html: true });
                compras.removeClass("hide");

                $(".detalle").on("click", function() {
                   var popover = $("[data-toggle='popover']");

                   $(this).attr("data-popover", "true");
                   popover.not("[data-popover='true']").popover("hide");
                   popover.removeAttr("data-popover");
                });
            });

            $("#modCompras .modal-title").html("Cliente: " + data.cliente + "<br/>" + data.totCompras + (data.totCompras == 1 ? " compra" : " compras") + " registradas");
            modCompras.modal("show");
            y++;
        }, "json");
    }
});