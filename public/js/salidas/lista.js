$(document).ready(function() {
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/salidas/lista");

    $.get("../salidas/getsalidas?estatus=ACTIVO", function(data) {
        var data = JSON.parse(data);

        $("#ajaxSpinner").hide();
        $("#panSalidas").removeClass("hide").show();

        // Tabla de las salidas de mercancía
        oTable = $("#salidas").DataTable({
            "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "aaData": data.aaData,
            "bServerSide": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "aaSorting": [],

            "aoColumns": [
                { "mData": "codSalida" },
                { "mData": "cliente" },
                { "mData": "fecha" },
                { "mData": "usuario" }
            ],

            "oTableTools": {
                "sRowSelect": "single",

                "aButtons": [
                    { "sExtends": "editor_create", "sButtonText": "Agregar", 

                        "fnInit": function(button) {
                            $(button).addClass("hide");
                            
                            if(data.privilegios.includes(21))
                                $(button).removeClass("hide");
                        },

                    	"fnClick": function() {
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/salidas/venta", "", "GET");
                    	}

                	},
                    
                    { "sExtends": "editor_edit", "sButtonText": "Actualizar/Ver",

                        "fnInit": function(button) {
                            $(button).addClass("hide disabled");
                            
                            if(data.privilegios.includes(23) || data.privilegios.includes(24))
                                $(button).removeClass("hide");
                        },

                        "fnClick": function(button, conf) {
                            var node = this.fnGetSelectedData();
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/salidas/venta?token=true", { "codigo": node[0].codSalida });
                        }

                    },

                    { "sExtends": "editor_edit", "sButtonText": "Imprimir Soporte",

                        "fnInit": function(button) {
                            $(button).addClass("hide disabled");
                            
                            if(data.privilegios.includes(25))
                                $(button).removeClass("hide");
                        },

                        "fnClick": function(button, conf) {
                            var node = this.fnGetSelectedData();
                            window.open("/" + (window.location.pathname).split("/")[1] + "/salidas/salida_mercancia/" + Number(node[0].codSalida), "Salida de Mercancía", "width=240, height=600");
                        }

                    }
                ]
        	},

            "language": {
                "lengthMenu": "_MENU_ &nbsp;&nbsp;"
            },

            "iDisplayLength": 50,    
        });

        $("#salidas_filter input").addClass("form-control").attr("placeholder", "Buscar Salida...");
        $("#salidas_length select").addClass("form-control");

        // Se añaden los íconos
        $("#ToolTables_salidas_0").prepend("<i class='ti ti-plus'/> ");
        $("#ToolTables_salidas_1").prepend("<i class='ti ti-pencil'/> ");
        $("#ToolTables_salidas_2").prepend("<i class='ti ti-printer'/> ");

        // Se añaden los elementos de la tabla al panel
        $(".panel-ctrls").append($("#salidas_filter").addClass("pull-right"));
        $(".panel-ctrls").append("<i class='separator hidden-xs'></i>");
        $('.panel-ctrls').append($('#salidas_length').addClass("pull-left mt-14").find("label").addClass("panel-ctrls-center hidden-xs"));
        $(".panel-ctrls").append("<i class='separator hidden-xs'></i>");
        $(".panel-ctrls").append($(".DTTT.btn-group").addClass("pull-left mt-10"));

        $(".panel-footer").append($("#salidas+.row"));
        $("#salidas_paginate>ul.pagination").addClass("pull-right pagination-lg");

        new $.fn.dataTable.FixedHeader(oTable);

        // ******************************** //
    });
});