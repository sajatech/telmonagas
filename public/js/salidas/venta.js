$(document).ready(function() {
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/salidas/lista");
	
	// Inicialización de variables
	var garantia = $("#garantia");
	var efectivo = $("#efectivo");
    var totalbs = 0;
    var total$ = 0;
    var salida_id = $("#actualizar").attr("data-salida-id");

    $("#observaciones").html("FACTURA Nº:                  , EMPRESA:");

	function urlGetDolar() {
	   return $.get("../dolares/urlgetdolar");
	}

    // Verificación de pagos
    $("#verificado").on("switchChange.bootstrapSwitch", function(event, state) {
		var id = $(this).attr("data-pago-id");

		if(id != undefined) {
			var pago = $("[data-pago-id=" + id + "]");

			pago.bootstrapSwitch("disabled", true);

			$.post("../salidas/verpago", { id: id, state: state }, function(respuesta) {
				pago.bootstrapSwitch("disabled", false);

		        notif({
		            msg: respuesta.text,
		            type: respuesta.type,
		            position: "center",
		            multiline: true
		        });                        
			}, "json");
		}
    });

	$("#cliente, #articulo, #cuenta, #moneda, #codigo").select2({width: "100%"});
	$("#parsley-id-multiple-forPago").remove();

	// Checkboxes personalizados y gestion de salidas por garantia
    $(".icheck input").iCheck({
        checkboxClass: "icheckbox_flat-blue",
        radioClass: "iradio_flat-blue"
    });

    garantia.on("ifChecked", function() {
    	$("#observaciones").text(":::SALIDA POR GARANTÍA:::");
    	$("#sumCosTotal").text("$ 0,00 / Bs. 0,00");
    	$(".selPreVenta").prop("disabled", "disabled");
    	$("#secPagos").hide();
        $("#tabLisPagos tbody").empty();
        $("#lisPagos").hide();
        arrPago = [];
    	totalbs = 0;
    	total$ = 0;
    });

    garantia.on("ifUnchecked", function() { 
    	$("#observaciones").html("FACTURA Nº:                  , EMPRESA:");
    	$(".selPreVenta").prop("disabled", false);
    	$("#secPagos").show();
    });

	// Habilitar botón "ver detalle" al seleccionar algún proveedor
	$("#cliente").select2({width: "100%"}).on("select2:select", function(e) {
		var verCliente = $("#verCliente");
		var id = e.params.data.id;

		if(id != "") {
		    $.ajax({
		        type: "POST",
		        url: "../clientes/getcliente",
		        dataType: "json",

		        data: {
		        	cliente: $("option:selected", "#cliente").val(),
		        },

		        success: function(respuesta) {
					verCliente.removeClass("disabled").attr("onclick", "verCliente('" + JSON.stringify(respuesta) + "')");
		        }
		    });
		} else
			verCliente.addClass("disabled");
	});

	$("#codigo").select2({width: "100%"}).on("select2:select", function(e) {
		var id = e.params.data.id;
		$("#articulo").val(id).trigger("change").trigger("select2:select");
	});

	// Agregación de nueva salida
	$("#articulo").select2({width: "100%"}).on("select2:select", function(e) {
		var id;
		var preAlt = $("#preloader-alt");

		preAlt.removeClass("hide").show();

		if(e.params != undefined) {
			id = e.params.data.id;
			$("#codigo").val(id).trigger("change");
		} else
			id = $("#articulo").val();

		if($.inArray(id, arrArticulo) < 0) {
			arrArticulo.push(id);

		    $.ajax({
		        type: "POST",
		        url: "../articulos/urlgetarticulo",
		        dataType: "json",

		        data: {
		        	articulo: id,
		        	tipo: "id"
		        },

		        success: function(respuesta) {
					preAlt.fadeOut("slow");

		        	if(respuesta.false == undefined) { 
						$("#lisSalidas").show().removeClass("hide");

			    		var trs = $("#tabLisSalidas tbody tr").length;

			    		if(trs == 0) {
							$("#sumCosTotal").text("$ 0,00 / Bs. 0,00");

			    			var aClass = respuesta.seriales == "false" ? "disabled" : false;

			    			var row  = "<tr id='salida" + respuesta.articulo.id + "'>";
			    				row += "<td class='codigo'>" + respuesta.articulo.art_codigo + "</td>";
			    				row += "<td><input type='number' name='cantidad[]' data-parsley-min='1' data-parsley-max='" + respuesta.existencia + "' class='cantidad form-control' required='required' placeholder='0' data-parsley-type='number'/></td>";
			    				row += "<td class='preVenta'><select name='preVenta[]' id='preVenta1' class='selPreVenta'><option value='PRECIO AL DETAL'>PRECIO AL DETAL</option><option value='PRECIO AL MAYOR'>PRECIO AL MAYOR</option></select></td>";
			    				row += "<td class='cosUnitario'>" + "$ <span class='spaCosUnitario'>" + number_format(respuesta.articulo.art_pre_unitario3) + "</span> / Bs. <span class='spaCosUnitarioBs'>" + number_format(respuesta.articulo.art_pre_unitario3 * respuesta.dolar)  + "</span></td>";
			    				row += "<td class='cosTotal'>$ <span class='spaCosTotal'>0,00</span> / Bs. <span class='spaCosTotalBs'>0,00</span></td>";
			    				row += "<td class='text-center'><a class='btn btn-md btn-primary btn-label eliSalida mr-xs tooltips' onclick='eliSalida(" + respuesta.articulo.id + ")' data-trigger='hover' data-original-title='Eliminar'><i class='ti ti-trash btn-label-custom'></i></a><a class='btn btn-md btn-primary btn-label mr-xs verArticulo tooltips' data-trigger='hover' data-original-title='Ver Descripción'><i class='ti ti-search btn-label-custom'></i></a><a class='btn btn-md btn-primary btn-label agrSeriales tooltips " + aClass + "' onclick='agrSeriales(" + respuesta.articulo.id + ")' data-trigger='hover' data-original-title='Agregar/Ver Seriales'><i class='ti ti-list btn-label-custom'></i></a></td>";
			    				row += "</tr>";
			    			
			    			$(row).appendTo("#tabLisSalidas tbody");
			    			$(".verArticulo").attr("onclick", "verArticulo('" + JSON.stringify(respuesta.articulo) + "', '" + respuesta.dolar + "', '" + JSON.stringify(respuesta.calculos) + "')");
		    				
		    				$("#preVenta1").select2({width: "100%"}).on("select2:select", function(e) {
								var id = e.params.data.id;
								var cosUnitario = id == "PRECIO AL MAYOR" ? respuesta.articulo.art_pre_unitario2 : respuesta.articulo.art_pre_unitario3;
		    					var montos = $(this).parent().next();
		    					
		    					montos.find(".spaCosUnitario").text(number_format(cosUnitario));
		    					montos.find(".spaCosUnitarioBs").text(number_format(cosUnitario * respuesta.dolar));
		    					$(".cantidad").keyup();
		    				});

			    			$(".cantidad").focus();
			    		} else {
			    			var tabLisSalidas = $("#tabLisSalidas tr:last");
							var clone = tabLisSalidas.clone();
							var cantidad = $(".cantidad", clone);
							var index = Number($(".selPreVenta", clone).attr("id").replace("preVenta", "")) + 1;
							var agrSeriales = $(".agrSeriales", clone);

							clone.attr("id", "salida" + respuesta.articulo.id);
							cantidad.val("").attr("data-parsley-max", respuesta.existencia);
							agrSeriales.removeClass("disabled").attr("onclick", "agrSeriales(" + respuesta.articulo.id + ")");
							respuesta.seriales == "false" ? agrSeriales.addClass("disabled") : false;
							$(".codigo", clone).text(respuesta.articulo.art_codigo);
							$(".selPreVenta", clone).attr("id", "preVenta" + index);
							$(".cosUnitario", clone).html("$ <span class='spaCosUnitario'>" + number_format(respuesta.articulo.art_pre_unitario3) + "</span> / Bs. <span class='spaCosUnitarioBs'>" + number_format(respuesta.articulo.art_pre_unitario3 * respuesta.dolar) + "</span>");
							$(".cosTotal", clone).html("$ <span class='spaCosTotal'>0,00</span> / Bs. <span class='spaCosTotalBs'>0,00</span>");
							$(".eliSalida", clone).attr("onclick", "eliSalida(" + respuesta.articulo.id + ")");
							$(".verArticulo", clone).attr("onclick", "verArticulo('" + JSON.stringify(respuesta.articulo) + "', '" + respuesta.dolar + "', '" + JSON.stringify(respuesta.calculos) + "')");
							$(".help-block", clone).removeClass("filled").empty();
							$(clone).insertAfter("#tabLisSalidas tr:last");
							$(".preVenta", clone).html("<select name='preVenta[]' id='preVenta" + index + "' class='selPreVenta'><option value='PRECIO AL DETAL'>PRECIO AL DETAL</option><option value='PRECIO AL MAYOR'>PRECIO AL MAYOR</option></select>");
							
							$("#preVenta" + index, clone).select2({width: "100%"}).on("select2:select", function(e) {
								var id = e.params.data.id;
								var cosUnitario = id == "PRECIO AL MAYOR" ? respuesta.articulo.art_pre_unitario2 : respuesta.articulo.art_pre_unitario3;
								var montos = $(this).parent().next();
		    					
		    					montos.find(".spaCosUnitario").text(number_format(cosUnitario));
		    					montos.find(".spaCosUnitarioBs").text(number_format(cosUnitario * respuesta.dolar));
		    					cantidad.keyup();
		    				});

							cantidad.focus();
			    		}

		    			$(".tooltips").tooltip();
					}
		        }
		    });
		} else {
	        notif({
	        	msg: "Ya se encuentra ese artículo en la lista.",
	        	type: "error",
	        	position: "center",
                multiline: true
	        });
		}
	});

	// Cálculo de costo total
	var suma;
	var sumCosTotal;
	$("#lisSalidas").on("keyup", ".cantidad", function() {
		var cantidad = $(this).val();
		var parent = $(this).parent().next().next();
		var cosUnitario = replace_number(parent.find(".spaCosUnitario").text());
		var cosTotal = Number(cantidad) * Number(cosUnitario);
		var cosUnitarioBs = replace_number(parent.find(".spaCosUnitarioBs").text());
		var cosTotalBs = Number(cantidad) * Number(cosUnitarioBs);

	    $.ajax({
	        type: "POST",
	        url: "../dolares/urlgetdolar",

	        success: function(respuesta) {
				parent.next().html("$ <span class='spaCosTotal'>" + number_format(cosTotal) + "</span> / Bs. <span class='spaCosTotalBs'>" + number_format(cosTotalBs) + "</span>");

				sumCosTotal = 0;
				$(".spaCosTotal").each(function() {
					sumCosTotal = Number(sumCosTotal) + Number(replace_number($(this).text()));
				});

        		var total = $("#total");

				suma = 0;
				$(".spaCosTotalBs").each(function() {
					suma = Number(suma) + Number(replace_number($(this).text()));
				});

        		var iva = suma * 16 / 100;

        		$("#sumCosTotal").text("$ " + number_format(sumCosTotal) + " / Bs. " + number_format(suma));
        		$("#subtotal").text(number_format(suma));
        		
        		var importe = $("#importe");
        		if($("#cheIva").is(":checked")) {
        			var sumatoria = suma + iva;

        			total.text(number_format(sumatoria));
					importe.val(number_format(sumatoria)); 
					$("#iva").val("Bs. " + number_format(iva));
        		} else {
        			total.text(number_format(suma));
					importe.val(number_format(suma));
        		}

        		$("#agrPago").removeClass("disabled");
				$("#cheIva").bootstrapSwitch("disabled", false);
	        }
	    });
	});

	var arrArticulo = [];
    // Registro de salida de mercancía
	$("#registrar").on("click", function() {

	// Ver https://developer.mozilla.org/es/docs/Web/API/NavigatorOnLine/onLine
	if(navigator.onLine) {
	       // Se guarda la referencia al formulario
	        var $f = $("#salida");
	        var loading = $("#loading");

	        $f.parsley().destroy();

	        if($f.parsley().validate()) {
				urlGetDolar().done(function(data) {
					var total = Number(total$ * data) + Number(totalbs);

			     	var regSalCallback = function(text) {
			     		if(text !== false && text == "LINPIESA") {
				            $(".selPreVenta").next(".help-block").remove();

				        	if($("#tabLisPagos tbody tr").length > 0 || $("#garantia").is(":checked")) {
					            loading.show().removeClass("hide");
					            var str = $f.serialize();

								var objArticulo = {};
								for(i in arrArticulo)
									objArticulo[i] = arrArticulo[i];

								var objPago = {};
								for(i in arrPago)
									objPago[i] = arrPago[i];

					            // Bloqueo de peticiones repetidas
					            if($f.data("locked") == undefined || !$f.data("locked")) {
					                $.ajax({  
					                    type: "POST",
					                    url: "../salidas/registrobd?articulos=" + JSON.stringify(objArticulo) + "&pagos=" + JSON.stringify(objPago) + "&iva=" + $("#iva").val(),
					                    data: str,
					                    dataType: "json",

					                    beforeSend: function() { 
					                        $f.data("locked", true);
					                    },

					                    success: function(respuesta) {
					                        loading.hide();
					                        
					                        if(respuesta.type != "error") {
					                            $("#cancelar").click();
					                            $("#verCliente").addClass("disabled");

												var impSopCallback = function(choice) {
													if(choice)
														window.open("/" + (window.location.pathname).split("/")[1] + "/salidas/salida_mercancia/" + respuesta.salida, "Salida de Mercancía", "width=240, height=600");
													else
														$.redirect("/" + (window.location.pathname).split("/")[1] + "/salidas/lista");
												}

												notif_confirm({
													"message": respuesta.text,
													"textaccept": "Sí, ¡deseo imprimirlo!",
													"textcancel": "No, imprimir más tarde",
													"fullscreen": true,
													"callback": impSopCallback
												});
					                        } else {
						                        notif({
						                            msg: respuesta.text,
						                            type: "error",
						                            position: "center",
						                            multiline: true
						                        });
					                        }
					                    },

					                    error: function () {
					                        loading.hide();

					                        notif({
					                            msg: "Sucedió un error, contacte con el administrador del sistema.",
					                            type: "error",
					                            position: "center",
					                            multiline: true
					                        });
					                    },

					                    complete: function() { 
					                        $f.data("locked", false);
					                    }               
					                });

					                return false;
					            }
					        } else {
					            notif({
					                msg: "Debe registrar el pago.",
					                type: "error",
					                position: "center",
					                multiline: true
					            });
					        }
			     		}
			     	}

		        	if(total >= Number(suma) + Number(replace_number(($("#iva").val()).replace("Bs. ", ""))))
	        			regSalCallback("LINPIESA");
				    else if($("#cheIva").is(":checked")) {
				        notif_prompt({
				        	"message": "El total de pagos registrados no coincide con el total de precios más IVA. Ingrese la clave máster para continuar.",
				        	"textaccept": "¡Deseo continuar!",
				        	"textcancel": "Me equivoqué",
				        	"fullscreen": true,
				        	"callback": regSalCallback
				        });

				        $(".notifit_prompt_input").removeAttr("type", "text").attr("type", "password");
				    } else
	        			regSalCallback("LINPIESA");
				});
	        } else
				$(".selPreVenta").next().remove();
	} else {
	    alert("Sin conexion a internet");
	}

    
	});

	// Preparación de modal para agregar nuevo cliente
	$("#agrCliente").on("click", function() {
		$("#forCliente").parsley().destroy();
		$("#modCliente .modal-title").text("Registro de cliente");
		$("#modCliente input, #modCliente textarea").val("").removeAttr("readonly");
		$("#regCliente, #canCliente").show();
		$("#modCliente").modal("show");

		setTimeout(function() {
			$("input#codCliente").focus();
		}, 1000);
	});

	// Preparación de modal para agregar datos de envío y recibo
	$("#notEntrega").on("click", function() {
		$("#forEnvio").parsley().destroy();
		$("#modEnvio input, #modEnvio textarea").val("");
		$("#salida-id").val(salida_id);
		$("#modEnvio").modal("show");

		setTimeout(function() {
			$("input#nombre1").focus();
		}, 1000);
	});

    // Función para aplicar máscara a campo de cédula o rif
    mascara = function(str) { 
        return (str == "v" || str == "V" || str == "e" || str == "E") ? "a-999999[99]" : "a-99999999-9";
    }

    var cedIdentidad = $("#cedIdentidad1, #cedIdentidad2");
    cedIdentidad.keyup(function() {
	    // Máscara a campo de cédula
	    cedIdentidad.inputmask({ 
	        mask: mascara($(this).val().substring(0, 1)),
	        greedy: false, 
	        clearIncomplete: true,
	        definitions: { 
	            "a": { 
	                validator: "[vVeEjJgG]" 
	            } 
	        } 
	    });
	});

	// Impresión de nota de entrega
	$("#impEnvio").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forEnvio");
        var loading = $("#loaEnvio");

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var str = $f.serialize();

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../salidas/nota_entrega",
                    data: str,

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();
                        $("#modEnvio").modal("hide");
						window.open('../salidas/nota_entrega?' + str, 'Nota de Entrega', 'width=240, height=600')
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });                

                return false;
            }
        }
	});

	// Preparación de modal para agregar nuevo artículo
	$("#agrArticulo").on("click", function() {
		$("#forArticulo").parsley().destroy();
		$("#modArticulo input, #modArticulo textarea").not("#fecCreacion, #fotReferencial").val("").not("#cosUniBs, #preUni1Bs, #preUni2Bs, #preUni3Bs").removeAttr("readonly");

		// Desbloquear botón para carga de foto referencial
		$("#canFotReferencial").click();
		$("#selFoto").unbind("click");
		$("#fileinput").find(".uneditable-input").removeClass("disabled");
		$("#canFotReferencial, #btn-file").removeClass("disabled");
		$("#divFotReferencial").hide();

		$("#regArticulo, #canArticulo").show();
		$("#modArticulo").modal("show");

		setTimeout(function() {
			$("input#codArticulo").focus();
		}, 1000);
	});

	// Actualización de salida
	$("#actualizar").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#salida");
        var loading = $("#loading");

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var str = $f.serialize();

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../salidas/actualizacionbd?id=" + $(this).attr("data-salida-id"),
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });                        
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }
	});

    // Resetear formulario de ingreso
    $("#cancelar").on("click", function () {
        $("#salida").each(function() {
            this.reset();
        });

        $("#pago").each(function() {
        	this.reset();
        });

		$("#cliente, #articulo, #cuenta, #moneda, #codigo").val(null).trigger("change");
		$("#salida").parsley().destroy();
		$("#pago").parsley().destroy();
        $("#tabLisSalidas tbody, #tabLisPagos tbody").empty();
        $("#lisSalidas, #lisPagos").hide();
        $("#subtotal, #total").text("0,00");
        $("#iva").val("Bs. 0,00");
        arrArticulo = [];
        arrPago = [];
    	totalbs = 0;
    	total$ = 0;

        return false;
    });

	// Función para eliminar un pago de la lista
	eliPago = function(pago, importe, moneda) {
		var bolivares = $("#bolivares");
		var dolares = $("#dolares");
		var repImporte = replace_number(importe);

		$("#pago" + pago).remove();
		moneda == "Bs." ? bolivares.text(number_format(Number(replace_number(bolivares.text())) - Number(repImporte))) : dolares.text(number_format(Number(replace_number(dolares.text())) - Number(repImporte)));
		arrPago.splice(arrPago.findIndex(x => x.id == pago));

		if($("#tabLisPagos tbody tr").length == 0)
			$("#lisPagos").hide();

    	totalbs = 0;
    	total$ = 0;

		$("#moneda").val("Bs.").trigger("change");
    	$("#agrPago").removeClass("disabled");

		if($("#tabLisPagos tbody tr").length == 0) {
			var importe = $("#importe");

    		if($("#cheIva").is(":checked")) {
        		var iva = suma * 16 / 100;
				importe.val(number_format(suma + iva)); 
    		} else
				importe.val(number_format(suma));
		}
	}

	// Función para eliminar una salida de la lista
	eliSalida = function(salida) {
		$("#salida" + salida).remove();
		
		delete arrArticulo[$.inArray(salida.toString(), arrArticulo)];

		if($("#tabLisSalidas tbody tr").length == 0) {
			$("#articulo, #codigo").val(null).trigger("change");
			$("#lisSalidas").hide();
			$("#importe").val("");
			$("#agrPago").addClass("disabled");
			$("#cheIva").bootstrapSwitch("state", false).bootstrapSwitch("disabled", true);
			$("#subtotal, #total").text("0,00");
		} else
			$(".cantidad").keyup();
	}

	// Preparación de modal para ver detalle de proveedor
	verCliente = function(cliente) {
		var json = JSON.parse(cliente);

		$("#modCliente .modal-title").text("Cliente: " + json.cli_nombre);
    	$("#codCliente").val(json.cli_codigo).attr("readonly", "readonly");
    	$("#nombre").val(json.cli_nombre).attr("readonly", "readonly");
    	$("#direccion").val(json.cli_direccion).attr("readonly", "readonly");
    	$("#telLocal").val(json.cli_tel_local).attr("readonly", "readonly");
    	$("#telCelular").val(json.cli_tel_celular).attr("readonly", "readonly");
    	$("#corElectronico").val(json.cli_cor_electronico).attr("readonly", "readonly");
	    $("#regCliente").hide();
	    $("#canCliente").hide();
		$("#modCliente").modal("show");
	}

	// Preparación de modal para ver detalle de artículo
	verArticulo = function(articulo, dolar, calculos) {
		var json = JSON.parse(articulo);
		var calJson = JSON.parse(calculos);
		var porCif = (json.art_pre_unitario1 != null && json.art_pre_unitario1 != 0) ? (Number(json.art_pre_unitario1) - Number(json.art_cos_unitario)) * 100 / Number(json.art_cos_unitario) : calJson.cal_pre_unitario1;
		var porMayor = (json.art_pre_unitario2 != null && json.art_pre_unitario2 != 0) ? (Number(json.art_pre_unitario2) - Number(json.art_pre_unitario1)) * 100 / Number(json.art_pre_unitario1) : calJson.cal_pre_unitario2;
		var porDetal = (json.art_pre_unitario3 != null && json.art_pre_unitario3 != 0) ? (Number(json.art_pre_unitario3) - Number(json.art_pre_unitario2)) * 100 / Number(json.art_pre_unitario2) : calJson.cal_pre_unitario3;

		$("#modArticulo .modal-title").text("Artículo: " + json.art_codigo);
		$("#codArticulo").val(json.art_codigo).attr("readonly", "readonly");
		$("#descripcion").val(json.art_descripcion).attr("readonly", "readonly");
		$("#cosUnitario").val(number_format(json.art_cos_unitario)).attr("readonly", "readonly");
		$("#preUnitario1").val(number_format(json.art_pre_unitario1)).attr("readonly", "readonly");
		$("#preUnitario2").val(number_format(json.art_pre_unitario2)).attr("readonly", "readonly");
		$("#preUnitario3").val(number_format(json.art_pre_unitario3)).attr("readonly", "readonly");
		$("#cosUniBs").val(number_format(json.art_cos_unitario * dolar)).attr("readonly", "readonly");
		$("#preUni1Bs").val(number_format(json.art_pre_unitario1 * dolar)).attr("readonly", "readonly");
		$("#preUni2Bs").val(number_format(json.art_pre_unitario2 * dolar)).attr("readonly", "readonly");
		$("#preUni3Bs").val(number_format(json.art_pre_unitario3 * dolar)).attr("readonly", "readonly");
		$("#preUni1").val(number_format(porCif)).attr("readonly", "readonly");
		$("#preUni2").val(number_format(porMayor)).attr("readonly", "readonly");
		$("#preUni3").val(number_format(porDetal)).attr("readonly", "readonly");
		$("#invInicial").val(json.art_inv_inicial).attr("readonly", "readonly");
		$("#fecCreacion").val(json.art_fec_creacion).attr("readonly", "readonly");
		$("#stoMinimo").val(json.art_sto_minimo).attr("readonly", "readonly");
		$("#ubicacion").val(json.art_ubicacion).attr("readonly", "readonly");

		// Evitar carga de foto referencial en esta pantalla
		$("#canFotReferencial").click();

		if(json.art_fot_referencial != null) {
			$(".fileinput-filename").html(json.art_fot_referencial);
			$("#fileinput").removeClass("fileinput-new").addClass("fileinput-exists").find(".uneditable-input").addClass("disabled");
			$("#canFotReferencial, #btn-file").addClass("disabled");
			$("#divFotReferencial").show().removeClass("hide");
			$("#divFotReferencial img").attr("src", "/" + (window.location.pathname).split("/")[1] + "/" + json.art_fot_referencial + "?" + Math.random());
		} else {
			$("#fileinput").find(".uneditable-input").addClass("disabled");
			$("#btn-file").addClass("disabled");
			$("#divFotReferencial").hide();
		}

		$("#selFoto").on("click", function(e) {
			e.preventDefault();
		})

		// ******************************** //

		$("#regArticulo").hide();
		$("#canArticulo").hide();
    	$("#modArticulo").modal("show");
	}

    // Resetar formulario de datos de envío y recibo
    $("#canEnvio").on("click", function () {
        $("#forEnvio").each(function() {
            this.reset();
        });

        $("#nombre1").focus();
        $("#salida-id").val(salida_id);


        return false;
    });

 	// Anulación de salidas
 	$("#elmSalida").on("click", function() {
     	var salSopCallback = function(text) {
     		if(text !== false && text == 'LINPIESA') {
                // Se guarda la referencia al formulario
                var $f = $("#salida");
                $f.parsley().destroy();

                if($f.parsley().validate()) { 
                    // Bloqueo de peticiones repetidas
                    if($f.data("locked") == undefined || !$f.data("locked")) {
						$("#forAnulacion").parsley().destroy();
						$("#modAnulacion textarea").val("");
						$("#modAnulacion").modal("show");

						setTimeout(function() {
							$("textarea#motAnulacion").focus();
						}, 1000);
                    }
                }
            } else {
            	notif({
            		"type": "error",
            		"msg": "Proceso Cancelado :(",
            		"position": "center"
            	})
            }
        }
        
        notif_prompt({
        	"message": "¿Desa anular esta salida?",
        	"textaccept": "Sí, ¡deseo anularla!",
        	"textcancel": "No, me equivoqué",
        	"fullscreen": true,
        	"callback": salSopCallback
        })

        $(".notifit_prompt_input").removeAttr("type","text");
     	$(".notifit_prompt_input").attr("type","password");
    });

 	$("#anuSalida").on("click", function() {
        // Se guarda la referencia al formulario
        var loading = $("#loaAnulacion");
        var $f = $("#forAnulacion");

        var str = $f.serializeArray();
        	str.push({ name: "id", value: $("#elmSalida").attr("data-salida-id") });
        	str.push({ name: "motAnulacion", value: $("#motAnulacion").val() });

        $f.parsley().destroy();

        if($f.parsley().validate()) { 
        	loading.show().removeClass("hide");

	    	$.ajax({  
	    		type: "POST",
	    		url: "../salidas/anulacionbd",
                data: str,
	    		dataType: "json",

	    		beforeSend: function() { 
	    			$f.data("locked", true);
	    		},

	    		success: function(respuesta) {
	    			loading.hide();
	    			$("#modAnulacion").modal("hide");

	    			if(respuesta.type != "error")
	    				$.redirect("/" + (window.location.pathname).split("/")[1] + "/salidas/anuladas");

	    			notif({
	    				msg: respuesta.text,
	    				type: respuesta.type,
	    				position: "center",
	    				multiline: true
	    			});                        
	    		},

	    		error: function () {
	    			loading.hide();

	    			notif({
	    				msg: "Sucedió un error, contacte con el administrador del sistema.",
	    				type: "error",
	    				position: "center",
	    				multiline: true
	    			});
	    		},

	    		complete: function() { 
	    			$f.data("locked", false);
	    		}               
	    	});

	    	return false;

	    }
 	});




    // Formatear campos de monto
    $("#importe, #cheIva").priceFormat({
        prefix: "",
        centsSeparator: ",",
        thousandsSeparator: "."
    });

    window.ParsleyValidator.addValidator("importe", function(value) {
        var response = value == "0,00" ? false : true;
        return response;
    });

    $("#fecPago").datepicker({
        todayHighlight: true,
        startView: 1,
        autoclose: true
    });

    $(".bootstrap-switch").bootstrapSwitch();
    $("#cheIva").bootstrapSwitch("disabled", true);

    var arrPago = [];
    var index = 1;
    $("#agrPago").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#pago");
        //var loading = $("#loading");
        var $this = $(this);

        $f.parsley().destroy();

        if($f.parsley().validate()) { 
			urlGetDolar().done(function(data) {
        		var flag;
				var total = $("#moneda").val() == "$" ? replace_number($("#importe").val()) * data : replace_number($("#importe").val());

		     	agrPagCallback = function(text) {
		     		if(text !== false && text == "LINPIESA") {
			        	var cuenta = $("#cuenta");
			        	var fecPago = $("#fecPago").val();
			        	var importe = $("#importe").val();
			        	var moneda = $("#moneda").val();
			        	var nroReferencia = $("#nroReferencia").val();
			        	var optCuenta = $("option:selected", cuenta);
			    		var trs = $("#tabLisPagos tbody tr").length;
			    		var verificado = $("#verificado").is(":checked") ? "SÍ" : "NO";
			    		var repImporte = replace_number(importe);

						$("#lisPagos").show().removeClass("hide");
			    		moneda == "Bs." ? totalbs = Number(totalbs) + Number(repImporte) : total$ = Number(total$) + Number(repImporte);

			         	var pago = {
			         		index: index,
			         		fecha: fecPago,
			         		numReferencia: nroReferencia,
			         		importe: importe,
			         		moneda: moneda,
			         		verificado: verificado,
			         		cuenta: cuenta.val(),
			         		forma: $("#pago").find(".icheck input[type=radio]:checked").val()
			         	};

			         	arrPago.push(pago);

			    		if(trs == 0) {
			    			var row  = "<tr id='pago" + index + "'>";
			    				row += "<td class='cuenta'>" + optCuenta.text() + "</td>";
			    				row += "<td class='fecPago'>" + fecPago + "</td>";
			    				row += "<td class='nroReferencia'>" + nroReferencia + "</td>";
			    				row += "<td class='importe'>" + moneda + " " + importe + "</td>";
			    				row += "<td class='text-center verificado'>" + verificado + "</td>";
			    				row += "<td class='text-center'><a class='btn btn-md btn-primary btn-label eliPago mr-xs tooltips' onclick='eliPago(" + index + ", \"" + importe + "\", \"" + moneda + "\")' data-trigger='hover' data-original-title='Eliminar'><i class='ti ti-trash btn-label-custom'></i></a></td>";
			    				row += "</tr>";
			    			
			    			$(row).appendTo("#tabLisPagos tbody");
			    		} else {
			    			var tabLisPagos = $("#tabLisPagos tr:last");
							var clone = tabLisPagos.clone();

							clone.attr("id", "pago" + index);
							$(".cuenta", clone).text(optCuenta.text());
							$(".fecPago", clone).text(fecPago);
							$(".nroReferencia", clone).text(nroReferencia);
							$(".importe", clone).text(moneda + " " + importe);
							$(".verificado", clone).text(verificado);
							$(".eliPago", clone).attr("onclick", "eliPago(" + index + ", \"" + importe + "\", \"" + moneda + "\")");
							$(clone).insertAfter("#tabLisPagos tr:last");
			    		}

						$(".tooltips").tooltip();
						$("#sumPagTotal").html("$ <span id='dolares'>" + number_format(total$) + "</span> - Bs. <span id='bolivares'>" + number_format(totalbs) + "</span>");
						$("#cuenta").val(null).trigger("change");
			        	index++;

				        $("#pago").each(function() {
				            this.reset();
				        });

				        $("#nroReferencia").removeAttr("disabled").parent().addClass("required");
				        $("#transferencia").iCheck("check");
		     		}
		     	}

	        	if(total >= Number(suma) + Number(replace_number(($("#iva").val()).replace("Bs. ", "")))) {
	        		if($("#tabLisPagos tbody tr").length == 0)
	        			$this.addClass("disabled");
	        		
	        		agrPagCallback("LINPIESA");
			    } else if($("#tabLisPagos tbody tr").length == 0) {
			        notif_prompt({
			        	"message": "Ingrese la clave máster para continuar.",
			        	"textaccept": "¡Deseo continuar!",
			        	"textcancel": "Me equivoqué",
			        	"fullscreen": true,
			        	"callback": agrPagCallback
			        });

			        $(".notifit_prompt_input").removeAttr("type", "text").attr("type", "password");
			    } else
	        		agrPagCallback("LINPIESA");
			});
        }

        return false;
    });

	efectivo.on("ifChecked", function() {
		var nroReferencia = $("#nroReferencia");
		nroReferencia.attr("disabled", "disabled").removeAttr("required").parent().removeClass("required");
		nroReferencia.next(".help-block").remove();
	});

	efectivo.on("ifUnchecked", function() {
		var nroReferencia = $("#nroReferencia");
		nroReferencia.removeAttr("disabled").attr("required", "required").parent().addClass("required");
	});

	// Habilitar monto de iva
    $("#cheIva").on("switchChange.bootstrapSwitch", function(event, state) {
    	var iva = $("#iva");
    	var total = $("#total");
    	var importe = $("#importe");

    	if(state) {
			var flag = true;

			$(".importe").each(function() { 
				flag = ($(this).text()).indexOf("$") >= 0 ? true : false;
			});

			if(($(".importe").length == 0 && $("#moneda").val() == "Bs.") || !flag) {
	    		var subtotal = replace_number($("#subtotal").text());
	    		var ivaVal = Number(subtotal) * 16 / 100;

	    		iva.val("Bs. " + number_format(ivaVal)).removeAttr("readonly");
	    		total.text(number_format(Number(subtotal) + Number(ivaVal)));
	    		
				if($(".importe").length == 0)
	    			importe.val(number_format(Number(replace_number(importe.val())) + Number(ivaVal)));
			} else {
				$("#cheIva").bootstrapSwitch("state", false);

		        notif({
		            msg: "El IVA no aplica para pagos en dólares.",
		            type: "error",
		            position: "center",
		            multiline: true
		        });                        
			}
    	} else {
    		iva.attr("readonly", "readonly").val("Bs. 0,00");
    		total.text($("#subtotal").text());

    		if($(".importe").length == 0 && $("#tabLisSalidas tbody tr").length > 0)
    			$("#moneda").val() == "$" && $(".importe").length == 0 ? importe.val(number_format(sumCosTotal)) : importe.val(number_format(suma));
    	}
	});

	$("#moneda").select2({width: "100%"}).on("select2:select", function(e) {
		var moneda = e.params.data.id;
		var trs = $("#tabLisPagos tbody tr").length;
		var importe = $("#importe");

		if(moneda == "$") {
			$("#cheIva").bootstrapSwitch("state", false);

			if(trs == 0)
				importe.val(number_format(sumCosTotal));
		} else if(trs == 0)
			importe.val(number_format(Number(suma) + Number(replace_number(($("#iva").val()).replace("Bs. ", "")))));
	});
});