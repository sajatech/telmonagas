$(document).ready(function() {
    // Función para búsqueda rápida de artículos
    function busRapida() {
        var actArticulo = $("#actArticulo");
        var selArticulo = $("#selArticulo");
        var selCodigo = $("#selCodigo");

        if(actArticulo.length > 0) {
            var id = actArticulo.attr("data-articulo-id");

            selArticulo.attr("data-articulo-id", id).val(id).trigger("change").trigger("select2:select");        
            selCodigo.val(id).trigger("change");
        } else {
            selArticulo.val(null).trigger("change");
            selCodigo.val(null).trigger("change");
            $("#respuesta").html("<div class='alert alert-info mb-n'><i class='ti ti-info-alt'></i>&nbsp; Seleccione un artículo para mostrar precios y existencia.</div>");
        }

        $("#modBusqueda").modal("show");
    }

    // Mediante combinación de teclas
    Mousetrap.bind("ctrl+f9", function() {
        busRapida();
    });

    // Mediante click en botón
    $("#busRapida").on("click", function() {
        busRapida();
    });

    // Búsqueda rápida de artículos
    $("#selArticulo").select2({width: "100%"}).on("select2:select", function(e) {
        var id;
        var respuesta = $("#respuesta");
            respuesta.html("<center><img src='/" + (window.location.pathname).split("/")[1] + "/img/loading.gif'" + " width='32px'/> Por favor, espere...</center>");

        if(e.params != undefined) {
            id = e.params.data.id;
            $("#selCodigo").val(id).trigger("change");
        } else
            id = $("#selArticulo").attr("data-articulo-id");

        respuesta.load("../articulos/busqueda", { "articulo": id }, function(response) {
            respuesta.html(response);

            var height = Number(($("#modBusqueda .modal-content").css("height")).replace("px", ""));

            if(height > 639)
                $("#modBusqueda .modal-backdrop").css("height", (height + 60) + "px");
        });
    });

    // Búsqueda por código de artículo
    $("#selCodigo").select2({width: "100%"}).on("select2:select", function(e) {
        var id = e.params.data.id;
        $("#selArticulo").attr("data-articulo-id", id).val(id).trigger("change").trigger("select2:select");
    });
});