$(document).ready(function() {
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/ingresos/lista");

    $.get("../ingresos/getingresos", function(data) {
        var data = JSON.parse(data);

        $("#ajaxSpinner").hide();
        $("#panIngresos").removeClass("hide").show();

        // Tabla de los ingresos de mercancía
        oTable = $("#ingresos").dataTable({
            "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "aaData": data.aaData,
            "bServerSide": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "aaSorting": [],

            "aoColumns": [
                { "mData": "codIngreso" },
                { "mData": "proveedor" },
                { "mData": "fecha" },
                { "mData": "usuario" }
            ],

            "oTableTools": {
                "sRowSelect": "single",

                "aButtons": [
                    { "sExtends": "editor_create", "sButtonText": "Agregar", 

                        "fnInit": function(button) {
                            $(button).addClass("hide");
                            
                            if(data.privilegios.includes(16))
                                $(button).removeClass("hide");
                        },

                    	"fnClick": function() {
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/ingresos/compra", "", "GET");
                    	}

                	},
                    
                    { "sExtends": "editor_edit", "sButtonText": "Actualizar/Ver",

                        "fnInit": function(button) {
                            $(button).addClass("hide disabled");
                            
                            if(data.privilegios.includes(18) || data.privilegios.includes(19))
                                $(button).removeClass("hide");
                        },

                        "fnClick": function(button, conf) {
                            var node = this.fnGetSelectedData();
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/ingresos/compra?token=true", { "codigo": node[0].codIngreso });
                        }

                    },

                    { "sExtends": "editor_edit", "sButtonText": "Imprimir Soporte",

                        "fnInit": function(button) {
                            $(button).addClass("hide disabled");
                            
                            if(data.privilegios.includes(20))
                                $(button).removeClass("hide");
                        },

                        "fnClick": function() {
                            var node = this.fnGetSelectedData();
                            window.open("/" + (window.location.pathname).split("/")[1] + "/ingresos/ingreso_mercancia/" + (node[0].codIngreso).replace(/^0+/, ""), "Ingreso de Mercancía", "width=240, height=600");
                        }

                    }
                ]
        	},

            "language": {
                "lengthMenu": "_MENU_ &nbsp;&nbsp;"
            },

            "iDisplayLength": 50,    
        });

        $("#ingresos_filter input").addClass("form-control").attr("placeholder", "Buscar Ingreso...");
        $("#ingresos_length select").addClass("form-control");

        // Se añaden los íconos
        $("#ToolTables_ingresos_0").prepend("<i class='ti ti-plus'/> ");
        $("#ToolTables_ingresos_1").prepend("<i class='ti ti-pencil'/> ");
        $("#ToolTables_ingresos_2").prepend("<i class='ti ti-printer'/> ");

        // Se añaden los elementos de la tabla al panel
        $(".panel-ctrls").append($("#ingresos_filter").addClass("pull-right"));
        $(".panel-ctrls").append("<i class='separator'></i>");
        $('.panel-ctrls').append($('#ingresos_length').addClass("pull-left mt-14").find("label").addClass("panel-ctrls-center"));
        $(".panel-ctrls").append("<i class='separator'></i>");
        $(".panel-ctrls").append($(".DTTT.btn-group").addClass("pull-left mt-10"));

        $(".panel-footer").append($("#ingresos+.row"));
        $("#ingresos_paginate>ul.pagination").addClass("pull-right pagination-lg");

        // ******************************** //
    });
});