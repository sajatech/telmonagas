$(document).ready(function() {
	$("#proveedor, #articulo").select2({width: "100%"});
    $("#breadcrumb").attr("href", "/" + (window.location.pathname).split("/")[1] + "/ingresos/lista");

	// Habilitar botón "ver detalle" al seleccionar algún proveedor
	$("#proveedor").select2({width: "100%"}).on("select2:select", function(e) {
		var verProveedor = $("#verProveedor");
		var id = e.params.data.id;

		if(id != "") {
		    $.ajax({
		        type: "POST",
		        url: "../proveedores/getproveedor",
		        dataType: "json",

		        data: {
		        	proveedor: $("option:selected", "#proveedor").val(),
		        },

		        success: function(respuesta) {
					verProveedor.removeClass("disabled").attr("onclick", "verProveedor('" + JSON.stringify(respuesta) + "')");
		        }
		    });
		} else
			verProveedor.addClass("disabled");
	});

	// Agregación de nuevo ingreso
	$("#articulo").select2({width: "100%"}).on("select2:select", function(e) {
		var id = e.params.data.id;

		if($.inArray(id, arrArticulo) < 0) {
			arrArticulo.push(id);

		    $.ajax({
		        type: "POST",
		        url: "../articulos/urlgetarticulo",
		        dataType: "json",

		        data: {
		        	articulo: id,
		        	tipo: "id"
		        },

		        success: function(respuesta) {
		        	if(respuesta.false == undefined) {
						$("#lisIngresos").show().removeClass("hide");

			    		var trs = $("#tabLisIngresos tbody tr").length;

			    		if(trs == 0) {
			    			var row  = "<tr id='ingreso" + respuesta.articulo.id + "'>";
			    				row += "<td class='codigo'>" + respuesta.articulo.art_codigo + "</td>";
			    				row += "<td><input type='text' name='cantidad[]' class='cantidad form-control' required='required' placeholder='0' data-parsley-type='number' data-parsley-error-message='<i class=\"fa fa-times-circle\"></i> Ingrese cantidad.'/></td>";
			    				row += "<td class='cosUnitario'>" + "$ <span class='spaCosUnitario'>" + number_format(respuesta.articulo.art_cos_unitario) + "</span> / Bs. " + number_format(respuesta.articulo.art_pre_unitario1)  + "</td>";
			    				row += "<td class='cosTotal'>$ <span class='spaCosTotal'>0,00</span> / Bs. 0,00</td>";
			    				row += "<td><a class='btn btn-md btn-primary btn-label eliIngreso mr-xs' onclick='eliIngreso(" + respuesta.articulo.id + ")'><i class='ti ti-trash'></i> Eliminar</a><a class='btn btn-md btn-primary btn-label verArticulo'><i class='ti ti-search'></i> Ver Artículo</a></td>";
			    				row += "</tr>";
			    			
			    			$(row).appendTo("#tabLisIngresos tbody");
			    			$(".verArticulo").attr("onclick", "verArticulo('" + JSON.stringify(respuesta.articulo) + "', '" + respuesta.dolar + "')");
			    			$(".cantidad").focus();
			    		} else {
			    			var tabLisIngresos = $("#tabLisIngresos tr:last");
							var clone = tabLisIngresos.clone(true);
							var cantidad = $(".cantidad", clone);

							clone.attr("id", "ingreso" + respuesta.articulo.id);
							$(".codigo", clone).text(respuesta.articulo.art_codigo);
							cantidad.val("");
							$(".cosUnitario", clone).html("$ <span class='spaCosUnitario'>" + number_format(respuesta.articulo.art_cos_unitario) + "</span> / Bs. " + number_format(respuesta.articulo.art_cos_unitario * respuesta.dolar));
							$(".cosTotal", clone).html("$ <span class='spaCosTotal'>0,00</span> / Bs. 0,00");
							$(".eliIngreso", clone).attr("onclick", "eliIngreso(" + respuesta.articulo.id + ")");
							$(".verArticulo", clone).attr("onclick", "verArticulo('" + JSON.stringify(respuesta.articulo) + "', '" + respuesta.dolar + "')");
							$(".help-block", clone).removeClass("filled").empty();
							$(clone).insertAfter("#tabLisIngresos tr:last");
							cantidad.focus();
			    		}
					}
		        }
		    });
		} else {
	        notif({
	        	msg: "Ya se encuentra ese artículo en la lista.",
	        	type: "error",
	        	position: "center",
                multiline: true
	        });
		}
	});

	// Cálculo de costo total
	$("#lisIngresos").on("keyup", ".cantidad", function() {
		var cantidad = $(this).val();
		var parent = $(this).parent().next();
		var cosUnitario = replace_number(parent.find(".spaCosUnitario").text());
		var cosTotal = Number(cantidad) * Number(cosUnitario);

	    $.ajax({
	        type: "POST",
	        url: "../dolares/urlgetdolar",

	        success: function(respuesta) {
				parent.next().html("$ <span class='spaCosTotal'>" + number_format(cosTotal) + "</span> / Bs. " + number_format(cosTotal * respuesta));

				var sumCosTotal = 0;
				$(".spaCosTotal").each(function() {
					sumCosTotal = Number(sumCosTotal) + Number(replace_number($(this).text()));
				});

        		$("#sumCosTotal").text("$ " + number_format(sumCosTotal) + " / Bs. " + number_format(sumCosTotal * respuesta));
	        }
	    });
	});

	var arrArticulo = [];
    // Registro de ingreso de mercancía
	$("#registrar").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#ingreso");
        var loading = $("#loading");

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var str = $f.serialize();

			var objArticulo = {};
			for(i in arrArticulo)
				objArticulo[i] = arrArticulo[i];

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../ingresos/registrobd?articulos=" + JSON.stringify(objArticulo),
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();
                        
                        if(respuesta.type != "error") {
                            $("#cancelar").click();
                            $("#verProveedor").addClass("disabled");

							var impSopCallback = function(choice) {
								if(choice)
									window.open("/" + (window.location.pathname).split("/")[1] + "/ingresos/ingreso_mercancia/" + respuesta.ingreso, "Ingreso de Mercancía", "width=240, height=600");
							}

							notif_confirm({
								"message": respuesta.text,
								"textaccept": "Sí, ¡deseo imprimirlo!",
								"textcancel": "No, imprimir más tarde",
								"fullscreen": true,
								"callback": impSopCallback
							});
                        } else {
	                        notif({
	                            msg: respuesta.text,
	                            type: "error",
	                            position: "center",
	                            multiline: true
	                        });
                        }
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }
	});

	// Preparación de modal para agregar nuevo proveedor
	$("#agrProveedor").on("click", function() {
		$("#forProveedor").parsley().destroy();
		$("#modProveedor .modal-title").text("Registro de proveedor");
		$("#modProveedor input, #modProveedor textarea").val("").removeAttr("readonly");
		$("#regProveedor, #canProveedor").show();
		$("#modProveedor").modal("show");

		setTimeout(function() {
			$("input#codProveedor").focus();
		}, 1000);
	});

	// Preparación de modal para agregar nuevo artículo
	$("#agrArticulo").on("click", function() {
		$("#forArticulo").parsley().destroy();
		$("#modArticulo input, #modArticulo textarea").not("#fecCreacion, #fotReferencial").val("").not("#cosUniBs, #preUni1Bs, #preUni2Bs, #preUni3Bs").removeAttr("readonly");

		// Desbloquear botón para carga de foto referencial
		$("#canFotReferencial").click();
		$("#selFoto").unbind("click");
		$("#fileinput").find(".uneditable-input").removeClass("disabled");
		$("#canFotReferencial, #btn-file").removeClass("disabled");
		$("#divFotReferencial").hide();

		$("#regArticulo, #canArticulo").show();
		$("#modArticulo").modal("show");

		setTimeout(function() {
			$("input#codArticulo").focus();
		}, 1000);
	});

	// Actualización de ingreso
	$("#actualizar").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#ingreso");
        var loading = $("#loading");

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var str = $f.serialize();

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../ingresos/actualizacionbd?id=" + $(this).attr("data-ingreso-id"),
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });                        
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }
	});

	// Eliminación de ingresos
    $("#elmIngreso").on("click", function() {

     	var salSopCallback = function(text) {
     		if(text !== false && text == 'LINPIESA')
     			{
                        // Se guarda la referencia al formulario
                        var $f = $("#ingreso");
						var loading = $("#loading");

                        $f.parsley().destroy();

                        if($f.parsley().validate()) { 
                            loading.show().removeClass("hide");
                            var formData = new FormData($f[0]);

                            // Bloqueo de peticiones repetidas
                            if($f.data("locked") == undefined || !$f.data("locked")) {
                                $.ajax({  
                                    type: "POST",
                                    url: "../ingresos/eliminacionbd?id=" + $("#elmIngreso").attr("data-ingreso-id"),
                                    data: formData,
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    dataType: "json",

                                    beforeSend: function() { 
                                        $f.data("locked", true);
                                    },

                                    success: function(respuesta) {
                                        loading.hide();

                                        if(respuesta.type!='error')
                                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/ingresos/lista");

                                        notif({
                                            msg: respuesta.text,
                                            type: respuesta.type,
                                            position: "center",
                                            multiline: true
                                        });                        
                                    },

                                    error: function () {
                                        loading.hide();

                                        notif({
                                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                                            type: "error",
                                            position: "center",
                                            multiline: true
                                        });
                                    },

                                    complete: function() { 
                                        $f.data("locked", false);
                                    }               
                                });

                                return false;
                            }
                        }
                    }
                    
                else{

            	notif({
            		'type': 'error',
            		'msg': 'Proceso Cancelado :('
            	})

            }
        }
        
        notif_prompt({
        	"message": "Desa Eliminar esta Ingreso?",
        	"textaccept": "Sí, ¡deseo eliminarlo!",
        	"textcancel": "No, me equivoque",
        	"fullscreen": true,
        	"callback": salSopCallback
        })

        $(".notifit_prompt_input").removeAttr("type","text");
     	$(".notifit_prompt_input").attr("type","password");
    });
    // Resetear formulario de ingreso
    $("#cancelar").on("click", function () {
        $("#ingreso").each(function() {
            this.reset();
			$("#proveedor, #articulo").val(null).trigger("change");
			$("#ingreso").parsley().destroy();
            $("#tabLisIngresos tbody").empty();
            $("#lisIngresos").hide();
            arrArticulo = [];
        });

        return false;
    });

	// Función para eliminar un ingreso de la lista
	eliIngreso = function(ingreso) {
		$("#ingreso" + ingreso).remove();
		delete arrArticulo[$.inArray(ingreso.toString(), arrArticulo)];

		if($("#tabLisIngresos tbody tr").length == 0) {
			$("#articulo").val(null).trigger("change");
			$("#lisIngresos").hide();
		}
	}

	// Preparación de modal para ver detalle de proveedor
	verProveedor = function(proveedor) {
		var json = JSON.parse(proveedor);

		$("#modProveedor .modal-title").text("Proveedor: " + json.pro_nombre);
    	$("#codProveedor").val(json.pro_codigo).attr("readonly", "readonly");
    	$("#nombre").val(json.pro_nombre).attr("readonly", "readonly");
    	$("#perContacto").val(json.pro_per_contacto).attr("readonly", "readonly");
    	$("#telLocal").val(json.pro_tel_local).attr("readonly", "readonly");
    	$("#telCelular").val(json.pro_tel_celular).attr("readonly", "readonly");
    	$("#direccion").val(json.pro_direccion).attr("readonly", "readonly");
    	$("#corElectronico").val(json.pro_cor_electronico).attr("readonly", "readonly");
	    $("#regProveedor").hide();
	    $("#canProveedor").hide();
		$("#modProveedor").modal("show");
	}

	// Preparación de modal para ver detalle de artículo
	verArticulo = function(articulo, dolar) {
		var json = JSON.parse(articulo);

		$("#modArticulo .modal-title").text("Artículo: " + json.art_codigo);
		$("#codArticulo").val(json.art_codigo).attr("readonly", "readonly");
		$("#descripcion").val(json.art_descripcion).attr("readonly", "readonly");
		$("#cosUnitario").val(number_format(json.art_cos_unitario)).attr("readonly", "readonly");
		$("#preUnitario1").val(number_format(json.art_pre_unitario1)).attr("readonly", "readonly");
		$("#preUnitario2").val(number_format(json.art_pre_unitario2)).attr("readonly", "readonly");
		$("#preUnitario3").val(number_format(json.art_pre_unitario3)).attr("readonly", "readonly");
		$("#cosUniBs").val(number_format(json.art_cos_unitario * dolar)).attr("readonly", "readonly");
		$("#preUni1Bs").val(number_format(json.art_pre_unitario1 * dolar)).attr("readonly", "readonly");
		$("#preUni2Bs").val(number_format(json.art_pre_unitario2 * dolar)).attr("readonly", "readonly");
		$("#preUni3Bs").val(number_format(json.art_pre_unitario3 * dolar)).attr("readonly", "readonly");
		$("#invInicial").val(json.art_inv_inicial).attr("readonly", "readonly");
		$("#fecCreacion").val(json.art_fec_creacion).attr("readonly", "readonly");
		$("#stoMinimo").val(json.art_sto_minimo).attr("readonly", "readonly");
		$("#ubicacion").val(json.art_ubicacion).attr("readonly", "readonly");
		
		// Evitar carga de foto referencial en esta pantalla
		$("#canFotReferencial").click();

		if(json.art_fot_referencial != null) {
			$(".fileinput-filename").html(json.art_fot_referencial);
			$("#fileinput").removeClass("fileinput-new").addClass("fileinput-exists").find(".uneditable-input").addClass("disabled");
			$("#canFotReferencial, #btn-file").addClass("disabled");
			$("#divFotReferencial").show().removeClass("hide");
			$("#divFotReferencial img").attr("src", "/" + (window.location.pathname).split("/")[1] + "/" + (json.art_fot_referencial).replace("files/", "") + "?" + Math.random());
		} else {
			$("#fileinput").find(".uneditable-input").addClass("disabled");
			$("#btn-file").addClass("disabled");
			$("#divFotReferencial").hide();
		}

		$("#selFoto").on("click", function(e) {
			e.preventDefault();
		})

		// ******************************** //

		$("#regArticulo").hide();
		$("#canArticulo").hide();
    	$("#modArticulo").modal("show");
	}
});