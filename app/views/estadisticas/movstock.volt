<style>
	#chartdiv {
	  width: 100%;
	  /*min-height: 500px;*/
	  height: 500px;
	}
</style>

{{ assets.outputCss() }}

<div id="preloader">
    <div id="page-loader"></div>
</div>
<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="container-fluid">
				<div data-widget-group="group1">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-success">
								<div class="panel-heading">
									<h1 class="title c-w fz-24">Top 10 de Artículos con Movimiento de Stock</h1>
								</div>
								<div class="panel-body bc-g">
									<div class="col-md-2">
										<div class="row">
											<div class="col-sm-12 mb-md">
		                                        <label class="checkbox-inline icheck">

		                                    		{{ radio_field("seleccione", "id": "opcion1", "value": "mas", "checked": "checked") }}

		                                    		<span>Artículos con más Movimiento de Stock</span>
		                                		</label>
		                                	</div>
		                                </div>
										<div class="row">
											<div class="col-sm-12 mb-md">		                                	
		                                        <label class="checkbox-inline icheck">

		                                    		{{ radio_field("seleccione", "id": "opcion2", "value": "menos") }}

		                                    		<span>Artículos con menos Movimiento de Stock</span>
		                                		</label>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12 mb-md">
												<label for="fecInicio" class="control-label">Fecha de Inicio</label>

												{{ text_field("fecInicio", "class": "input-small form-control cur-pointer", "placeholder": "Ingrese Fecha") }}

											</div>
										</div>
										<div class="row">
											<div class="col-sm-12 mb-md">
												<label for="fecFin" class="control-label">Fecha de Fin</label>

												{{ text_field("fecFin", "class": "input-small form-control cur-pointer", "placeholder": "Ingrese Fecha") }}

											</div>
										</div>
										<div class="row">
											<div class="col-sm-12 mt-md mb-md">

												{{ submit_button("Procesar", "class": "btn btn-inverse btn", "id": "procesar", "style": "width: 100%", "data-articulos-offset": 0) }}

											</div>
										</div>
										<div class="row">
											<div class="col-sm-6 mt-md mb-md pr-n">

												{{ tag_html("button", ["type": "submit", "class": "btn btn-success disabled", "style": "width: 100%", "id": "anterior", "data-articulos-offset": 0]) }}
													<span class="ti ti-angle-double-left"></span> Ant.
												{{ tag_html_close("button") }}

											</div>
											<div class="col-sm-6 mt-md mb-md">

												{{ tag_html("button", ["type": "submit", "class": "btn btn-success", "style": "width: 100%", "id": "siguiente", "data-articulos-offset": 0]) }}
													Sig. <span class="ti ti-angle-double-right"></span>
												{{ tag_html_close("button") }}

											</div>
										</div>
									</div>
									<div class="col-md-10 fs-14">
										<div id="chartdiv" class="mb-md"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>

{{ assets.outputJs() }}