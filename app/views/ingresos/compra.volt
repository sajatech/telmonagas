{{ assets.outputCss() }}

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            {#<ol class="breadcrumb">                        
				<li>

					{{ link_to("inicio", "Inicio") }}

				</li>
				<li>

					{{ link_to("inventario/ingreso", "Inventario") }}

				</li>
				<li class="active">

					{{ link_to("inventario/ingreso", "Ingreso de Mercancía") }}

				</li>
            </ol>#}
            <div class="container-fluid">
				<div data-widget-group="group1">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-success">
								<div class="panel-heading">
									<h1 class="title c-w fz-24">

										{% if ingreso is defined %}
											{{ "Actualización de Ingreso" }}
										{% else %}
											{{ "Ingreso de Mercancía por Compra a Proveedor" }}
										{% endif %}

									</h1>
									<div class="panel-ctrls">

        								{% if funciones.in_array(1, priRol) %}
											{{ link_to("#", "<i class='ti ti-plus'></i><span> Agregar Artículo</span>", "id": "agrArticulo", "class": "mt-sm btn btn-default ctrls pull-right", false) }}
										{% endif %}

        								{% if funciones.in_array(6, priRol) %}
											{{ link_to("#", "<i class='ti ti-plus'></i><span> Agregar Proveedor</span>", "id": "agrProveedor", "class": "mt-sm btn btn-default ctrls pull-right mr-xs", false) }}
										{% endif %}
										
										{% if ingreso is defined and funciones.in_array(16, priRol) %}
											{{ link_to("ingresos/compra", "<i class='ti ti-plus'></i><span> Agregar Ingreso</span>", "class": "btn btn-default ctrls pull-right mt-sm mr-xs") }}
										{% endif %}

        								{% if funciones.in_array(17, priRol) %}
											{{ link_to("ingresos/lista", "<i class='ti ti-search'></i><span> Buscar Ingreso</span>", "class": "btn btn-default ctrls pull-right mt-sm mr-xs") }}
										{% endif %}

										{% if ingreso is defined and funciones.in_array(20, priRol) %}
											{{ link_to("#", "<i class='ti ti-printer'></i><span> Imprimir Soporte</span>", "onclick": "window.open('" ~ funciones.gethostname() ~ "ingresos/ingreso_mercancia/" ~ ingreso.id ~ "', 'Ingreso de Mercancía', 'width=240, height=600')", "class": "btn btn-default ctrls pull-right mt-sm mr-xs", false) }}
										{% endif %}

									</div>
								</div>
								<div class="panel-body bc-g">

									{{ form("autocomplete": "off", "id": "ingreso", "data-parsley-validate": "data-parsley-validate") }}

										<div class="row">
											<div class="col-sm-12 required">
												<h4 class="mb-md mt-n">Nombre o Código del Proveedor</h4>
											</div>
											<div class="col-sm-10 closest mb-md">
												<select name="proveedor" id="proveedor" required="required">
													<option value="">-- SELECCIONE --</option>

													{% for proveedor in proveedores %}

														<option value="{{ proveedor.id }}" {% if ingreso is defined and proveedor.id == ingreso.pro_id %} selected="selected" {% endif %}>{{ proveedor.pro_nombre ~ " (" ~ proveedor.pro_codigo ~ ")" }}</option>

													{% endfor %}

												</select>
											</div>
											<div class="col-sm-2">

												{% if ingreso is defined and funciones.in_array(9, priRol) %}
													{% set addClass = "" %}
												{% else %}
													{% set addClass = "disabled" %}
												{% endif %}

												<a id="verProveedor" onclick="verProveedor('<?php echo htmlentities(json_encode($proIngreso)); ?>')" class="btn btn-inverse btn-label pull-right mb-md {{ addClass }}" style="width: 100%">
													<i class='ti ti-search'></i> Ver Detalle
												</a>

											</div>
										</div>
										<div class="row">
											<div class="col-sm-12 mb-md">
												<h4 class="mb-md mt-n">Observaciones del Ingreso al Inventario</h4>
												
												{{ text_area("observaciones", "class": "form-control", "placeholder": "Ingrese Observaciones del Ingreso al Inventario", "rows": 3, "value": ingreso is defined ? ingreso.ing_observaciones : null) }}

											</div>
										</div>
										<div class="row">
											<div class="col-sm-12 mb-md required">
												<h4 class="mb-md mt-n">Código o Descripción del Artículo</h4>
												<select name="articulo" id="articulo" {% if ingreso is not defined %} required="required" {% endif %} {% if ingreso is defined %} disabled="disabled" {% endif %}>
													<option value="">-- SELECCIONE --</option>

													{% for articulo in articulos %}

														<option value="{{ articulo.id }}">{{ articulo.art_codigo ~ " (" ~ articulo.art_descripcion ~ ")" }}</option>

													{% endfor %}

												</select>
											</div>
										</div>
										<span {% if ingreso is not defined %} class="hide" {% endif %} id="lisIngresos">
											<hr/>
											<h4 class="mt-xl">Lista de Ingresos</h4>
											<table cellpadding="0" cellspacing="0" border="0" class="table table-striped" id="tabLisIngresos">
											    <thead>
											        <tr class="info">
											            <th>Código de Artículo</th>
											            <th>Cantidad</th>
											            <th>Costo del Artículo</th>
											            <th>Costo Total</th>
											            <th>Acciones</th>
											        </tr>
											    </thead>
											    <tbody>

											    	{% if lisIngresos is defined %}
											    		{% set sumCosTotal = 0 %}
											    		{% for lisIng in lisIngresos  %}
											    			{% set cosTotal = lisIng.mov_cantidad * lisIng.art_cos_unitario %}
											    			{% set sumCosTotal = sumCosTotal + cosTotal %}

												    		<tr id="ingreso{{ lisIng.id }}">
												    			<td> {{ lisIng.art_codigo }} </td>
												    			<td> <input type="text" class="form-control" readonly="readonly" value="{{ lisIng.mov_cantidad }}"/> </td>
												    			<td> {{ "$ " ~ funciones.number_format(lisIng.art_cos_unitario) ~ " / Bs. " ~ funciones.number_format(lisIng.art_cos_unitario * dolar) }} </td>
												    			<td> {{ "$ " ~ funciones.number_format(cosTotal) ~ " / Bs. " ~ funciones.number_format(cosTotal * dolar) }} </td>
												    			<td> 
																	<a class="btn btn-md btn-primary btn-label" onclick="verArticulo('<?php echo htmlentities(json_encode($lisIng)); ?>', '<?php echo $dolar; ?>')">
																		<i class="ti ti-search"></i> Ver Artículo</a>
																	</td>
																</td>
												    		</tr>

												    	{% endfor %}
											    	{% endif %}

											    </tbody>
											</table>
											<div align="right">
											  <b>
											    
											    {{ "Total de Costos: " }} 

											    <span id="sumCosTotal">

											    	{% if ingreso is defined %}
											    		{{ "$ " ~ funciones.number_format(sumCosTotal) ~ " / Bs. " ~ funciones.number_format(sumCosTotal * dolar) }}
											    	{% else %}
											    		{{ "$ 0,00 / Bs. 0,00" }}
											    	{% endif %}

											    </span>
											  </b>
											</div>
										</span>

									{{ end_form() }}

								</div>
								<div class="panel-footer">
									<div class="clearfix">
										<span id="loading" class="loading hide">

											{{ image("img/loading.gif", "width": "32px") }}

										</span>

								    	{% if ingreso is defined and funciones.in_array(8, priRol) %}
											{{ submit_button("Actualizar", "class": "btn btn-inverse", "id": "actualizar", "data-ingreso-id": ingreso.id) }}

											{% if funciones.in_array(57, priRol) %}

											{{ submit_button("Eliminar", "class": "btn btn-warning bc-e51", "id": "elmIngreso", "data-ingreso-id": ingreso.id) }}

											{% endif %}

										{% else %}
											{% if ingreso is not defined %}
												{{ submit_button("Registrar", "class": "btn btn-inverse", "id": "registrar") }}
												{{ submit_button("Cancelar", "class": "btn btn-default", "id": "cancelar") }}
											{% endif %}
										{% endif %}



									</div>	
								</div>								
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>

{# Modal de proveedor #}
<div class="modal fade" id="modProveedor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bc-8bc">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h2 class="modal-title c-w fz-24"></h2>
			</div>
			<div class="modal-body bc-g">

				{{ partial("partials/proveedores") }}

			</div>
			<div class="modal-footer">
				<span id="loaProveedor" class="hide loading">

					{{ image("img/loading.gif", "width": "32px") }}

				</span>
				<button type="button" class="btn btn-inverse" id="regProveedor">Registrar</button>
				<button type="button" class="btn btn-default" id="canProveedor">Cancelar</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar">Cerrar</button>
			</div>
		</div>
	</div>
</div>

{# ******************************** #}

{# Modal de artículo #}
<div class="modal fade" id="modArticulo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bc-8bc">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h2 class="modal-title c-w fz-24">Registro de artículo</h2>
			</div>
			<div class="modal-body bc-g">

				{{ partial("partials/articulos") }}

			</div>
			<div class="modal-footer">
				<span id="loaArticulo" class="hide loading">

					{{ image("img/loading.gif", "width": "32px") }}

				</span>
				<button type="button" class="btn btn-inverse" id="regArticulo">Registrar</button>
				<button type="button" class="btn btn-default" id="canArticulo">Cancelar</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar2">Cerrar</button>
			</div>
		</div>
	</div>
</div>

{# ******************************** #}

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
        errorsContainer: function(el) {
            return el.$element.closest(".closest");
        },

      	errorsWrapper: "<span class='help-block'></span>", 
      	errorTemplate: "<span></span>"
    };
</script>