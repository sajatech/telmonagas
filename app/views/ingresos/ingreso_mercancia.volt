<style type="text/css">
    body {
        background-color: white;
        font-family: SourceSansPro;
        color: #000;
        font-size: 18px;
    }

    .tg {
        border-collapse: collapse;
        border-spacing: 0;
        margin: 0px auto;
    }

    .tg td {
        font-family: SourceSansPro;
        padding: 12px;
        border-style: solid;
        border-width: 1px;
        overflow: hidden;
        word-break: normal;
        border-color: #eeeeee;
    }

    .tg .tg-yw4l { 
        vertical-align: top;
    }

    .label { 
        text-transform: uppercase; 
        border: none;
        color: #000000;  
        font-family: SourceSansPro;
    }

    .titulo {
        margin-top: 4px;
        margin-bottom: 4px;
        font-family: SourceSansProBold;
        font-size: 24px;
        color: #000000;
        text-align: center;
    }

    .legend {
        margin-bottom: 4px;
        margin-top: 4px;
        font-family: SourceSansProBold;
    }

    .fs-24 {
        font-size: 24px;
    }

    .total {
        color: #000; 
        margin-top: 4px; 
        margin-bottom: 4px;  
    }
</style>

<div style="position: absolute">
    TELECOMUNICACIONES MONAGAS<br/>
    R.I.F. J-40458220-7<br/>
</div>
<div style="text-align: right; margin-bottom: 48px">
    <div style="height: 20px"></div>

    {{ image("img/logo.png", "height": "27px") }}

</div>
<div class="titulo">
    <b>SOPORTE DE INGRESO DE MERCANCÍA<br/></b>
</div>
<table width="100%" style="border: none !important">
    <tr>
        <td colspan="6">
            <span class="fs-24">Código</span>
            <span>

                {{ funciones.str_pad(ingMercancia[0].id) }}

            </span>
        </td>
        <td colspan="6" align="right">
            <span class="fs-24">Fecha</span>
            <span>

                {{ funciones.cambiaf_a_normal(ingMercancia[0].ing_fec_creacion) }}

            </span>
        </td>
    </tr>
</table>
<div class="legend" style="border-left: 0; border-right: 0; background-color: #b2ebf2" align="center">
    <b>DATOS DEL PROVEEDOR</b>
</div>
<table class="tg" width="100%">
    <tr>
        <td class="tg-yw4l" colspan="6" style="border-left: 0" height="64px">
            <div class="label">Código de Proveedor</div>

            {{ ingMercancia[0].pro_codigo }}

        </td>
        <td class="tg-yw4l" colspan="6" height="64px" style="border-right: 0">
            <div class="label">Nombre</div>

            {{ ingMercancia[0].pro_nombre }}

        </td>
    </tr>
    <tr>
        <td class="tg-yw4l" colspan="6" style="border-left: 0; border-right: 0" height="64px">
            <div class="label">Persona de Contacto</div>

            {{ ingMercancia[0].pro_per_contacto }}

        </td>
        <td class="tg-yw4l" colspan="3" height="32px">
            <div class="label">Teléfono Celular</div>

            {{ ingMercancia[0].pro_tel_celular }}        

        </td>
        <td class="tg-yw4l" colspan="3" height="32px" style="border-left: 0; border-right: 0">
            <div class="label">Correo Electrónico</div>

            {{ ingMercancia[0].pro_cor_electronico }}

        </td>
    </tr>
</table>

{% if ingMercancia[0].ing_observaciones|length > 0 %}

    <div class="legend" style="border-left: 0; border-right: 0; background-color: #b2ebf2" align="center">
        <b>OBSERVACIONES DEL INGRESO AL INVENTARIO</b>
    </div>
    <table class="tg" width="100%">
        <tr>
            <td class="tg-yw4l" colspan="6" style="border-left: 0; border-right: 0" height="64px">

                {{ ingMercancia[0].ing_observaciones }}

            </td>
        </tr>
    </table>

{% endif %}

<div class="legend" style="border-left: 0; border-right: 0; background-color: #b2ebf2" align="center">
    <b>LISTA DE INGRESOS</b>
</div>
<table class="tg" width="100%">

    {% set sumCosTotal = 0 %}
    {% for ingreso in lisIngresos %}
        {% set cosTotal = ingreso.art_cos_unitario * ingreso.mov_cantidad %}

        <tr>
            <td class="tg-yw4l first" colspan="4" style="border-left: 0" height="32px">
                <div class="label">Código de Artículo</div>

                {{ ingreso.art_codigo }}

            </td>
            <td class="tg-yw4l" colspan="3" height="32px" style="border-right: 0">
                <div class="label">Cantidad</div>

                {{ ingreso.mov_cantidad }}

            </td>
            <td class="tg-yw4l" colspan="1" height="32px" style="border-right: 0">
                <div class="label">Costo del Artículo</div>

                {{ "$ " ~ funciones.number_format(ingreso.art_cos_unitario) ~ " / Bs. " ~ funciones.number_format(ingreso.art_cos_unitario * dolar) }}

            </td>
            <td class="tg-yw4l" colspan="1" height="32px" style="border-right: 0">
                <div class="label">Costo Total</div>

                {{ "$ " ~ funciones.number_format(cosTotal) ~ " / Bs. " ~ funciones.number_format(cosTotal * dolar) }}

            </td>
        </tr>

        {% set sumCosTotal = sumCosTotal + cosTotal %}
    {% endfor %}

</table>
<div class="total fs-24" align="right">
  <b>
    
    {{ "Total de Costos: $ " ~ funciones.number_format(sumCosTotal) ~ " / Bs. " ~ funciones.number_format(sumCosTotal * dolar) }}

  </b>
</div>
{#<div align="right" style="margin-top: 36px">
    
    {{ "<b>Generado Por:</b> " ~ ingMercancia[0].usu_nombre }}

</div>
<div align="right">
    
    {{ "Recibido Por: JORGE SALAS" }}

</div>#}