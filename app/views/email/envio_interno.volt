<h1 style="color: blue">RUNDO</h1>

<div style="margin-bottom: 18px; color: blue">
	Sr(es): {{ nombre }}<br/>
</div>

<div style="margin-bottom: 18px; color: blue">
	Le notificamos que le hemos realizado un envío el día: {{ fecha }}. Por medio de la Empresa: {{ empresa }} , con el numero de Guia: {{ guia }}. <br><br>

	Observaciones: {{ detalle }} <br><br>

	Monto de la Venta: {{ monto }} <br><br>
</div>
<hr/>