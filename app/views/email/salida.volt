<h1 style="color: red">RUNDO</h1>

<div style="margin-bottom: 18px; color: red">
	APRECIADO CLIENTE:<br/>
	¡Gracias por su compra!
</div>

<div style="margin-bottom: 18px; color: red">
	Su pedido lo hemos procesado. En las próximas horas hábiles laborables de la empresa de transporte de encomiendas que usted eligió, le será enviado y usted nuevamente será notificado a través de su correo electrónico con detalles de la compra y Nro. de guía.
</div>

<div style="margin-bottom: 18px; color: red">

	{% if lisPagos is defined %}
		
			{% for lisPag in lisPagos  %}
				{% if lisPag.pag_moneda == "BOLÍVARES" %}
				{% set moneda = "Bs. " %}
				{% else %}
				{% set moneda = "$ " %}
				{% endif %}

				{% if lisPag.pag_verificado == true %}
				{% set verifica = "SI" %}
				{% else %}
				{% set verifica = "NO" %}
				{% endif %}

			<br/>
			<b>{{ lisPag.ban_nombre }} | {{ lisPag.pag_num_referencia }} | MONTO: {{ moneda ~ funciones.number_format(lisPag.pag_importe) }} | VERIFICADO: {{ verifica }} | FECHA: {{ funciones.cambiaf_a_normal(lisPag.pag_fecha) }}</b>

			<br/>
<!-- 			<tr>
				<td> {{ lisPag.ban_nombre }} </td>
				<td> {{ lisPag.pag_num_referencia }} </td>
				<td> MONTO: {{ moneda ~ funciones.number_format(lisPag.pag_importe) }} </td>

			{% if lisPag.pag_verificado == true %}
				{% set verifica = "SI" %}
			{% else %}
				{% set verifica = "NO" %}
			{% endif %}

				<td> VERIFICADO: {{ verifica }} </td>
				<td> FECHA: {{ funciones.cambiaf_a_normal(lisPag.pag_fecha) }} </td>
			</tr> -->

			{% endfor %}
	{% endif %}
</div>

<div style="margin-bottom: 18px; color: red">
	Apreciado cliente, Gracias por tu compra, te invito a conocer otros de mis productos haciendo click en este link <a href="http://rundo.com.ve/repuestos-accesorios.html">http://rundo.com.ve/repuestos-accesorios.html</a>.
</div>

<div style="margin-bottom: 18px; color: red">
	<b>Email:</b> oficina.tcm@gmail.com<br/>
	(291) 400-8498 / (291) 642-6828
</div>

<hr/>
<div style="margin-bottom: 18px; color: red">
{{ salMercancia }}
</div>
{#<h1>TOTAL: {{ "Bs. " ~ total }}</h1>#}