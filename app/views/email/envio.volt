<h1 style="color: green">RUNDO</h1>

<div style="margin-bottom: 18px; color: green">
	Sr(es): {{ nombre }}<br/>
</div>

<div style="margin-bottom: 18px; color: green">
	Le notificamos que le hemos realizado un envío el día: {{ fecha }}. Por medio de la Empresa: {{ empresa }} , con el numero de Guia: {{ guia }}. <br><br>

	Observaciones: {{ detalle }} <br><br>
</div>

<div style="margin-bottom: 18px; color: green">
	Apreciado cliente, Gracias por tu compra, te invito a conocer otros de mis productos haciendo click en este link <a href="http://rundo.com.ve/repuestos-accesorios.html">http://rundo.com.ve/repuestos-accesorios.html</a>.
</div>

<div style="margin-bottom: 18px; color: green">
	<b>Email:</b> oficina.tcm@gmail.com<br/>
	(291) 400-8498 / (291) 642-6828
</div>

<hr/>