<style type="text/css">
    body {
        background-color: white;
        font-family: SourceSansPro;
        color: #000;
        font-size: 16px;
    }

    .tg {
        border-collapse: collapse;
        border-spacing: 0;
        margin: 0px auto;
    }

    .tg td {
        font-family: SourceSansPro;
        padding: 12px;
        border-style: solid;
        border-width: 1px;
        overflow: hidden;
        word-break: normal;
        border-color: #eeeeee;
    }

    .tg .tg-yw4l { 
        vertical-align: top;
    }

    .label { 
        text-transform: uppercase; 
        border: none;
        color: #000000;  
        font-family: SourceSansPro;
    }

    .titulo {
        margin-top: 4px;
        margin-bottom: 4px;
        font-family: SourceSansProBold;
        font-size: 24px;
        color: #000000;
        text-align: center;
    }

    .legend {
        margin-bottom: 4px;
        margin-top: 4px;
        font-family: SourceSansProBold;
    }

    .fs-12 {
        font-size: 12px;
    }

    .fs-24 {
        font-size: 24px;
    }

    .total {
        color: #000; 
        margin-top: 4px; 
        margin-bottom: 4px;  
    }
</style>

<div style="position: absolute" class="titulo">
    INVERSIONES RUNDO / TELECOMUNICACIONES MONAGAS<br/>
    <br/>
</div>
<div style="text-align: right; margin-bottom: 24px">
    <div style="height: 20px"></div>

</div><br/>
<div class="titulo">
    <b>CÁLCULO DE PRECIOS DE IMPORTACIÓN<br/></b>
</div>
<table width="100%" style="border: none !important">
    <tr>
        <td colspan="6">
            <span class="fs-24">Tasa Dolar</span>
            <span>

                {{ funciones.number_format(factura.fac_tasa_dolar) }} Bs.

            </span>
        </td>
        <td colspan="6" align="right">
            <span class="fs-24">Fecha</span>
            <span>

                {{ funciones.cambiaf_a_normal(factura.fac_fecha) }}

            </span>
        </td>
    </tr>
</table>
<div class="legend" style="border-left: 0; border-right: 0; background-color: #b2ebf2" align="center">
    <b>DATOS DE LA FACTURA</b>
</div>
<table class="tg" width="100%">
    <tr>
        <td class="tg-yw4l" colspan="6" style="border-left: 0" height="64px">
            <div class="label">Número de Factura</div>

            {{ factura.fac_numero }}

        </td>
        <td class="tg-yw4l" colspan="6" height="64px" style="border-right: 0">
            <div class="label">Monto Factura En $</div>

            {{ funciones.number_format(factura.fac_monto) }}

        </td>
        <td class="tg-yw4l" colspan="6" style="border-left: 0" height="32px">
            <div class="label">Costo de Flete ó Servicio P.A.P $</div>

            {{ funciones.number_format(factura.fac_monto_flete) }}     

        </td>
    </tr>
    <tr>
        <td class="tg-yw4l" colspan="6" style="border-left: 0" height="32px">
            <div class="label">Peso Según Fact. Kg.</div>

            {{ funciones.number_format(factura.fac_peso) }}       

        </td>
        <td class="tg-yw4l" colspan="6" height="32px" style="border-left: 0; border-right: 0">
            <div class="label">Cantidad de Articulos</div>

            {{ factura.fac_bultos }}  

        </td>
        <td class="tg-yw4l" colspan="6" height="32px" style="border-left: 0; border-right: 0">
            <div class="label">Peso Real en Kg.</div>

            {{ funciones.number_format(factura.fac_peso_real) }}  
            <div class="label">Embalaje en Kg.</div>

            {{ funciones.number_format(factura.fac_peso - factura.fac_peso_real) }}  

        </td>

    </tr>
    <tr>
        <td class="tg-yw4l" colspan="6" height="32px" style="border-left: 0; border-right: 0">
            <div class="label">Aduana-Impuesto Bs.</div>

            {{ funciones.number_format(factura.fac_aduana_bs) }}  

        </td>
        <td class="tg-yw4l" colspan="6" style="border-left: 0" height="32px">
            <div class="label">Transporte Nacional Bs.</div>

            {{ funciones.number_format(factura.fac_transporte_nac) }}       

        </td>
        <td class="tg-yw4l" colspan="6" height="32px" style="border-left: 0; border-right: 0">
            <div class="label">Gastos Operativos</div>

            {{ funciones.number_format(factura.fac_gastos_ope) }}  

        </td>
    </tr>
    <tr>
        <td class="tg-yw4l" colspan="6" height="32px" style="border-left: 0; border-right: 0">
            <div class="label">Comisión Banco por Transf. $</div>

            {{ funciones.number_format(factura.fac_imp1) }}  

        </td>
        <td class="tg-yw4l" colspan="6" style="border-left: 0" height="32px">
            <div class="label">Agente Aduanal Bs.</div>

            {{ funciones.number_format(factura.fac_imp2) }}       

        </td>
        <td class="tg-yw4l" colspan="6" height="32px" style="border-left: 0; border-right: 0">
            <div class="label">Otros Gastos en Bs.</div>

            {{ funciones.number_format(factura.fac_imp3) }}  

        </td>

    </tr>
</table>

{% if factura.fac_observacion|length > 0 %}

    <div class="legend" style="border-left: 0; border-right: 0; background-color: #b2ebf2" align="center">
        <b>OBSERVACIONES</b>
    </div>
    <table class="tg" width="100%">
        <tr>
            <td class="tg-yw4l" colspan="6" style="border-left: 0; border-right: 0" height="64px">

                {{ factura.fac_observacion }}

            </td>
        </tr>
    </table>

{% endif %}

<div class="legend" style="border-left: 0; border-right: 0; background-color: #b2ebf2" align="center">
    <b>ITEM CALCULADOS</b>
</div>
<table class="tg" width="100%">
        <tr>
            <td class="tg-yw4l first" colspan="4" style="border-left: 0" height="32px">
                <div class="label">Código</div>
            </td>
            <td class="tg-yw4l" colspan="3" height="32px" style="border-right: 0">
                <div class="label">Descripción</div>
            </td>
            <td class="tg-yw4l" colspan="1" height="32px" style="border-right: 0">
                <div class="label">Cantidad</div>
            </td>
            <td class="tg-yw4l" colspan="1" height="32px" style="border-right: 0">
                <div class="label">Peso en Gramos</div>
            </td>
            <td class="tg-yw4l" colspan="1" height="32px" style="border-right: 0">
                <div class="label">Costo en $</div>
            </td>
            <td class="tg-yw4l" colspan="1" height="32px" style="border-right: 0">
                <div class="label">Monto PPG $</div>
            </td>
            <td class="tg-yw4l" colspan="1" height="32px" style="border-right: 0">
                <div class="label" style="color:green">Costo Real $</div>
            </td>
            <td class="tg-yw4l " colspan="1" height="32px" style="border-right: 0">
                <div class="label" style="color:green">% Incremento</div>
            </td>
        </tr>
        {% if lisItems is defined %}

        {% for lisIt in lisItems  %}

        <tr>
            <td class="tg-yw4l first" colspan="4" style="border-left: 0" height="32px">{{ lisIt.item_codigo }}</td>
            <td class="tg-yw4l fs-12" colspan="3" height="32px" style="border-right: 0">{{ lisIt.item_descripcion }}</td>
            <td class="tg-yw4l" colspan="1" height="32px" style="border-right: 0">{{ lisIt.item_cantidad }}</td>
            <td class="tg-yw4l" colspan="1" height="32px" style="border-right: 0">{{ funciones.number_format(lisIt.item_peso) }}</td>
            <td class="tg-yw4l" colspan="1" height="32px" style="border-right: 0">{{ funciones.number_format(lisIt.item_costo) }}</td>
            <td class="tg-yw4l" colspan="1" height="32px" style="border-right: 0">{{ funciones.number_format(lisIt.item_costo * (lisIt.item_gpg / 100)) }}</td>
            <td class="tg-yw4l" colspan="1" height="32px" style="border-right: 0; color:green">{{ funciones.number_format(lisIt.item_precio) }}</td>
            <td class="tg-yw4l" colspan="1" height="32px" style="border-right: 0; color:green">{{ funciones.number_format(((lisIt.item_precio - lisIt.item_costo) * 100) / lisIt.item_costo) }}</td>
        </tr>

        {% endfor %}
        {% endif %}

</table>
<div style="position: absolute; bottom: 10px;"> 
<table width="100%">
    <tr>
        <td colspan="2"><hr/></td>
    </tr>
    <tr>
        <td class="fs-12"> {{ date("d/m/Y") }} - Telecomunicaciones Monagas, C.A. | R.I.F. J-40458220-7. Copyright {{ date("Y") }}. Control de Productos: Rundo / Telecomunicaciones Monagas. </td>
    </tr>
</table>
</div>