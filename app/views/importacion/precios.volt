{{ assets.outputCss() }}

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            {#<ol class="breadcrumb">                        
				<li>

					{{ link_to("inicio", "Inicio") }}

				</li>
				<li>

					{{ link_to("inventario/ingreso", "Inventario") }}

				</li>
				<li class="active">

					{{ link_to("inventario/ingreso", "Ingreso de Mercancía") }}

				</li>
            </ol>#}
            <div class="container-fluid">
				<div data-widget-group="group1">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-success">
								<div class="panel-heading">
									<h1 class="title c-w fz-24">

										{{ "Cálculo de Precios Importación" }}

									</h1>
									<div class="panel-ctrls">

        								{% if funciones.in_array(74, priRol) %}
											{{ link_to("lista", "<i class='ti ti-search'></i><span> Buscar Facturas</span>", "id": "agrCliente", "class": "mt-sm btn btn-default ctrls pull-right mr-xs", false) }}
										{% endif %}

									</div>
								</div>
								<div class="panel-body bc-g">

									{{ partial("partials/datos_factura") }}

									{{ partial("partials/items_factura") }}

								</div>
								

								<div class="panel-footer">
									<div class="clearfix">
									{% if factura is not defined %}
										<span id="loading" class="loading hide">

											{{ image("img/loading.gif", "width": "32px") }}

										</span>

												{{ submit_button("Generar Calculos", "class": "btn btn-inverse", "id": "registrar") }}
												{{ submit_button("Cancelar", "class": "btn btn-default", "id": "cancelar") }}

									{% else %}	
												{{ submit_button("Generar PDF", "class": "btn btn-inverse", "id": "generar_pdf", "onclick": "window.open('" ~ funciones.gethostname() ~ "importacion/factura_pdf/" ~ factura.id ~ "', 'Calculador de Precios', 'width=240, height=600')") }}

												{{ submit_button("Eliminar", "class": "btn btn-warning bc-e51", "id": "elmFactura", "data-factura-id": factura.id) }}

									{% endif %}	
												
									</div>	
								</div>				
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
        errorsContainer: function(el) {
            return el.$element.closest(".closest");
        },

      	errorsWrapper: "<span class='help-block'></span>", 
      	errorTemplate: "<span></span>"
    };
</script>