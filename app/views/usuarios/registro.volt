{{ assets.outputCss() }}

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="container-fluid">
				<div data-widget-group="group1">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-success">
								<div class="panel-heading">
									<h1 class="title c-w fz-24">Registro de Usuarios</h1>
									<div class="panel-ctrls">

                						{% if funciones.in_array(27, priRol) %}
											{{ link_to("usuarios/lista", "<i class='ti ti-search'></i><span> Buscar Usuario</span>", "class": "btn btn-default ctrls pull-left mt-sm") }}
										{% endif %}

									</div>
								</div>
								<div class="panel-body bc-g">

									{{ partial("partials/usuarios") }}

								</div>
								<div class="panel-footer">
									<div class="clearfix">
										<span id="loaUsuario" class="loading hide">

											{{ image("img/loading.gif", "width": "32px") }}

										</span>

										{{ submit_button("Registrar", "class": "btn btn-inverse", "id": "regUsuario") }}
										{{ submit_button("Cancelar", "class": "btn btn-default", "id": "canUsuario") }}

									</div>	
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
        errorsContainer: function(el) {
            return el.$element.closest(".closest");
        },
        
		errorsWrapper: "<span class='help-block'></span>", 
		errorTemplate: "<span></span>"
    };
</script>