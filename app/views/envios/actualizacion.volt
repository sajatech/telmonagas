{{ assets.outputCss() }}

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="container-fluid">
				<div data-widget-group="group1">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-success">
								<div class="panel-heading">
									<h1 class="title c-w fz-24">

										{{ "Reenvío de Correo" }}

									</h1>
									<div class="panel-ctrls">

        								{% if funciones.in_array(11, priRol) %}
											{{ link_to("lista", "<i class='ti ti-search'></i><span> Buscar Envíos</span>", "class": "mt-sm btn btn-default ctrls pull-right mr-xs", false) }}
										{% endif %}

									</div>
								</div>
								<div class="panel-body bc-g">

									{{ partial("partials/envios") }}

								</div>
								<div class="panel-footer">
									<div class="clearfix">
										<span id="loading" class="loading hide">

											{{ image("img/loading.gif", "width": "32px") }}

										</span>


											{{ submit_button("Reenviar", "class": "btn btn-inverse", "id": "actualizar", "data-envio-id": envio.id) }}

											{% if funciones.in_array(55, priRol) %}

											{{ submit_button("Eliminar", "class": "btn btn-warning bc-e51", "id": "elmEnvio", "data-envio-id": envio.id) }}

											{% endif %}


									</div>	
								</div>								
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
        errorsContainer: function(el) {
            return el.$element.closest(".closest");
        },

      	errorsWrapper: "<span class='help-block'></span>", 
      	errorTemplate: "<span></span>"
    };
</script>