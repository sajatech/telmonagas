{{ assets.outputCss() }}

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="container-fluid">
                <div data-widget-group="group1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <h1 class="title c-w fz-24">Listados de Promociones</h1>
                                    <div class="panel-ctrls"></div>
                                </div>
                                <div class="panel-body no-padding">
                                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="proveedores">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Asunto</th>
                                                <th>Fecha de Programacion</th>
                                                <th>Tiempo de Envio</th>
                                                <th>Total Enviados</th>
                                                <th>Estatus</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="panel-footer"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{# Modal de articulos #}
<div class="modal fade" id="modPromo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bc-8bc">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title c-w fz-24">Asunto Promoción:</h2>
            </div>
            <div class="modal-body bc-g">

            <table class="table table-striped table-bordered" cellspacing="0" width="100%" id="salidas">
                <thead>
                    <tr>
                        <th>Cuerpo del Correo</th>
                    </tr>
                </thead>
                <tbody id="tabla_modal">

                </tbody>
            </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar">Cerrar</button>
            </div>
        </div>
    </div>
</div>
{{ assets.outputJs() }}