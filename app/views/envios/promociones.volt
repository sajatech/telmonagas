{{ assets.outputCss() }}

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            {#<ol class="breadcrumb">                        
				<li>

					{{ link_to("inicio", "Inicio") }}

				</li>
				<li>

					{{ link_to("inventario/ingreso", "Inventario") }}

				</li>
				<li class="active">

					{{ link_to("inventario/ingreso", "Ingreso de Mercancía") }}

				</li>
            </ol>#}
            <div class="container-fluid">
				<div data-widget-group="group1">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-success">
								<div class="panel-heading">
									<h1 class="title c-w fz-24">

										{{ "Promociones" }}

									</h1>
									<div class="panel-ctrls">

        								
											{{ link_to("promocionesLista", "<i class='ti ti-search'></i><span> Buscar Promociones</span>", "id": "agrCliente", "class": "mt-sm btn btn-default ctrls pull-right mr-xs", false) }}
								

									</div>
								</div>
								<div class="panel-body bc-g">

									{{ partial("partials/promo") }}

								</div>
								<div class="panel-footer">
									<div class="clearfix">
										<span id="loading" class="loading hide">

											{{ image("img/loading.gif", "width": "32px") }}

										</span>
											{% if continuar is defined %}
												{{ submit_button("Continuar Envio", "class": "btn btn-inverse", "id": "continuar") }}
												<input type="hidden" id="id_promo" value="{{ id }}">
											{% else %}
												{{ submit_button("Enviar", "class": "btn btn-inverse", "id": "registrar") }}
												<input type="hidden" id="id_promo" value="0">
											{% endif %}
												{{ submit_button("Cancelar", "class": "btn btn-default", "id": "cancelar") }}
												
									</div>	
								</div>								
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
<div id="preloader-alt" class="hide">
    <div id="page-loader-alt"></div>
</div>
{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
        errorsContainer: function(el) {
            return el.$element.closest(".closest");
        },

      	errorsWrapper: "<span class='help-block'></span>", 
      	errorTemplate: "<span></span>"
    };
</script>