<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            {#<ol class="breadcrumb">                        
				<li>

					{{ link_to("inicio", "Inicio") }}

				</li>
				<li>

					{{ link_to("proveedores/registro", "Proveedores") }}

				</li>
				<li class="active">

					{{ link_to("proveedores/lista", "Actualización de Proveedores") }}

				</li>
            </ol>#}
            <div class="container-fluid">
				<div data-widget-group="group1">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-success">
								<div class="panel-heading">
									<h1 class="title c-w fz-24">Actualización de Proveedores</h1>
									<div class="panel-ctrls">

                						{% if funciones.in_array(7, priRol) %}
											{{ link_to("proveedores/lista", "<i class='ti ti-search'></i><span> Buscar Proveedor</span>", "class": "btn btn-default ctrls pull-left mt-sm mr-xs") }}
										{% endif %}

                						{% if funciones.in_array(6, priRol) %}
											{{ link_to("proveedores/registro", "<i class='ti ti-plus'></i><span> Agregar Proveedor</span>", "class": "btn btn-default ctrls pull-left mt-sm") }}
										{% endif %}

									</div>
								</div>
								<div class="panel-body bc-g">

									{{ partial("partials/proveedores") }}

								</div>

        						{% if funciones.in_array(8, priRol) %}

									<div class="panel-footer">
										<div class="clearfix">
											<span id="loaProveedor" class="loading hide">

												{{ image("img/loading.gif", "width": "32px") }}

											</span>

											{{ submit_button("Actualizar", "class": "btn btn-inverse btn", "id": "actProveedor", "data-proveedor-id": proveedor.id) }}

										</div>	
									</div>

								{% endif %}

							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
        errorsContainer: function(el) {
            return el.$element.closest(".closest");
        },

  		errorsWrapper: "<span class='help-block'></span>", 
  		errorTemplate: "<span></span>"
    };
</script>