{{ assets.outputCss() }}

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="container-fluid">
                <div data-widget-group="group1">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="ajaxSpinner" class="fs-16 text-center mt-md">

                                {{ image("img/loading.gif", "width": "32px") }}
                                {{ "Por favor, espere..." }}

                            </div>
                            <div class="panel panel-success hide" id="panGastos">
                                <div class="panel-heading">
                                    <h1 class="title c-w fz-24">Lista de Conceptos de Gastos</h1>
                                    <div class="panel-ctrls"></div>
                                </div>
                                <div class="panel-body no-padding">
                                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="categorias">
                                        <thead>
                                            <tr>
                                                <th>Código</th>
                                                <th>Fecha</th>
                                                <th>Nombre del Concepto</th>
                                                <th>Registrado por</th>
                                                <th>Estatus</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>                
                                <div class="panel-footer"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{# Modal de categorías de gastos #}
<div class="modal fade" id="modCategorias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bc-8bc">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title c-w fz-24"></h2>
            </div>
            <div class="modal-body bc-g">

                {{ form("autocomplete": "off", "id": "categoria", "data-parsley-validate": "data-parsley-validate") }}

                    <div class="row">
                        <div class="col-sm-12 closest mb-md">
                            <label for="nombre" class="control-label">Nombre</label>

                            {{ text_field("nombre", "class": "form-control", "placeholder": "Ingrese Nombre", "required": "required") }}

                        </div>
                    </div>

                {{ end_form() }}

            </div>
            <div class="modal-footer">
                <span id="loading" class="hide loading">

                    {{ image("img/loading.gif", "width": "32px") }}

                </span>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar">Cerrar</button>
                <button type="button" class="btn btn-inverse accion"></button>
            </div>
        </div>
    </div>
</div>

{# ******************************** #}

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
        errorsContainer: function(el) {
            return el.$element.closest(".closest");
        },

        errorsWrapper: "<span class='help-block'></span>",
        errorTemplate: "<span></span>"
    };
</script>