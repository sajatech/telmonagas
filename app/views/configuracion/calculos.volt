{{ assets.outputCss() }}

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            {#<ol class="breadcrumb">                        
				<li>

					{{ link_to("inicio", "Inicio") }}

				</li>
				<li>

					{{ link_to("clientes/lista", "Clientes") }}

				</li>
				<li class="active">

					{{ link_to("clientes/registro", "Registro de Clientes") }}

				</li>
            </ol>#}
            <div class="container-fluid">
				<div data-widget-group="group1">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-success">
								<div class="panel-heading">
									<h1 class="title c-w fz-24">Valores en Porcentaje para Cálculos de Precio</h1>
								</div>
								<div class="panel-body bc-g">

									{{ form("autocomplete": "off", "id": "forCalculo", "data-parsley-validate": "data-parsley-validate") }}

										<div class="row">
											<div class="col-sm-3 closest mb-md">
												<label for="preCifVenezuela" class="control-label required">% Gastos de Nacionalización</label>
												<div class="input-group">

													{{ text_field("preCifVenezuela", "class": "form-control", "placeholder": "0,00", "required": "required", "value": calculo is defined ? funciones.number_format(calculo.cal_pre_unitario1) : null, "data-parsley-valor": "data-parsley-valor") }}

													<span class="input-group-addon">%</span>
												</div>
											</div>
											<div class="col-sm-3 closest mb-md">
												<label for="preMayor" class="control-label required">% Precio al Mayor</label>
												<div class="input-group">

													{{ text_field("preMayor", "class": "form-control", "placeholder": "0,00", "required": "required", "value": calculo is defined ? funciones.number_format(calculo.cal_pre_unitario2) : null, "data-parsley-valor": "data-parsley-valor") }}

													<span class="input-group-addon">%</span>
												</div>
											</div>
											<div class="col-sm-3 closest mb-md">
												<label for="preDetal" class="control-label required">% Precio al Detal</label>
												<div class="input-group">

													{{ text_field("preDetal", "class": "form-control", "placeholder": "0,00", "required": "required", "value": calculo is defined ? funciones.number_format(calculo.cal_pre_unitario3) : null, "data-parsley-valor": "data-parsley-valor") }}

													<span class="input-group-addon">%</span>
												</div>
											</div>
											<div class="col-sm-3 closest mb-md">
												<label for="preDetal" class="control-label required">% Precio Mercado Libre</label>
												<div class="input-group">

													{{ text_field("merLibre", "class": "form-control", "placeholder": "0,00", "required": "required", "value": calculo is defined ? funciones.number_format(calculo.cal_mer_libre) : null, "data-parsley-valor": "data-parsley-valor") }}

													<span class="input-group-addon">%</span>
												</div>
											</div>
										</div>

									{{ end_form() }}
								
								</div>
								<div class="panel-footer">
									<div class="clearfix">
										<span id="loaCalculo" class="loading hide">

											{{ image("img/loading.gif", "width": "32px") }}

										</span>

										{{ submit_button("Actualizar", "class": "btn btn-inverse", "id": "actCalculo") }}
										{{ submit_button("Cancelar", "class": "btn btn-default", "id": "canCalculo") }}

									</div>	
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
        errorsContainer: function(el) {
            return el.$element.closest(".closest");
        },

  		errorsWrapper: "<span class='help-block'></span>", 
  		errorTemplate: "<span></span>"
    };
</script>