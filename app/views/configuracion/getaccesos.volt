<div class="row p-md">
	<div class="col-md-12">
		
		<table class="table table-striped table-bordered nowrap dt-responsive" cellspacing="0" width="100%" id="accesos">
			<thead>
				<tr>
					<th>ID </th>
					<th>Fecha</th>
					<th>Usuario</th>
					<th>Acciones</th>
					<th>Ip</th>
					<th>Sistema</th>
					<th>Locacion</th>
					
				</tr>
			</thead>
			<tbody>

				{% for acceso in accesos %}

					<tr>
						<td>{{ acceso.id }}</td>
						<td>{{ date("d/m/Y h:i:s a", strtotime(acceso.fecha)) }}</td>
						<td>{{ acceso.usu_nombre  }}</td>
						<td>{{ acceso.acc_accion  }}</td>
						<td>{{ acceso.acc_ip }}</td>
						<td>{{ acceso.acc_detalles }}</td>
						<td>{{ funciones.pais_ip(acceso.acc_ip) }}</td>
					</tr>

				{% endfor %}

			</tbody>
		</table>
	</div>
</div>