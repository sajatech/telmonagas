{{ assets.outputCss() }}

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="container-fluid">
                <div data-widget-group="group1">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="ajaxSpinner" class="fs-16 text-center mt-md">

                                {{ image("img/loading.gif", "width": "32px") }}
                                {{ "Por favor, espere..." }}

                            </div>
                            <div class="panel panel-success hide" id="panCuentas">
                                <div class="panel-heading">
                                    <h1 class="title c-w fz-24">Cuentas Bancarias para Pagos de Salidas de Mercancía</h1>
                                    <div class="panel-ctrls"></div>
                                </div>
                                <div class="panel-body no-padding">
                                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="cuentas">
                                        <thead>
                                            <tr>
                                                <th>Código</th>
                                                <th>Código de Banco</th>
                                                <th>Banco</th>
                                                <th>Número</th>
                                                <th>Tipo</th>
                                                <th>Estatus</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>                
                                <div class="panel-footer"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{# Modal de cuentas bancarias #}
<div class="modal fade" id="modCuentas" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bc-8bc">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title c-w fz-24"></h2>
            </div>
            <div class="modal-body bc-g">

                {{ form("autocomplete": "off", "id": "cuenta", "data-parsley-validate": "data-parsley-validate") }}

                    <div class="row">
                        <div class="col-sm-12 closest mb-md">
                            <label for="banco" class="control-label required">Banco</label>
                            <select name="banco" id="banco" required="required">
                                <option value="">-- SELECCIONE --</option>

                                {% for banco in bancos %}

                                    <option value="{{ banco.id }}" data-banco-codigo="{{ banco.ban_codigo }}">{{ banco.ban_nombre }}</option>

                                {% endfor %}

                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 closest mb-md">
                            <label for="numCuenta" class="control-label required">Número de Cuenta</label>

                            {{ text_field("numCuenta", "class": "form-control", "placeholder": "Ingrese Número de Cuenta", "required": "required", "data-parsley-minlength": 20, "data-parsley-maxlength": 20, "maxlength": 20) }}

                        </div>
                        <div class="col-sm-6 closest mb-md">
                            <label for="tipCuenta" class="control-label required">Tipo de Cuenta</label>

                            {{ select_static("tipCuenta", ["CUENTA CORRIENTE": "CUENTA CORRIENTE", "CUENTA DE AHORRO": "CUENTA DE AHORRO"], "useEmpty": true, "emptyText": "-- SELECCIONE --", "emptyValue": "", "required": "required") }}

                        </div>
                    </div>

                {{ end_form() }}

            </div>
            <div class="modal-footer">
                <span id="loaCuenta" class="hide loading">

                    {{ image("img/loading.gif", "width": "32px") }}

                </span>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar">Cerrar</button>
                <button type="button" class="btn btn-inverse accion"></button>
            </div>
        </div>
    </div>
</div>

{# ******************************** #}

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
        errorsContainer: function(el) {
            return el.$element.closest(".closest");
        },

        errorsWrapper: "<span class='help-block'></span>",
        errorTemplate: "<span></span>"
    };
</script>