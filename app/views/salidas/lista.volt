{{ assets.outputCss() }}

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="container-fluid">
                <div data-widget-group="group1">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="ajaxSpinner" class="fs-16 text-center mt-md">

                                {{ image("img/loading.gif", "width": "32px") }}
                                {{ "Por favor, espere..." }}

                            </div>
                            <div class="panel panel-success hide" id="panSalidas">
                                <div class="panel-heading">
                                    <h1 class="title c-w fz-24">Salidas de Mercancía Registradas</h1>
                                    <div class="panel-ctrls"></div>
                                </div>
                                <div class="panel-body no-padding">
                                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered nowrap dt-responsive" id="salidas">
                                        <thead>
                                            <tr>
                                                <th>Código</th>
                                                <th>Cliente</th>
                                                <th>Fecha</th>
                                                <th>Registrado por</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>                
                                <div class="panel-footer"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{ assets.outputJs() }}