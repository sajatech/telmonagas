<style type="text/css">
    body {
        background-color: white;
        font-family: SourceSansPro;
        color: #000;
        font-size: 18px;
    }

    .tg {
        border-collapse: collapse;
        border-spacing: 0;
        margin: 0px auto;
    }

    .tg td {
        font-family: SourceSansPro;
        padding: 12px;
        border-style: solid;
        border-width: 1px;
        overflow: hidden;
        word-break: normal;
        border-color: #eeeeee;
    }

    .tg .tg-yw4l { 
        vertical-align: top;
    }

    .label { 
        text-transform: uppercase; 
        border: none;
        color: #000000;  
        font-family: SourceSansPro;
    }

    .titulo {
        margin-top: 4px;
        margin-bottom: 4px;
        font-family: SourceSansProBold;
        font-size: 24px;
        color: #000000;
        text-align: center;
    }

    .legend {
        margin-bottom: 4px;
        margin-top: 4px;
        font-family: SourceSansProBold;
    }

    .fs-12 {
        font-size: 12px;
    }

    .fs-24 {
        font-size: 24px;
    }

    .total {
        color: #000; 
        margin-top: 4px; 
        margin-bottom: 4px;  
    }
</style>

<div style="position: absolute">
    INVERSIONES RUNDO / TELECOMUNICACIONES MONAGAS<br/>
    <br/>
</div>
<div style="text-align: right; margin-bottom: 24px">
    <div style="height: 20px"></div>

</div>
<div class="titulo">
    <b>SOPORTE DE SALIDA DE MERCANCÍA<br/></b>
</div>
<table width="100%" style="border: none !important">
    <tr>
        <td colspan="6">
            <span class="fs-24">Código</span>
            <span>

                {{ funciones.str_pad(salMercancia[0].id) }}

            </span>
        </td>
        <td colspan="6" align="right">
            <span class="fs-24">Fecha</span>
            <span>

                {{ funciones.cambiaf_a_normal(salMercancia[0].sal_fec_creacion) }}

            </span>
        </td>
    </tr>
</table>
<div class="legend" style="border-left: 0; border-right: 0; background-color: #b2ebf2" align="center">
    <b>DATOS DEL CLIENTE</b>
</div>
<table class="tg" width="100%">
    <tr>
        <td class="tg-yw4l" colspan="6" style="border-left: 0" height="64px">
            <div class="label">R.I.F. o Cédula de Identidad</div>

            {{ salMercancia[0].cli_codigo }}

        </td>
        <td class="tg-yw4l" colspan="6" height="64px" style="border-right: 0">
            <div class="label">Nombre</div>

            {{ salMercancia[0].cli_nombre }}

        </td>
    </tr>
    <tr>
        <td class="tg-yw4l" colspan="6" style="border-left: 0" height="32px">
            <div class="label">Teléfono Celular</div>

            {{ salMercancia[0].cli_tel_celular }}        

        </td>
        <td class="tg-yw4l" colspan="6" height="32px" style="border-left: 0; border-right: 0">
            <div class="label">Correo Electrónico</div>

            {{ salMercancia[0].cli_cor_electronico }}

        </td>
    </tr>
</table>

{% if salMercancia[0].sal_observaciones|length > 0 %}

    <div class="legend" style="border-left: 0; border-right: 0; background-color: #b2ebf2" align="center">
        <b>OBSERVACIONES DE LA SALIDA DEL INVENTARIO</b>
    </div>
    <table class="tg" width="100%">
        <tr>
            <td class="tg-yw4l" colspan="6" style="border-left: 0; border-right: 0" height="64px">

                {{ salMercancia[0].sal_observaciones }}

            </td>
        </tr>
    </table>

{% endif %}

<div class="legend" style="border-left: 0; border-right: 0; background-color: #b2ebf2" align="center">
    <b>LISTA DE SALIDAS</b>
</div>
<table class="tg" width="100%">
                <tr>
            <td class="tg-yw4l first" colspan="4" style="border-left: 0" height="32px">
                <div class="label">Código de Artículo</div>
            </td>
            <td class="tg-yw4l" colspan="3" height="32px" style="border-right: 0">
                <div class="label">Cantidad</div>
            </td>
            <td class="tg-yw4l" colspan="1" height="32px" style="border-right: 0">
                <div class="label">Precio del Artículo</div>
            </td>
            <td class="tg-yw4l" colspan="1" height="32px" style="border-right: 0">
                <div class="label">Precio Total</div>
            </td>
        </tr>
    {% set sumCosTotal = 0 %}
    {% for salida in lisSalidas %}
        {% set cosTotal = salida.mov_importe * salida.mov_cantidad %}

        <tr>
            <td class="tg-yw4l first" colspan="4" style="border-left: 0" height="32px">

                {{ salida.art_codigo }}

            </td>
            <td class="tg-yw4l" colspan="3" height="32px" style="border-right: 0">

                {{ salida.mov_cantidad }}

            </td>
            <td class="tg-yw4l" colspan="1" height="32px" style="border-right: 0">

                {{ "$ " ~ funciones.number_format(salida.mov_importe) ~ " / Bs. " ~ funciones.number_format(salida.mov_importe * dolar) }}

            </td>
            <td class="tg-yw4l" colspan="1" height="32px" style="border-right: 0">

                {{ "$ " ~ funciones.number_format(cosTotal) ~ " / Bs. " ~ funciones.number_format(cosTotal * dolar) }}

            </td>
        </tr>

        {% set sumCosTotal = sumCosTotal + cosTotal %}
    {% endfor %}

</table>
<div class="total fs-24" align="right">
  <b>
    
    {{ "Total de Precios: $ " ~ funciones.number_format(sumCosTotal) ~ " / Bs. " ~ funciones.number_format(sumCosTotal * dolar) }}

  </b>
</div>
<div align="right">
    
    {{ "<b>Generado Por:</b> " ~ salMercancia[0].usu_nombre }}

</div>
<div style="position: absolute; bottom: 10px;"> 
<table width="100%">
    <tr>
        <td colspan="2"><hr/></td>
    </tr>
    <tr>
        <td class="fs-12"> {{ date("d/m/Y") }} - Telecomunicaciones Monagas, C.A. | R.I.F. J-40458220-7. Copyright {{ date("Y") }}. Control de Productos: Rundo / Telecomunicaciones Monagas. </td>
    </tr>
</table>
</div>
<div style="page-break-before:always;">
<div class="legend" style="border-left: 0; border-right: 0; background-color: #b2ebf2; margin-top: 36px" align="center">
    <b>DATOS DEL ENVÍO</b>
</div>
<table class="tg" width="100%">
    <tr>
        <td class="tg-yw4l" colspan="6" style="border-left: 0" height="64px">
            <div class="label">R.I.F. o Cédula de Identidad</div>

            {{ salMercancia[0].cli_codigo }}

        </td>
        <td class="tg-yw4l" colspan="6" height="64px" style="border-right: 0">
            <div class="label">Nombre</div>

            {{ salMercancia[0].cli_nombre }}

        </td>
    </tr>
    <tr>
        <td class="tg-yw4l" colspan="6" rowspan="2" style="border-left: 0" height="32px">
            <div class="label">Dirección</div>

            {{ salMercancia[0].cli_direccion }}        

        </td>
        <td class="tg-yw4l" colspan="3" height="32px">
            <div class="label">Teléfono Local</div>

            {{ salMercancia[0].cli_tel_local }}        

        </td>
        <td class="tg-yw4l" colspan="3" style="border-right: 0" height="32px">
            <div class="label">Teléfono Celular</div>

            {{ salMercancia[0].cli_tel_celular }}        

        </td>
    </tr>
    <tr>
        <td class="tg-yw4l" colspan="6" height="32px" style="border-right: 0">
            <div class="label">Correo Electrónico</div>

            {{ salMercancia[0].cli_cor_electronico }}

        </td>
    </tr>
</table>
</div>