<style type="text/css">
    body {
        background-color: white;
        font-family: SourceSansPro;
        color: #000;
        font-size: 18px;
    }

    .tg {
        border-collapse: collapse;
        border-spacing: 0;
        margin: 0px auto;
    }

    .tg td {
        font-family: SourceSansPro;
        padding: 12px;
        border-style: solid;
        border-width: 1px;
        overflow: hidden;
        word-break: normal;
        border-color: #eeeeee;
    }

    .tg .tg-yw4l { 
        vertical-align: top;
    }

    .label { 
        text-transform: uppercase; 
        border: none;
        color: #000000;  
        font-family: SourceSansPro;
    }

    .titulo {
        margin-top: 4px;
        margin-bottom: 4px;
        font-family: SourceSansProBold;
        font-size: 24px;
        color: #000000;
        text-align: center;
    }

    .legend {
        margin-bottom: 4px;
        margin-top: 4px;
        font-family: SourceSansProBold;
    }

    .fs-12 {
        font-size: 12px;
    }

    .fs-24 {
        font-size: 24px;
    }

    .total {
        color: #000; 
        margin-top: 4px; 
        margin-bottom: 4px;  
    }
</style>

<div style="position: absolute">
    INVERSIONES RUNDO / TELECOMUNICACIONES MONAGAS<br/>
    <br/>
</div>
<div style="text-align: right; margin-bottom: 24px">
    <div style="height: 20px"></div>

</div>
<table width="100%" style="border: none !important">
    <tr>
        <td colspan="6">
            <span class="fs-20">Señores:</span>
            <span>

                {{ salMercancia[0].cli_nombre }}

            </span><br>
            <span class="fs-20">R.I.F. o Cédula de Identidad:</span>
            <span>

                {{ salMercancia[0].cli_codigo }}

            </span>
        </td>
        <td colspan="6" align="right">
            <span class="fs-24">Fecha</span>
            <span>

                {{ funciones.cambiaf_a_normal(salMercancia[0].sal_fec_creacion) }}

            </span>
        </td>
    </tr>
</table><br></br>
<div class="titulo">
    <b>NOTA DE ENTREGA Nº {{ funciones.str_pad(salMercancia[0].id) }}<br/></b>
</div><br></br>
<table class="tg" width="100%">

    {% set i = 1 %}
        <tr>
            <td width="10%">
                <div class="label">Item</div>
            </td>
            <td width="20%" class="tg-yw4l first" height="32px">
                <div class="label">REF</div>
            </td>
            <td width="15%" class="tg-yw4l" height="32px">
                <div class="label">Cantidad</div>
            </td>
            <td width="55%" class="tg-yw4l" height="32px">
                <div class="label">Descripción</div>
            </td>
        </tr>
    {% for salida in lisSalidas %}
        <tr>
            <td class="tg-yw4l" height="32px">

                {{ i }}

            </td>
            <td class="tg-yw4l" height="32px">

                {{ salida.art_codigo }}

            </td>
            <td class="tg-yw4l" height="32px" width="10%">

                {{ salida.mov_cantidad }}

            </td>
            <td class="tg-yw4l" height="32px">

                {{ salida.art_descripcion }}

            </td>
        </tr>

        {% set i = i + 1 %}
    {% endfor %}

</table>
<br></br>
<div style="position: absolute;
  bottom: 20px; left: 4%; right: 4%">
<table width="100%" class="tg">
    <tr>
        <td>ENTREGADO POR</td>
        <td>RECIBIDO POR</td>
    </tr>
    <tr>
        <td>

        </td>
        <td>

        </td>
    </tr>
</table>
<BR></BR>
<table width="100%">
    <tr>
        <td class="fs-12"> {{ date("d/m/Y") }} - Telecomunicaciones Monagas, C.A. | R.I.F. J-40458220-7. Copyright {{ date("Y") }}. Control de Productos: Rundo / Telecomunicaciones Monagas. </td>
    </tr>
</table>
</div>

<div style="page-break-before:always;">
<table width="100%" class="tg">
    <tr>
        <td style="background-color: #F27D80; font-size: 30px ">DATOS DE ENVIO<br>
            <span style="font-size:16px;">
                {% set fecha = funciones.explode(salMercancia[0].sal_fec_creacion,'-') %}
             <b>   {{ funciones.substr(fecha[0],2,2) }}{{ fecha[1] }}{{ funciones.str_pad_3(totalSalMes + 1) }}  </b>
            </span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 30px">
            <B>NOMBRE:</B> {{ nombre1 }}<br/>
            <B>CEDULA:</B> {{ cedIdentidad1 }}<br/>
            <B>DIRECCION / TLF:</B> {{ direccion1 }}
        </td>
    </tr><br><br><br><br>
    <tr>
        <td style="background-color: #72D1FA; font-size: 30px">DATOS DE QUIEN RECIBE<br>
            <span style="font-size:16px;">
              <b>  {{ funciones.substr(fecha[0],2,2) }}{{ fecha[1] }}{{ funciones.str_pad_3(totalSalMes + 1) }} </b>
            </span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 30px">
            <B>NOMBRE:</B> {{ nombre2 }}<br/>
            <B>CEDULA:</B> {{ cedIdentidad2 }}<br/>
            <B>DIRECCION / TLF:</B> {{ direccion2 }}
        </td>
    </tr>
</table>

</div>