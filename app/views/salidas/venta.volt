{{ assets.outputCss() }}

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="container-fluid">
				<div data-widget-group="group1">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-success">
								<div class="panel-heading">
									<h1 class="title c-w fz-24">

										{% if salida is defined %}
											{% if anulada is defined %}
												{{ "Revisión de Salida Anulada" }}
											{% else %}
												{{ "Actualización de Salida" }}
											{% endif %}
										{% else %}
											{{ "Salida de Mercancía por Venta a Cliente" }}
										{% endif %}

									</h1>
									<div class="panel-ctrls">

										{% if funciones.in_array(1, priRol) %}
											{{ link_to("#", "<i class='ti ti-plus'></i><span> Agregar Artículo</span>", "id": "agrArticulo", "class": "mt-sm btn btn-default ctrls pull-right", false) }}
										{% endif %}

        								{% if funciones.in_array(11, priRol) %}
											{{ link_to("#", "<i class='ti ti-plus'></i><span> Agregar Cliente</span>", "id": "agrCliente", "class": "mt-sm btn btn-default ctrls pull-right mr-xs", false) }}
										{% endif %}
										
										{% if salida is defined and funciones.in_array(21, priRol) %}
											{{ link_to("salidas/venta", "<i class='ti ti-plus'></i><span> Agregar Salida</span>", "class": "btn btn-default ctrls pull-right mt-sm mr-xs") }}
										{% endif %}

        								{% if funciones.in_array(22, priRol) %}
        									{% set busSalida = anulada is defined ? "salidas/anuladas" : "salidas/lista" %}
											{{ link_to(busSalida, "<i class='ti ti-search'></i><span> Buscar Salida</span>", "class": "btn btn-default ctrls pull-right mt-sm mr-xs") }}
										{% endif %}

										{% if salida is defined and funciones.in_array(25, priRol) and anulada is not defined %}
											{{ link_to("#", "<i class='ti ti-printer'></i><span> Imprimir Soporte</span>", "onclick": "window.open('" ~ funciones.gethostname() ~ "salidas/salida_mercancia/" ~ salida.id ~ "', 'Salida de Mercancía', 'width=240, height=600')", "class": "btn btn-default ctrls pull-right mt-sm mr-xs", false) }}
											{{ link_to("#", "<i class='ti ti-file'></i><span> Nota de Entrega</span>", "id": "notEntrega", "class": "btn btn-default ctrls pull-right mt-sm mr-xs", false) }}
										{% endif %}

									</div>
								</div>
								<div class="panel-body bc-g">

									{{ form("autocomplete": "off", "id": "salida", "data-parsley-validate": "data-parsley-validate", "data-parsley-excluded": "input[name=garantia], input[name=preVenta]") }}

										{% if salida is defined %}

											<div class="row">
												<div class="col-sm-12">
													<h4>Fecha: {{ funciones.cambiaf_a_normal(salida.sal_fec_creacion) }} ::: Código: {{ funciones.str_pad(salida.id) }}</h4>

													{% if salida.sal_garantia == 1 %}

														<h4 style="color: red"> {{ "SALIDA POR GARANTIA" }} </h4>

													{% endif %}

													{% if anulacion is defined %}

														<h4 style="color: red"> {{ "SALIDA ANULADA: " ~ anulacion }} </h4>

													{% endif %}
													
												</div>
											</div>

										{% endif %}

										<h3 class="mt-xs">Datos de la Venta de Artículos</h3>
										<hr class="mb-26 mt-n"/>
										<div class="row">
											<div class="col-sm-12 required">
												<h4 class="mb-md mt-n">Nombre o Código del Cliente</h4>
											</div>
											<div class="col-sm-10 closest mb-md">
												<select name="cliente" id="cliente" required="required">
													<option value="">-- SELECCIONE --</option>

													{% for cliente in clientes %}

														<option value="{{ cliente.id }}" {% if salida is defined and cliente.id == salida.cli_id %} selected="selected" {% endif %}>{{ cliente.cli_nombre ~ " (" ~ cliente.cli_codigo ~ ")" }}</option>

													{% endfor %}

												</select>
											</div>
											<div class="col-sm-2">

												{% if salida is defined and funciones.in_array(14, priRol) %}
													{% set addClass = "" %}
												{% else %}
													{% set addClass = "disabled" %}
												{% endif %}

												<a id="verCliente" onclick="verCliente('<?php echo htmlentities(json_encode($cliSalida)); ?>')" class="btn btn-inverse btn-label pull-right mb-md {{ addClass }}" style="width: 100%">
													<i class='ti ti-search'></i> Ver Detalle
												</a>
											</div>
										</div>
										{% if salida is not defined and funciones.in_array(60, priRol) %}

										<div class="row">
											<div class="col-sm-6 mb-md">
												<label class="radio-inline icheck">
													
													{{ check_field("garantia", "id": "garantia", "class": "privilegios","value": "1") }}

                									<span style="color:red">Salida de Productos por Garantía</span>
												</label>
											</div>
										</div>

										{% endif %}

										<div class="row">
											<div class="col-sm-12 mb-md">
												<h4 class="mb-md mt-n required">Observaciones de la Salida del Inventario</h4>
												
												{{ text_area("observaciones", "class": "form-control required", "placeholder": "Ingrese Observaciones de la Salida del Inventario", "rows": 3, "value": salida is defined ? salida.sal_observaciones : null) }}

											</div>
										</div>
										<div class="row">
											<div class="col-sm-4 closest mb-md required">
												<h4 class="mb-md mt-n">Código del Artículo</h4>
												<select name="codigo" id="codigo" {% if salida is not defined %} required="required" {% endif %} {% if salida is defined %} disabled="disabled" {% endif %}>
													<option value="">-- SELECCIONE --</option>

													{% for articulo in articulos %}

														<option value="{{ articulo.id }}"> {{ articulo.art_codigo }} </option>

													{% endfor %}

												</select>
											</div>
											<div class="col-sm-8 closest mb-md required">
												<h4 class="mb-md mt-n">Descripción</h4>
												<select name="articulo" id="articulo" {% if salida is not defined %} required="required" {% endif %} {% if salida is defined %} disabled="disabled" {% endif %}>
													<option value="">-- SELECCIONE --</option>

													{% for articulo in articulos %}

														<option value="{{ articulo.id }}"> {{ articulo.art_descripcion }} </option>

													{% endfor %}

												</select>
											</div>
										</div>
										<span {% if salida is not defined %} class="hide" {% endif %} id="lisSalidas">
											<hr/>
											<h4 class="mt-xl">Lista de Salidas</h4>
											<table cellpadding="0" cellspacing="0" border="0" class="table table-striped" id="tabLisSalidas">
											    <thead>
											        <tr class="info">
											            <th>Código de Artículo</th>
											            <th>Cantidad</th>
											            <th>Precio de Venta</th>
											            <th>Precio Unitario</th>
											            <th>Precio Total</th>
											            <th class="text-center">Acciones</th>
											        </tr>
											  	</thead>
											    <tbody>

											    	{% if lisSalidas is defined %}
											    		{% set sumCosTotal = 0 %}
											    		{% for lisSal in lisSalidas  %}
											    			{% set cosTotal = lisSal.mov_cantidad * lisSal.mov_importe %}
											    			{% set sumCosTotal = sumCosTotal + cosTotal %}

												    		<tr id="salida{{ lisSal.id }}">
												    			<td> {{ lisSal.art_codigo }} </td>
												    			<td> <input type="text" class="form-control" readonly="readonly" value="{{ lisSal.mov_cantidad }}"/> </td>
												    			<td> {{ lisSal.mov_pre_venta }} </td>
												    			<td> {{ "$ " ~ funciones.number_format(lisSal.mov_importe) ~ " / Bs. " ~ funciones.number_format(lisSal.mov_importe * dolar) }} </td>
												    			<td> {{ "$ " ~ funciones.number_format(cosTotal) ~ " / Bs. " ~ funciones.number_format(cosTotal * dolar) }} </td>
												    			<td> 
																	<a class="btn btn-md btn-primary btn-label" onclick="verArticulo('<?php echo htmlentities(json_encode($lisSal)); ?>', '<?php echo $dolar; ?>', '<?php echo htmlentities(json_encode($calculos)); ?>')">
																		<i class="ti ti-search"></i> Ver Artículo</a>
																	</td>
																</td>
												    		</tr>

												    	{% endfor %}
											    	{% endif %}

											    </tbody>
											</table>
											<div align="right">
											  <b>
											    
											    {{ "Total de Precios: " }} 

											    <span id="sumCosTotal">

											    	{% if salida is defined %}

											    		{% if salida.sal_garantia == 1 %}

														{{ "$ 0,00 / Bs. 0,00" }}

														{% else %}

											    		{{ "$ " ~ funciones.number_format(sumCosTotal) ~ " / Bs. " ~ funciones.number_format(sumCosTotal * dolar) }}
											    		{% endif %}

											    	{% else %}
											    		{{ "$ 0,00 / Bs. 0,00" }}
											    	{% endif %}

											    </span>
											  </b>
											</div>
										</span>

									{{ end_form() }}

									{% if salida is not defined %}

										<span id="secPagos">
											<h3 class="mt-xs">Registro de Pagos</h3>
											<hr class="mb-26 mt-n"/>

											{{ form("autocomplete": "off", "id": "pago", "data-parsley-validate": "data-parsley-validate", "data-parsley-excluded": "input[name=forPago], input[name=verificado]") }}

												<div class="row">
													<div class="col-sm-5 closest mb-md required">
														<h4 class="mb-md mt-n">Cuenta</h4>												
														<select name="cuenta" id="cuenta" required="required">
															<option value="">-- SELECCIONE --</option>

															{% for cuenta in cuentas %}

																<option value="{{ cuenta.id }}"> {{ cuenta.ban_nombre ~ " (*****" ~ funciones.substr(cuenta.cue_numero, 14, 6) ~ ")" }} </option>

															{% endfor %}

														</select>
													</div>
													<div class="col-sm-5 mb-md required">
														<h4 class="mb-md mt-n">Forma de Pago</h4>
														<label class="radio-inline icheck" style="padding-top: 0 !important">
															
															{{ radio_field("forPago", "id": "efectivo", "value": "EFECTIVO") }} <span>Efec.</span>

														</label>
														<label class="radio-inline icheck" style="padding-top: 0 !important">
															
															{{ radio_field("forPago", "id": "tarjeta", "value": "TARJETA") }} Tarj.

														</label>
														<label class="radio-inline icheck" style="padding-top: 0 !important">
															
															{{ radio_field("forPago", "id": "transferencia", "value": "TRANSFERENCIA", "checked": "checked") }} Transf.

														</label>
														<label class="radio-inline icheck" style="padding-top: 0 !important">
															
															{{ radio_field("forPago", "id": "cheque", "value": "CHEQUE") }} Cheq.

														</label>
														<label class="radio-inline icheck" style="padding-top: 0 !important">
															
															{{ radio_field("forPago", "id": "merPago", "value": "MERCADO PAGO") }} Mercado Pag.

														</label>
													</div>
													<div class="col-sm-2 closest mb-md required">
														<h4 class="mb-md mt-n">Fecha de Pago</h4>
														<div class="input-group">
															<span class="input-group-addon">
																<i class="ti ti-calendar"></i>
															</span>

															{{ text_field("fecPago", "class": "form-control cur-pointer", "required": "required", "placeholder": date("d/m/Y"), "value": date("d/m/Y"), "data-parsley-error-message": "<i class='fa fa-times-circle'></i> Ingrese fecha.") }}

														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-3 mb-md required">
														<h4 class="mb-md mt-n">Referencia</h4>
														
														{{ text_field("nroReferencia", "class": "form-control", "placeholder": "Ingrese Nro. de Referencia", "required": "required") }}

													</div>
													<div class="col-sm-3 mb-md required">
														<h4 class="mb-md mt-n">Importe (Bs./$)</h4>
														
														{{ text_field("importe", "class": "form-control", "placeholder": "0,00", "required": "required", "data-parsley-importe": "data-parsley-importe") }}

													</div>
													<div class="col-sm-3 closest mb-md required">
														<h4 class="mb-md mt-n">Moneda</h4>
														
														{{ select_static("moneda", ["Bs.": "BOLÍVARES (Bs.)", "$": "DÓLARES ($)"], "required": "required") }}

													</div>
													<div class="col-sm-1 mb-md">
														<h4 class="mb-md mt-n">Verificado</h4>
														
														{{ check_field("verificado", "class": "bootstrap-switch", "data-on-color": "primary", "data-off-color": "default", "data-on-text": "SÍ", "data-off-text": "NO") }}

													</div>
													<div class="col-sm-2 mb-md">
														<h4 class="mb-md mt-n"></h4>

														{{ link_to("#", "<i class='fa fa-plus'></i> Agregar Pago", "class": "btn btn-inverse mb-md mt-md disabled", "id": "agrPago", "style": "width: 100%") }}

													</div>
												</div>

											{{ end_form() }}

										</span>

									{% endif %}

									<span {% if salida is not defined %} class="hide" {% endif %} id="lisPagos">

								    	{% if (lisPagos is defined and lisPagos|length > 0) or salida is not defined %}

											<hr/>
											<h4 class="mt-xl">Lista de Pagos</h4>
											<table cellpadding="0" cellspacing="0" border="0" class="table table-striped" id="tabLisPagos">
											    <thead>
											        <tr class="info">
											            <th>Cuenta</th>
											            <th>Fecha de Pago</th>
											            <th>Referencia</th>
											            <th>Importe</th>
											            <th class="text-center">Verificado</th>
											            
											            {% if salida is not defined %}

											            	<th class="text-center">Acciones</th>

											            {% endif %}

											        </tr>
											    </thead>
											    <tbody>

													{% if lisPagos is defined %}
											    		{% set sumPagTotBs = 0 %}
											    		{% set sumPagTotDol = 0 %}
											    		{% for lisPag in lisPagos  %}
												    		{% if lisPag.pag_moneda == "BOLÍVARES" %}
											    				{% set sumPagTotBs = sumPagTotBs + lisPag.pag_importe %}
											    				{% set moneda = "Bs. " %}
											    			{% else %}
											    				{% set sumPagTotDol = sumPagTotDol + lisPag.pag_importe %}
											    				{% set moneda = "$ " %}
											    			{% endif %}

												    		<tr>
												    			<td> {{ lisPag.ban_nombre ~ " (*****" ~ funciones.substr(lisPag.cue_numero, 14, 6) ~ ")" }} </td>
												    			<td> {{ funciones.cambiaf_a_normal(lisPag.pag_fecha) }} </td>
												    			<td> {{ lisPag.pag_num_referencia }} </td>
												    			<td> {{ moneda ~ funciones.number_format(lisPag.pag_importe) }} </td>
												    			<td class="text-center"> {{ check_field("verificado", "class": "bootstrap-switch", "data-on-color": "primary", "data-off-color": "default", "data-on-text": "SÍ", "data-off-text": "NO", "checked": lisPag.pag_verificado == true ? "checked" : null, "data-pago-id": anulada is defined ? null : lisPag.id) }} </td>
												    		</tr>

												    	{% endfor %}
												    {% endif %}

											    </tbody>
											</table>
											<div align="right" class="mb-md">
											  <b>
											    
											    {{ "Total de Pagos: " }} 

											    <span id="sumPagTotal">

											    	{% if salida is defined %}
											    		{% if salida.sal_garantia == 1 %}
															{{ "$ 0,00 - Bs. 0,00" }}
														{% else %}
											    			{{ "$ " ~ funciones.number_format(sumPagTotDol) ~ " - Bs. " ~ funciones.number_format(sumPagTotBs) }}
											    		{% endif %}
											    	{% else %}
											    		{{ "$ <span id='dolares'>0,00</span> - Bs. <span id='bolivares'>0,00</span>" }}
											    	{% endif %}

											    </span>
											  </b>
											</div>

									    {% endif %}

									</span>
									<hr class="mb-26 mt-n"/>
									<div class="row">
										<div class="col-xs-6 col-sm-10 mb-md text-right pr-n">
											Subtotal:
										</div>
										<div class="col-xs-6 col-sm-2 mb-md pl-lg">
											Bs. <span id="subtotal"> {{ salida is defined ? funciones.number_format(sumCosTotal * dolar) : "0,00" }} </span>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6 col-sm-2 mb-md pull-right">
											
											{{ text_field("iva", "class": "form-control", "placeholder": "Bs. 0,00", "readonly": "readonly", "data-parsley-importe": "data-parsley-importe", "value": salida is defined ? "Bs. " ~ funciones.number_format(salida.sal_iva) : "Bs. 0,00") }}

										</div>
										<div class="col-xs-6 col-sm-10 mb-md text-right pr-n">

						            		{% if salida is not defined %}
												{{ check_field("cheIva", "class": "bootstrap-switch", "data-on-color": "primary", "data-off-color": "default", "data-on-text": "SÍ", "data-off-text": "NO") }}
											{% endif %}

											<span class="pl-md">IVA (16 %):</span>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6 col-sm-10 mb-md text-right pr-n">
											Total:
										</div>
										<div class="col-xs-6 col-sm-2 mb-md pl-lg">
											Bs. <span id="total"> {{ salida is defined ? funciones.number_format(sumCosTotal * dolar + salida.sal_iva) : "0,00" }} </span>
										</div>
									</div>
								</div>

								{% if anulada is not defined %}

									<div class="panel-footer">
										<div class="clearfix">
											<span id="loading" class="loading hide">

												{{ image("img/loading.gif", "width": "32px") }}

											</span>

									    	{% if salida is defined and funciones.in_array(23, priRol) and anulada is not defined %}
												{{ submit_button("Actualizar", "class": "btn btn-inverse", "id": "actualizar", "data-salida-id": salida.id) }}

												{% if funciones.in_array(56, priRol) %}
													{{ submit_button("Anular", "class": "btn btn-warning bc-e51", "id": "elmSalida", "data-salida-id": salida.id) }}
												{% endif %}
											{% else %}
												{% if salida is not defined %}
													{{ submit_button("Registrar", "class": "btn btn-inverse", "id": "registrar") }}
													{{ submit_button("Cancelar", "class": "btn btn-default", "id": "cancelar") }}
												{% endif %}
											{% endif %}

										</div>	
									</div>

								{% endif %}

							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>

{# Modal de cliente #}
<div class="modal fade" id="modCliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bc-8bc">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h2 class="modal-title c-w fz-24"></h2>
			</div>
			<div class="modal-body bc-g">

				{{ partial("partials/clientes") }}

			</div>
			<div class="modal-footer">
				<span id="loaCliente" class="hide loading">

					{{ image("img/loading.gif", "width": "32px") }}

				</span>
				<button type="button" class="btn btn-inverse" id="regCliente">Registrar</button>
				<button type="button" class="btn btn-default" id="canCliente">Cancelar</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar">Cerrar</button>
			</div>
		</div>
	</div>
</div>

{# ******************************** #}

{# Modal de artículo #}
<div class="modal fade" id="modArticulo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bc-8bc">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h2 class="modal-title c-w fz-24">Registro de artículo</h2>
			</div>
			<div class="modal-body bc-g">

				{{ partial("partials/articulos") }}

			</div>
			<div class="modal-footer">
				<span id="loaArticulo" class="hide loading">

					{{ image("img/loading.gif", "width": "32px") }}

				</span>
				<button type="button" class="btn btn-inverse" id="regArticulo">Registrar</button>
				<button type="button" class="btn btn-default" id="canArticulo">Cancelar</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar2">Cerrar</button>
			</div>
		</div>
	</div>
</div>

{# ******************************** #}

{# Modal de datos de envío y recibo #}
<div class="modal fade" id="modEnvio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bc-8bc">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h2 class="modal-title c-w fz-24">Datos del envío de pedido</h2>
			</div>
			<div class="modal-body bc-g">

				{{ form("autocomplete": "off", "id": "forEnvio", "data-parsley-validate": "data-parsley-validate") }}

					<h4 class="mt-n">Persona que Envía</h4>
					<hr class="mb-xl mt-n"/>
					<div class="row">
						<div class="col-sm-6 mb-md">
							<label for="nombre1" class="control-label required">Nombre</label>
							<input type="hidden" name="salida-id" id="salida-id">
							{{ text_field("nombre1", "class": "form-control", "placeholder": "Ingrese Nombre", "required": "required", "value":"MANUEL BERMUDEZ" ) }}


						</div>
						<div class="col-sm-6 mb-md">
							<label for="cedIdentidad1" class="control-label required">Cédula de Identidad</label>

							{{ text_field("cedIdentidad1", "class": "form-control", "placeholder": "Ingrese Cédula de Identidad", "required": "required","value":"V-15813013") }}

						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 mb-md">
							<label for="direccion1" class="control-label required">Dirección / Tlf</label>
								
							{{ text_area("direccion1", "class": "form-control", "placeholder": "Ingrese Dirección", "rows": 2, "required": "required", "value":"AV. LIBERTADOR, RESIDENCIA PARAISO, LOCAL NR 04  MATURIN ESTADO MONAGAS / TLF: 0291 4008498") }}

						</div>
					</div>
					<h4 class="mt-xs">Persona que Recibe</h4>
					<hr class="mb-xl mt-n"/>
					<div class="row">
						<div class="col-sm-6 mb-md">
							<label for="nombre2" class="control-label required">Nombre</label>
								
							{{ text_field("nombre2", "class": "form-control", "placeholder": "Ingrese Nombre", "required": "required",  "value": cliSalida.cli_nombre) }}

						</div>
						<div class="col-sm-6 mb-md">
							<label for="cedIdentidad2" class="control-label required">Cédula de Identidad</label>

							{{ text_field("cedIdentidad2", "class": "form-control", "placeholder": "Ingrese Cédula de Identidad", "required": "required", "value": cliSalida.cli_codigo) }}

						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 mb-md">
							<label for="direccion2" class="control-label required">Dirección / Tlf</label>
								
							{{ text_area("direccion2", "class": "form-control", "placeholder": "Ingrese Dirección", "rows": 2, "required": "required" ,"value": cliSalida.cli_direccion ~ " / " ~ cliSalida.cli_tel_celular ) }}

						</div>
					</div>

				{{ end_form() }}

			</div>
			<div class="modal-footer">
				<span id="loaEnvio" class="hide loading">

					{{ image("img/loading.gif", "width": "32px") }}

				</span>
				<button type="button" class="btn btn-inverse" id="impEnvio">Imprimir</button>
				<button type="button" class="btn btn-default" id="canEnvio">Precargar</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar3">Cerrar</button>
			</div>
		</div>
	</div>
</div>

{# ******************************** #}

{# Modal de motivo de anulación de salida #}
<div class="modal fade" id="modAnulacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bc-8bc">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h2 class="modal-title c-w fz-24">Anulación de salida con código #{{ funciones.str_pad(salida.id) }}</h2>
			</div>
			<div class="modal-body bc-g">

				{{ form("autocomplete": "off", "id": "forAnulacion", "data-parsley-validate": "data-parsley-validate") }}

					<div class="row">
						<div class="col-sm-12 mb-md">
							<label for="motAnulacion" class="control-label required">Motivo de Anulación</label>

							{{ text_area("motAnulacion", "class": "form-control", "placeholder": "Ingrese Motivo de Anulación", "rows": 4, "required": "required") }}

						</div>
					</div>

				{{ end_form() }}

			</div>
			<div class="modal-footer">
				<span id="loaAnulacion" class="hide loading">

					{{ image("img/loading.gif", "width": "32px") }}

				</span>
				<button type="button" class="btn btn-inverse" id="anuSalida">Anular</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar4">Cerrar</button>
			</div>
		</div>
	</div>
</div>

{# ******************************** #}

{{ partial("partials/seriales") }}

<div id="preloader-alt" class="hide">
    <div id="page-loader-alt"></div>
</div>

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
        errorsContainer: function(el) {
            return el.$element.closest(".closest");
        },

      	errorsWrapper: "<span class='help-block'></span>", 
      	errorTemplate: "<span></span>"
    };
</script>