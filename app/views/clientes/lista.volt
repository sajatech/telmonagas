{{ assets.outputCss() }}

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="container-fluid">
                <div data-widget-group="group1">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="ajaxSpinnerClientes" class="fs-16 text-center mt-md">

                                {{ image("img/loading.gif", "width": "32px") }}
                                {{ "Por favor, espere..." }}

                            </div>
                            <div class="panel panel-success hide" id="panClientes">
                                <div class="panel-heading">
                                    <h1 class="title c-w fz-24">Clientes Registrados</h1>
                                    <div class="panel-ctrls"></div>
                                </div>
                                <div class="panel-body no-padding">
                                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered nowrap dt-responsive" id="clientes">
                                        <thead>
                                            <tr>
                                                <th width="7.10%">Código</th>
                                                <th width="8.44%">R.I.F. o C.I.</th>
                                                <th width="12.78%">Nombre</th>
                                                <th width="27.92%">Dirección</th>
                                                <th width="13.77%">Teléfono Celular</th>
                                                <th width="23.89%">Correo Electrónico</th>
                                                <th width="6.10%">Estatus</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>                
                                <div class="panel-footer"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modCompras" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bc-8bc">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title c-w fz-24"></h2>
            </div>
            <div class="modal-body">
                <div id="ajaxSpinner" class="fs-16 text-center mt-md">

                    {{ image("img/loading.gif", "width": "32px") }}
                    {{ "Por favor, espere..." }}

                </div>
                <table id="compras" class="table table-striped table-bordered hide" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Fecha</th>
                            <th>Precio pagado</th>
                            <th class="text-center">Artículos</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar">Cerrar</button>
            </div>
        </div>
    </div>
</div>

{{ assets.outputJs() }}