<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            {#<ol class="breadcrumb">                        
				<li>

					{{ link_to("inicio", "Inicio") }}

				</li>
				<li>

					{{ link_to("proveedores/registro", "Proveedores") }}

				</li>
				<li class="active">

					{{ link_to("proveedores/lista", "Actualización de Proveedores") }}

				</li>
            </ol>#}
            <div class="container-fluid">
				<div data-widget-group="group1">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-success">
								<div class="panel-heading">
									<h1 class="title c-w fz-24">Actualización de Clientes</h1>
									<div class="panel-ctrls">

        								{% if funciones.in_array(12, priRol) %}
											{{ link_to("clientes/lista", "<i class='ti ti-search'></i><span> Buscar Cliente</span>", "class": "btn btn-default ctrls pull-left mt-sm mr-xs") }}
										{% endif %}

        								{% if funciones.in_array(11, priRol) %}
											{{ link_to("clientes/registro", "<i class='ti ti-plus'></i><span> Agregar Cliente</span>", "class": "btn btn-default ctrls pull-left mt-sm") }}
										{% endif %}

									</div>
								</div>
								<div class="panel-body bc-g">

									{{ partial("partials/clientes") }}

								</div>
								<div class="panel-footer">
									<div class="clearfix">
										<span id="loaCliente" class="loading hide">

											{{ image("img/loading.gif", "width": "32px") }}

										</span>

        								{% if funciones.in_array(13, priRol) %}
											{{ submit_button("Actualizar", "class": "btn btn-inverse btn", "id": "actCliente", "data-cliente-id": cliente.id) }}
										{% endif %}

									</div>	
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
        errorsContainer: function(el) {
            return el.$element.closest(".closest");
        },

  		errorsWrapper: "<span class='help-block'></span>", 
  		errorTemplate: "<span></span>"
    };
</script>