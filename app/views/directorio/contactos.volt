{{ assets.outputCss() }}

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="container-fluid">
                <div data-widget-group="group1">
                    <div class="row">
                        <div class="col-md-6">
                            <div id="ajaxSpinnerContactos" class="fs-16 text-center mt-md">

                                {{ image("img/loading.gif", "width": "32px") }}
                                {{ "Cargando contactos..." }}

                                <br/>
                                <br/>
                            </div>
                            <div class="panel panel-success hide" id="panContactos">
                                <div class="panel-heading">

                                    {#{% if funciones.in_array(44, priRol) %}#}
                                    {% if session.get("nivel1")["id"] == 1 or session.get("nivel1")["id"] == 2 or session.get("nivel1")["id"] == 3 %}

                                        {{ link_to("#", "<i class='ti ti-plus'></i><span> Más Información</span>", "id": "masInformacion", "class": "btn btn-default ctrls mt-md", false) }}
                                    
                                    {#{% endif %}#}
                                    {% else %}

                                        <h1 class="title c-w fz-24">Contactos Registrados</h1>

                                    {% endif %}
                                    <div class="panel-ctrls"></div>
                                </div>
                                <div class="panel-body no-padding">
                                    <table class="table table-striped table-bordered nowrap tcur-pointer dt-responsive fs-16" id="contactos" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th width="">Código</th>
                                                <th width="51.16%">Nombre</th>
                                                <th width="24.42%">Teléfono 1</th>
                                                <th width="24.42%">Teléfono 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>                
                                <div class="panel-footer"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <h1 class="title c-w fz-24">Registro de Contactos</h1>
                                    <div class="panel-ctrls"></div>
                                </div>
                                <div class="panel-body bc-g">

                                    {{ form("autocomplete": "off", "id": "forContacto", "data-parsley-validate": "data-parsley-validate") }}

                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-3 mb-md">
                                                <center><i class="fa fa-user" style="font-size: 64px"></i></center>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-3 mb-md">
                                                <label for="nombre" class="control-label required">Nombre</label>

                                                {{ text_field("nombre", "class": "form-control", "placeholder": "Ingrese Nombre", "required": "required") }}

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-3 mb-md">
                                                <label for="telefono1" class="control-label required">Teléfono 1</label>

                                                {{ text_field("telefono1", "class": "form-control", "placeholder": "Ingrese Teléfono", "required": "required", "data-inputmask": "'mask': '(999) 999-9999', 'clearIncomplete': true") }}

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-3 mb-md">
                                                <label for="telefono2" class="control-label">Teléfono 2</label>

                                                {{ text_field("telefono2", "class": "form-control", "placeholder": "Ingrese Teléfono", "data-inputmask": "'mask': '(999) 999-9999', 'clearIncomplete': true") }}

                                            </div>
                                        </div>

                                    {{ end_form() }}

                                </div>
                                <div class="panel-footer">
                                    <div class="clearfix">
                                        <span id="loaContacto" class="loading hide">

                                            {{ image("img/loading.gif", "width": "32px") }}

                                        </span>

                                        {{ submit_button("Registrar", "class": "btn btn-inverse", "id": "regContacto", "data-contacto-accion": "registrar") }}
                                        {{ submit_button("Cancelar", "class": "btn btn-default", "id": "canContacto") }}
                                        {{ submit_button("Eliminar", "class": "btn btn-warning bc-e51 hide", "id": "eliContacto") }}

                                    </div>  
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{# Modal de más información #}
<div class="modal fade" id="modInformacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bc-8bc">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title c-w fz-24">Contactos registrados por usuario</h2>
            </div>
            <div class="modal-body">
                <div id="ajaxSpinner" class="fs-16 text-center mt-md">

                    {{ image("img/loading.gif", "width": "32px") }}
                    {{ "Por favor, espere..." }}

                </div>
                <div id="resultado"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar">Cerrar</button>
            </div>
        </div>
    </div>
</div>

{# ******************************** #}

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
        errorsWrapper: "<span class='help-block'></span>", 
        errorTemplate: "<span></span>"
    };
</script>