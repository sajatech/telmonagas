{{ assets.outputCss() }}

<div id="preloader">
    <div id="page-loader"></div>
</div>
<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="container-fluid">
                <div data-widget-group="group1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3" style="width: fit-content">
                                    <center>
                                        <div id="container"></div>

                                        {{ hidden_field("datepicker", "value": date("Y-m-d")) }}

                                    </center>
                                </div>
                                <div class="col-md-9 pl-n pr-n">
                                    <div class="panel panel-inbox">
                                        <div class="panel-body">

                                            {{ form("autocomplete": "off", "id": "inbox-compose-form", "data-parsley-validate": "data-parsley-validate", "class": "form-horizontal p-md") }}

                                                <div class="form-group mb-n">
                                                    <div class="col-xs-12">

                                                        {{ text_area("summernote", "class": "form-control", "placeholder": "Ingrese Detalle", "rows": 4) }}

                                                    </div>
                                                </div>                                            

                                            {{ end_form() }}

                                            <div class="inbox-mail-footer">
                                                <div class="clearfix">
                                                    <div class="pull-right">
                                                        <span id="loaNota" class="loading hide">

                                                            {{ image("img/loading.gif", "width": "32px") }}

                                                        </span>

                                                        {{ submit_button("Registrar", "class": "btn btn-inverse", "id": "regNota") }}
                                                        {{ submit_button("Eliminar", "class": "btn btn-default", "id": "eliNota") }}

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{ assets.outputJs() }}