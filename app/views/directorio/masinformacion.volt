<div class="row p-md">
	<div class="col-md-12">
		<table class="table table-striped table-bordered" cellspacing="0" width="100%" id="tabMasInformacion">
			<thead>
				<tr>
					<th>Usuario</th>
					<th>Contactos</th>
				</tr>
			</thead>
			<tbody>

				{% for usuario in usuarios %}

					<tr>
						<td> {{ usuario["usuario"] }}  </td>
						<td> {{ usuario["contactos"] }} </td>
					</tr>

				{% endfor %}

			</tbody>
		</table>
	</div>
</div>