<div align="center" id="entrar" style="position: absolute; bottom: 0%; left: 50%; color: grey; cursor: default">.</div>
<div class="container" id="login-form">
	<div class="row">
		<div class="col-md-4">
			{% set imagen = image("img/logoTitulo.png") %}

			{{ link_to("", imagen, "class": "login-logo hide") }}
			<div class="panel panel-default" style="border: solid 1px blue">
<!-- 				<div class="panel-heading">
					<h2>Iniciar Sesión</h2>
				</div> -->
				<div class="panel-body">

					{{ form("class": "form-horizontal", "id": "login", "autocomplete": "off", "data-parsley-validate": "data-parsley-validate", "onkeypress": "if(event.keyCode == 13) enviar_formulario()") }}

					<div class="form-group mb-md">
                        <div class="col-xs-12">

							{{ text_field("nombreUsuario", "class": "form-control", "placeholder": "Nombre de Usuario", "required": "required", "autofocus": "autofocus") }}

                        </div>
					</div>
					<div class="form-group mb-n">
                        <div class="col-xs-12">

							{{ password_field("contrasena", "class": "form-control", "placeholder": "Contraseña", "required": "required") }}

                        </div>
					</div>

					{{ end_form() }}

				</div>
				<div class="panel-footer">
					<div class="clearfix">
						<span id="loading" class="hide loading">

							{{ image("img/loading.gif", "width": "32px") }}

						</span>

						{{ submit_button("Acceder", "class": "btn btn-primary pull-right", "id": "acceder") }}

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

{{ assets.outputJs() }}

<script>
	window.ParsleyConfig = {
		  errorsWrapper: "<span class='help-block'></span>",
		  errorTemplate: "<span></span>"
	};

	function enviar_formulario() {
   		$("#acceder").click();
	}
</script>