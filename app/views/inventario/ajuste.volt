{{ assets.outputCss() }}

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            {#<ol class="breadcrumb">                        
				<li>

					{{ link_to("inicio", "Inicio") }}

				</li>
				<li>

					{{ link_to("inventario/ingreso", "Inventario") }}

				</li>
				<li class="active">

					{{ link_to("inventario/ingreso", "Ingreso de Mercancía") }}

				</li>
            </ol>#}
            <div class="container-fluid">
				<div data-widget-group="group1">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-success">
								<div class="panel-heading">
									<h1 class="title c-w fz-24">

										{% if ajuste is defined %}
											{{ "Detalle de Ajuste de Inventario" }}
										{% else %}
											{{ "Movimientos de Ingreso o Salida de Artículos" }}
										{% endif %}

									</h1>
									<div class="panel-ctrls">

										{% if ajuste is defined and funciones.in_array(47, priRol) %}
											{{ link_to("inventario/ajuste", "<i class='ti ti-plus'></i><span> Agregar Ajuste</span>", "class": "btn btn-default ctrls pull-right mt-sm mr-xs") }}
										{% endif %}

        								{% if funciones.in_array(48, priRol) %}
											{{ link_to("inventario/ajustes", "<i class='ti ti-search'></i><span> Buscar Ajuste</span>", "class": "btn btn-default ctrls pull-right mt-sm mr-xs") }}
										{% endif %}

									</div>
								</div>
								<div class="panel-body bc-g">

									{{ form("autocomplete": "off", "id": "ajuste", "data-parsley-validate": "data-parsley-validate") }}

										<div class="row">
											<div class="col-sm-6 closest mb-md">
												<h4 class="mb-md mt-n required">Motivo de Ajuste</h4>
												<select name="motAjuste" id="motAjuste" required="required">
													<option value="">-- SELECCIONE --</option>
													<option value="1" {% if ajuste is defined and motAjuste == "1" %} selected="selected" {% endif %}>AJUSTE POR INVENTARIO FÍSICO DE MERCANCÍA</option>
													<option value="2" {% if ajuste is defined and motAjuste == "2" %} selected="selected" {% endif %}>DEVOLUCIÓN POR COMPRA O VENTA DE ARTÍCULOS</option>
												</select>
											</div>
											<div class="col-sm-6 closest">
												<h4 class="mb-md mt-n required">Tipo de Movimiento</h4>
		                                        <label class="checkbox-inline icheck" style="padding-top: 0 !important">

		                                    		{{ radio_field("tipMovimiento", "id": "carga", "value": "CARGA", "checked": "checked") }}

		                                    		<span>Ingreso o Entrada</span>
		                                		</label>
		                                        <label class="checkbox-inline icheck tipMovimiento" style="padding-top: 0 !important">

		                                    		{{ radio_field("tipMovimiento", "id": "descarga", "value": "DESCARGA", "checked": ajuste is defined and tipMovimiento == "DESCARGA" ? "checked" : null) }}

		                                    		<span>Salida o Despacho</span>
		                                		</label>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12 mb-md">
												<h4 class="mb-md mt-n">Observaciones del Ajuste de Inventario</h4>
												
												{{ text_area("observaciones", "class": "form-control", "placeholder": "Ingrese Observaciones del Ajuste de Inventario", "rows": 3, "value": ajuste is defined ? ajuste.aju_observaciones : null) }}

											</div>
										</div>
										<div class="row">
											<div class="col-sm-12 closest mb-md required">
												<h4 class="mb-md mt-n">Código o Descripción del Artículo</h4>
												<select name="articulo" id="articulo" {% if ajuste is not defined %} required="required" {% endif %} {% if ajuste is defined %} disabled="disabled" {% endif %}>
													<option value="">-- SELECCIONE --</option>

													{% for articulo in articulos %}

														<option value="{{ articulo.id }}">{{ articulo.art_codigo ~ " (" ~ articulo.art_descripcion ~ ")" }}</option>

													{% endfor %}

												</select>
											</div>
										</div>
										<span {% if ajuste is not defined %} class="hide" {% endif %} id="lisAjustes">
											<hr/>
											<h4 class="mt-xl">Lista de Ajustes</h4>
											<table cellpadding="0" cellspacing="0" border="0" class="table table-striped" id="tabLisAjustes">
											    <thead>
											        <tr class="info">
											            <th>Código de Artículo</th>
											            <th>Cantidad</th>
											            <th>Costo del Artículo</th>
											            <th>Costo Total</th>
											            <th>Acciones</th>
											        </tr>
											    </thead>
											    <tbody>

											    	{% if lisAjustes is defined %}
											    		{% set sumCosTotal = 0 %}
											    		{% for lisAju in lisAjustes  %}
											    			{% set cosTotal = lisAju.mov_cantidad * lisAju.art_cos_unitario %}
											    			{% set sumCosTotal = sumCosTotal + cosTotal %}

												    		<tr id="ajuste{{ lisAju.id }}">
												    			<td> {{ lisAju.art_codigo }} </td>
												    			<td> <input type="text" class="form-control" readonly="readonly" value="{{ lisAju.mov_cantidad }}"/> </td>
												    			<td> {{ "$ " ~ funciones.number_format(lisAju.art_cos_unitario) ~ " / Bs. " ~ funciones.number_format(lisAju.art_cos_unitario * dolar) }} </td>
												    			<td> {{ "$ " ~ funciones.number_format(cosTotal) ~ " / Bs. " ~ funciones.number_format(cosTotal * dolar) }} </td>
												    			<td> 
																	<a class="btn btn-md btn-primary btn-label" onclick="verArticulo('<?php echo htmlentities(json_encode($lisAju)); ?>', '<?php echo $dolar; ?>')">
																		<i class="ti ti-search"></i> Ver Artículo</a>
																	</td>
																</td>
												    		</tr>

												    	{% endfor %}
											    	{% endif %}

											    </tbody>
											</table>
											<div align="right">
											  <b>
											    
											    {{ "Total de Costos: " }} 

											    <span id="sumCosTotal">

											    	{% if ajuste is defined %}
											    		{{ "$ " ~ funciones.number_format(sumCosTotal) ~ " / Bs. " ~ funciones.number_format(sumCosTotal * dolar) }}
											    	{% else %}
											    		{{ "$ 0,00 / Bs. 0,00" }}
											    	{% endif %}

											    </span>
											  </b>
											</div>
										</span>

									{{ end_form() }}

								</div>
								<div class="panel-footer">
										<div class="clearfix">
											<span id="loaAjuste" class="loading hide">

												{{ image("img/loading.gif", "width": "32px") }}

											</span>
								{% if ajuste is not defined %}

									

									    	{#{% if ajuste is defined and funciones.in_array(8, priRol) %}
												{{ submit_button("Actualizar", "class": "btn btn-inverse", "id": "actualizar", "data-ingreso-id": ingreso.id) }}
											{% else %}#}
												{#{% if ajuste is not defined %}#}
													{{ submit_button("Registrar", "class": "btn btn-inverse", "id": "regAjuste") }}
													{{ submit_button("Cancelar", "class": "btn btn-default", "id": "canAjuste") }}
												{#{% endif %}#}
											{#{% endif %}#}

										
								{% endif %}	

								{% if ajuste is defined and funciones.in_array(58, priRol) %}

									{{ submit_button("Eliminar", "class": "btn btn-warning bc-e51", "id": "elmAjuste", "data-ajuste-id": ajuste.id) }}

								{% endif %}	
									</div>	
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>

{# Modal de artículo #}
<div class="modal fade" id="modArticulo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bc-8bc">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h2 class="modal-title c-w fz-24">Registro de artículo</h2>
			</div>
			<div class="modal-body bc-g">

				{{ partial("partials/articulos") }}

			</div>
			<div class="modal-footer">
				<span id="loaArticulo" class="hide loading">

					{{ image("img/loading.gif", "width": "32px") }}

				</span>
				<button type="button" class="btn btn-inverse" id="regArticulo">Registrar</button>
				<button type="button" class="btn btn-default" id="canArticulo">Cancelar</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar2">Cerrar</button>
			</div>
		</div>
	</div>
</div>

{# ******************************** #}

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
        errorsContainer: function(el) {
            return el.$element.closest(".closest");
        },

      	errorsWrapper: "<span class='help-block'></span>", 
      	errorTemplate: "<span></span>"
    };
</script>