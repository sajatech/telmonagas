<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<title>
			.:: Control de Productos ::.
		</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-touch-fullscreen" content="yes">
		<meta name="description" content="Saja Tech">
		<meta name="author" content="Saja Tech">

		{{ stylesheet_link("assets/fonts/font-awesome/css/font-awesome.min.css") }}
		{{ stylesheet_link("assets/fonts/themify-icons/themify-icons.css") }}
		{{ stylesheet_link("assets/css/styles.css?v=12.03.01") }}
		{{ stylesheet_link("assets/css/custom.css?v=26.04.04") }}
		{{ stylesheet_link("assets/plugins/notifIt/css/notifIt.css") }}
		{{ stylesheet_link("assets/plugins/form-select2/select2.css") }}

	    <!--[if lt IE 10]>
	        <script type="text/javascript" src="assets/js/media.match.min.js"></script>
	        <script type="text/javascript" src="assets/js/respond.min.js"></script>
	        <script type="text/javascript" src="assets/js/placeholder.min.js"></script>
	    <![endif]-->
	    <!-- The following CSS are included as plugins and can be removed if unused-->

    </head>
    <body class="horizontal-nav animated-content">
<!--         
        {% if router.getRewriteUri() != "/reportes/movimientos_articulos" and router.getRewriteUri() != "/reportes/pagos_bancarios" and router.getRewriteUri() != "/reportes/pdf_envios" and router.getRewriteUri() != "/reportes/pdf_salidas" and router.getRewriteUri() != "/reportes/pdf_productos" and router.getRewriteUri() != "/salidas/nota_entrega/" and router.getRewriteUri() != "/salidas/nota_entrega/" and router.getRewriteUri() != "/reportes/gastos_emitidos" %}

	        <div id="preloader">
	            <div id="page-loader"></div>
	        </div>

	    {% endif %} -->

		{{ content() }}

        <script type="text/javascript">
/*            $(window).load(function(e) {
                $("#preloader").fadeOut("slow");
                $("body").css({ "overflow": "visible" });
            });*/
        </script>

         {% if router.getRewriteUri() == "/envios/promociones"%}

         	<style type="text/css">

         	/*custom ccs para plugin summernote en promociones*/
			.panel-heading.note-toolbar .dropdown-menu {
			    min-width: 190px!important;
			    left: 0%;
			}

         	</style>

	    {% endif %}
    </body>
</html>