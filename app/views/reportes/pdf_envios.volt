<style type="text/css">
    body {
        background-color: white;
        font-family: SourceSansPro;
        color: #000;
        font-size: 18px;
    }

	table {
		font-size: 14px;
		vertical-align: top;
	}

	.tj {
		border-top: 1px solid #eeeeee;
		padding: 4px;
	}

    .tg {
        border-collapse: collapse;
        border-spacing: 0;
        margin: 0px auto;
    }

    .tg td {
        font-family: SourceSansPro;
        padding: 12px;
        border-style: solid;
        border-width: 1px;
        overflow: hidden;
        word-break: normal;
        border-color: #eeeeee;
    }

    .tg .tg-yw4l { 
        vertical-align: top;
    }

    .titulo {
        margin-top: 4px;
        margin-bottom: 4px;
        font-family: SourceSansProBold;
        font-size: 24px;
        color: #000000;
        text-align: center;
    }

    .fs-24 {
        font-size: 24px;
    }

    .total {
        color: #000; 
        margin-top: 4px; 
        margin-bottom: 4px;  
    }
</style>
<div style="position: absolute">
    CONTROL DE PRODUCTOS<br/>
    TELECOMUNICACIONES MONAGAS<br/>
    R.I.F. J-08020670-3<br/>
</div>
<div style="text-align: right; margin-bottom: 48px">
	<div style="height: 20px"></div>

    {{ image("img/logo.png", "height": "27px") }}

</div>
<div class="titulo">
    <b>RESUMEN DE ENVIOS<br/></b>
</div>
<div style="margin-top: 32px"></div>
<div class="legend" style="border-left: 0; border-right: 0; background-color: #b2ebf2" align="center">
    <b>LISTADO DE ENVIOS</b>
    <?php if($this->request->get("desde")) { ?>
    del <?=$this->request->get("desde");?> al <?= $this->request->get("hasta");?>
    <?php }?>
</div>
<table width="100%" style="margin-top: 16px">
	<thead>
	    <tr>
	        <th class="tg">Fecha</th>
            <th class="tg">Cliente</th>
            <th class="tg">Empresa Envio</th>
            <th class="tg">Estatus</th>
            <th class="tg">Procesado por</th>
            <th class="tg">Monto Venta (Bs)</th>
	    </tr>
	</thead>
	<tbody>

	  {% for envio in envios %}

	    <tr {% if loop.index % 2 != 0 %} style="background-color: #999" {% endif %}>
	      <td class="tj">{{ funciones.cambiaf_a_normal(envio.env_fecha) }}</td>
	      <td class="tj" style="font-size:11px">{{ envio.env_nombre  }}</td>
	      <td class="tj">{{ envio.env_empresa ~ " (" ~ envio.env_guia ~ ")" }}</td>
	      <th class="tj" style="font-size:11px">{{ envio.env_correo_estatus }}</th>
          <td class="tj">{{ envio.usu_nombre }}</td>
          <td class="tj">{{ funciones.number_format(envio.env_monto_salida) }}</td>
	    </tr>

	  {% endfor %}

	</tbody>
</table>
<div align="right" style="margin-top: 32px">
    <span class="total fs-24">
    	<b>

    		{{ "Total de Envios: " ~ totEnvios }} <br/>

    	</b>
    </span>
    <span class="total fs-24">
    	<b>

    		{{ "Monto Total Ventas (Bs): " ~ funciones.number_format(totalVentas) }} <br/>

    	</b>
    </span>
    <span class="total fs-24">
    	<div align="right"><b>Top 3 de Clientes:</b> <br> 
        {{ topEnvios[0].env_nombre ~ " con " ~ topEnvios[0].total  ~ " envios" }}<br>
        {{ topEnvios[1].env_nombre ~ " con " ~ topEnvios[1].total  ~ " envios" }}<br>
        {{ topEnvios[2].env_nombre ~ " con " ~ topEnvios[2].total  ~ " envios" }}<br> </div><br>
    </span>
  </b>
</div>
<div align="right" style="margin-top: 32px">
    
    {{ "<b>Reporte Generado Por:</b> " ~ funciones.getNombreUsuario() }}

</div>