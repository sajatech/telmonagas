<style type="text/css">
    body {
        background-color: white;
        font-family: SourceSansPro;
        color: #000;
        font-size: 18px;
    }

    table {
        font-size: 14px;
        vertical-align: top;
    }

    .tj {
        border-top: 1px solid #eeeeee;
        padding: 4px;
    }

    .tg {
        border-collapse: collapse;
        border-spacing: 0;
        margin: 0px auto;
    }

    .tg td {
        font-family: SourceSansPro;
        padding: 12px;
        border-style: solid;
        border-width: 1px;
        overflow: hidden;
        word-break: normal;
        border-color: #eeeeee;
    }

    .tg .tg-yw4l { 
        vertical-align: top;
    }

    .titulo {
        margin-top: 4px;
        margin-bottom: 4px;
        font-family: SourceSansProBold;
        font-size: 24px;
        color: #000000;
        text-align: center;
    }

    .fs-24 {
        font-size: 24px;
    }

    .total {
        color: #000; 
        margin-top: 4px; 
        margin-bottom: 4px;  
    }
</style>
<div style="position: absolute">
    CONTROL DE PRODUCTOS<br/>
    TELECOMUNICACIONES MONAGAS<br/>
    R.I.F. J-08020670-3<br/>
</div>
<div style="text-align: right; margin-bottom: 48px">
    <div style="height: 20px"></div>

    {{ image("img/logo.png", "height": "27px") }}

</div>
<div class="titulo">
    <b>RESUMEN DE PRODUCTOS<br/></b>
</div>
<div style="margin-top: 32px"></div>
<div class="legend" style="border-left: 0; border-right: 0; background-color: #b2ebf2" align="center">
<div align="left"><b>Total Productos: </b> {{ total }}</div>

</div>
<table width="100%" style="margin-top: 16px">
    <thead>
        <tr>
            <th class="tg">Codigo</th>
            <th class="tg">Descripcion</th>
            <th class="tg">Cant. Ingresada</th>
            <th class="tg">Cant. Ventas</th>
            <th class="tg">Cant. Existente</th>
            <th class="tg">Valor % de Venta</th>
        </tr>
    </thead>
    <tbody>

        {% for productos in product %}

        <tr>
            <td class="tj">{{ productos.art_codigo }}</td>
            <td class="tj">{{ productos.art_descripcion }}</td>
            <td class="tj">{{ productos.art_inv_inicial + productos.mov_carga }}</td>
            {% if productos.total_ventas is not empty %}    
            <td class="tj">{{ productos.total_ventas }} </td>
            {% else %}
            <td class="tj">0 </td>
            {% endif %}
            <td class="tj">{{ productos.art_inv_inicial - productos.total_ventas + productos.mov_carga }}</td>
            {% if productos.total_ventas is not empty %}
            <td class="tj">{{ funciones.number_format( (productos.total_ventas * 100) / (productos.art_inv_inicial + productos.mov_carga) )  }} % </td>
            {% else %}
            <td class="tj">0 % </td>
            {% endif %}
        </tr>

            

        {% endfor %}

    </tbody>
</table>

<div align="right" style="margin-top: 32px">
    
    {{ "<b>Reporte Generado Por:</b> " ~ funciones.getNombreUsuario() }}

</div>