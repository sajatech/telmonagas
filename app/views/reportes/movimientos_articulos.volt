<style type="text/css">
    body {
        background-color: white;
        font-family: SourceSansPro;
        color: #000;
        font-size: 18px;
    }

	table {
		font-size: 14px;
		vertical-align: top;
	}

	.tj {
		border-top: 1px solid #eeeeee;
		padding: 4px;
	}

    .tg {
        border-collapse: collapse;
        border-spacing: 0;
        margin: 0px auto;
    }

    .tg td {
        font-family: SourceSansPro;
        padding: 12px;
        border-style: solid;
        border-width: 1px;
        overflow: hidden;
        word-break: normal;
        border-color: #eeeeee;
    }

    .tg .tg-yw4l { 
        vertical-align: top;
    }

    .titulo {
        margin-top: 4px;
        margin-bottom: 4px;
        font-family: SourceSansProBold;
        font-size: 24px;
        color: #000000;
        text-align: center;
    }

    .fs-24 {
        font-size: 24px;
    }

    .total {
        color: #000; 
        margin-top: 4px; 
        margin-bottom: 4px;  
    }
</style>
<div style="position: absolute">
    CONTROL DE PRODUCTOS<br/>
    TELECOMUNICACIONES MONAGAS<br/>
    R.I.F. J-08020670-3<br/>
</div>
<div style="text-align: right; margin-bottom: 48px">
	<div style="height: 20px"></div>

    {{ image("img/logo.png", "height": "27px") }}

</div>
<div class="titulo">
    <b>RESUMEN DE INGRESOS Y SALIDAS<br/></b>
</div>
<div style="margin-top: 32px"></div>
<div class="legend" style="border-left: 0; border-right: 0; background-color: #b2ebf2" align="center">
    <b>MOVIMIENTOS DE ARTÍCULOS</b>
</div>
<table width="100%" style="margin-top: 16px">
	<thead>
	  <tr>
	    <th class="tg">Fecha</th>
	    <th class="tg">Procesado por</th>
	    <th class="tg">Código de Artículo</th>
	    <th class="tg">Cantidad</th>
        <th class="tg">Tipo de Mov.</th>
        <th class="tg">Motivo</th>
	  </tr>
	</thead>
	<tbody>

	  {% set totIngresos = 0 %}
	  {% set totSalidas = 0 %}
	  {% set totExistencias = 0 %}
	  {% for movimiento in movimientos %}
	  	{% set totExistencias = totExistencias + movimiento.art_inv_inicial %}

		{% if movimiento.mov_tipo == "CARGA" %}
			{% set tipMovimiento = "INGRESO" %}
	  		{% set totIngresos = totIngresos + movimiento.mov_cantidad %}
	  		{% set totExistencias2 = totExistencias + movimiento.mov_cantidad %}
		{% else %}
			{% set tipMovimiento = "SALIDA" %}
	  		{% set totSalidas = totSalidas + movimiento.mov_cantidad %}
	  		{% set totExistencias2 = totExistencias - movimiento.mov_cantidad %}
		{% endif %}

        {% if movimiento.mov_pre_venta == "GARANTIA" %}
            {% set motivo = "GARANTIA" %}
        {% else %}
            {% set motivo = movimiento.mot_nombre %}
        {% endif %}

	    <tr {% if loop.index % 2 != 0 %} style="background-color: #fcfcfc" {% endif %}>
	      <td class="tj">{{ funciones.cambiaf_a_normal(movimiento.mov_fec_creacion) }}</td>
	      <td class="tj">{{ movimiento.usu_nombre ~ " (" ~ movimiento.usu_codigo ~ ")" }}</td>
	      <td class="tj">{{ movimiento.art_codigo }}</td>
	      <th class="tj">{{ movimiento.mov_cantidad }}</th>
          <td class="tj">{{ tipMovimiento }}</td>
          <td class="tj">{{ motivo }}</td>
	    </tr>

	  {% endfor %}

	</tbody>
</table>
<div align="right" style="margin-top: 32px">
    <span class="total fs-24">
    	<b>

    		{{ "Total de Ingresos: " ~ totIngresos }} <br/>

    	</b>
    </span>
    <span class="total fs-24">
    	<b>

    		{{ "Total de Salidas: " ~ totSalidas }} <br/>

    	</b>
    </span>
    {#<span class="total fs-24">
    	<b>

    		{{ "Total de Existencias: " ~ totExistencias }} <br/>

    	</b>
    </span>#}
  </b>
</div>
<div align="right" style="margin-top: 32px">
    
    {{ "<b>Generado Por:</b> " ~ funciones.getNombreUsuario() }}

</div>