<style type="text/css">
    body {
        background-color: white;
        font-family: SourceSansPro;
        color: #000000;
        font-size: 18px;
    }

	table {
		font-size: 12px;
		vertical-align: top;
	}

	.tj {
		border: 0.1mm solid #000000;
		padding: 5px;
	}

    .tg {
        border-collapse: collapse;
        border-spacing: 0;
        border: 0.1mm solid #000000;
        padding: 5px;
        background-color: #b2ebf2;
        text-align: center !important;
    }

    .tg th {
        font-family: SourceSansPro;
        padding: 12px;
        vertical-align: middle;
    }

    .titulo {
        margin-top: 4px;
        margin-bottom: 4px;
        font-family: SourceSansProBold;
        font-size: 24px;
        color: #000000;
        text-align: center;
    }

    .fs-24 {
        font-size: 24px;
    }

    .total {
        color: #000000; 
        margin-top: 4px; 
        margin-bottom: 4px;  
    }
</style>
<div style="position: absolute; line-height: 20px !important">
    Control de Productos<br/>
    Telecomunicaciones Monagas<br/>
    R.I.F. J-08020670-3<br/>
</div>
<div style="text-align: right; margin-bottom: 36px">
	<div style="height: 15px"></div>

    {{ image("img/logo.png", "height": "27px") }}

</div>
<div class="titulo">
    <b>DETALLES DE INGRESOS Y EGRESOS EN CUENTAS BANCARIAS<br/></b>
</div>
<table width="100%" style="margin-top: 24px">
	<thead>
	  <tr>
	    <th class="tg">FECHA</th>
	    <th class="tg">CUENTA BANCARIA</th>
	    <th class="tg">REFERENCIA</th>
	    <th class="tg">CLIENTE O DESCRIPCIÓN</th>
        <th class="tg">IMPORTE</th>
        <th class="tg">TIPO</th>
	  </tr>
	</thead>
	<tbody>

    {% set totBs = 0 %}
    {% set totDol = 0 %}
    {% set totEgrDol = 0 %}
    {% set totEgrBol = 0 %}
    {% for pago in pagos %}
        {% if pago["pag_importe"] is defined %}
            {% set fecha = funciones.cambiaf_a_normal(pago["fecha"]) %}

            {% if funciones.compararFechas(fecha, "20/08/2018") > 0 %}
                {% set importe = pago["pag_importe"] %}
            {% else %}
                {% set importe = pago["pag_importe"] / 10000 %}
            {% endif %}

            {% if pago["pag_moneda"] == "BOLÍVARES" %}
                {% set moneda = "Bs. " %}
                {% set totBs = totBs + importe %}
            {% else %}
                {% set moneda = "$ " %}
                {% set totDol = totDol + importe %}
            {% endif %}

    	    <tr {% if loop.index % 2 == 0 %} style="background-color: #EEEEEE" {% endif %} >
    	      <td class="tj"> {{ fecha }} </td>
    	      <td class="tj"> {{ pago["ban_nombre"] ~ " (*****" ~ funciones.substr(pago["cue_numero"], 14, 6) ~ ")" }} </td>
    	      <td class="tj"> {{ pago["pag_num_referencia"] }} </td>
    	      <td class="tj"> {{ pago["cli_nombre"] ~ " (" ~ pago["cli_codigo"] ~ ")" }} </td>
              <td class="tj" style="text-align: ',' center !important"> {{ moneda ~ funciones.number_format(importe) }} </td>
              <td class="tj"> {{ "<span style='padding: 0em 0.3em; font-size: 85%; display: inline-block; line-height: 1.35; color: rgba(255, 255, 255, 0.9); background-color: #8bc34a'>INGRESO</span>" }} </td>
    	    </tr>

        {% else %}
            {% if pago["egr_moneda"] == "BOLÍVARES" %}
                {% set moneda = "Bs. " %}
                {% set totEgrBol = totEgrBol + pago["egr_importe"] %}
            {% else %}
                {% set moneda = "$ " %}
                {% set totEgrDol = totEgrDol + pago["egr_importe"] %}
            {% endif %}

            <tr {% if loop.index % 2 == 0 %} style="background-color: #EEEEEE" {% endif %} >
                <td class="tj"> {{ funciones.cambiaf_a_normal(pago["fecha"]) }} </td>
                <td class="tj"> {{ pago["ban_nombre"] ~ " (*****" ~ funciones.substr(pago["cue_numero"], 14, 6) ~ ")" }} </td>
                <td class="tj"> {{ "" }} </td>
                <td class="tj"> {{ pago["egr_descripcion"] }} </td>
                <td class="tj"> {{ moneda ~ funciones.number_format(pago["egr_importe"]) }} </td>
                <td class="tj"> {{ "<span style='padding: 0em 0.3em; font-size: 85%; display: inline-block; line-height: 1.35; color: rgba(255, 255, 255, 0.9); background-color: #e51c23'>EGRESO</span>" }} </td>
            </tr>

        {% endif %}
    {% endfor %}

	</tbody>
</table>
<div align="right" style="margin-top: 32px; margin-bottom: 32px">
    <span class="total fs-24">
    	<b>

            {{ "Ingresos: $ " ~ funciones.number_format(totDol) ~ " - Bs. " ~ funciones.number_format(totBs) }} <br/>
            {{ "Egresos: $ " ~ funciones.number_format(totEgrDol) ~ " - Bs. " ~ funciones.number_format(totEgrBol) }} <br/>
            {{ "Total: $ " ~ funciones.number_format(totDol - totEgrDol) ~ " - Bs. " ~ funciones.number_format(totBs - totEgrBol) }} <br/>

    	</b>
    </span>
  </b>
</div>
<div align="right">
    
    {{ "<b>Generado Por:</b> " ~ funciones.getNombreUsuario() }}

</div>