<div class="row p-md">
	<div class="col-md-12">
		<table class="table table-striped table-bordered" cellspacing="0" width="100%" id="movimientos">
			<thead>
				<tr>
					<th>Fecha</th>
					<th>Procesado por</th>
					<th>Código de Artículo</th>
					<th>Cantidad</th>
					<th>Tipo de Movimiento</th>
					<th>Motivo</th>
				</tr>
			</thead>
			<tbody>

				{% for movimiento in movimientos %}
					{% set replace = funciones.str_replace("-", "", movimiento.mov_fec_creacion) %}

					{% if movimiento.mov_tipo == "CARGA" %}
						{% set tipMovimiento = "INGRESO" %}
					{% else %}
						{% set tipMovimiento = "SALIDA" %}
					{% endif %}

					{% if movimiento.mov_pre_venta == "GARANTIA" %}
						{% set motivo = "GARANTIA" %}
					{% else %}
						{% set motivo = movimiento.mot_nombre %}
					{% endif %}

					<tr>
						<td> {{ "<span>" ~ replace ~ "</span>" ~ funciones.cambiaf_a_normal(movimiento.mov_fec_creacion) }} </td>
						<td> {{ movimiento.usu_nombre ~ " (" ~ movimiento.usu_codigo ~ ")" }} </td>
						<td> {{ movimiento.art_codigo }} </td>
						<td> {{ movimiento.mov_cantidad }} </td>
						<td> {{ tipMovimiento }} </td>
						<td> {{ motivo }} </td>
					</tr>

				{% endfor %}

			</tbody>
		</table>
	</div>
</div>