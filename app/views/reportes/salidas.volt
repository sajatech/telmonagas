<style>
	.form-control[readonly] {
		background-color: white;
	}
</style>

{{ assets.outputCss() }}

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="container-fluid">
				<div data-widget-group="group1">
					<div class="panel panel-success">
						<div class="panel-heading">
							<h1 class="title c-w fz-24">{##}Resumen de Salidas</h1>
						</div>
						<div class="panel-body bc-g">

							{{ form("autocomplete": "off", "id": "forMovimiento", "class": "form-horizontal") }}

								<div class="row mb-md">
									<label class="col-sm-3 text-right">Rango de Fechas</label>
									<div class="col-sm-7">
										<div class="input-daterange input-group col-xs-12" id="datepicker-range">

											{{ text_field("desde", "class": "input-small form-control cur-pointer", "placeholder": "Ingrese Fecha de Inicio") }}

											<span class="input-group-addon">al</span>

											{{ text_field("hasta", "class": "input-small form-control cur-pointer", "placeholder": "Ingrese Fecha de Fin") }}

										</div>
									</div>
								</div>
								<div class="row mb-md">
									<label for="usuario" class="col-sm-3 text-right">Procesado por</label>
									<div class="col-sm-7">
										<select name="usuario" id="usuario" required="required">
											<option value="">-- SELECCIONE --</option>

											{% for usuario in usuarios %}

												<option value="{{ usuario.id }}">{{ usuario.usu_nombre ~ " (" ~ usuario.usu_codigo ~ ")" }}</option>

											{% endfor %}

										</select>
									</div>
								</div>
								<div class="row mb-md">
									<label for="articulo" class="col-sm-3 text-right">Código del Artículo</label>
									<div class="col-sm-7">
										<select name="codigo" id="codigo" class="articulo">
											<option value="">-- SELECCIONE --</option>

											{% for articulo in articulos %}

												<option value="{{ articulo.id }}"> {{ articulo.art_codigo }} </option>

											{% endfor %}

										</select>
									</div>
								</div>			
								<div class="row mb-md">
									<label for="articulo" class="col-sm-3 text-right">Descripción del Artículo</label>
									<div class="col-sm-7">
										<select name="descripcion" id="descripcion" class="articulo">
											<option value="">-- SELECCIONE --</option>

											{% for articulo in articulos %}

												<option value="{{ articulo.id }}"> {{ articulo.art_descripcion }} </option>

											{% endfor %}

										</select>
									</div>
								</div>			

							{{ end_form() }}

						</div>
						<div class="panel-footer">
							<div class="row">
								<div class="col-sm-7 col-sm-offset-3">

									{{ submit_button("Consultar", "class": "btn btn-inverse btn", "id": "consultar") }}

            						{% if funciones.in_array(46, priRol) %}

										{{ link_to("#", "<i class='ti ti-download'></i> Generar Pdf", "class": "btn btn-success disabled", "id": "getsalidaspdf", false) }}

									{% endif %}

									<span id="loaSalida" class="loading-right hide">

										{{ image("img/loading.gif", "width": "32px") }}

									</span>
								</div>
							</div>
						</div>
						<div class="panel-body p-n" id="resultado"></div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
          errorsWrapper: "<span class='help-block'></span>", 
          errorTemplate: "<span></span>"
    };
</script>