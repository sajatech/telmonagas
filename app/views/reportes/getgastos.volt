<style type="text/css">
	.tablas {
		font-size: 22px;
		vertical-align: top;
		font-weight: bold;
	}

	.red {
		color: red;
	}
</style>

<div class="row p-md">
	<div class="col-md-12">
		<div class="row tablas">
			<div class="col-sm-2">Total Bolivares: </div>
			<div class="col-sm-3 red" align="left" id="totImportes"></div>
		</div>
		<div class="row tablas">
			<div class="col-sm-2">Total Dolares: </div>
			<div class="col-sm-3 red" align="left" id="totDivisas"></div>
		</div>
		<div class="row tablas">
			<div class="col-sm-2">Total Euros: </div>
			<div class="col-sm-3 red" align="left" id="totEuro"></div>
		</div>
		<br/>
		<table class="table table-striped table-bordered" cellspacing="0" width="100%" id="gastos">
			<thead>
				<tr>
					<th>Descripción</th>
					<th>Fecha</th>
					<th width="107px">Monto</th>
					<th width="162px">Divisa</th>
					<th>Concepto</th>
					<th>Estatus</th>
				</tr>
			</thead>
			<tbody>

				{% set total = 0 %}
				{% set total_dolar = 0 %}
				{% set total_euro = 0 %}
				{% for gasto in gastos %}
					{% set importe = gasto.gas_cantidad * gasto.gas_costo %}
					{% set importe = funciones.number_format(importe) %}

					<tr>
						<td> {{ gasto.gas_descripcion }} </td>
						<td> {{ funciones.cambiaf_a_normal(gasto.gas_fecha) }} </td>
						<td> {{ importe }} </td>
						<td> {{ gasto.gas_divisas }} </td>
						<td> {{ gasto.cat_nombre }} </td>
						<td>
							<span class="label {{ gasto.gas_estatus == "PAGADO" ? "label-success" : "label-danger" }}">
						 		
						 		{{ gasto.gas_estatus }} 

						 	</span>
						 </td>
					</tr>

					{% if gasto.gas_estatus != "CANCELADO" and gasto.gas_divisas == "BOLIVARES" %}
						{% set total = total + (gasto.gas_cantidad * gasto.gas_costo) %}
					{% endif %}
					{% if gasto.gas_estatus != "CANCELADO" and gasto.gas_divisas == "DOLARES" %}
						{% set total_dolar = total_dolar + (gasto.gas_cantidad * gasto.gas_costo) %}
					{% endif %}
					{% if gasto.gas_estatus != "CANCELADO" and gasto.gas_divisas == "EUROS" %}
						{% set total_euro = total_euro + (gasto.gas_cantidad * gasto.gas_costo) %}
					{% endif %}

				{% endfor %}

			</tbody>
		</table>
		<span id="total" class="hide"> {{ "Bs. " ~ funciones.number_format(total) }} </span>
		<span id="spaTotDivisas" class="hide"> {{ "$ " ~ funciones.number_format(total_dolar) }} </span>
		<span id="spaTotEuro" class="hide"> {{ "€ " ~ funciones.number_format(total_euro) }} </span>
	</div>
</div>