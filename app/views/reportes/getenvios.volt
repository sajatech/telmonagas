<div class="row p-md">
	<div class="col-md-12">
		<div align="right"><b>Total Envios: </b> {{ totEnvios }}</div>
		<div align="right"><b>Monto Total Ventas (Bs): </b>  {{ funciones.number_format(totalVentas) }}</div>
		<div align="right"><b>Top 3 de Clientes:</b> <br> 
		{{ topEnvios[0].env_nombre ~ " con " ~ topEnvios[0].total  ~ " envios" }}<br>
		{{ topEnvios[1].env_nombre ~ " con " ~ topEnvios[1].total  ~ " envios" }}<br>
		{{ topEnvios[2].env_nombre ~ " con " ~ topEnvios[2].total  ~ " envios" }}<br> </div><br>
		<table class="table table-striped table-bordered nowrap dt-responsive" cellspacing="0" width="100%" id="envios">
			<thead>
				<tr>
					<th>Fecha</th>
					<th>Cliente</th>
					<th>Empresa Envio</th>
					<th>Estatus</th>
					<th>Procesado por</th>
					<th>Monto Venta (Bs)</th>
					<!--<th>Acciones</th>-->
				</tr>
			</thead>
			<tbody>

				{% for envio in envios %}

					<tr>
						<td>{{ funciones.cambiaf_a_normal(envio.env_fecha) }}</td>
						<td>{{ envio.env_nombre  }}</td>
						<td>{{ envio.env_empresa ~ " (" ~ envio.env_guia ~ ")" }}</td>
						<td>{{ envio.env_correo_estatus }}</td>
						<td>{{ envio.usu_nombre }}</td>
						<td>{{ funciones.number_format(envio.env_monto_salida) }}</td>
					</tr>

				{% endfor %}

			</tbody>
		</table>
	</div>
</div>