{{ assets.outputCss() }}

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="container-fluid">
				<div data-widget-group="group1">
					<div class="panel panel-success">
						<div class="panel-heading">
							<h1 class="title c-w fz-24">Detalles de Ingresos y Egresos en Cuentas Bancarias</h1>
							<div class="panel-ctrls">

        						{% if funciones.in_array(77, priRol) %}

									{{ link_to("#", "<i class='ti ti-plus'></i><span> Agregar Egreso</span>", "id": "agrEgreso", "class": "btn btn-default ctrls pull-left mt-sm mr-xs", false) }}
								
								{% endif %}

							</div>
						</div>
						<div class="panel-body bc-g">

							{{ form("autocomplete": "off", "id": "forPago", "class": "form-horizontal") }}

								<div class="row mb-md">
									<label class="col-sm-3 text-right">Rango de Fechas</label>
									<div class="col-sm-7">
										<div class="input-daterange input-group col-xs-12" id="datepicker-range">

											{{ text_field("desde", "class": "input-small form-control cur-pointer", "placeholder": "Ingrese Fecha de Inicio") }}

											<span class="input-group-addon">al</span>

											{{ text_field("hasta", "class": "input-small form-control cur-pointer", "placeholder": "Ingrese Fecha de Fin") }}

										</div>
									</div>
								</div>
								<div class="row mb-md">
									<label for="banco" class="col-sm-3 text-right">Cuenta Bancaria</label>
									<div class="col-sm-7">
										<select name="cuenta" id="cuenta" required="required">
											<option value="">-- SELECCIONE --</option>

											{% for cuenta in cuentas %}

												<option value="{{ cuenta.id }}"> {{ cuenta.ban_nombre ~ " (*****" ~ funciones.substr(cuenta.cue_numero, 14, 6) ~ ")" }} </option>

											{% endfor %}

										</select>
									</div>
								</div>				
								<div class="row mb-md">
									<label for="descripcion" class="col-sm-3 text-right">Descripción / Cliente</label>
									<div class="col-sm-7">
										{{ text_field("descripcion", "class": "form-control", "placeholder": "Buscar por Descripción / Cliente") }}
									</div>
								</div>								

							{{ end_form() }}

						</div>
						<div class="panel-footer">
							<div class="row">
								<div class="col-sm-7 col-sm-offset-3">

									{{ submit_button("Consultar", "class": "btn btn-inverse btn", "id": "consultar") }}

            						{% if funciones.in_array(46, priRol) %}

										{{ link_to("#", "<i class='ti ti-download'></i> Generar Pdf", "class": "btn btn-success disabled", "id": "genpagospdf", false) }}

									{% endif %}

									<span id="loaPago" class="loading-right hide">

										{{ image("img/loading.gif", "width": "32px") }}

									</span>
								</div>
							</div>
						</div>
						<div class="panel-body p-n" id="resultado"></div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>

{# Modal de egresos en cuentas bancarias #}
<div class="modal fade" id="modEgresos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bc-8bc">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title c-w fz-24">Registro de egreso</h2>
            </div>

			{#{% if funciones.in_array(39, priRol) %}#}

	            <div class="modal-body bc-g">

	                {{ form("autocomplete": "off", "id": "forEgreso", "data-parsley-validate": "data-parsley-validate") }}

	                    <div class="row">
	                        <div class="col-sm-12 closest mb-md">
	                            <label for="modCuenta" class="control-label required">Cuenta Bancaria</label>
								<select name="modCuenta" id="modCuenta" required="required">
									<option value="">-- SELECCIONE --</option>

									{% for cuenta in cuentas %}

										<option value="{{ cuenta.id }}"> {{ cuenta.ban_nombre ~ " (*****" ~ funciones.substr(cuenta.cue_numero, 14, 6) ~ ")" }} </option>

									{% endfor %}

								</select>
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class="col-sm-4 closest mb-md">
	                            <label for="importe" class="control-label required">Importe (Bs./$)</label>

								{{ text_field("importe", "class": "form-control", "placeholder": "0,00", "required": "required", "data-parsley-importe": "data-parsley-importe", "data-parsley-error-message": "<i class='fa fa-times-circle'></i> Ingrese importe.") }}

	                        </div>
							<div class="col-sm-4 closest mb-md required">
								<h4 class="mb-md mt-n">Moneda</h4>
								
								{{ select_static("moneda", ["BOLÍVARES": "BOLÍVARES (Bs.)", "DÓLARES": "DÓLARES ($)"], "required": "required") }}

							</div>
	                        <div class="col-sm-4 closest mb-md">
	                            <label for="fecEgreso" class="control-label required">Fecha de Egreso</label>
								<div class="input-group">
									<span class="input-group-addon">
										<i class="ti ti-calendar"></i>
									</span>

									{{ text_field("fecEgreso", "class": "input-small form-control cur-pointer", "required": "required", "placeholder": date("d/m/Y"), "value": date("d/m/Y"), "data-parsley-error-message": "<i class='fa fa-times-circle'></i> Ingrese fecha.") }}

								</div>
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class="col-sm-12 closest mb-md">
	                            <label for="descripcion" class="control-label required">Descripción</label>
								
								{{ text_area("descripcion", "class": "form-control", "placeholder": "Ingrese Descripción", "rows": 4, "required": "required") }}

	                        </div>
	                    </div>

	                {{ end_form() }}
	                
	            </div>

	        {#{% endif %}#}

            <div class="modal-footer">
                <span id="loaEgreso" class="hide loading">

                    {{ image("img/loading.gif", "width": "32px") }}

                </span>
                <button type="button" class="btn btn-inverse" id="regEgreso">Registrar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar">Cerrar</button>
            </div>

            {{ end_form() }}

        </div>
    </div>
</div>

{# ******************************** #}

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
          errorsWrapper: "<span class='help-block'></span>", 
          errorTemplate: "<span></span>"
    };
</script>