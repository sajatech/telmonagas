<style type="text/css">
    body {
        background-color: white;
        font-family: SourceSansPro;
        color: #000;
        font-size: 18px;
    }

	table {
		font-size: 14px;
		vertical-align: top;
	}

	.tj {
		border-top: 1px solid #eeeeee;
		padding: 4px;
	}

    .tg {
        border-collapse: collapse;
        border-spacing: 0;
        margin: 0px auto;
    }

    .tg td {
        font-family: SourceSansPro;
        padding: 12px;
        border-style: solid;
        border-width: 1px;
        overflow: hidden;
        word-break: normal;
        border-color: #eeeeee;
    }

    .tg .tg-yw4l { 
        vertical-align: top;
    }

    .titulo {
        margin-top: 4px;
        margin-bottom: 4px;
        font-family: SourceSansProBold;
        font-size: 24px;
        color: #000000;
        text-align: center;
    }

    .fs-24 {
        font-size: 24px;
    }

    .total {
        color: #000; 
        margin-top: 4px; 
        margin-bottom: 4px;  
    }
</style>
<div style="position: absolute">
    CONTROL DE PRODUCTOS<br/>
    TELECOMUNICACIONES MONAGAS<br/>
    R.I.F. J-08020670-3<br/>
</div>
<div style="text-align: right; margin-bottom: 48px">
	<div style="height: 20px"></div>

    {{ image("img/logo.png", "height": "27px") }}

</div>
<div class="titulo">
    <b>RESUMEN DE SALIDAS<br/></b>
</div>
<div style="margin-top: 32px"></div>
<div class="legend" style="border-left: 0; border-right: 0; background-color: #b2ebf2" align="center">
    <b>LISTADO DE SALIDAS</b>
    <?php if($this->request->get("desde")) { ?>
    del <?=$this->request->get("desde");?> al <?= $this->request->get("hasta");?>
    <?php }?>
</div>
<table width="100%" style="margin-top: 16px">
	<thead>
	    <tr>
            <th>Codigo</th>
            <th>Fecha</th>
            <th>Cliente</th>
            <th>Procesado por</th>
            <th>Monto Costo (Bs)</th>
            <th>Monto Venta (Bs)</th>
	    </tr>
	</thead>
	<tbody>

	    {% for salida in salidas %}

            <tr {% if loop.index % 2 != 0 %} style="background-color: #999" {% endif %}>
                <td class="tj">{{ funciones.str_pad(salida.id) }}</td>
                <td class="tj">{{ funciones.cambiaf_a_normal(salida.sal_fec_creacion) }}</td>
                <td class="tj">{{ salida.cli_nombre  }} {% if salida.sal_garantia == 1 %} {{ "(POR GARANTIA)" }} {% endif %}</td>
                <td class="tj">{{ salida.usu_nombre }}</td>
                <td class="tj">{{ funciones.number_format(salida.suma_costo) }}</td>
                <td class="tj">{{ funciones.number_format(salida.valor_suma + salida.sal_iva) }}</td>

            </tr>

        {% endfor %}

	</tbody>
</table>
<div align="right" style="margin-top: 32px">
    <span class="total fs-24">
    	<b>

    		{{ "Total de salidas: " ~ totSalidas }} <br/>

    	</b>
    </span>
    <span class="total fs-24">
    	<b>

            {{ "Monto Total Ventas (Bs): " ~ funciones.number_format(totalVentas) }} <br/>

            {{ "Monto Total Costo (Bs): " ~ funciones.number_format(totalCosto) }} <br/>

            {{ "Monto Total Gastos Operativos (Bs): " ~ funciones.number_format(totalGastos) }} <br/>

            {{ "Monto Total Ganancias (Bs): " ~ funciones.number_format(totalVentas-totalCosto-totalGastos) }} <br/>

            {{ "Monto Total Ganancias ($): " ~ funciones.number_format((totalVentas-totalCosto-totalGastos) / dolar) }} <br/>

    	</b>
    </span>
<!--     <span class="total fs-24">
    	<div align="right"><b>Top 3 de Clientes:</b> <br> 
        {{ topSalidas[0].cli_nombre ~ " con " ~ topSalidas[0].total  ~ " ventas" }}<br>
        {{ topSalidas[1].cli_nombre ~ " con " ~ topSalidas[1].total  ~ " ventas" }}<br>
        {{ topSalidas[2].cli_nombre ~ " con " ~ topSalidas[2].total  ~ " ventas" }}<br> </div><br>
    </span> -->
  </b>
</div>
<div align="right" style="margin-top: 32px">
    
    {{ "<b>Reporte Generado Por:</b> " ~ funciones.getNombreUsuario() }}

</div>