{{ assets.outputCss() }}

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="container-fluid">
				<div data-widget-group="group1">
					<div class="panel panel-success">
						<div class="panel-heading">
							<h1 class="title c-w fz-24">Reporte de Gastos Emitidos</h1>
						</div>
						<div class="panel-body bc-g">

							{{ form("autocomplete": "off", "id": "forGasto", "class": "form-horizontal") }}

								<div class="row mb-md">
									<label class="col-sm-3 text-right">Rango de Fechas</label>
									<div class="col-sm-7">
										<div class="input-daterange input-group col-xs-12" id="datepicker-range">

											{{ text_field("desde", "class": "input-small form-control cur-pointer", "placeholder": "Ingrese Fecha de Inicio") }}

											<span class="input-group-addon">al</span>

											{{ text_field("hasta", "class": "input-small form-control cur-pointer", "placeholder": "Ingrese Fecha de Fin") }}

										</div>
									</div>
								</div>
								<div class="row mb-md">
									<label for="banco" class="col-sm-3 text-right">Concepto de Gasto</label>
									<div class="col-sm-7">
										<select name="catGasto" id="catGasto" required="required">
											<option value="">-- SELECCIONE --</option>

											{% for categoria in catGastos %}

												<option value="{{ categoria.id }}"> {{ categoria.cat_nombre }} </option>

											{% endfor %}

										</select>
									</div>
								</div>								

							{{ end_form() }}

						</div>
						<div class="panel-footer">
							<div class="row">
								<div class="col-sm-7 col-sm-offset-3">

									{{ submit_button("Consultar", "class": "btn btn-inverse btn", "id": "consultar") }}

            						{#{% if funciones.in_array(46, priRol) %}#}

										{{ link_to("#", "<i class='ti ti-download'></i> Generar Pdf", "class": "btn btn-success disabled", "id": "gengastospdf", false) }}

									{#{% endif %}#}

									<span id="loaGasto" class="loading-right hide">

										{{ image("img/loading.gif", "width": "32px") }}

									</span>
								</div>
							</div>
						</div>
						<div class="panel-body p-n" id="resultado"></div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
          errorsWrapper: "<span class='help-block'></span>", 
          errorTemplate: "<span></span>"
    };
</script>