{{ assets.outputCss() }}

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            {#<ol class="breadcrumb">                        
				<li>

					{{ link_to("inicio", "Inicio") }}

				</li>
				<li>

					{{ link_to("reportes/pagos", "Reportes") }}

				</li>
				<li class="active">

					{{ link_to("reportes/pagos", "General de Pagos") }}

				</li>
            </ol>#}
            <div class="container-fluid">
				<div data-widget-group="group1">
					<div class="panel panel-success">
						<div class="panel-heading">
							<h1 class="title c-w fz-24">Resumen de Envios</h1>
						</div>
						<div class="panel-body bc-g">

							{{ form("autocomplete": "off", "id": "forMovimiento", "class": "form-horizontal") }}

								<div class="row mb-md">
									<label class="col-sm-3 text-right">Rango de Fechas</label>
									<div class="col-sm-7">
										<div class="input-daterange input-group col-xs-12" id="datepicker-range">

											{{ text_field("desde", "class": "input-small form-control") }}

											<span class="input-group-addon">al</span>

											{{ text_field("hasta", "class": "input-small form-control") }}

										</div>
									</div>
								</div>
								<div class="row mb-md">
									<label for="usuario" class="col-sm-3 text-right">Procesado por</label>
									<div class="col-sm-7">
										<select name="usuario" id="usuario" required="required">
											<option value="">-- SELECCIONE --</option>

											{% for usuario in usuarios %}

												<option value="{{ usuario.id }}">{{ usuario.usu_nombre ~ " (" ~ usuario.usu_codigo ~ ")" }}</option>

											{% endfor %}

										</select>
									</div>
								</div>										

							{{ end_form() }}

						</div>
						<div class="panel-footer">
							<div class="row">
								<div class="col-sm-7 col-sm-offset-3">

									{{ submit_button("Consultar", "class": "btn btn-inverse btn", "id": "consultar") }}

            						{% if funciones.in_array(46, priRol) %}

										{{ link_to("#", "<i class='ti ti-download'></i> Generar Pdf", "class": "btn btn-success disabled", "id": "getenviospdf", false) }}

									{% endif %}

									<span id="loaEnvio" class="loading-right hide">

										{{ image("img/loading.gif", "width": "32px") }}

									</span>
								</div>
							</div>
						</div>
						<div class="panel-body p-n" id="resultado"></div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
          errorsWrapper: "<span class='help-block'></span>", 
          errorTemplate: "<span></span>"
    };
</script>