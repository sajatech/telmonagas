<div class="row p-md">
	<div class="col-md-12">
		<div align="left"><b>Total Items: </b> {{ total }}</div><br>
		<table class="table table-striped table-bordered" cellspacing="0" width="100%" id="productos">
			<thead>
				<tr>
					<th>Codigo</th>
					<th>Descripcion</th>
					<th>Cant. Ingresada</th>
					<th>Cant. Ventas</th>
					<th>Cant. Existente</th>
					<th>Valor % de Venta</th>
				</tr>
			</thead>
			<tbody>
				{% for productos in product %}

					<tr>
						<td>{{ productos.art_codigo }}</td>
						<td>{{ productos.art_descripcion }}</td>
						<td>{{ productos.art_inv_inicial + productos.mov_carga }}</td>
					{% if productos.total_ventas is not empty %}	
						<td>{{ productos.total_ventas }} </td>
					{% else %}
						<td>0 </td>
					{% endif %}	
						<td>{{ productos.art_inv_inicial - productos.total_ventas + productos.mov_carga}}</td>	
					{% if productos.total_ventas is not empty %}							
						<td>{{ funciones.number_format( (productos.total_ventas * 100) / (productos.art_inv_inicial + productos.mov_carga) )  }} % </td>
					{% else %}
						<td>0 % </td>
					</tr>
					{% endif %}

				{% endfor %}

			</tbody>
		</table>
	</div>
</div>