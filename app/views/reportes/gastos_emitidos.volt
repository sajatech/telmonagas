<style type="text/css">
    body {
        background-color: white;
        font-family: SourceSansPro;
        color: #000000;
        font-size: 18px;
    }

	table {
		font-size: 12px;
		vertical-align: top;
	}

	.tj {
		border: 0.1mm solid #000000;
		padding: 5px;
	}

    .tg {
        border-collapse: collapse;
        border-spacing: 0;
        border: 0.1mm solid #000000;
        padding: 5px;
        background-color: #b2ebf2;
        text-align: center !important;
    }

    .tg th {
        font-family: SourceSansPro;
        padding: 12px;
        vertical-align: middle;
    }

    .titulo {
        margin-top: 4px;
        margin-bottom: 4px;
        font-family: SourceSansProBold;
        font-size: 24px;
        color: #000000;
        text-align: center;
    }

    .fs-24 {
        font-size: 24px;
    }

    .total {
        color: #000000; 
        margin-top: 4px; 
        margin-bottom: 4px;  
    }
</style>
<div style="position: absolute; line-height: 20px !important">
    Control de Productos<br/>
    Telecomunicaciones Monagas<br/>
    R.I.F. J-08020670-3<br/>
</div>
<div style="text-align: right; margin-bottom: 36px">
	<div style="height: 15px"></div>

    {{ image("img/logo.png", "height": "27px") }}

</div>
<div class="titulo">
    <b>REPORTE DE GASTOS EMITIDOS<br/></b>
</div>
<table width="100%" style="margin-top: 24px">
	<thead>
	  <tr>
	    <th class="tg">DESCRIPCIÓN</th>
	    <th class="tg">FECHA</th>
	    <th class="tg">IMPORTE</th>
	    <th class="tg">CONCEPTO</th>
        <th class="tg">ESTATUS</th>
	  </tr>
	</thead>
	<tbody>

        {% set total = 0 %}
        {% for gasto in gastos %}
            {% set importe = gasto.gas_cantidad * gasto.gas_costo %}

            <tr {% if loop.index % 2 == 0 %} style="background-color: #EEEEEE" {% endif %} >
              <td class="tj"> {{ gasto.gas_descripcion }} </td>
              <td class="tj"> {{ funciones.cambiaf_a_normal(gasto.gas_fecha) }} </td>
              <td class="tj" style="text-align: ',' center !important"> {{ "Bs. " ~ funciones.number_format(importe) }} </td>
              <td class="tj"> {{ gasto.cat_nombre }} </td>
              <td class="tj"> {{ gasto.gas_estatus }} </td>
            </tr>
	   
            {% if gasto.gas_estatus != "CANCELADO" %}
                {% set total = total + importe %}
            {% endif %}
            
        {% endfor %}

	</tbody>
</table>
<div align="right" style="margin-top: 32px">
    <span class="total fs-24">
    	<b>

    		{{ "Total de Gastos: Bs. " ~ funciones.number_format(total) }} <br/>

    	</b>
    </span>
  </b>
</div>
<div align="right">
    
    {{ "<b>Generado Por:</b> " ~ funciones.getNombreUsuario() }}

</div>