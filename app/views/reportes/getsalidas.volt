<style type="text/css">
	.tablas {
		font-size: 20px;
		vertical-align: top;
		font-weight: bold;
	}

	.red {
		color: red;
	}

	.font16 {
		font-size: 16px;
	}
</style>

<div class="row p-md">
	<div class="col-md-12">

		{% if totalVentasAntes is defined %}
			<div class="row tablas" align="left">
				<div class="col-sm-3 mb-md font16"> * Antes del 27/07/2018: </div>
				<div class="col-sm-7 mb-md font16" align="left">{{  "Bs. " ~ funciones.number_format(totalVentasAntes)}}</div>
			</div>
		{% endif %}

		<div class="row tablas">
			<div class="col-sm-3 mb-md">Total Salidas: </div>
			<div class="col-sm-2 mb-md" align="left"> {{ totSalidas }}</div>
			<div class="col-sm-5 mb-md" align="right">IVA Acumulado: </div>
			<div class="col-sm-2 mb-md" id="ivaAcumulado"></div>
		</div>
		<div class="row tablas">
			<div class="col-sm-3 mb-md">Pagos en Divisas: </div>
			<div class="col-sm-7 mb-md" align="left" id="pagDivisas"></div>
		</div>
		<div class="row tablas">
			<div class="col-sm-2 mb-md"></div>
			<div class="col-sm-3 mb-md" align="left"></div>
			<div class="col-sm-1 mb-md" align="center"></div>
			<div class="col-sm-3 mb-md td-u" align="left">Al día de las ventas</div>
			<div class="col-sm-1 mb-md" align="center"></div>
			<div class="col-sm-2 mb-md td-u" align="left">Al día de hoy</div>
		</div>
		<div class="row tablas">
			<div class="col-sm-3 mb-md">Precios de los Productos: </div>
			<div class="col-sm-2 mb-md" align="left" id="totVentas1"></div>
			<div class="col-sm-1 mb-md" align="center"> = </div>
			<div class="col-sm-3 mb-md red" align="left" id="totVentas2"></div>
			<div class="col-sm-1 mb-md" align="center"> = </div>
			<div class="col-sm-2 mb-md red" align="left" id="totVentas3"></div>
		</div>
		<div class="row tablas">
			<div class="col-sm-3 mb-md">Monto Pagado: </div>
			<div class="col-sm-2 mb-md" align="left" id="totPagado1"></div>
			<div class="col-sm-1 mb-md" align="center"> = </div>
			<div class="col-sm-3 mb-md red" align="left" id="totPagado2"></div>
			<div class="col-sm-1 mb-md" align="center"> = </div>
			<div class="col-sm-2 mb-md red" align="left" id="totPagado3"></div>
		</div>
		<div class="row tablas">
			<div class="col-sm-3 mb-md">Costo de los Productos: </div>
			<div class="col-sm-2 mb-md" align="left" id="totCostos1"></div>
			<div class="col-sm-1 mb-md" align="center"> = </div>
			<div class="col-sm-3 mb-md red" align="left" id="totCostos2"></div>
			<div class="col-sm-1 mb-md" align="center"> = </div>
			<div class="col-sm-2 mb-md red" align="left" id="totCostos3"></div>
		</div>
		<div class="row tablas">
			<div class="col-sm-3 mb-md">Gastos Generales: </div>
			<div class="col-sm-2 mb-md" align="left"> {{ "Bs. " ~ funciones.number_format(gastos[0].tot_gastos) }} </div>
			<div class="col-sm-1 mb-md" align="center"> = </div>
			<div class="col-sm-3 mb-md red" align="left"> {{ "$ " ~ funciones.number_format(gastos[0].tot_gas_dolares) }} </div>
			<div class="col-sm-1 mb-md" align="center"> = </div>
			<div class="col-sm-2 mb-md red" align="left"> {{ "$ " ~ funciones.number_format(gastos[0].tot_gastos / dolar) }} </div>
		</div>
		<div class="row tablas">
			<div class="col-sm-3 mb-md">Ganancias: </div>
			<div class="col-sm-2 mb-md " align="left" id="totGanancias1"></div>
			<div class="col-sm-1 mb-md" align="center"> = </div>
			<div class="col-sm-3 mb-md red" align="left" id="totGanancias2"></div>
			<div class="col-sm-1 mb-md" align="center"> = </div>
			<div class="col-sm-2 mb-md red" align="left" id="totGanancias3"></div>
		</div>
		<br/>
		<table class="table table-striped table-bordered nowrap dt-responsive" cellspacing="0" width="100%" id="salidas">
			<thead>
				<tr>
					<th>Código</th>
					<th>Fecha</th>
					<th>Cliente</th>
					<th>Procesado por</th>
					<th>Precio de Costo</th>
					<th>Precio de Venta</th>
					<th>Precio Pagado</th>
					<th>Artículos</th>
				</tr>
			</thead>
			<tbody>

				{% set totVentas1 = 0 %}
				{% set totVentas2 = 0 %}
				{% set totCostos1 = 0 %}
				{% set totCostos2 = 0 %}
				{% set totPagado1 = 0 %}
				{% set totPagado2 = 0 %}
				{% set pagDivisas = 0 %}
				{% set pagDivisas2 = 0 %}
				{% set ivaAcumulado = 0 %}
				{% for salida in salidas %}
					{% set monVenta = salida.mon_venta + salida.sal_iva %}
					{% set monPagBolivares = salida.pag_bolivares / salida.pag_bol_count %}
					{% set monPagDolares = salida.pag_dolares / salida.pag_dol_count %}
					{% set totVentas1 = totVentas1 + monVenta %}
					{% set totVentas2 = totVentas2 + salida.mon_ven_dolares %}
					{% set totCostos1 = totCostos1 + salida.mon_costo %}
					{% set totCostos2 = totCostos2 + salida.mon_cos_dolares %}
					{% set totPagado1 = totPagado1 + monPagBolivares %}
					{% set totPagado2 = totPagado2 + (salida.pag_bol_dolares / salida.pag_bol_count) %}
					{% set pagDivisas = pagDivisas + monPagDolares %}
					{% set pagDivisas2 = pagDivisas2 + (monPagDolares * salida.dol_monto) %}
					{% set ivaAcumulado = ivaAcumulado + salida.sal_iva %}

					<tr>
						<td width="6.79%"> {{ funciones.str_pad(salida.id) }} </td>
						<td width="7.74%"> {{ funciones.cambiaf_a_normal(salida.sal_fec_creacion) }} </td>
						<td width="17.62%"> {{ salida.cli_nombre  }} {% if salida.sal_garantia == 1 %} {{ "(POR GARANTIA)" }} {% endif %} </td>
						<td width="11.34%"> {{ salida.usu_nombre }} </td>
						<td width="17.76%"> {{ "Bs. " ~ funciones.number_format(salida.mon_costo) ~ " = $ " ~ funciones.number_format(salida.mon_cos_dolares) }} </td>
						<td width="17.76%"> {{ "Bs. " ~ funciones.number_format(monVenta) ~ " = $ " ~ funciones.number_format(salida.mon_ven_dolares) }} </td>
						<td width="16.41%"> {{ "Bs. " ~ funciones.number_format(monPagBolivares) ~ " / $ " ~ funciones.number_format(monPagDolares) }}</td>
						<td width="4.58%"> <a class="btn btn-success detalle" data-salida-id="{{ salida.id }}">Detalle</a> </td>
					</tr>

				{% endfor %}

			</tbody>
		</table>

		{% set pagado = totPagado1 / dolar %}
		{% set costos = totCostos1 / dolar %}

		<span id="spaTotCostos1" class="hide"> {{ "Bs. " ~ funciones.number_format(totCostos1) }} </span>
		<span id="spaTotCostos2" class="hide"> {{ "$ " ~ funciones.number_format(totCostos2) }} </span>
		<span id="spaTotCostos3" class="hide"> {{ "$ " ~ funciones.number_format(costos) }} </span>
		<span id="spaTotVentas1" class="hide"> {{ "Bs. " ~ funciones.number_format(totVentas1) }} </span>
		<span id="spaTotVentas2" class="hide"> {{ "$ " ~ funciones.number_format(totVentas2) }} </span>
		<span id="spaTotVentas3" class="hide"> {{ "$ " ~ funciones.number_format(totVentas1 / dolar) }} </span>
		<span id="spaTotPagado1" class="hide"> {{ "Bs. " ~ funciones.number_format(totPagado1) }} </span>
		<span id="spaTotPagado2" class="hide"> {{ "$ " ~ funciones.number_format(totPagado2) }} </span>
		<span id="spaTotPagado3" class="hide"> {{ "$ " ~ funciones.number_format(totPagado1 / dolar) }} </span>
		<span id="spaTotGanancias1" class="hide"> {{ "Bs. " ~ funciones.number_format(totPagado1 - totCostos1 - gastos[0].tot_gastos + pagDivisas2) }} </span>
		<span id="spaTotGanancias2" class="hide"> {{ "$ " ~ funciones.number_format(totPagado2 - totCostos2 - gastos[0].tot_gas_dolares + pagDivisas) }} </span>
		<span id="spaTotGanancias3" class="hide"> {{ "$ " ~ funciones.number_format(pagado - costos - (gastos[0].tot_gastos / dolar) + pagDivisas) }} </span>
		<span id="spaPagDivisas" class="hide"> {{ "$ " ~ funciones.number_format(pagDivisas) }} </span>
		<span id="spaIvaAcumulado" class="hide"> {{ "Bs. " ~ funciones.number_format(ivaAcumulado) }} </span>
	</div>
</div>

{# Modal de articulos #}
<div class="modal fade" id="modArt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bc-8bc">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h2 class="modal-title c-w fz-24">Articulos de la Salida</h2>
			</div>
			<div class="modal-body bc-g">

			<table class="table table-striped table-bordered" cellspacing="0" width="100%" id="salidas">
				<thead>
					<tr>
						<th>Articulo</th>
						<th>Cantidad</th>
					</tr>
				</thead>
				<tbody id="tabla_modal">

				</tbody>
			</table>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar">Cerrar</button>
			</div>
		</div>
	</div>
</div>

{# ******************************** #}

{{ javascript_include("js/reportes/getsalidas.js") }}