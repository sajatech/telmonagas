{#{{ dump(egresos) }}#}

<div class="row p-md">
	<div class="col-md-12">
		<table class="table table-striped table-bordered nowrap dt-responsive" cellspacing="0" width="100%" id="pagos">
			<thead>
				<tr>
					<th>Fecha</th>
					<th>Cuenta Bancaria</th>
					<th>Referencia</th>
					<th>Cliente o Descripción</th>
					<th>Importe</th>
					<th>Tipo</th>
				</tr>
			</thead>
			<tbody>

				{% set totaldol = 0 %}
				{% set totalbs = 0 %}
				{% set totEgrDol = 0 %}
				{% set totEgrBol = 0 %}
				{% for pago in pagos %}
					{% if pago["pag_fecha"] is defined %}
						{% set replace = funciones.str_replace("-", "", pago["pag_fecha"]) %}
						{% set fecha = funciones.cambiaf_a_normal(pago["pag_fecha"]) %}

						{% if funciones.compararFechas(fecha, "20/08/2018") > 0 %}
							{% set importe = pago["pag_importe"] %}
						{% else %}
							{% set importe = pago["pag_importe"] / 10000 %}
						{% endif %}

						{% if pago["pag_moneda"] == "BOLÍVARES" %}
							{% set moneda = "Bs. " %}
							{% set totalbs = totalbs + importe %}
						{% else %}
							{% set moneda = "$ " %}
							{% set totaldol = totaldol + importe %}
						{% endif %}

						<tr>
							<td> {{ "<span class='replace'>" ~ replace ~ "</span>" ~ fecha }} </td>
							<td> {{ pago["ban_nombre"] ~ " (*****" ~ funciones.substr(pago["cue_numero"], 14, 6) ~ ")" }} </td>
							<td> {{ pago["pag_num_referencia"] }} </td>
							<td> {{ "<span class='egrDescripcion'>" ~ pago["cli_nombre"] ~ " (" ~ pago["cli_codigo"] ~ ")" ~ "</span>" }} </td>
							<td> {{ moneda ~ funciones.number_format(importe) }} </td>
							<td> {{ "<span class='label label-success'>INGRESO</span>" }} </td>
						</tr>

					{% else %}
						{% set replace = funciones.str_replace("-", "", pago["egr_fecha"]) %}

						{% if pago["egr_moneda"] == "BOLÍVARES" %}
							{% set moneda = "Bs. " %}
							{% set totEgrBol = totEgrBol + pago["egr_importe"] %}
						{% else %}
							{% set moneda = "$ " %}
							{% set totEgrDol = totEgrDol + pago["egr_importe"] %}
						{% endif %}

						<tr>
							<td> {{ "<span class='replace'>" ~ replace ~ "</span>" ~ funciones.cambiaf_a_normal(pago["egr_fecha"]) }} </td>
							<td> {{ pago["ban_nombre"] ~ " (*****" ~ funciones.substr(pago["cue_numero"], 14, 6) ~ ")" }} </td>
							<td> {{ "" }} </td>
							<td> {{ "<span class='egrDescripcion'>" ~ pago["egr_descripcion"] ~ "</span>" }} </td>
							<td> {{ moneda ~ funciones.number_format(pago["egr_importe"]) }} </td>
							<td> {{ "<span class='label label-danger'>EGRESO</span>" }} </td>
						</tr>

					{% endif %}
				{% endfor %}

			</tbody>
		</table>
		<span id="ingresosbs" class="hide"> {{ "Bs. " ~ funciones.number_format(totalbs) }} </span>
		<span id="ingresosdol" class="hide"> {{ "$ " ~ funciones.number_format(totaldol) }} </span>
		<span id="egresosbs" class="hide"> {{ "Bs. " ~ funciones.number_format(totEgrBol) }} </span>
		<span id="egresosdol" class="hide"> {{ "$ " ~ funciones.number_format(totEgrDol) }} </span>
		<span id="totalbs" class="hide"> {{ "Bs. " ~ funciones.number_format(totalbs - totEgrBol) }} </span>
		<span id="totaldol" class="hide"> {{ "$ " ~ funciones.number_format(totaldol - totEgrDol) }} </span>
	</div>
</div>