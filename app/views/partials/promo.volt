{{ form("autocomplete": "off", "id": "forEnvio", "data-parsley-validate": "data-parsley-validate") }}
{% if envio is not defined %}
<div class="row">
	<div class="col-sm-6 closest mb-md">

	</div>
	<div class="col-sm-6 closest mb-md">
		<div class="col-sm-6">
			<label class="radio-inline icheck">
				{{ check_field("todos", "id": "todos", "class": "privilegios","value": "1") }}
                Enviar a Todos ({{ totCliente }} correos)
			</label>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-6 closest mb-md">
		<label for="asunto" class="control-label required">Asunto del Correo </label>

		{{ text_area("asunto", "class": "form-control", "placeholder": "Ingrese Asunto","value": asunto is defined ? asunto : null, "rows": 4, "required": "required") }}

	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label required">Envio Selectivo</label>
		<div class="col-sm-6">
			<label class="radio-inline icheck">
				
				Envio por Lotes
				
			</label>
				{{ numeric_field("lote", "class": "form-control", "placeholder": "Ingrese Cantidad", "max": totCliente) }}		
			<a href='#' id='deselect-all'>Deseleccionar</a>
			<select name="selectivo[]" multiple="multiple" id="multi-select2">

			{% for cliente in clientes %}

				{% if continuar is defined %}
					<option value="{{ cliente }}">{{ cliente }}</option>
				{% else %}
					<option value="{{ cliente.cli_cor_electronico }}">{{ cliente.cli_cor_electronico }}</option>
				{% endif %}


			{% endfor %}

			</select>
		</div>
	</div>
</div>
<BR>
{% endif %}


<div class="row">
    <div class="col-xs-12">
    	<div class="panel panel-magenta">
	        <div class="panel-heading">
	            <h2>TEXTO DEL CORREO</h2>
	        </div>
	        <div class="panel-body collapse in">

	            <div class="col-12">
	                <!-- <textarea name="ckeditor" id="" cols="80" rows="20" class="ckeditor"></textarea> -->

	                <textarea id="summernote" name="editordata" cols="800" rows="50">{{  cuerpo is defined ? cuerpo : null }}</textarea>
	            </div>

	        </div>
	    </div>
    </div>
</div>

{{ end_form() }}