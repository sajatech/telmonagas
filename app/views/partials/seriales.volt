{# Modal de seriales de artículos #}
<div class="modal fade" id="modSeriales" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bc-8bc">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title c-w fz-24"></h2>
            </div>

			{% if funciones.in_array(39, priRol) %}

	            <div class="modal-body bc-g">

	                {{ form("autocomplete": "off", "id": "forSerial", "data-parsley-validate": "data-parsley-validate") }}

	                    <div class="row">
	                        <div class="col-sm-12 closest mb-md mt-md">

	                            {{ text_field("serial", "class": "form-control", "placeholder": "Ingrese Número de Serial y Presione la Tecla Enter", "required": "required") }}

	                        </div>
	                    </div>

	                {# end_form() #}
	                
	            </div>

	        {% endif %}

            <div class="modal-body">
				<table id="seriales" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>#</th>
							<th>Serial</th>
							<th>Estatus</th>
							<th>Acciones</th>
						</tr>
					</thead>
				</table>
            </div>
            <div class="modal-footer">
                <span id="loaSerial" class="hide loading">

                    {{ image("img/loading.gif", "width": "32px") }}

                </span>
                <button type="button" class="btn btn-inverse" id="regSerial" data-articulo-id="{{ articulo.id }}">Registrar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar">Cerrar</button>
				
				{{ submit_button("", "class": "hide", "id": "agrSerial") }}

            </div>

            {{ end_form() }}

        </div>
    </div>
</div>

{# ******************************** #}

{{  javascript_include("assets/js/jquery-1.10.2.min.js") }}
{{  javascript_include("js/partials/seriales.js") }}