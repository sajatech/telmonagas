{{ form("autocomplete": "off", "id": "forProveedor", "data-parsley-validate": "data-parsley-validate") }}

	<div class="row">
		<div class="col-sm-6 mb-md">
			<label for="codProveedor" class="control-label required">Código de Proveedor</label>
				
			{{ text_field("codProveedor", "class": "form-control", "placeholder": "Ingrese Código de Proveedor", "required": "required", "value": proveedor is defined ? proveedor.pro_codigo : null) }}

		</div>
		<div class="col-sm-6 mb-md">
			<label for="nombre" class="control-label required">Nombre</label>

			{{ text_field("nombre", "class": "form-control", "placeholder": "Ingrese Nombre", "required": "required", "value": proveedor is defined ? proveedor.pro_nombre : null) }}

		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 mb-md">
			<label for="perContacto" class="control-label required">Persona de Contacto</label>

			{{ text_field("perContacto", "class": "form-control", "placeholder": "Ingrese Nombres y Apellidos", "required": "required", "value": proveedor is defined ? proveedor.pro_per_contacto : null) }}

		</div>												
		<div class="col-sm-3 mb-md">
			<label for="telLocal" class="control-label">Teléfono Local</label>

			{{ text_field("telLocal", "class": "form-control","data-inputmask": "'mask': '(999) 999-9999', 'clearIncomplete': true", "placeholder": "Ingrese Teléfono Local", "value": proveedor is defined ? proveedor.pro_tel_local : null) }}

		</div>
		<div class="col-sm-3 mb-md">
			<label for="telCelular" class="control-label required">Teléfono Celular</label>

			{{ text_field("telCelular", "class": "form-control", "placeholder": "Ingrese Teléfono Celular", "required": "required","data-inputmask": "'mask': '(999) 999-9999', 'clearIncomplete': true", "value": proveedor is defined ? proveedor.pro_tel_celular : null) }}

		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 mb-md">
			<label for="direccion" class="control-label">Dirección</label>
			
			{{ text_area("direccion", "class": "form-control", "placeholder": "Ingrese Dirección", "rows": 4, "value": proveedor is defined ? proveedor.pro_direccion : null) }}

		</div>
		<div class="col-sm-6 mb-md">
			<label for="corElectronico" class="control-label required">Correo Electrónico</label>

			{{ email_field("corElectronico", "class": "form-control", "placeholder": "Ingrese Correo Electrónico", "required": "required", "value": proveedor is defined ? proveedor.pro_cor_electronico : null) }} 

		</div>
	</div>

{{ end_form() }}

{{  javascript_include("assets/js/jquery-1.10.2.min.js") }}
{{  javascript_include("js/partials/proveedores.js") }}