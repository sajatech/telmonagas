{% if factura is not defined %}

<span id="itemFactura">
	<h3 class="mt-xs">Item de la Factura</h3>
	<hr class="mb-26 mt-n"/>

	{{ form("autocomplete": "off", "id": "item", "data-parsley-validate": "data-parsley-validate") }}

	<div class="row">
		<div class="col-sm-2 mb-md required">
			<h4 class="mb-md mt-n">Código</h4>

			{{ text_field("item_codigo", "class": "form-control tx12", "placeholder": "Código", "required": "required") }}

		</div>
		<div class="col-sm-3 mb-md required">
			<h4 class="mb-md mt-n">Descripción</h4>

			{{ text_field("item_descripcion", "class": "form-control", "placeholder": "Descripcion Articulo", "required": "required") }}

		</div>
		<div class="col-sm-1 mb-md required">
			<h4 class="mb-md mt-n">Cant.</h4>

			{{ text_field("item_cantidad", "class": "form-control tx12", "placeholder": "Cant.","min":"1" , "required": "required") }}

		</div>
		<div class="col-sm-2 mb-md required">
			<h4 class="mb-md mt-n">Peso Gr.</h4>

			{{ text_field("item_peso", "class": "form-control", "placeholder": "Gramos.", "required": "required") }}

		</div>
		<div class="col-sm-2 mb-md required">
			<h4 class="mb-md mt-n">Costo $</h4>

			{{ text_field("item_costo", "class": "form-control", "placeholder": "$", "required": "required") }}

		</div>

		<div class="col-sm-1 mb-md required">
			<h4 class="mb-md mt-n"> % PPG </h4>

			{{ text_field("gastos_garantia", "class": "form-control", "placeholder": "GPG %","required": "required") }}

		</div>
		<div class="col-sm-1 mb-md">
			<h4 class="mb-md mt-n"></h4>

			{{ link_to("#", "<i class='fa fa-plus'></i> Item", "class": "btn btn-inverse mb-md mt-md", "id": "addItem", "style": "width: 100%") }}

		</div>
	</div>

	{{ end_form() }}

</span>

{% endif %}

<span {% if factura is not defined %} class="hide" {% endif %} id="lisItems">

	{% if (lisItems is defined and lisItems|length > 0) or factura is not defined %}

	<hr/>
	<h4 class="mt-xl">Items Cargados</h4>
	<table cellpadding="0" cellspacing="0" border="0" class="table table-striped" id="tabLisItems">
		<thead>
			<tr class="info">
				<th>Codigo</th>
				<th>Descripción</th>
				<th>Cantidad</th>
				<th>Peso en Gramos</th>
				<th>Costo en $</th>
				<th>Monto PPG $</th>
				<th style="color:green">Costo Real $</th>
				<th style="color:green">% Incremento</th>

				{% if factura is not defined %}

				<th class="text-center">Acciones</th>

				{% endif %}

			</tr>
		</thead>
		<tbody>

			{% if lisItems is defined %}

			{% for lisIt in lisItems  %}

			<tr>
				<th>{{ lisIt.item_codigo }}</th>
				<th>{{ lisIt.item_descripcion }}</th>
				<th>{{ lisIt.item_cantidad }}</th>
				<th>{{ funciones.number_format(lisIt.item_peso) }}</th>
				<th>{{ funciones.number_format(lisIt.item_costo) }}</th>
				<th>{{ funciones.number_format(lisIt.item_costo * (lisIt.item_gpg / 100)) }}</th>
				<th style="color:green">{{ funciones.number_format(lisIt.item_precio) }}</th>
				<th style="color:green">{{ funciones.number_format(((lisIt.item_precio - lisIt.item_costo) * 100) / lisIt.item_costo) }}</th>
			</tr>

			{% endfor %}
			{% endif %}

		</tbody>
	</table>
<!-- 	<div align="right" class="mb-md">
		<b>

			{{ "Total de Pagos: " }} 

			<span id="sumPagTotal">

			</span>
		</b>
	</div> -->

	{% endif %}

</span>