{{ form("autocomplete": "off", "id": "forGasto", "data-parsley-validate": "data-parsley-validate") }}

	<div class="row">
		<div class="col-sm-4 mb-md closest">
			<label for="categoria" class="control-label required">Concepto</label>
			<select name="categoria" id="categoria" required="required">
				<option value="">-- SELECCIONE --</option>

				{% for categoria in catGatos %}

					<option value="{{ categoria.id }}" {% if gasto is defined and categoria.id == gasto.cat_id %} selected="selected" {% endif %}>{{ categoria.cat_nombre }}</option>

				{% endfor %}

			</select>
		</div>
		<div class="col-sm-6 mb-md">
			<label for="descripcion" class="control-label required">Descripción</label>

			{{ text_field("descripcion", "class": "form-control", "placeholder": "Ingrese Descripción", "required": "required", "value": gasto is defined ? gasto.gas_descripcion : null) }}

		</div>
		<div class="col-sm-2 mb-md">
			<label for="fecha" class="control-label required">Fecha</label>

			{{ text_field("fecGasto", "class": "form-control cur-pointer", "placeholder": date("d/m/Y"), "required": "required", "value": gasto is defined ? funciones.cambiaf_a_normal(gasto.gas_fecha) : date("d/m/Y"), "data-date-container": "#conFluid" ) }}

		</div>
	</div>
	<div class="row">
<!-- 		<div class="col-sm-2 mb-md">
			<label for="cantidad" class="control-label required">Cantidad</label>
				
			{{ text_field("cantidad", "class": "form-control", "placeholder": "0", "required": "required", "data-parsley-min": 1, "value": gasto is defined ? gasto.gas_cantidad : 1) }}

		</div> -->
		<div class="col-sm-4 mb-md">
			<label for="costo" class="control-label required">Monto</label>
				
			{{ text_field("costo", "class": "form-control", "placeholder": "0,00", "required": "required", "data-parsley-costo": "data-parsley-costo", "data-parsley-error-message": "<i class='fa fa-times-circle'></i> Ingrese Costo", "value": gasto is defined ? funciones.number_format(gasto.gas_costo) : null) }}

		</div>
<!-- 		<div class="col-sm-2 mb-md">
			<label for="importe" class="control-label required">Total (Bs.)</label>
				
			{{ text_field("importe", "class": "form-control", "placeholder": "0,00", "readonly": "readonly", "value": gasto is defined ? funciones.number_format(gasto.gas_cantidad * gasto.gas_costo) : null) }}

		</div> -->
		<div class="col-sm-4 mb-md">
			<label for="divisas" class="control-label required">Divisa</label>
				
				<select name="divisa" id="divisa" required="required" class="form-control">
						
					<option value="BOLIVARES" {% if gastos is defined and gastos.gas.divisas == "BOLIVARES" %} selected="selected" {% endif %}>BOLIVARES</option>
					<option value="DOLARES" {% if gastos is defined and gastos.gas.divisas == "DOLARES" %} selected="selected" {% endif %}>DOLARES</option>
					<option value="EUROS" {% if gastos is defined and gastos.gas.divisas == "EUROS" %} selected="selected" {% endif %}>EUROS</option>

				</select>

		</div>
		<div class="col-sm-4 mb-md">
			<label for="estatus" class="control-label required">Estatus</label>
				
			{{ select_static("estatus", "class": "form-control", ["PAGADO": "PAGADO", "CANCELADO": "CANCELADO"], "required": "required") }}
			
		</div>
	</div>

{{ end_form() }}

{{  javascript_include("assets/js/jquery-1.10.2.min.js") }}
{{  javascript_include("js/partials/gastos.js") }}