{{ form("autocomplete": "off", "id": "forCliente", "data-parsley-validate": "data-parsley-validate") }}

	<div class="row">
		<div class="col-sm-6 mb-md">
			<label for="codCliente" class="control-label required">R.I.F. o Cédula de Identidad</label>
				
			{{ text_field("codCliente", "class": "form-control", "placeholder": "Ingrese R.I.F. o Cédula de Identidad", "required": "required", "value": cliente is defined ? cliente.cli_codigo : null) }}

		</div>
		<div class="col-sm-6 mb-md">
			<label for="nombre" class="control-label required">Nombre</label>

			{{ text_field("nombre", "class": "form-control", "placeholder": "Ingrese Nombre", "required": "required", "value": cliente is defined ? cliente.cli_nombre : null) }}

		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 mb-md">
			<label for="direccion" class="control-label required">Dirección</label>
			
			{{ text_area("direccion", "class": "form-control", "placeholder": "Ingrese Dirección", "required": "required", "rows": 4, "value": cliente is defined ? cliente.cli_direccion : null) }}

		</div>
		<div class="col-sm-6">
			<div class="short-div mb-md">
				<div class="row">
					<div class="col-sm-6 mb-md">
						<label for="telLocal" class="control-label">Teléfono Local</label>

						{{ text_field("telLocal", "class": "form-control", "placeholder": "Ingrese Teléfono Local", "data-inputmask": "'mask': '(999) 999-9999', 'clearIncomplete': true", "value": cliente is defined ? cliente.cli_tel_local : null) }}

					</div>
					<div class="col-sm-6 mb-md">
						<label for="telCelular" class="control-label required">Teléfono Celular</label>

						{{ text_field("telCelular", "class": "form-control", "placeholder": "Ingrese Teléfono Celular", "required": "required", "data-inputmask": "'mask': '(999) 999-9999', 'clearIncomplete': true", "value": cliente is defined ? cliente.cli_tel_celular : null) }}

					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 mb-md">
						<label for="corElectronico" class="control-label required">Correo Electrónico</label>

						{{ email_field("corElectronico", "class": "form-control", "placeholder": "Ingrese Correo Electrónico", "required": "required", "value": cliente is defined ? cliente.cli_cor_electronico : null) }} 

					</div>
				</div>
			</div>
		</div>
	</div>

{{ end_form() }}

{{  javascript_include("assets/js/jquery-1.10.2.min.js") }}
{{  javascript_include("js/partials/clientes.js") }}