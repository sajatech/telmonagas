{{ form("autocomplete": "off", "id": "forEnvio", "data-parsley-validate": "data-parsley-validate") }}
{% if envio is not defined %}
<div class="row">
	<div class="col-sm">
		<h4 class="mb-md mt-n">Nombre ó RIF del Cliente</h4>
	</div>
	<div class="col-sm-10 closest mb-md">
		<select name="cliente" id="cliente">
			
			<option value="">-- SELECCIONE --</option>

			{% for cliente in clientes %}

			<option value="{{ cliente.id }}" {% if salida is defined and cliente.id == salida.cli_id %} selected="selected" {% endif %}>{{ cliente.cli_nombre ~ " (" ~ cliente.cli_codigo ~ ")" }}</option>

			{% endfor %}

		</select>
	</div>
	<div class="col-sm-2">

		{% if salida is defined and funciones.in_array(14, priRol) %}
		{% set addClass = "" %}
		{% else %}
		{% set addClass = "disabled" %}
		{% endif %}

		<a id="verCliente" onclick="verCliente('<?php echo htmlentities(json_encode($cliSalida)); ?>')" class="btn btn-inverse btn-label pull-right mb-md {{ addClass }}" style="width: 100%">
			<i class='ti ti-pencil'></i> Precargar
		</a>
	</div>
</div>
{% endif %}

<div class="row">
	<div class="col-sm-3 mb-md">
		<label for="codCliente" class="control-label required">R.I.F. o Cédula de Identidad</label>
		
		{{ text_field("codCliente", "class": "form-control", "placeholder": "Ingrese R.I.F. o Cédula de Identidad", "required": "required", "value": envio is defined ? envio.env_rif : null) }}

	</div>
	<div class="col-sm-3 mb-md">
		<label for="nombre" class="control-label required">Nombre</label>

		{{ text_field("nombre", "class": "form-control", "placeholder": "Ingrese Nombre", "required": "required", "value": envio is defined ? envio.env_nombre : null) }}

	</div>
	<div class="col-sm-3 mb-md">
		<label for="corElectronico" class="control-label required">Correo Electrónico</label>

		{{ email_field("corElectronico", "class": "form-control", "placeholder": "Ingrese Correo Electrónico", "required": "required", "value": envio is defined ? envio.env_email : null) }} 

	</div>
	<div class="col-sm-3 mb-md">
		<label for="nombre" class="control-label required">Monto Total de la Venta</label>

		{{ text_field("EnvMontoSalida", "class": "form-control", "placeholder": "0,00", "required": "required", "value": envio is defined ? envio.env_monto_salida : null, "data-articulo-value": envio is defined ? envio.env_monto_salida: "", "data-parsley-monto": "data-parsley-monto") }}

	</div>
</div>
<div class="row">
	<div class="input-daterange  col-sm-3 mb-md" id="datepicker-range">
		<label for="EnvFecha" class="control-label required">Fecha del Envio</label>

		{{ text_field("EnvFecha", "class": "input-small form-control","value":date("d/m/Y")) }}

	</div>
	<div class="col-sm-3 mb-md">
		<label for="codCliente" class="control-label required">Empresa de Envio</label>
		
		{{ text_field("EnvEmpresa", "class": "form-control", "placeholder": "Ingrese Empresa", "required": "required", "value": envio is defined ? envio.env_empresa : null) }}

	</div>
	<div class="col-sm-3 mb-md">
		<label for="nombre" class="control-label required">Número de Guia</label>

		{{ text_field("EnvGuia", "class": "form-control", "placeholder": "Ingrese Guia", "required": "required", "value": envio is defined ? envio.env_guia : null) }}

	</div>
	<div class="col-sm-3 mb-md">
		<label for="nombre" class="control-label required">Costo del Envio en Bs.</label>

		{{ text_field("EnvMonto", "class": "form-control", "placeholder": "0,00", "required": "required", "value": envio is defined ? funciones.number_format(envio.env_monto) : "0,00", "data-articulo-value": envio is defined ? envio.env_monto: "", "data-parsley-monto": "data-parsley-monto") }}

	</div>
</div>
<div class="row">
	<div class="col-sm-12 mb-md">
		<h4 class="mb-md mt-n">Observaciones</h4>
		
		{{ text_area("EnvDetalle", "class": "form-control", "placeholder": "Ingrese Observaciones", "rows": 3, "value": envio is defined ? envio.env_detalle : null) }}

	</div>
</div>

{{ end_form() }}