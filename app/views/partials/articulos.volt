{{ form("autocomplete": "off", "id": "forArticulo", "data-parsley-validate": "data-parsley-validate") }}

	<div class="row">
		<div class="col-sm-3">
			<div class="short-div mb-md closest">
				<label for="codArticulo" class="control-label required">Código de Artículo</label>

				{{ text_field("codArticulo", "class": "form-control", "placeholder": "Ingrese Código", "required": "required", "value": articulo is defined ? articulo.art_codigo : null) }}
			
			</div>
			<div class="short-div mb-md">
				<label for="fecCreacion" class="control-label">Fec. Creación</label>

				{{ text_field("fecCreacion", "class": "form-control", "value": articulo is defined ? funciones.cambiaf_a_normal(articulo.art_fec_creacion) : date("d/m/Y"), "readonly": "readonly") }}

			</div>
		</div>
		<div class="col-sm-6 closest mb-md">
			<label for="descripcion" class="control-label required">Descripción</label>
			
			{{ text_area("descripcion", "class": "form-control", "placeholder": "Ingrese Descripción", "rows": 4, "required": "required", "value": articulo is defined ? articulo.art_descripcion : null) }}

		</div>
		<div class="col-sm-3">
			<div class="short-div mb-md closest">
				<label for="invInicial" class="control-label">Inv. Inicial</label>

				{{ text_field("invInicial", "class": "form-control", "placeholder": "0", "data-parsley-type": "number", "value": articulo is defined ? articulo.art_inv_inicial : null) }}
			
			</div>
			<div class="short-div mb-md">
				<label for="stoMinimo" class="control-label">Stock Mínimo</label>

				{{ text_field("stoMinimo", "class": "form-control", "placeholder": "0", "data-parsley-type": "number", "value": articulo is defined ? articulo.art_sto_minimo : null) }}

			</div>
		</div>
	</div>
	<div class="row">

		{% if !funciones.in_array(41, priRol) and !funciones.in_array(42, priRol) %}

			<div class="col-sm-3"></div>

		{% endif %}

		{% if funciones.in_array(41, priRol) %}

			<div class="col-sm-3 closest">
				<div class="short-div mb-xs">
					<label for="cosUnitario" class="control-label required">Precio FOB País de Origen</label>
					<div class="input-group">
						<span class="input-group-addon">$</span>

						{{ text_field("cosUnitario", "class": "form-control", "placeholder": "0,00", "required": "required", "value": articulo is defined ? funciones.number_format(articulo.art_cos_unitario) : "0,00", "data-articulo-value": articulo is defined ? articulo.art_cos_unitario: "", "data-parsley-monto": "data-parsley-monto") }}

					</div>
				</div>
				<div class="short-div mb-md">
					<div class="input-group">
						<span class="input-group-addon">Bs.</span>

						{{ text_field("cosUniBs", "class": "form-control", "placeholder": "0,00", "value": articulo is defined ? funciones.number_format(articulo.art_cos_unitario * dolar) : "0,00", "data-articulo-value": articulo is defined ? articulo.art_cos_unitario * dolar : "") }}

					</div>
				</div>
			</div>

		{% endif %}

		{% if funciones.in_array(42, priRol) %}
			{% if articulo is defined %}
				{% set porCif = (articulo.art_pre_unitario1 != null and articulo.art_pre_unitario1 != 0) ? (articulo.art_pre_unitario1 - articulo.art_cos_unitario) * 100 / articulo.art_cos_unitario : calculo.cal_pre_unitario1 %}
				{% set porMayor = (articulo.art_pre_unitario2 != null and articulo.art_pre_unitario2 != 0) ? (articulo.art_pre_unitario2 - articulo.art_pre_unitario1) * 100 / articulo.art_pre_unitario1 : calculo.cal_pre_unitario2 %}
				{% set porDetal = (articulo.art_pre_unitario3 != null and articulo.art_pre_unitario3 != 0) ? (articulo.art_pre_unitario3 - articulo.art_pre_unitario2) * 100 / articulo.art_pre_unitario2 : calculo.cal_pre_unitario3 %}
				{% set porML = (articulo.art_pre_mer_libre != null and articulo.art_pre_mer_libre != 0) ? (articulo.art_pre_mer_libre - articulo.art_cos_unitario) * 100 / articulo.art_cos_unitario : calculo.cal_mer_libre %}

			{% else %}
				{% set porCif = calculo.cal_pre_unitario1 %}
				{% set porMayor = calculo.cal_pre_unitario2 %}
				{% set porDetal = calculo.cal_pre_unitario3 %}
				{% set porML = calculo.cal_mer_libre %}
			{% endif %}

			<div class="col-sm-3 closest">
				<div class="short-div mb-xs">
					<label for="preUnitario1" class="control-label required">Gastos de Nacionalización</label>
					<div class="input-group">

						{{ text_field("preUni1", "class": "form-control", "placeholder": "0,00", "required": "required", "value": funciones.number_format(porCif), "data-articulo-value": porCif) }}

						<span class="input-group-addon">%</span>
					</div>
				</div>
				<div class="short-div mb-xs">
					<div class="input-group">
						<span class="input-group-addon">$</span>

						{{ text_field("preUnitario1", "class": "form-control", "placeholder": "0,00", "required": "required", "value": articulo is defined ? funciones.number_format(articulo.art_pre_unitario1) : "0,00", "data-parsley-monto": "data-parsley-monto", "data-articulo-value": articulo is defined ? articulo.art_pre_unitario1 : "") }}

					</div>
				</div>
				<div class="short-div mb-md">
					<div class="input-group">
						<span class="input-group-addon">Bs.</span>

						{{ text_field("preUni1Bs", "class": "form-control", "placeholder": "0,00", "value": articulo is defined ? funciones.number_format(articulo.art_pre_unitario1 * dolar) : "0,00", "data-articulo-value": articulo is defined ? articulo.art_pre_unitario1 * dolar : "") }}

					</div>
				</div>
			</div>

		{% endif %}

		<div class="col-sm-3">
			<div class="short-div mb-xs">
				<label for="preUnitario2" class="control-label">Precio al Mayor</label>
				<div class="input-group">

					{{ text_field("preUni2", "class": "form-control", "placeholder": "0,00", "value": funciones.number_format(porMayor), "data-articulo-value": porMayor) }}

					<span class="input-group-addon">%</span>
				</div>
			</div>
			<div class="short-div mb-xs">
				<div class="input-group">
					<span class="input-group-addon">$</span>

					{{ text_field("preUnitario2", "class": "form-control", "placeholder": "0,00", "required": "required", "value": articulo is defined ? funciones.number_format(articulo.art_pre_unitario2) : "0,00", "data-articulo-value": articulo is defined ? articulo.art_pre_unitario2 : "") }}

				</div>
			</div>
			<div class="short-div mb-md">
				<div class="input-group">
					<span class="input-group-addon">Bs.</span>

					{{ text_field("preUni2Bs", "class": "form-control", "placeholder": "0,00", "value": articulo is defined ? funciones.number_format(articulo.art_pre_unitario2 * dolar) : "0,00", "data-articulo-value": articulo is defined ? articulo.art_pre_unitario2 * dolar: "") }}

				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="short-div mb-xs">
				<label for="preUnitario3" class="control-label">Precio al Detal</label>
				<div class="input-group">

					{{ text_field("preUni3", "class": "form-control", "placeholder": "0,00", "value": funciones.number_format(porDetal), "data-articulo-value": porDetal) }}

					<span class="input-group-addon">%</span>
				</div>
			</div>
			<div class="short-div closest mb-xs">
				<div class="input-group">
					<span class="input-group-addon">$</span>

					{{ text_field("preUnitario3", "class": "form-control", "placeholder": "0,00", "value": articulo is defined ? funciones.number_format(articulo.art_pre_unitario3) : "0,00", "data-articulo-value": articulo is defined ? articulo.art_pre_unitario3: "") }}

				</div>
			</div>
			<div class="short-div mb-md">
				<div class="input-group">
					<span class="input-group-addon">Bs.</span>

					{{ text_field("preUni3Bs", "class": "form-control", "placeholder": "0,00", "value": articulo is defined ? funciones.number_format(articulo.art_pre_unitario3 * dolar) : "0,00", "data-articulo-value": articulo is defined ? articulo.art_pre_unitario3 * dolar : "") }}

				</div>
			</div>
		</div>

		{% if !funciones.in_array(41, priRol) and !funciones.in_array(42, priRol) %}

			<div class="col-sm-3"></div>

		{% endif %}

	</div>
	<div class="row">
		{% if funciones.in_array(80, priRol) %}

		<div class="col-sm-3 closest">
			<div class="short-div mb-xs">
				<label for="preMerLibre" class="control-label" style="color:red">Precio Mercado Libre</label>
				<div class="input-group">

					{{ text_field("porML", "class": "form-control", "placeholder": "0,00", "value": funciones.number_format(porML), "data-articulo-value": porML) }}

					<span class="input-group-addon">%</span>
				</div>
			</div>
			<div class="short-div mb-xs">
				<div class="input-group">
					<span class="input-group-addon">$</span>

					{{ text_field("preMerLibre", "class": "form-control", "placeholder": "0,00", "required": "required", "value": articulo is defined ? funciones.number_format(articulo.art_pre_unitario2 + (porML * articulo.art_pre_unitario2 / 100) ) : "0,00", "data-parsley-monto": "data-parsley-monto", "data-articulo-value": articulo is defined ? articulo.art_pre_mer_libre : "") }}

				</div>
			</div>
			<div class="short-div mb-md">
				<div class="input-group">
					<span class="input-group-addon">Bs.</span>

					{{ text_field("preMerLibreBs", "class": "form-control", "placeholder": "0,00", "value": articulo is defined ? funciones.number_format((articulo.art_pre_unitario2 + (porML * articulo.art_pre_unitario2 / 100) ) * dolar) : "0,00", "data-articulo-value": articulo is defined ? (articulo.art_pre_unitario2 + (porML * articulo.art_pre_unitario2 / 100) ) * dolar : "") }}

				</div>
			</div>
		</div>

		{% endif %}
		<div class="col-sm-3 mb-md">
			<label for="ubicacion" class="control-label">Ubicación</label>

			{{ text_field("ubicacion", "class": "form-control", "placeholder": "Ingrese Ubicación", "value": articulo is defined ? articulo.art_ubicacion : null) }}
			{{ link_to("img/MAPA.jpg", "target": "_new", "<< Ver Ubicación >>", "class": "navbar-brand") }}

		</div>
		<div class="col-sm-5 closest mb-md">
            <div class="fileinput {{ articulo.art_fot_referencial|length > 0 ? "fileinput-exists" : "fileinput-new" }}" data-provides="fileinput" id="fileinput">
                <div class="fileinput-new thumbnail">

                    {{ image("img/holder.png", "alt": "...") }}

                </div>
                <div class="fileinput-preview fileinput-exists thumbnail cur-pointer" id="ima-thumbnail">

                    {{ image(articulo.art_fot_referencial, "alt": "...") }}

                </div>
                <div>
                    <span class="btn btn-default btn-file" class="input-group-btn" id="selFoto" data-trigger="hover" title="Puede subir fotos con extensión jpg o png, teniendo en cuenta el peso máximo de 1 Mb.">
                        <span class="fileinput-new">Seleccione foto</span>
                        <span class="fileinput-exists">Cambiar</span>
                        
                        {{ file_field("fotUsuario", "style": "color:transparent") }}

                    </span>

                    {{ link_to("#", "Cancelar", "class": "btn btn-default fileinput-exists", "data-dismiss": "fileinput", "id": "canFotUsuario") }}

                </div>
            </div>

		</div>
	</div>

{{ end_form() }}

{{  javascript_include("assets/js/jquery-1.10.2.min.js") }}
{{  javascript_include("js/partials/articulos.js?V=2.0") }}