{{ form("autocomplete": "off", "id": "forUsuario", "data-parsley-validate": "data-parsley-validate") }}

	<div class="row">
		<div class="col-sm-3 mb-md">
			<label for="codUsuario" class="control-label required">Cédula de Identidad</label>
				
			{{ text_field("codUsuario", "class": "form-control", "placeholder": "Ingrese Cédula de Identidad", "required": "required", "value": usuario is defined ? usuario.usu_codigo : null) }}

		</div>
		<div class="col-sm-5 mb-md">
			<label for="nombre" class="control-label required">Nombre</label>

			{{ text_field("nombre", "class": "form-control", "placeholder": "Ingrese Nombre", "required": "required", "value": usuario is defined ? usuario.usu_nombre : null) }}

		</div>
		<div class="col-sm-4 closest mb-md">
			<label for="corElectronico" class="control-label">Correo Electrónico</label>
			<div class="input-group">
				<span class="input-group-addon">
					<i class="ti ti-email"></i>
				</span>

				{{ email_field("corElectronico", "class": "form-control", "placeholder": "Ingrese Correo Electrónico", "value": usuario is defined ? usuario.usu_cor_electronico : null) }} 

			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3 mb-md">
			<label for="telefono" class="control-label">Teléfono</label>

			{{ text_field("telefono", "class": "form-control", "placeholder": "Ingrese Teléfono", "data-inputmask": "'mask':'(999) 999-9999', 'clearIncomplete': true", "value": usuario is defined ? usuario.usu_telefono : null) }}

		</div>
		<div class="col-sm-4 closest mb-md">
			<label for="fecNacimiento" class="control-label">Fecha de Nacimiento</label>
			<div class="input-group date" id="datepicker-startview">
				<span class="input-group-addon">
					<i class="ti ti-calendar"></i>
				</span>
			
				{{ text_field("fecNacimiento", "class": "form-control cur-pointer", "placeholder": "Ingrese Fecha de Nacimiento", "readonly": "readonly", "value": usuario is defined ? funciones.cambiaf_a_normal(usuario.usu_fec_nacimiento) : null) }}

			</div>
		</div>
		<div class="col-sm-5 mb-md">
			<label for="cargo" class="control-label">Cargo</label>
			
			{{ text_field("cargo", "class": "form-control", "placeholder": "Ingrese Cargo", "value": usuario is defined ? usuario.usu_cargo : null) }}

		</div>
	</div>
	<div class="row">
		<div class="col-sm-3 mb-md">
			<label for="nomUsuario" class="control-label required">Nombre de Usuario</label>

			{{ text_field("nomUsuario", "class": "form-control text-lowercase", "placeholder": "Ingrese Nombre de Usuario", "required": "required", "pattern": "[A-Za-z]", "value": usuario is defined ? usuario.usu_nom_usuario : null) }}

		</div>
		<div class="col-sm-2 mb-md">
			<label for="contrasena" class="control-label required">Contraseña</label>

			{{ password_field("contrasena", "class": "form-control tooltips", "data-trigger": "hover", "data-original-title": "Tooltip text goes here. Tooltip text goes here.", "placeholder": "Ingrese Contraseña", "required": "required", "data-parsley-error-message": "<i class='fa fa-times-circle'></i> Ingrese contraseña.") }}

		</div>
		<div class="col-sm-2 mb-md">
			<label for="repContrasena" class="control-label required">Repita la Cont.</label>

			{{ password_field("repContrasena", "class": "form-control", "placeholder": "Ingrese Contraseña", "required": "required", "data-parsley-equalto": "#contrasena", "data-parsley-error-message": "<i class='fa fa-times-circle'></i> Ingrese contraseña.") }}

		</div>
		<div class="col-sm-5 closest mb-md">
			<label for="tipUsuario" class="control-label required">Tipo de Usuario</label>
			<select name="tipUsuario" id="tipUsuario" required="required">
				<option value="">-- SELECCIONE --</option>

				{% for rol in roles %}

					<option value="{{ rol.id }}" {% if usuario is defined and rol.id == usuario.rol_id %} selected="selected" {% endif %}>{{ rol.rol_nombre }}</option>

				{% endfor %}

			</select>
		</div>
	</div>

{{ end_form() }}

{{  javascript_include("assets/js/jquery-1.10.2.min.js") }}
{{  javascript_include("js/partials/usuarios.js?V=1") }}