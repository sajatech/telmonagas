{{ form("autocomplete": "off", "id": "forFactura", "data-parsley-validate": "data-parsley-validate") }}

<div class="row">
	<div class="col-sm-2 mb-md">
		<label for="fac_tasa_dolar" class="control-label required">Tasa Dolar al Día Bs.</label>

		{{ text_field("fac_tasa_dolar", "class": "form-control","value":factura is defined ? funciones.number_format(factura.fac_tasa_dolar) : funciones.number_format(dolar) )}}

	</div>
	<div class="input-daterange  col-sm-2 mb-md" id="datepicker-range">
		<label for="fac_fecha" class="control-label required">Fecha del Factura</label>

		{{ text_field("fac_fecha", "class": "input-small form-control","value": factura is defined ? funciones.cambiaf_a_normal(factura.fac_fecha) : date("d/m/Y")) }}

	</div>
	<div class="col-sm-2 mb-md">
		<label for="fac_numero" class="control-label required label-bold">Número de Factura</label>
		
		{{ text_field("fac_numero", "class": "form-control", "placeholder": "Ingrese Número", "required": "required", "value": factura is defined ? factura.fac_numero : null) }}

	</div>
	<div class="col-sm-3 mb-md">
		<label for="fac_monto" class="control-label required">Monto Factura en <span style="color:red">$</span></label>

		{{ text_field("fac_monto", "class": "form-control", "placeholder": "Ingrese Monto en $", "required": "required","readonly":"readonly", "value": factura is defined ? funciones.number_format(factura.fac_monto) : 0.00) }}

	</div>
	<div class="col-sm-3 mb-md">
		<label for="fac_monto_flete" class="control-label label-bold">Costo de Flete ó Servicio P.A.P <span style="color:red">$</span></label>

		{{ text_field("fac_monto_flete", "class": "form-control", "placeholder": "Ingrese Monto en $", "value": factura is defined ? funciones.number_format(factura.fac_monto_flete) : 0.00) }}

	</div>

</div>
<div class="row">
	<div class="col-sm-2 mb-md">
		<label for="fac_peso" class="control-label required label-bold">Peso Según Fact. Kg.</label>

		{{ text_field("fac_peso", "class": "form-control", "placeholder": "Ingrese Peso en Kg", "required": "required", "value": factura is defined ? funciones.number_format(factura.fac_peso) : null) }}
	</div>

	<div class="col-sm-2 mb-md">
		<label for="fac_peso_porcentaje" class="control-label required">Peso Real en Kg.</label>

		{{ text_field("fac_peso_porcentaje", "class": "form-control", "placeholder": "Peso en Kg", "required": "required","readonly":"readonly", "value": factura is defined ? funciones.number_format(factura.fac_peso_real) : 0) }}

	</div>

	<div class="col-sm-2 mb-md">
		<label for="fac_peso_embalaje" class="control-label required">Peso Embalaje Kg.</label>

		{{ text_field("fac_peso_embalaje", "class": "form-control", "placeholder": "Peso en Kg", "required": "required","readonly":"readonly", "value": factura is defined ? funciones.number_format(factura.fac_peso - factura.fac_peso_real) : 0) }}

	</div>

	<div class="col-sm-2 mb-md">
		<label for="fac_bultos" class="control-label required">Cant. de Articulos</label>

		{{ numeric_field("fac_bultos", "class": "form-control", "placeholder": "Ingrese Cantidad de Art.", "required": "required","readonly":"readonly", "value": factura is defined ? factura.fac_bultos : 0) }}

	</div>
	<div class="col-sm-2 mb-md"> 
		<label for="fac_aduana_bs" class="control-label label-bold"> Aduana-Impuesto Bs.</label>

		{{ text_field("fac_aduana_bs", "class": "form-control", "placeholder": "Ingrese Gasto en Bs.", "value": factura is defined ? funciones.number_format(factura.fac_aduana_bs) : 0.00) }}

	</div>
	<div class="col-sm-2 mb-md">
		<label for="fac_transporte_nac" class="control-label label-bold">Transporte Nac Bs.</label>

		{{ text_field("fac_transporte_nac", "class": "form-control", "placeholder": "Ingrese Monto en Bs.", "value": factura is defined ? funciones.number_format(factura.fac_transporte_nac) : 0.00) }}

	</div>

</div>
<div class="row">
	<div class="col-sm-3 mb-md">
		<label for="fac_gastos_ope" class="control-label label-bold">Gastos Operativos en Bs.</label>

		{{ text_field("fac_gastos_ope", "class": "form-control", "placeholder": "Ingrese Gastos en Bs", "value": factura is defined ? funciones.number_format(factura.fac_gastos_ope) : 0.00) }}

	</div>
	<div class="col-sm-3 mb-md">
		<label for="fac_imp1" class="control-label label-bold">Comisión Banco por Transf. <span style="color:red">$</span></label>

		{{ text_field("fac_imp1", "class": "form-control", "placeholder": "Ingrese Monto en $.", "value": factura is defined ? funciones.number_format(factura.fac_imp1) : 0.00) }}

	</div>
	<div class="col-sm-3 mb-md">
		<label for="fac_imp2" class="control-label label-bold">Agente Aduanal en Bs.</label>

		{{ text_field("fac_imp2", "class": "form-control", "placeholder": "Ingrese Monto en Bs.", "value": factura is defined ? funciones.number_format(factura.fac_imp2) : 0.00) }}

	</div>
	<div class="col-sm-3 mb-md">
		<label for="fac_imp3" class="control-label label-bold">Otros Gastos en Bs.</label>

		{{ text_field("fac_imp3", "class": "form-control", "placeholder": "Ingrese Monto en Bs.", "value": factura is defined ? funciones.number_format(factura.fac_imp3) : 0.00) }}

	</div>

</div>
<div class="row">
	<div class="col-sm-12 mb-md">
		<h4 class="mb-md mt-n">Observaciones</h4>
		
		{{ text_area("fac_observacion", "class": "form-control", "placeholder": "Ingrese Observaciones", "rows": 3, "value": factura is defined ? factura.fac_observacion : null) }}

	</div>
</div>

{{ end_form() }}