{{ assets.outputCss() }}

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="container-fluid">
                <div data-widget-group="group1">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="ajaxSpinner" class="fs-16 text-center mt-md">

                                {{ image("img/loading.gif", "width": "32px") }}
                                {{ "Por favor, espere..." }}

                            </div>
                            <div class="panel panel-success hide" id="panArticulos">
                                <div class="panel-heading">
                                    <h1 class="title c-w fz-24">Artículos Inactivos</h1>
                                    <div class="panel-ctrls"></div>
                                </div>
                                <div class="panel-body no-padding">
                                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered nowrap dt-responsive" id="articulos">
                                        <thead>
                                            <tr>
                                                <th width="13.37%">Código</th>
                                                <th width="22.99%">Descripción</th>
                                                <th width="8.69%">Cant. Inicial</th>
                                                <th width="9.60%">Cant. Mínima</th>
                                                <th width="19.96%">Precio Mayor</th>
                                                <th width="19.04%">Precio Detal</th>
                                                <th width="6.35%">Estatus</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>                
                                <div class="panel-footer"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{ assets.outputJs() }}