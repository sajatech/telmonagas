{{ assets.outputCss() }}

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="container-fluid">
                <div data-widget-group="group1">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="ajaxSpinner" class="fs-16 text-center mt-md">

                                {{ image("img/loading.gif", "width": "32px") }}
                                {{ "Por favor, espere..." }}

                            </div>
                            <div class="panel panel-success hide" id="panArticulos">
                                <div class="panel-heading">
                                    <h1 class="title c-w fz-24">Listado de Artículos Registrados</h1>
                                    <div class="panel-ctrls" id="panCtrls"></div>
                                </div>
                                <div class="panel-body">
                                    <div class="panel panel-black" data-widget='{"draggable": "false"}' id="panFiltros">
                                        <div class="panel-heading">
                                            <span class="fz-24"><i class="ti ti-filter"></i>Filtros</span>
                                            <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
                                        </div>
                                        <div class="panel-body" id="panBody">
                                            <div class="col-sm-6" style="text-align: right">
                                                <label class="checkbox-inline icheck tipBusqueda" style="padding-top: 0 !important">

                                                    {{ radio_field("tipBusqueda", "id": "descripcion", "value": "DESCRIPCIÓN", "checked": "checked") }}

                                                    <span>Descripción de Artículo</span>
                                                </label>
                                                <label class="checkbox-inline icheck" style="padding-top: 0 !important">

                                                    {{ radio_field("tipBusqueda", "id": "codigo", "value": "CÓDIGO") }}

                                                    <span>Código</span>
                                                </label>
                                            </div>
                                            <div class="col-sm-6" id="filter"></div>
                                        </div>
                                    </div>
                                    <table class="table table-striped table-bordered nowrap tcur-pointer dt-responsive" id="articulos" style="width:100%">
                                        <thead class="fs-16">
                                            <tr>
                                                <th width="13.37%">Código</th>
                                                <th width="22.99%">Descripción</th>
                                                <th width="8.69%">Cant. Inicial</th>
                                                <th width="9.60%">Cant. Mínima</th>
                                                <th width="19.96%">Precio Mayor</th>
                                                <th width="19.04%">Precio Detal</th>
                                                <th width="6.35%">Estatus</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>                
                                <div class="panel-footer"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modMarcas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title"></h2>
            </div>
            <div class="modal-body">

                {{ form("autocomplete": "off", "id": "marca", "data-parsley-validate": "data-parsley-validate") }}

                    <div class="form-group">
                        <label for="nombre" class="control-label">Nombre</label>

                        {{ text_field("nombre", "class": "form-control", "placeholder": "Ingrese Nombre", "required": "required") }}

                    </div>
                    <a id="regModelo" class="create-register tooltips" data-trigger="hover" data-original-title="Registrar modelo">
                        <i class="ti ti-plus"></i>
                    </a>
                    <table id="modelos" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Modelos asociados</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                    </table>
            </div>
            <div class="modal-footer">
                <span id="loading" class="hide loading">

                    {{ image("img/loading.gif", "width": "32px") }}

                </span>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar">Cerrar</button>
                <button type="button" class="btn btn-primary accion"></button>
            </div>

            {{ end_form() }}

        </div>
    </div>
</div>

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
        errorsWrapper: "<span class='help-block'></span>",
        errorTemplate: "<span></span>"
    };
</script>