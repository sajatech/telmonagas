{% if articulo.entradas|length == 0 %}
    {% set entradas = 0 %}
{% else %}
    {% set entradas = articulo.entradas %}
{% endif %}

{% if articulo.salidas|length == 0 %}
    {% set salidas = 0 %}
{% else %}
    {% set salidas = articulo.salidas %}
{% endif %}

{% if articulo.articulo.art_inv_inicial|length == 0 %}
    {% set invInicial = 0 %}
{% else %}
    {% set invInicial = articulo.articulo.art_inv_inicial %}
{% endif %}

<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="busqueda">
    <thead>
        <tr>
            <th colspan="7" class="text-center">{{ articulo.articulo.art_codigo ~ " (" ~ articulo.articulo.art_descripcion ~ " )" }}</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center fw-700 va-middle">Inv. Inicial</td>
            <td colspan="6" class="fs-24">{{ invInicial ~ " unidades" }}</td>
        </tr>
        <tr>
            <td class="text-center fw-700 va-middle">Precio al Mayor</td>
            <td colspan="6" class="fs-24">{{ "$ " ~ funciones.number_format(articulo.articulo.art_pre_unitario2) ~ " / Bs. " ~ funciones.number_format(articulo.articulo.art_pre_unitario2 * articulo.dolar) }}</td>
        </tr>
        <tr>
            <td class="text-center fw-700 va-middle">Precio al Detal</td>
            <td colspan="6" class="fs-24">{{ "$ " ~ funciones.number_format(articulo.articulo.art_pre_unitario3) ~ " / Bs. " ~ funciones.number_format(articulo.articulo.art_pre_unitario3 * articulo.dolar) }}</td>
        </tr>
        <tr>
            <td class="text-center fw-700 va-middle">Entradas</td>
            <td colspan="6" class="fs-24">{{ "Desde: " ~ articulo.desEntrada ~ " Hasta: " ~ articulo.hasEntrada ~ " -> " ~ entradas ~ " unidad(es)" }}</td>
        </tr>
        <tr>
            <td class="text-center fw-700 va-middle">Salidas</td>
            <td colspan="6" class="fs-24">{{ "Desde: " ~ articulo.desSalida ~ " Hasta: " ~ articulo.hasSalida ~ " -> " ~ salidas ~ " unidad(es)" }}</td>
        </tr>
        <tr>
            <td class="text-center fw-700 va-middle">Existencia del Artículo</td>
            <td colspan="6" class="fs-24">{{ articulo.existencia }}</td>
        </tr>
    </tbody>
</table>