{{ assets.outputCss() }}
<style type="text/css">
	.tablas {
		font-size: 24px;
		vertical-align: top;
		font-weight: bold;
	}

	.red {

		color:red;
		
	}
</style>
<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            {#<ol class="breadcrumb">                        
				<li>

					{{ link_to("inicio", "Inicio") }}

				</li>
				<li>

					{{ link_to("reportes/pagos", "Reportes") }}

				</li>
				<li class="active">

					{{ link_to("reportes/pagos", "General de Pagos") }}

				</li>
            </ol>#}
            <div class="container-fluid">
				<div data-widget-group="group1">
					<div class="panel panel-success">
						<div class="panel-heading">
							<h1 class="title c-w fz-24">Inventario de Productos</h1>
						</div>

						<div class="panel-body p-n" id="resultado">

							<div class="row p-md">
								<div class="col-md-12">
										
		</div>
		<div class="row tablas">
			<div class="col-sm-3 mb-md">Total Productos: </div>
			<div class="col-sm-3 mb-md " align="left">{{  totalExistencia }}</div>
		</div>
		<div class="row tablas">
			<div class="col-sm-3 mb-md">Costo del Inventario: </div>
			<div class="col-sm-3 mb-md " align="left">{{  "Bs. " ~ funciones.number_format(totalPrecio * dolar)}}</div>
			<div class="col-sm-1 mb-md" align="center"> = </div>
			<div class="col-sm-3 mb-md red" align="left">{{ "$ " ~ funciones.number_format(totalPrecio) }}</div>
		</div>

		<br>
								<table class="table table-striped table-bordered" cellspacing="0" width="100%" id="productos">
										<thead>
											<tr>
												<th>Codigo</th>
												<th>Descripcion</th>
												<th>Existencia</th>
												<th>Total $</th>

											</tr>
										</thead>
										<tbody>

											{% for prod in productos %}

											<tr>
												<td>{{ prod.articulo.art_codigo }}</td>
												<td>{{ prod.articulo.art_descripcion }}</td>
												<td>{{ prod.existencia }} </td>
												<td>{{ funciones.number_format(prod.existencia *  prod.articulo.art_pre_unitario1) }}</td>
											</tr>

											{% endfor %}

										</tbody>
									</table> 
								</div>
							</div>

						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
          errorsWrapper: "<span class='help-block'></span>", 
          errorTemplate: "<span></span>"
    };
</script>