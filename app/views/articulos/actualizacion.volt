{{ assets.outputCss() }}

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="container-fluid">
				<div data-widget-group="group1">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-success">
								<div class="panel-heading">
									<h1 class="title c-w fz-24"></i>

										{% if anulado is defined %}
											{{ "Revisión de Artículo Anulado" }}
										{% else %}
											{{ "Actualización de Artículos" }}
										{% endif %}

									</h1>
									<div class="panel-ctrls">

                						{% if funciones.in_array(2, priRol) %}
        									{% set busArticulo = anulado is defined ? "articulos/inactivos" : "articulos/lista" %}
											{{ link_to(busArticulo, "<i class='ti ti-search'></i><span> Buscar Artículo</span>", "class": "btn btn-default ctrls pull-left mt-sm mr-xs") }}
										{% endif %}

                						{% if funciones.in_array(1, priRol) %}
											{{ link_to("articulos/registro", "<i class='ti ti-plus'></i><span> Agregar Artículo</span>", "class": "btn btn-default ctrls pull-left mt-sm mr-xs") }}
										{% endif %}

                						{% if funciones.in_array(39, priRol) or funciones.in_array(40, priRol) %}
											{{ link_to("#", "<i class='ti ti-plus'></i><span> Agregar/Ver Seriales</span>", "id": "agrSeriales", "class": "btn btn-default ctrls pull-left mt-sm mr-xs", "data-articulo-id": articulo.id, false) }}
										{% endif %}

                						{% if funciones.in_array(44, priRol) %}
											{{ link_to("#", "<i class='ti ti-plus'></i><span> Más Información</span>", "id": "busRapida", "class": "btn btn-default ctrls pull-right mt-sm", false) }}
										{% endif %}

									</div>
								</div>
								<div class="panel-body bc-g">

									{{ partial("partials/articulos") }}

								</div>

        						{% if (funciones.in_array(3, priRol) and anulado is not defined) or funciones.in_array(5, priRol) %}

									<div class="panel-footer">
										<div class="clearfix">
											<span id="loaArticulo" class="loading hide">

												{{ image("img/loading.gif", "width": "32px") }}

											</span>

    										{% if funciones.in_array(3, priRol) and anulado is not defined %}
												{{ submit_button("Actualizar", "class": "btn btn-inverse btn", "id": "actArticulo", "data-articulo-id": articulo.id) }}
											{% endif %}
											
											{% if funciones.in_array(5, priRol) %}
												{% if anulado is not defined %}
													{{ submit_button("Desactivar", "class": "btn btn-warning bc-e51", "id": "desArticulo", "data-articulo-id": articulo.id) }}
												{% else %}
													{{ submit_button("Activar", "class": "btn btn-warning bc-e51", "id": "activArticulo", "data-articulo-id": articulo.id) }}
												{% endif %}
											{% endif %}

										</div>	
									</div>

								{% endif %}

							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>

{{ partial("partials/seriales") }}

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
        errorsContainer: function(el) {
            return el.$element.closest(".closest");
        },

  		errorsWrapper: "<span class='help-block'></span>", 
  		errorTemplate: "<span></span>"
    };
</script>