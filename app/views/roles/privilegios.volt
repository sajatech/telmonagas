{{ assets.outputCss() }}

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="container-fluid">
                <div data-widget-group="group1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <h1 class="title c-w fz-24">Privilegios de Rol: {{ nomRol }}</h1>
                                    <div class="panel-ctrls">

                                        {% if funciones.in_array(34, priRol) %}
                                            {{ link_to("configuracion/roles", "<i class='ti ti-search'></i><span> Buscar Rol</span>", "class": "btn btn-default ctrls pull-left mt-sm mr-xs") }}
                                        {% endif %}

                                    </div>
                                </div>
                                <div class="panel-body bc-g">

                                    {{ form("id": "forPrivilegios") }}

                                        <div class="row">
                                            <div class="col-sm-4 mb-md">
                                                <h3>Sección de Artículos</h3>

                                                {% for priArticulo in priArticulos %}

                                                    <div class="checkbox red icheck">
                                                        <label>

                                                            {{ check_field("privilegios[]", "id": priArticulo.id, "class": "privilegios", "checked": funciones.in_array(priArticulo.id, privilegios) ? true : null, "value": priArticulo.id) }}
                                                                {{ priArticulo.pri_nombre }}

                                                        </label>
                                                    </div>

                                                {% endfor %}

                                            </div>
                                            <div class="col-sm-4 mb-md">
                                                <h3>Sección de Proveedores</h3>

                                                {% for priProveedor in priProveedores %}

                                                    <div class="checkbox red icheck">
                                                        <label>

                                                            {{ check_field("privilegios[]", "id": priProveedor.id, "class": "privilegios", "checked": funciones.in_array(priProveedor.id, privilegios) ? true : null, "value": priProveedor.id) }}
                                                                {{ priProveedor.pri_nombre }}

                                                        </label>
                                                    </div>

                                                {% endfor %}

                                            </div>
                                            <div class="col-sm-4 mb-md">
                                                <h3>Sección de Clientes</h3>

                                                {% for priCliente in priClientes %}

                                                    <div class="checkbox red icheck">
                                                        <label>

                                                            {{ check_field("privilegios[]", "id": priCliente.id, "class": "privilegios", "checked": funciones.in_array(priCliente.id, privilegios) ? true : null, "value": priCliente.id) }}
                                                                {{ priCliente.pri_nombre }}

                                                        </label>
                                                    </div>

                                                {% endfor %}

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4 mb-md">
                                                <h3>Sección de Ingresos</h3>

                                                {% for priIngreso in priIngresos %}

                                                    <div class="checkbox red icheck">
                                                        <label>

                                                            {{ check_field("privilegios[]", "id": priIngreso.id, "class": "privilegios", "checked": funciones.in_array(priIngreso.id, privilegios) ? true : null, "value": priIngreso.id) }}
                                                                {{ priIngreso.pri_nombre }}

                                                        </label>
                                                    </div>

                                                {% endfor %}

                                            </div>
                                            <div class="col-sm-4 mb-md">
                                                <h3>Sección de Salidas</h3>

                                                {% for priSalida in priSalidas %}

                                                    <div class="checkbox red icheck">
                                                        <label>

                                                            {{ check_field("privilegios[]", "id": priSalida.id, "class": "privilegios", "checked": funciones.in_array(priSalida.id, privilegios) ? true : null, "value": priSalida.id) }}
                                                                {{ priSalida.pri_nombre }}

                                                        </label>
                                                    </div>

                                                {% endfor %}

                                            </div>
                                            <div class="col-sm-4 mb-md">
                                                <h3>Sección de Usuarios</h3>

                                                {% for priUsuario in priUsuarios %}

                                                    <div class="checkbox red icheck">
                                                        <label>

                                                            {{ check_field("privilegios[]", "id": priUsuario.id, "class": "privilegios", "checked": funciones.in_array(priUsuario.id, privilegios) ? true : null, "value": priUsuario.id) }}
                                                                {{ priUsuario.pri_nombre }}

                                                        </label>
                                                    </div>

                                                {% endfor %}

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4 mb-md">
                                                <h3>Precios del Dólar</h3>

                                                {% for priDolar in priDolares %}

                                                    <div class="checkbox red icheck">
                                                        <label>

                                                            {{ check_field("privilegios[]", "id": priDolar.id, "class": "privilegios", "checked": funciones.in_array(priDolar.id, privilegios) ? true : null, "value": priDolar.id) }}
                                                                {{ priDolar.pri_nombre }}

                                                        </label>
                                                    </div>

                                                {% endfor %}

                                            </div>
                                            <div class="col-sm-4 mb-md">
                                                <h3>Roles de Usuario</h3>

                                                {% for privilegio in priRoles %}

                                                    <div class="checkbox red icheck">
                                                        <label>

                                                            {{ check_field("privilegios[]", "id": privilegio.id, "class": "privilegios", "checked": funciones.in_array(privilegio.id, privilegios) ? true : null, "value": privilegio.id) }}
                                                                {{ privilegio.pri_nombre }}

                                                        </label>
                                                    </div>

                                                {% endfor %}

                                            </div>
                                            <div class="col-sm-4 mb-md">
                                                <h3>Cálculos de Precio</h3>

                                                {% for privilegio in priCalculos %}

                                                    <div class="checkbox red icheck">
                                                        <label>

                                                            {{ check_field("privilegios[]", "id": privilegio.id, "class": "privilegios", "checked": funciones.in_array(privilegio.id, privilegios) ? true : null, "value": privilegio.id) }}
                                                                {{ privilegio.pri_nombre }}

                                                        </label>
                                                    </div>

                                                {% endfor %}

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4 mb-md">
                                                <h3>Sección de Reportes</h3>

                                                {% for priReporte in priReportes %}

                                                    <div class="checkbox red icheck">
                                                        <label>

                                                            {{ check_field("privilegios[]", "id": priReporte.id, "class": "privilegios", "checked": funciones.in_array(priReporte.id, privilegios) ? true : null, "value": priReporte.id) }}
                                                                {{ priReporte.pri_nombre }}

                                                        </label>
                                                    </div>

                                                {% endfor %}

                                            </div>
                                            <div class="col-sm-4 mb-md">
                                                <h3>Sección de Inventario</h3>

                                                {% for priInv in priInventario %}

                                                    <div class="checkbox red icheck">
                                                        <label>

                                                            {{ check_field("privilegios[]", "id": priInv.id, "class": "privilegios", "checked": funciones.in_array(priInv.id, privilegios) ? true : null, "value": priInv.id) }}
                                                                {{ priInv.pri_nombre }}

                                                        </label>
                                                    </div>

                                                {% endfor %}

                                            </div>
                                            <div class="col-sm-4 mb-md">
                                                <h3>Sección de Envíos</h3>

                                                {% for priEnv in priEnvio %}

                                                    <div class="checkbox red icheck">
                                                        <label>

                                                            {{ check_field("privilegios[]", "id": priInv.id, "class": "privilegios", "checked": funciones.in_array(priEnv.id, privilegios) ? true : null, "value": priEnv.id) }}
                                                                {{ priEnv.pri_nombre }}

                                                        </label>
                                                    </div>

                                                {% endfor %}

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4 mb-md">
                                                <h3>Sección de Bancos</h3>

                                                {% for priBan in priBanco %}

                                                    <div class="checkbox red icheck">
                                                        <label>

                                                            {{ check_field("privilegios[]", "id": priInv.id, "class": "privilegios", "checked": funciones.in_array(priBan.id, privilegios) ? true : null, "value": priBan.id) }}
                                                                {{ priBan.pri_nombre }}

                                                        </label>
                                                    </div>

                                                {% endfor %}

                                            </div>
                                            <div class="col-sm-4 mb-md">
                                                <h3>Conceptos de Gastos</h3>

                                                {% for priCatGasto in priCatGastos %}

                                                    <div class="checkbox red icheck">
                                                        <label>

                                                            {{ check_field("privilegios[]", "id": priCatGasto.id, "class": "privilegios", "checked": funciones.in_array(priCatGasto.id, privilegios) ? true : null, "value": priCatGasto.id) }}
                                                                {{ priCatGasto.pri_nombre }}

                                                        </label>
                                                    </div>

                                                {% endfor %}

                                            </div>
                                            <div class="col-sm-4 mb-md">
                                                <h3>Sección de Gastos</h3>

                                                {% for priGasto in priGastos %}

                                                    <div class="checkbox red icheck">
                                                        <label>

                                                            {{ check_field("privilegios[]", "id": priGasto.id, "class": "privilegios", "checked": funciones.in_array(priGasto.id, privilegios) ? true : null, "value": priGasto.id) }}
                                                                {{ priGasto.pri_nombre }}

                                                        </label>
                                                    </div>

                                                {% endfor %}

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4 mb-md">
                                                <h3>Sección de Importación</h3>

                                                {% for priImpor in priImportacion %}

                                                    <div class="checkbox red icheck">
                                                        <label>

                                                            {{ check_field("privilegios[]", "id": priImpor.id, "class": "privilegios", "checked": funciones.in_array(priImpor.id, privilegios) ? true : null, "value": priImpor.id) }}
                                                                {{ priImpor.pri_nombre }}

                                                        </label>
                                                    </div>

                                                {% endfor %}

                                            </div>
                                            <div class="col-sm-4 mb-md">
                                                <h3>Sección de Estadísticas</h3>

                                                {% for priEstadistica in priEstadisticas %}

                                                    <div class="checkbox red icheck">
                                                        <label>

                                                            {{ check_field("privilegios[]", "id": priEstadistica.id, "class": "privilegios", "checked": funciones.in_array(priEstadistica.id, privilegios) ? true : null, "value": priEstadistica.id) }}
                                                                {{ priEstadistica.pri_nombre }}

                                                        </label>
                                                    </div>

                                                {% endfor %}

                                            </div>
                                        </div>

                                    {{ end_form() }}

                                </div>

                                {% if funciones.in_array(37, priRol) %}

                                    <div class="panel-footer">
                                        <div class="clearfix">
                                            <span id="loaPrivilegios" class="loading hide">

                                                {{ image("img/loading.gif", "width": "32px") }}

                                            </span>

                                            {{ submit_button("Registrar", "class": "btn btn-inverse", "id": "regPrivilegios", "data-rol-id": idRol) }}
                                            {{ submit_button("Cancelar", "class": "btn btn-default", "id": "canPrivilegios") }}

                                        </div>
                                    </div>

                                {% endif %}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{ assets.outputJs() }}