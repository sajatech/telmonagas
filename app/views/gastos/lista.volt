{{ assets.outputCss() }}

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="container-fluid">
                <div data-widget-group="group1">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="ajaxSpinner" class="fs-16 text-center mt-md">

                                {{ image("img/loading.gif", "width": "32px") }}
                                {{ "Por favor, espere..." }}

                            </div>
                            <div class="panel panel-success hide" id="panGastos">
                                <div class="panel-heading">
                                    <h1 class="title c-w fz-24">{##}Gastos Registrados</h1>
                                    <div class="panel-ctrls"></div>
                                </div>
                                <div class="panel-body no-padding">
                                    <table class="table table-striped table-bordered nowrap tcur-pointer dt-responsive" id="gastos" style="width:100%">
                                        <thead class="fs-16">
                                            <tr>
                                                <th>Código</th>
                                                <th>Descripción</th>
                                                <th>Fecha</th>
                                                <th width="185px">Monto</th>
                                                <th width="127px">Divisa</th>
                                                <th>Concepto</th>
                                                <th>Estatus</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>                
                                <div class="panel-footer"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{ assets.outputJs() }}