<?php
if(!$this->session->get("nivel5")["id"] && !$this->session->get("nivel4")["id"] && !$this->session->get("nivel3")["id"] && !$this->session->get("nivel2")["id"] && !$this->session->get("nivel1")["id"])
	return $this->response->redirect("login");
?>

<header id="topnav" class="navbar navbar-default {{ router.getRewriteUri() != "/articulos/lista" and router.getRewriteUri() != "/clientes/lista" and router.getRewriteUri() != "/salidas/lista" and router.getRewriteUri() != "/gastos/lista" and router.getRewriteUri() != "/usuarios/lista" and router.getRewriteUri() != "/reportes/envios" and router.getRewriteUri() != "/reportes/salidas" and router.getRewriteUri() != "/reportes/pagos" and router.getRewriteUri() != "/directorio/contactos" ? "navbar-fixed-top" : "" }} role="banner">
	<ul class="nav navbar-nav toolbar pull-left">
	    <li class="toolbar-icon-bg">

			{% set link4 = "
				<span class='icon-bg'>
					<i class='ti ti-arrow-circle-left'></i>
				</span>" 
			%}
			
			{{ link_to("Javascript: history.back()", link4, "class": "mr-sm", false) }}

	    </li>
	</ul>
	<div class="logo-area">
		
		{{ link_to("articulos/lista", "Saja Tech", "class": "navbar-brand") }}

	</div>
	<ul class="nav navbar-nav toolbar pull-right">
        <li class="toolbar-icon-bg hidden-xs" id="trigger-fullscreen">

			{% set link3 = "
				<span class='icon-bg'>
					<i class='ti ti-fullscreen'></i>
				</span>" 
			%}
			
			{{ link_to("#", link3, "class": "toggle-fullscreen", false) }}

        </li>
		<?php /*<li class="dropdown toolbar-icon-bg">

			{% set link12 = "
				<span class='icon-bg'>
					<i class='ti ti-bell'></i>
				</span>
				<span class='badge badge-deeporange'>
					2
				</span>"
			%}
			
			{{ link_to("#", link12, "class": "hasnotifications dropdown-toggle", "data-toggle": "dropdown") }}

			<div class="dropdown-menu notifications arrow">
				<div class="topnav-dropdown-header">
					<span>
						Notifications
					</span>
				</div>
				<div class="scroll-pane">
					<ul class="media-list scroll-content">
						<li class="media notification-success">

							{% set link13 = "
								<div class='media-left'>
									<span class='notification-icon'>
										<i class='ti ti-check'></i>
									</span>
								</div>
								<div class='media-body'>
									<h4 class='notification-heading'>
										Update 1.0.4 successfully pushed
									</h4>
									<span class='notification-time'>
										8 mins ago
									</span>
								</div>"
							%}
							
							{{ link_to("#", link13, false) }}

						</li>
						<li class="media notification-info">

							{% set link14 = "
								<div class='media-left'>
									<span class='notification-icon'>
										<i class='ti ti-check'></i>
									</span>
								</div>
								<div class='media-body'>
									<h4 class='notification-heading'>
										Update 1.0.3 successfully pushed
									</h4>
									<span class='notification-time'>
										24 mins ago
									</span>
								</div>"
							%}
							
							{{ link_to("#", link14, false) }}

						</li>
						<li class="media notification-teal">

							{% set link15 = "
								<div class='media-left'>
									<span class='notification-icon'>
										<i class='ti ti-check'></i>
									</span>
								</div>
								<div class='media-body'>
									<h4 class='notification-heading'>
										Update 1.0.2 successfully pushed
									</h4>
									<span class='notification-time'>
										16 hours ago
									</span>
								</div>"
							%}
							
							{{ link_to("#", link15, false) }}

						</li>
						<li class="media notification-indigo">

							{% set link16 = "
								<div class='media-left'>
									<span class='notification-icon'>
										<i class='ti ti-check'></i>
									</span>
								</div>
								<div class='media-body'>
									<h4 class='notification-heading'>
										Update 1.0.1 successfully pushed
									</h4>
									<span class='notification-time'>
										2 days ago
									</span>
								</div>"
							%}
							
							{{ link_to("#", link16, false) }}

						</li>
						<li class="media notification-danger">

							{% set link17 = "
								<div class='media-left'>
									<span class='notification-icon'>
										<i class='ti ti-arrow-up'></i>
									</span>
								</div>
								<div class='media-body'>
									<h4 class='notification-heading'>
										Initial Release 1.0
									</h4>
									<span class='notification-time'>
										4 days ago
									</span>
								</div>"
							%}
							
							{{ link_to("#", link17, false) }}

						</li>
					</ul>
				</div>
				<div class="topnav-dropdown-footer">

					{{ link_to("#", "See all notifications", false) }}

				</div>
			</div>
		</li>*/ ?>
		<li class="dropdown toolbar-icon-bg">

			{% set imagen8 = image("assets/img/avatar.png", "class": "img-circle", "alt": "") %}
			
			{{ link_to("#", imagen8, "class": "dropdown-toggle username", "data-toggle": "dropdown") }}

			<ul class="dropdown-menu userinfo arrow">
				<li>

					{% set link19 = "
						<i class='ti ti-user'></i>
						<span> " ~ funciones.getNombreUsuario() ~ "</span>
						<span class='badge badge-info pull-right'></span>"
					%}
					
					{{ link_to("#", link19, false) }}

				</li>
				<?php /*<li>

					{% set link20 = "
						<i class='ti ti-panel'></i>
						<span>
							Account
						</span>"
					%}
					
					{{ link_to("#", link20, false) }}

				</li>
				<li>

					{% set link21 = "
						<i class='ti ti-settings'></i>
						<span>
							Settings
						</span>"
					%}
					
					{{ link_to("#", link21, false) }}

				</li>
				<li class="divider"></li>
				<li>

					{% set link22 = "
						<i class='ti ti-stats-up'></i>
						<span>
							Earnings
						</span>"
					%}
					
					{{ link_to("#", link22, false) }}

				</li>
				<li>

					{% set link23 = "
						<i class='ti ti-view-list-alt'></i>
						<span>
							Statement
						</span>"
					%}
					
					{{ link_to("#", link23, false) }}

				</li>
				<li>

					{% set link24 = "
						<i class='ti ti-money'></i>
						<span>
							Withdrawals
						</span>"
					%}
					
					{{ link_to("#", link24, false) }}

				</li>
				<li class="divider"></li>*/ ?>
				<li>

					{% set link25 = "
						<i class='ti ti-shift-right'></i>
						<span>
							Cerrar Sesión
						</span>"
					%}
					
					{{ link_to(funciones.gethostname() ~ "login", "Cerrar Sesión", false) }}

				</li>
			</ul>
		</li>
	</ul>
	<div class="toolbar-icon-bg hidden-xs" id="toolbar-search">
        <div class="input-group">
        	<div class="div-input-group">
    			CONTROL DE PRODUCTOS: RUNDO / TELECOMUNICACIONES MONAGAS
			</div>
		</div>
  	</div>
</header>
<nav class="navbar navbar-blue" role="navigation" id="headernav">
    <div class="navbar-header">

<div style="
    color: white;
    width: 60%;
    margin: 16px;
    display: none;
    font-size: initial;
" class="title-xs">TELECOMUNICACIONES MONAGAS</div>

        <div style="display: inline"><button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#horizontal-navbar">
            <i class="ti ti-menu"></i>
        </button></div>
    </div>

    <div class="collapse navbar-collapse large-icons-nav" id="horizontal-navbar">
        <ul class="nav navbar-nav smart-menu">

        	{% if funciones.in_array(1, priRol) or funciones.in_array(2, priRol) or funciones.in_array(81, priRol) %}

                <li {% if router.getRewriteUri() == "/articulos/registro" or router.getRewriteUri() == "/articulos/lista" or router.getRewriteUri() == "/articulos/actualizacion" or router.getRewriteUri() == "/articulos/inactivos" %} class="active" {% endif %}>

					{% set link27 = "
                		<i class='ti ti-layout-grid3'></i> 
                		<span class='ml-sm'>
                			Artículos
                			<i class='fa fa-angle-down mr-md'></i>
                		</span>"
					%}

					{{ link_to("articulos/lista", link27, "class": "dropdown-toggle") }}

                    <ul class="dropdown-menu">

                    	{% if funciones.in_array(1, priRol) %}

	                        <li>

								{{ link_to("articulos/registro", "Agregar Artículo") }}

	                        </li>

	                    {% endif %}
                    	
                    	{% if funciones.in_array(2, priRol) %}

	                        <li>

								{{ link_to("articulos/lista", "Buscar Artículos") }}

	                        </li>

	                    {% endif %}

                    	{% if funciones.in_array(81, priRol) %}

	                        <li>

								{{ link_to("articulos/inactivos", "Artículos Inactivos") }}

	                        </li>

	                    {% endif %}

                    </ul>
                </li>

         	{% endif %}

        	{% if funciones.in_array(6, priRol) or funciones.in_array(7, priRol) %}

                <li {% if router.getRewriteUri() == "/proveedores/registro" or router.getRewriteUri() == "/proveedores/lista" or router.getRewriteUri() == "/proveedores/actualizacion" %} class="active" {% endif %}>

					{% set link28 = "
                		<i class='ti ti-tag'></i> 
                		<span class='ml-sm'>
                			Proveedores
                			<i class='fa fa-angle-down mr-md'></i>
                		</span>"
					%}

					{{ link_to("proveedores/lista", link28, "class": "dropdown-toggle") }}

                    <ul class="dropdown-menu">

                    	{% if funciones.in_array(6, priRol) %}

	                        <li>

								{{ link_to("proveedores/registro", "Agregar Proveedor") }}

	                        </li>

	                    {% endif %}

                    	{% if funciones.in_array(7, priRol) %}
                        
	                        <li>

								{{ link_to("proveedores/lista", "Buscar Proveedores") }}

	                        </li>

	                    {% endif %}

                    </ul>
                </li>

            {% endif %}

        	{% if funciones.in_array(11, priRol) or funciones.in_array(12, priRol) %}

                <li {% if router.getRewriteUri() == "/clientes/registro" or router.getRewriteUri() == "/clientes/lista" or router.getRewriteUri() == "/clientes/actualizacion" %} class="active" {% endif %}>

					{% set link29 = "
                		<i class='ti ti-face-smile'></i> 
                		<span class='ml-sm'>
                			Clientes
                			<i class='fa fa-angle-down mr-md'></i>
                		</span>"
					%}

					{{ link_to("clientes/lista", link29, "class": "dropdown-toggle") }}

                    <ul class="dropdown-menu">

                    	{% if funciones.in_array(11, priRol) %}

	                        <li>

								{{ link_to("clientes/registro", "Agregar Cliente") }}

	                        </li>

	                    {% endif %}

                    	{% if funciones.in_array(12, priRol) %}

	                        <li>

								{{ link_to("clientes/lista", "Buscar Clientes") }}

	                        </li>

	                    {% endif %}

                    </ul>
                </li>

            {% endif %}

        	{% if funciones.in_array(16, priRol) or funciones.in_array(17, priRol) %}

                <li {% if router.getRewriteUri() == "/ingresos/compra" or router.getRewriteUri() == "/ingresos/lista" %} class="active" {% endif %}>

					{% set link30 = "
                		<i class='ti ti-angle-double-down'></i> 
                		<span class='ml-sm'>
                			Ingresos
                			<i class='fa fa-angle-down mr-md'></i>
                		</span>"
					%}

					{{ link_to("ingresos/lista", link30, "class": "dropdown-toggle") }}

                    <ul class="dropdown-menu">

                    	{% if funciones.in_array(16, priRol) %}

	                        <li>

								{{ link_to("ingresos/compra", "Agregar Ingreso") }}

	                        </li>

	                    {% endif %}

                    	{% if funciones.in_array(17, priRol) %}

	                        <li>

								{{ link_to("ingresos/lista", "Buscar Ingresos") }}

	                        </li>

	                    {% endif %}

                    </ul>
                </li>

            {% endif %}

        	{% if funciones.in_array(21, priRol) or funciones.in_array(22, priRol) %}

                <li {% if router.getRewriteUri() == "/salidas/venta" or router.getRewriteUri() == "/salidas/lista" or router.getRewriteUri() == "/salidas/anuladas" %} class="active" {% endif %}>

					{% set link31 = "
                		<i class='ti ti-angle-double-up'></i> 
                		<span class='ml-sm'>
                			Salidas
                			<i class='fa fa-angle-down mr-md'></i>
                		</span>"
					%}

					{{ link_to("salidas/lista", link31, "class": "dropdown-toggle") }}

                    <ul class="dropdown-menu">

                    	{% if funciones.in_array(21, priRol) %}

	                        <li>

								{{ link_to("salidas/venta", "Agregar Salida") }}

	                        </li>

	                    {% endif %}

                    	{% if funciones.in_array(22, priRol) %}

	                        <li>

								{{ link_to("salidas/lista", "Buscar Salidas") }}

	                        </li>

	                    {% endif %}

                    	{% if funciones.in_array(78, priRol) %}

	                        <li>

								{{ link_to("salidas/anuladas", "Salidas Anuladas") }}

	                        </li>

	                    {% endif %}

                    </ul>
                </li>

            {% endif %}

        	{% if funciones.in_array(47, priRol) %}

                <li {% if router.getRewriteUri() == "/inventario/ajustes" %} class="active" {% endif %}>

					{% set link33 = "
                		<i class='ti ti-bag'></i> 
                		<span class='ml-sm'>
                			Inventario
                			<i class='fa fa-angle-down mr-md'></i>
                		</span>"
					%}

					{{ link_to("#", link33, "class": "dropdown-toggle", false) }}

                    <ul class="dropdown-menu">

                    	{% if funciones.in_array(21, priRol) %}

	                        <li>

								{{ link_to("inventario/ajuste", "Ajus. de Inv.") }}

	                        </li>

	                    {% endif %}

                    	{#{% if funciones.in_array(21, priRol) %}#}

	                        <li>

								{{ link_to("inventario/ajustes", "Listar Ajustes") }}

	                        </li>

	                    {#{% endif %}#}

                    </ul>
                </li>

            {% endif %}

            {% if funciones.in_array(52, priRol) %}

                <li {% if router.getRewriteUri() == "/envios/registro" or router.getRewriteUri() == "/envios/actualizacion" or router.getRewriteUri() == "/envios/lista" %} class="active" {% endif %}>

					{% set link33 = "
                		<i class='ti ti-truck'></i> 
                		<span class='ml-sm'>
                			Envíos
                			<i class='fa fa-angle-down mr-md'></i>
                		</span>"
					%}

					{{ link_to("#", link33, "class": "dropdown-toggle", false) }}

                    <ul class="dropdown-menu">

                    	{% if funciones.in_array(50, priRol) %}

	                        <li>

								{{ link_to("envios/registro", "Nuevo Envío") }}

	                        </li>

	                    {% endif %}

                    	{% if funciones.in_array(52, priRol) %}

	                        <li>

								{{ link_to("envios/lista", "Buscar Envíos") }}

	                        </li>

	                    {% endif %}

	                    {% if funciones.in_array(61, priRol) %}
	                        <li>

								{{ link_to("envios/promociones", "Promociones") }}

	                        </li>

	                    {% endif %}

                    </ul>
                </li>

            {% endif %}

        	{% if funciones.in_array(69, priRol) or funciones.in_array(70, priRol) %}

	            <li {% if router.getRewriteUri() == "/gastos/registro" or router.getRewriteUri() == "/gastos/lista" or router.getRewriteUri() == "/gastos/actualizacion" %} class="active" {% endif %}>

					{% set link34 = "
	            		<i class='ti ti-money'></i> 
	            		<span class='ml-sm'>
	            			Gastos
	            			<i class='fa fa-angle-down mr-md'></i>
	            		</span>"
					%}

					{{ link_to("gastos/lista", link34, "class": "dropdown-toggle") }}

	                <ul class="dropdown-menu">

	                	{% if funciones.in_array(69, priRol) %}

	                        <li>

								{{ link_to("gastos/registro", "Agregar Gasto") }}

	                        </li>

	                    {% endif %}

	                    {% if funciones.in_array(70, priRol) %}

	                        <li>

								{{ link_to("gastos/lista", "Buscar Gastos") }}

	                        </li>

	                    {% endif %}

	                </ul>
	            </li>

           	{% endif %}

        	{#{% if funciones.in_array(26, priRol) or funciones.in_array(27, priRol) %}#}

                <li {% if router.getRewriteUri() == "/directorio/notas" or router.getRewriteUri() == "/directorio/contactos" %} class="active" {% endif %}>

					{% set notas = "
                		<i class='ti ti-agenda'></i> 
                		<span class='ml-sm'>
                			Directorio
							<i class='fa fa-angle-down mr-md'></i>                			
                		</span>"
					%}

					{{ link_to("directorio/notas", notas, "class": "dropdown-toggle") }}

                    <ul class="dropdown-menu">

                    	{#{% if funciones.in_array(26, priRol) %}#}

	                        <li>

								{{ link_to("directorio/notas", "Notas") }}

	                        </li>

	                    {#{% endif %}#}

                    	{#{% if funciones.in_array(27, priRol) %}#}

	                        <li>

								{{ link_to("directorio/contactos", "Contactos") }}

	                        </li>

	                    {#{% endif %}#}

                    </ul>
                </li>

         	{#{% endif %}#}

        	{% if funciones.in_array(45, priRol) or funciones.in_array(53, priRol) or funciones.in_array(59, priRol) or funciones.in_array(62, priRol) or funciones.in_array(63, priRol) or funciones.in_array(75, priRol) or funciones.in_array(76, priRol) %}

                <li {% if router.getRewriteUri() == "/reportes/movimientos" or router.getRewriteUri() == "/reportes/envios" or router.getRewriteUri() == "/reportes/salidas" or router.getRewriteUri() == "/reportes/pagos" or router.getRewriteUri() == "/reportes/gastos" or router.getRewriteUri() == "/articulos/inventario" or router.getRewriteUri() == "/importacion/precios" or router.getRewriteUri() == "/importacion/lista" %} class="active" {% endif %}>
					<a class="dropdown-toggle">
                    	<i class="ti ti-folder"></i> 
                    	<span class="ml-sm">
                    		Reportes
                    		<i class="fa fa-angle-down mr-md"></i>
                    	</span>
                    </a>
                    <ul class="dropdown-menu">

	                    {% if funciones.in_array(53, priRol) %}

	                        <li>

								{{ link_to("reportes/envios", "1) Envíos") }}

	                        </li>

	                    {% endif %}

                     	{% if funciones.in_array(59, priRol) %}

	                        <li>

								{{ link_to("reportes/salidas", "2) Salidas") }}

	                        </li>

	                    {% endif %}

                     	{% if funciones.in_array(63, priRol) %}

	                        <li>

								{{ link_to("reportes/pagos", "3) P. Bancarios") }}

	                        </li>

	                    {% endif %}

                        {% if funciones.in_array(62, priRol) %}

                        <li>

							{{ link_to("reportes/productos_ventas", "4) C/S. Movimientos") }}

                        </li>

                        {% endif %}

                        {% if funciones.in_array(76, priRol) %}

	                        <li>

								{{ link_to("reportes/gastos", "5) Gastos") }}

	                        </li>

	                    {% endif %}

                        {% if funciones.in_array(45, priRol) %}

	                        <li>

								{{ link_to("reportes/movimientos", "6) Entradas/Salidas") }}

	                        </li>

	                    {% endif %}

	                    {% if funciones.in_array(75, priRol) %}

	                        <li>

								{{ link_to("articulos/inventario", "7) Inventario Total") }}

	                        </li>

	                    {% endif %}
                    </ul>
                </li>

            {% endif %}

        	{% if funciones.in_array(32, priRol) or funciones.in_array(34, priRol) or funciones.in_array(64, priRol) or funciones.in_array(65, priRol) or funciones.in_array(66, priRol) or funciones.in_array(67, priRol) or funciones.in_array(68, priRol) %}

                <li {% if router.getRewriteUri() == "/configuracion/dolar" or router.getRewriteUri() == "/configuracion/roles" or router.getRewriteUri() == "/roles/privilegios" or router.getRewriteUri() == "/configuracion/calculos" or router.getRewriteUri() == "/configuracion/cuentas" or router.getRewriteUri() == "/configuracion/gastos" %} class="active" {% endif %}>
					<a class="dropdown-toggle">
                    	<i class="ti ti-settings"></i> 
                    	<span class="ml-sm">
                    		Configuración
                    		<i class="fa fa-angle-down mr-md"></i>
                    	</span>
                    </a>
                    <ul class="dropdown-menu">

                    	{% if funciones.in_array(32, priRol) %}

	                        <li>

								{{ link_to("configuracion/dolar", "Precios del Dólar") }}

	                        </li>

	                    {% endif %}

                    	{% if funciones.in_array(34, priRol) %}

	                        <li>

								{{ link_to("configuracion/roles", "Roles de Usuario") }}

	                        </li>

	                    {% endif %}
                    	
                    	{% if funciones.in_array(43, priRol) %}

	                        <li>

								{{ link_to("configuracion/calculos", "Cálculos de Precio") }}

	                        </li>

	                    {% endif %}

                    	{% if funciones.in_array(64, priRol) %}

	                        <li>

								{{ link_to("configuracion/cuentas", "Cuentas Bancarias") }}

	                        </li>

	                    {% endif %}

                    	{% if funciones.in_array(65, priRol) or funciones.in_array(66, priRol) or funciones.in_array(67, priRol) or funciones.in_array(68, priRol) %}

	                        <li>

								{{ link_to("configuracion/gastos", "Conceptos de Gastos") }}

	                        </li>

	                    {% endif %}

	                    {% if funciones.in_array(74, priRol) %}

	                        <li>

								{{ link_to("importacion/precios", "Precios Importación") }}

	                        </li>
	                     {% endif %}
	                     {% if funciones.in_array(84, priRol) %}
	                        <li>

								{{ link_to("configuracion/accesos", "Accesos") }}

	                        </li>

	                    {% endif %}

                    </ul>
                </li>

            {% endif %}

        	{% if funciones.in_array(26, priRol) or funciones.in_array(27, priRol) %}

                <li {% if router.getRewriteUri() == "/usuarios/registro" or router.getRewriteUri() == "/usuarios/lista" or router.getRewriteUri() == "/usuarios/actualizacion" %} class="active" {% endif %}>

					{% set link32 = "
                		<i class='ti ti-user'></i> 
                		<span class='ml-sm'>
                			Usuarios
                			<i class='fa fa-angle-down mr-md'></i>
                		</span>"
					%}

					{{ link_to("usuarios/lista", link32, "class": "dropdown-toggle") }}

                    <ul class="dropdown-menu">

                    	{% if funciones.in_array(26, priRol) %}

	                        <li>

								{{ link_to("usuarios/registro", "Agregar Usuario") }}

	                        </li>

	                    {% endif %}

                    	{% if funciones.in_array(27, priRol) %}

	                        <li>

								{{ link_to("usuarios/lista", "Buscar Usuarios") }}

	                        </li>

	                    {% endif %}

                    </ul>
                </li>

         	{% endif %}

        	{% if funciones.in_array(82, priRol) or funciones.in_array(83, priRol) %}

                <li {% if router.getRewriteUri() == "/estadisticas/valor-venta" or router.getRewriteUri() == "/estadisticas/movimiento-stock" %} class="active" {% endif %}>

					{% set link32 = "
                		<i class='ti ti-pie-chart'></i> 
                		<span class='ml-sm'>
                			Estadísticas
                			<i class='fa fa-angle-down mr-md'></i>
                		</span>"
					%}

					{{ link_to("#", link32, "class": "dropdown-toggle", false) }}

                    <ul class="dropdown-menu">

                    	{% if funciones.in_array(82, priRol) %}

	                        <li>

								{{ link_to("estadisticas/valor-venta", "Artículos con Valor % de Venta") }}

	                        </li>

	                    {% endif %}

                    	{% if funciones.in_array(83, priRol) %}

	                        <li>

								{{ link_to("estadisticas/movimiento-stock", "Artículos con Movimiento de Stock") }}

	                        </li>

	                    {% endif %}

                    </ul>
                </li>

         	{% endif %}

        </ul>
    </div>
</nav>
<div class="container-fluid">
	<div class="row pt-sm mb-sm">
		<div class="col-md-6">
			<ol class="breadcrumb">  
				<li>

					{{ link_to("articulos/lista", "Inicio") }}

				</li>
				<li>

					{{ link_to("", funciones.ucwords(router.getControllerName()), "id": "breadcrumb") }}

				</li>
				<li class="active">

					{{ link_to(router.getControllerName() ~ "/" ~ router.getActionName(), funciones.ucwords(router.getActionName()) ) }}

				</li>
			</ol>
		</div>
		<div class="col-md-2">
			<div class="bc-ffd total" style="background-color: #ffdd00;">Total Clientes: {{ totClientes }}</div>
		</div>
		<div class="col-md-2">
			<div class="bc-e51 total c-w">Total Ítems: {{ totItems }}</div>
		</div>
		<div class="col-md-2">
			<div class="bc-03a total" style="color: #ffffff;">Total Artículos: {{ totArticulos }}</div>
		</div>
	</div>
</div>

{{ content() }}

{# Modal de búsqueda rápida de artículos #}
<div class="modal fade" id="modBusqueda" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bc-8bc">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title c-w fz-24">Búsqueda rápida de artículos</h2>
            </div>
            <div class="modal-body bc-g">
                <div class="row">
                    <div class="col-sm-4 closest mb-md">
						<h4 class="mb-md mt-n">Código del Artículo</h4>
						<select name="selCodigo" id="selCodigo" required="required">
							<option value="">-- SELECCIONE --</option>

							{% for articulo in articulos %}

								<option value="{{ articulo.id }}"> {{ articulo.art_codigo }} </option>

							{% endfor %}

						</select>
                    </div>
                    <div class="col-sm-8 closest mb-md">
						<h4 class="mb-md mt-n">Descripción</h4>
						<select name="selArticulo" id="selArticulo" required="required">
							<option value="">-- SELECCIONE --</option>

							{% for articulo in articulos %}

								<option value="{{ articulo.id }}"> {{ articulo.art_descripcion }} </option>

							{% endfor %}

						</select>
                    </div>
                </div>
            </div>
            <div class="modal-body" id="respuesta"></div>
        </div>
    </div>
</div>

{# ******************************** #}

{% if funciones.in_array(44, priRol) %}

	{{ javascript_include("assets/plugins/mousetrap/mousetrap.min.js") }}
	{{ javascript_include("assets/plugins/form-select2/select2.min.js") }}
	{{ javascript_include("js/layouts/main.js") }}

{% endif %}

{#<footer role="contentinfo">
	<div class="clearfix">
		<ul class="list-unstyled list-inline pull-left">
    		<li>
    			<h6 style="margin: 0;">
    				&copy; {{ date("Y") }} Telecomunicaciones Monagas
    			</h6>
    		</li>
		</ul>
	</div>
</footer>#}