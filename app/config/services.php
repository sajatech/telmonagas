<?php

use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Db\Adapter\Pdo\Postgresql as DbAdapter;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Mvc\Router;

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();

/**
 * Registering a router
 */
$di['router'] = function() {
    $router = new Router();
    
    $router->add('/', array(
        'controller' => 'inicio',
        'action' => 'index'
    ));

    $router->add('/login', array(
        'controller' => 'inicio',
        'action' => 'cerrar'
    ));

    $router->add('/estadisticas/valor-venta', array(
        'controller' => 'estadisticas',
        'action' => 'valventa'
    ))->convert(
        'slug',
        function($slug) {
            return str_replace('-', '', $slug);
        }
    );

    $router->add('/estadisticas/movimiento-stock', array(
        'controller' => 'estadisticas',
        'action' => 'movstock'
    ))->convert(
        'slug',
        function($slug) {
            return str_replace('-', '', $slug);
        }
    );

    return $router;
};

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->set('url', function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
}, true);

/**
 * Setting up the view component
 */
$di->set('view', function () use ($config) {

    $view = new View();

    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines(array(
        '.volt' => function ($view, $di) use ($config) {

            $volt = new VoltEngine($view, $di);

            $volt->setOptions(array(
                'compiledPath' => $config->application->cacheDir,
                'compiledSeparator' => '_'
            ));

            $compiler = $volt->getCompiler();
            $compiler->addFunction('strtotime', 'strtotime');

            return $volt;
        },
        '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
    ));

    return $view;
}, true);

/** 
 * Registramos el gestor de eventos
 */
$di->set('dispatcher', function() use ($di) {
    $eventsManager = $di->getShared('eventsManager');
    $roles = new Seguridad($di);
    // Escuchamos eventos en el componente dispatcher usando el plugin Seguridad
    $eventsManager->attach('dispatch', $roles);
    $dispatcher = new Phalcon\Mvc\Dispatcher();
    $dispatcher->setEventsManager($eventsManager);
   
    return $dispatcher;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function () use ($config) {
    return new DbAdapter(array(
        'host' => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname' => $config->database->dbname
    ));
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->set('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Start the session the first time some component request the session service
 */
$di->set('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

/**
 * Para el uso de funciones en volt
 */
$di->set('funciones', function() {
    return new Funciones();
});

/**
 * Para la generación de archivos PDF
 */
$di->set('mpdf', function() {
    require __DIR__ . '../../library/mpdf60/mpdf.php';
    return new mPDF('', 'Letter', '0', '', '10', '10', '10', '20', '12', '16');
});

/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    return include __DIR__ . '../../config/config.php';
});

/**
 * Para el envío de correos electrónicos
 */
$di->setShared('mailService', function() {
    return new MailService();
});

/**
 * Carga de dependencias e instancia en el inyector
 */
$di->setShared('email', function() {
    require __DIR__ . '../../library/phpmailer/PHPMailer.php';
    require __DIR__ . '../../library/phpmailer/SMTP.php';
    return new PHPMailer();    
});

