<?php

/**
 * Composer autoload
 */
include __DIR__ . '/../../vendor/autoload.php';

/**
 * Environment variables
 */
$dotenv = new Dotenv\Dotenv(__DIR__ . '/../../');
$dotenv->load();

return new \Phalcon\Config(array(
    'database' => array(
        'adapter'  => 'Postgresql',
        'host'     => getenv('DATABASE_HOST'),
        'username' => getenv('DATABASE_USER'),
        'password' => getenv('DATABASE_PASS'),
        'dbname'   => getenv('DATABASE_NAME')
    ),
    'application' => array(
        'controllersDir' => __DIR__ . '/../../app/controllers/',
        'libraryDir'     => __DIR__ . '/../../app/library/',
        'modelsDir'      => __DIR__ . '/../../app/models/',
        'viewsDir'       => __DIR__ . '/../../app/views/',
        'pluginsDir'     => __DIR__ . '/../../app/plugins/',
        'libraryDir'     => __DIR__ . '/../../app/library/',
        'cacheDir'       => __DIR__ . '/../../app/cache/',
        'baseUri'        => '/telmonagas/',
    ),
    'email' => array(
        'templatesDir' =>   __DIR__ . '/../../app/views/email/',
        'from'         =>   'sajatechve@gmail.com',
        'fromName'     =>   'Telecomunicaciones Monagas',
        'host'         =>   'smtp.gmail.com',
        'SMTPAuth'     =>   true,
        'username'     =>   'sajatechve@gmail.com',
        'password'     =>   'Maturin2014',
        'SMTPSecure'   =>   'ssl',
        'port'         =>   465
    )
));