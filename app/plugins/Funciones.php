<?php 

use Phalcon\Mvc\User\Plugin;
 
class Funciones extends Plugin {

	public function strtoupper_utf8($cadena) {
		$convertir_de = array(
		   "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u",
		   "v", "w", "x", "y", "z", "à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë","ę", "ì", "í", "î", "ï",
		   "ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú", "û", "ü", "ý", "а", "б", "в", "г", "д", "е", "ё", "ж",
		   "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы",
		   "ь", "э", "ю", "я"
		);

		$convertir_a = array(
		   "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
		   "V", "W", "X", "Y", "Z", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É", "Ê", "Ë","Ę", "Ì", "Í", "Î", "Ï",
		   "Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û", "Ü", "Ý", "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж",
		   "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ъ",
		   "Ь", "Э", "Ю", "Я"
		);
		
		return str_replace($convertir_de, $convertir_a, $cadena);
	}

	public function strtolower_utf8($cadena) {
	      $convertir_a = array(
	           "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u",
	           "v", "w", "x", "y", "z", "à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë","ę", "ì", "í", "î", "ï",
	           "ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú", "û", "ü", "ý", "а", "б", "в", "г", "д", "е", "ё", "ж",
	           "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы",
	           "ь", "э", "ю", "я"
	      );
	      
	      $convertir_de = array(
	           "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
	           "V", "W", "X", "Y", "Z", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É", "Ê", "Ë","Ę", "Ì", "Í", "Î", "Ï",
	           "Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û", "Ü", "Ý", "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж",
	           "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ъ",
	           "Ь", "Э", "Ю", "Я"
	      );
	      
	      return str_replace($convertir_de, $convertir_a, $cadena);
	 }

	public function cambiaf_a_sql($fecha) { 
		if(!empty($fecha)) {
			list($dia, $mes, $ano) = explode("/", $fecha);
		   	$lafecha = $ano . "-" . $mes . "-" . $dia; 	   	
		   	return $lafecha; 
		} else
			return null;
	}

	public function cambiaf_a_normal($fecha) {
		if(!empty($fecha)) {
			list($ano, $mes, $dia) = explode("-", $fecha);
		   	$lafecha = $dia . "/" . $mes . "/" . $ano; 
		   	return $lafecha; 
		} else
			return null;
	}

    public function number_format($numero) {
    	return !empty($numero) ? number_format($numero, 2, ",", ".") : "0,00";
    }

    public function getUsuario() {
        if(!empty($this->session->get("nivel1")["id"]))
            return $this->session->get("nivel1")["id"];
        else if(!empty($this->session->get("nivel2")["id"]))
            return $this->session->get("nivel2")["id"];
        else if(!empty($this->session->get("nivel3")["id"]))
            return $this->session->get("nivel3")["id"];
        else if(!empty($this->session->get("nivel4")["id"]))
            return $this->session->get("nivel4")["id"];
    }

    public function getNombreUsuario() {
        if(!empty($this->session->get("nivel1")["nombre"]))
            return $this->session->get("nivel1")["nombre"];
        else if(!empty($this->session->get("nivel2")["nombre"]))
            return $this->session->get("nivel2")["nombre"];
        else if(!empty($this->session->get("nivel3")["nombre"]))
            return $this->session->get("nivel3")["nombre"];
        else if(!empty($this->session->get("nivel4")["nombre"]))
            return $this->session->get("nivel4")["nombre"];
    }

    public function getRolUsuario() {
        if(!empty($this->session->get("nivel1")["rol"]))
            return $this->session->get("nivel1")["rol"];
        else if(!empty($this->session->get("nivel2")["rol"]))
            return $this->session->get("nivel2")["rol"];
        else if(!empty($this->session->get("nivel3")["rol"]))
            return $this->session->get("nivel3")["rol"];
        else if(!empty($this->session->get("nivel4")["rol"]))
            return $this->session->get("nivel4")["rol"];
    }

    public function cambiam_a_numeric($numero) {
    	if(!empty($numero))
    		return str_replace(",", ".", str_replace(".", "", $numero));
    }

	public function str_pad($codigo) {
        return str_pad($codigo, 8, "0", STR_PAD_LEFT);
	}

	public function str_pad_3($codigo) {
        return str_pad($codigo, 3, "0", STR_PAD_LEFT);
	}

	public function explode($cadena, $separador) {
        return explode($separador, $cadena);
	}

	public function substr($cadena, $posicion, $longitud) {
		return substr($cadena, $posicion, $longitud);
	}

	public function nombremes($mes) {
		setlocale(LC_TIME, "spanish");  
		$nombre = strftime("%B", mktime(0, 0, 0, $mes, 1, 2000)); 
		return ucfirst($nombre);
	} 

	public function normaliza($cadena) {
		$originales = "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ";
		$modificadas = "aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr";
		$cadena = utf8_decode($cadena);
		$cadena = strtr($cadena, utf8_decode($originales), $modificadas);
		$cadena = strtolower($cadena);
		
		return utf8_encode($cadena);
	}

	public function ucwords($cadena) {
		return ucwords($cadena);
	}

	// Función que elimina los elementos duplicados de un array
	public function eliminarRepetidos($array) {
	    $nuevoArray = Array();
	 
	    // Bucle recorriendo todo el array
	    for($i=0; $i < count($array); $i++) {
	        $repetido=false;
	 
	        /* Por cada posicion que recorremos, revisamos que no esté
	           ya en el array de valores, revisando desde el primer elemento
	           hasta el que estamos revisando */
	        for($j = 0; $j < $i; $j++) {
	            if($array[$i] == $array[$j])
	                $repetido = true;
	        }
	 
	        if(!$repetido)
	            $nuevoArray[] = $array[$i];
	    }
	 
	    return $nuevoArray;
	}

	public function str_replace($convertir_de, $convertir_a, $cadena) {
		return str_replace($convertir_de, $convertir_a, $cadena);
	}

	public function rand() {
		return rand();
	}

	public function gethostname() {
		return preg_replace("/public([\/\\\\])index.php$/", "", $_SERVER["PHP_SELF"]);
	}

	public function in_array($needle, $haystack) {
		return in_array($needle, $haystack) ? true : false;
	}

	public function fun_eliminarDobleEspacios($cadena) {
	    $limpia = ""; 
	    $parts = array(); 
	     
	    // Se divide la cadena con todos los espacios que hayan
	    $parts = explode(" ",$cadena); 
	     
	    foreach($parts as $subcadena) { 
	        // De cada subcadena se elimina sus espacios a los lados 
	        $subcadena = trim($subcadena); 
	         
	        // Luego se vuelven a unir con un espacio para formar la nueva cadena limpia 
	        // Se omiten los que sean unicamente espacios en blanco 
	        if($subcadena != "") 
	        	$limpia .= $subcadena . " "; 
	    } 
	    
	    $limpia = trim($limpia); 
	     
	    return $limpia; 
	}

	// Función para encriptar datos enviados por url
	public function encrypt($string, $key) {
		$result = "";

		for($i = 0; $i < strlen($string); $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key)) - 1, 1);
			$char = chr(ord($char) + ord($keychar));
			$result .= $char;
		}

		$result = base64_encode($result);
		$result = str_replace(array('+', '/', '='), array('-', '_', '.'), $result);
		return $result;
	}

	// Función para desencriptar datos recibiddos por url
	public function dencrypt($string, $key) {
		$string = str_replace(array('-', '_', '.'), array('+', '/', '='), $string);
		$result = '';
		$string = base64_decode($string);

		for($i=0; $i<strlen($string); $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key)) - 1, 1);
			$char = chr(ord($char) - ord($keychar));
			$result .= $char;
		}
		
		return $result;
	}

	public function compararFechas($primera, $segunda) {
		$valoresPrimera = explode ("/", $primera);   
		$valoresSegunda = explode ("/", $segunda); 
		$diaPrimera = $valoresPrimera[0];  
		$mesPrimera = $valoresPrimera[1];  
		$anyoPrimera = $valoresPrimera[2]; 
		$diaSegunda = $valoresSegunda[0];  
		$mesSegunda = $valoresSegunda[1];  
		$anyoSegunda = $valoresSegunda[2];
		$diasPrimeraJuliano = gregoriantojd($mesPrimera, $diaPrimera, $anyoPrimera);  
		$diasSegundaJuliano = gregoriantojd($mesSegunda, $diaSegunda, $anyoSegunda);     

		if(!checkdate($mesPrimera, $diaPrimera, $anyoPrimera))
			return 0;
		else if(!checkdate($mesSegunda, $diaSegunda, $anyoSegunda))
			return 0;
		else
			return  $diasPrimeraJuliano - $diasSegundaJuliano;
	}

	public function get_format($df) {

	    $str = '';
	    $str .= ($df->invert == 1) ? ' - ' : '';
	    if ($df->y > 0) {
	        // years
	        $str .= ($df->y > 1) ? $df->y . ' Anos ' : $df->y . ' Ano ';
	    } if ($df->m > 0) {
	        // month
	        $str .= ($df->m > 1) ? $df->m . ' Meses ' : $df->m . ' Mes ';
	    } if ($df->d > 0) {
	        // days
	        $str .= ($df->d > 1) ? $df->d . ' Dias ' : $df->d . ' Dia ';
	    } if ($df->h > 0) {
	        // hours
	        $str .= ($df->h > 1) ? $df->h . ' Horas ' : $df->h . ' Hora ';
	    } if ($df->i > 0) {
	        // minutes
	        $str .= ($df->i > 1) ? $df->i . ' Min ' : $df->i . ' Min ';
	    } if ($df->s > 0) {
	        // seconds
	        $str .= ($df->s > 1) ? $df->s . ' Seg ' : $df->s . ' Seg ';
	    }

	    return $str;
	}

	public function pais_ip($ip) {

		//$ip = '201.242.229.68';

		// Contiene el texto como JSON que retorna geoplugin a partir de la IP
		// Puedes usar un método más sofisticado para hacer un llamado a geoplugin 
		// usando librerias como UNIREST etc
		$informacionSolicitud = file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip);

		// Convertir el texto JSON en un array
		$dataSolicitud = json_decode($informacionSolicitud);

		// Ver contenido del array
		//echo var_dump($dataSolicitud);

		//echo $dataSolicitud->geoplugin_city.'<br>';
		//echo $dataSolicitud->geoplugin_region.'<br>';
		//echo $dataSolicitud->geoplugin_countryName.'<br>'
		// Imprimiria algo similar a (dependiendo de la IP proporcionada) :
		// geoplugin_countryCode => "DE",
		// geoplugin_countryName => "Germany"
		// geoplugin_continentCode => "EU"
		return  $dataSolicitud->geoplugin_city.' /' .$dataSolicitud->geoplugin_region. ' / ' .$dataSolicitud->geoplugin_countryName;

	}
	
}