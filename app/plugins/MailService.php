<?php

use Phalcon\Mvc\User\Plugin,
	Phalcon\Mvc\View;

class MailService extends Plugin {

	public function getTemplate($name, $params, $mail) {
	    return $this->view->getRender($this->config->email->templatesDir, $name, $params, function($view) {
	        $view->setRenderLevel(View::LEVEL_LAYOUT);
	    });	    
	}

	public function send($name, $params, $addresses, $subject) {
		// Se obtiene la instancia de PHPMailer
		$email = $this->getDi()->getShared("email");
		// Se obtiene la configuración del email app/config/config.php
		$emailConfig = $this->config->email;
		// ******************************** //
		$email->isSMTP();
		$email->Host = $this->config->email->host;
		$email->SMTPAuth = $emailConfig->SMTPAuth;
		$email->Username = $emailConfig->username;
		$email->Password = $emailConfig->password;
		$email->SMTPSecure = $emailConfig->SMTPSecure;
		$email->Port = $emailConfig->port;
		$email->setFrom($emailConfig->from, $emailConfig->fromName);

		foreach($addresses as $address)
			$email->addAddress($address);

		$email->isHTML(true);
		$email->Subject = $subject;
		$email->Body = $this->getTemplate($name, $params, $email);

		return !$email->send() ? $email : true;
	}
    
}