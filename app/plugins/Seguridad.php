<?php

use Phalcon\Events\Event,
	Phalcon\Mvc\User\Plugin,
	Phalcon\Mvc\Dispatcher,
	Phalcon\Acl;

// Plugin roles para llevar a cabo roles de usuarios en la herramienta de inventario
class Seguridad extends Plugin {

	// Lógica para crear una herramienta con roles de usuarios
	public function getAcl() {
		if(!isset($this->persistent->acl)) {
	 		// Se crea la instancia de acl para crear los roles
		 	$acl = new Phalcon\Acl\Adapter\Memory();
			// Por defecto la acción será denegar el acceso a cualquier zona
		 	$acl->setDefaultAction(Phalcon\Acl::ALLOW);

		 	// Registramos los roles que deseamos tener en nuestra herramienta
			$roles = array(
				"admin" => new Phalcon\Acl\Role("Admin"),
				"vendedor" => new Phalcon\Acl\Role("Vendedor")
			);

			// Se añaden los roles al acl
			foreach ($roles as $rol)
				$acl->addRole($rol);

			// Zonas accesibles sólo para rol admin
			$adminAreas = array(
				"clientes" => array(
					"registro",
					"registrobd",
					"lista",
					"getclientes",
					"actualizacion",
					"actualizacionbd",
					"descliente",
					"activcliente"
				),

				"configuracion" => array(
					"marcas",
					"getmarcas",
					"regmarca",
					"actmarca",
					"desmarca",
					"activmarca",
					"getmodelos",
					"regmodelo",
					"elimodelo",
					"dolar",
					"regdolar",
					"actdolar",
					"roles",
					"calculos",
					"regcalculo"
				),

				"ingresos" => array(
					"compra",
					"registrobd",
					"ingreso_mercancia",
					"lista",
					"getingresos",
					"actualizacionbd"
				),

				"proveedores" => array(
					"registro",
					"getproveedor",
					"registrobd",
					"lista",
					"getproveedores",
					"actualizacion",
					"actualizacionbd",
					"desproveedor",
					"activproveedor"
				),

				"salidas" => array(
					"venta",
					"registrobd",
					"lista",
					"getsalidas",
					"actualizacionbd"
				),

				"dolares" => array(
					"getdolares"
				),

				"calculos" => array(
					"getcalculos"
				),

				"seriales" => array(
					"registrobd",
					"getseriales",
					"desserial"
				),

				"reportes" => array(
					"movimientos",
					"getmovimientos",
					"movimientos_articulos"
				),

				"usuarios" => array(
					"registro",
					"registrobd",
					"lista",
					"getusuarios",
					"actualizacion",
					"actualizacionbd",
					"desusuario",
					"activusuario"
				),

				"roles" => array(
					"getroles",
					"privilegios",
					"privilegiosbd"
				)
			);

			// Se añaden las zonas de admin a los recursos de la herramienta
			foreach($adminAreas as $resource => $actions)
				$acl->addResource(new Phalcon\Acl\Resource($resource), $actions);

			// Zonas accesibles para usuarios vendedores de la herramienta
			$vendedorAreas = array(
				"articulos" => array(
					"getarticulo",
					"lista",
					"registro",
					"registrobd",
					"getarticulos",
					"actualizacion",
					"actualizacionbd",
					"desarticulo",
					"activarticulo",
					"busqueda"
				),

				"clientes" => array(
					"getcliente"
				),

				"dolares" => array(
					"urlgetdolar"
				),

				"inventario" => array("salida"),
				"proveedores" => array("getproveedor")

				/*"usuarios" => array(
					"registro",
					"registrobd",
					"getusuario",
					"lista",
					"listabd",
					"desactivacion",
					"actualizacion",
					"actualizacionbd"
				),*/
			);

			// Se añaden las zonas para usuarios vendedores a los recursos de la herramienta
			foreach($vendedorAreas as $resource => $actions)
				$acl->addResource(new Phalcon\Acl\Resource($resource), $actions);

			// Zonas públicas de la herramienta
			$publicAreas = array(
				"index" => array(
					"index",
					"login",
					"cerrar",
				)
			);

			// Se añaden las zonas públicas a los recursos de la herramienta
			foreach($publicAreas as $resource => $actions)
				$acl->addResource(new Phalcon\Acl\Resource($resource), $actions);

			// Se da acceso a todos los usuarios a las zonas públicas de la herramienta
			foreach($roles as $rol) {
				foreach($publicAreas as $resource => $actions)
					$acl->allow($rol->getName(), $resource, "*");
			}

			// Se da acceso a la zona de admins solo a rol Admin
			foreach($adminAreas as $resource => $actions) {
				foreach($actions as $action)
					$acl->allow("Admin", $resource, $action);
			}

			// Se da acceso a las zonas de vendedores
			foreach($vendedorAreas as $resource => $actions) {
				// Se da acceso a los vendedores
				foreach($actions as $action)
					$acl->allow("Vendedor", $resource, $action);

				// Se da acceso a los admins
				foreach($actions as $action)
					$acl->allow("Admin", $resource, $action);
			}

			// El acl queda almacenado en sesión
			$this->persistent->acl = $acl;
		}

		 return $this->persistent->acl;
	}

	// Esta acción se llama antes de ejecutar cualquier acción en la aplicación
	public function beforeDispatch(Event $event, Dispatcher $dispatcher) {
		// Esta sesión sólo la tendrá el admin
		$admin = $this->session->get("admin");
		// Esta sesión sólo la tendrá el usuario vendedor
		$vendedor = $this->session->get("vendedor");

		// Si no es usuario admin es vendedor
		$rol = !$admin ? "Vendedor" : "Admin";

		// Nombre del controlador al que se intenta acceder
		$controller = $dispatcher->getControllerName();
		// Nombre de la acción a la que se intenta acceder
		$action = $dispatcher->getActionName();
		// Se obtiene la Lista de Control de Acceso (acl) que se ha creado
		$acl = $this->getAcl();
		// Boolean (true|false), si se tienes permisos devuelve true en otro caso false
		$allowed = $acl->isAllowed($rol, $controller, $action);

		// Si el usuario no tiene acceso a la zona que intenta acceder
		if($allowed != Acl::ALLOW) {
			if($dispatcher->getControllerName() != "index") {
				$this->response->redirect("login");
				return false;
			}

			return;
		}
	}

}