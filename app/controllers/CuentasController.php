<?php

use Phalcon\Mvc\Model\Query;

class CuentasController extends ControllerBase {
    
    // Obtención de la lista de cuentas bancarias
    public function getcuentasAction() {
        $this->view->disable();

        $query = "SELECT
            c.id AS codCuenta,
            c.cue_numero,
            c.cue_tipo,
            c.cue_estatus,
            b.id AS codBanco,
            b.ban_nombre
            FROM
            Cuentas AS c,
            Bancos AS b
            WHERE
            c.ban_id = b.id";
        
        $cuentas = (new Query($query, $this->getDI()))->execute();
        $arrCuentas = array();

        foreach($cuentas as $clave => $cuenta) {
            $arrCuentas["aaData"][$clave]["codigo"] = $this->funciones->str_pad($cuenta->codCuenta);
            $arrCuentas["aaData"][$clave]["codBanco"] = $cuenta->codBanco;
            $arrCuentas["aaData"][$clave]["banco"] = $cuenta->ban_nombre;
            $arrCuentas["aaData"][$clave]["numero"] = $cuenta->cue_numero;
            $arrCuentas["aaData"][$clave]["tipo"] = $cuenta->cue_tipo;
            $arrCuentas["aaData"][$clave]["estatus"] = "<span class='label " . ($cuenta->cue_estatus == "ACTIVO" ? "label-success" : "label-danger") . "'>" . $cuenta->cue_estatus . "</span>";
        }

        //$arrDolares["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 7);

        return json_encode($arrCuentas);
    }

}