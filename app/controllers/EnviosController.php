<?php

use Phalcon\Mvc\Model\Query;

class EnviosController extends ControllerBase {

    //funcion para vista de registrar envios
    public function registroAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets->addCss("assets/plugins/form-select2/select2.css");

        $this->assets
            ->addJs("assets/js/number.format.js")
            ->addJs("assets/js/jquery.price_format.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
            ->addJs("assets/plugins/form-select2/select2.min.js")
            ->addJs("assets/plugins/bootbox/bootbox.js")
            ->addJs("assets/plugins/form-jasnyupload/fileinput.min.js")
            ->addJs("assets/plugins/form-inputmask/jquery.inputmask.bundle.js")
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("js/envios/registro.js");

        // Lista de clientes
        $clientes = Clientes::find(array("cli_estatus = 'ACTIVO' AND solo_promo = '0'", "order" => "cli_nombre, cli_codigo"));
        $this->view->setVar("clientes", $clientes);

    }

    // Almacenamiento del envio en la base de datos
    public function registrobdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();

                // Datos del proveedor
                $envio = new Envios();

                $envio->setEnvFecha($this->funciones->cambiaf_a_sql($this->request->getPost("EnvFecha")));
                $envio->setEnvRif($this->funciones->strtoupper_utf8($this->request->getPost("codCliente")));
                $envio->setEnvNombre($this->funciones->strtoupper_utf8($this->request->getPost("nombre")));
                $envio->setEnvEmpresa($this->funciones->strtoupper_utf8($this->request->getPost("EnvEmpresa")));
                $envio->setEnvMonto($this->funciones->cambiam_a_numeric($this->request->getPost("EnvMonto")));
                $envio->setEnvMontoSalida($this->request->getPost("EnvMontoSalida"));
                $envio->setEnvGuia($this->funciones->strtoupper_utf8($this->request->getPost("EnvGuia")));
                $envio->setEnvEmail($this->funciones->normaliza($this->request->getPost("corElectronico")));
                $envio->setEnvDetalle($this->funciones->strtoupper_utf8($this->request->getPost("EnvDetalle")));
                
                $envio->setUsuId($this->funciones->getUsuario());

                // ******************************** //

                if(!$envio->save()) {
                    foreach($envio->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }

                // Se envía el correo
                if(count($errores) == 0) { 
                
                    $send = $this->mailService->send("envio_interno", array("nombre" => $this->funciones->strtoupper_utf8($this->request->getPost("nombre")), "fecha" => $this->request->getPost("EnvFecha"),"empresa"=>$this->funciones->strtoupper_utf8($this->request->getPost("EnvEmpresa")),"guia"=>$this->funciones->strtoupper_utf8($this->request->getPost("EnvGuia")),"detalle"=>$this->funciones->strtoupper_utf8($this->request->getPost("EnvDetalle")), "monto"=>$this->request->getPost("EnvMontoSalida")), ["oficina.tcm@gmail.com", "rundopac@gmail.com","maturin.manuel@gmail.com"], "RUNDO: ENVIO REALIZADO");

                    $send_cliente = $this->mailService->send("envio", array("nombre" => $this->funciones->strtoupper_utf8($this->request->getPost("nombre")), "fecha" => $this->request->getPost("EnvFecha"),"empresa"=>$this->funciones->strtoupper_utf8($this->request->getPost("EnvEmpresa")),"guia"=>$this->funciones->strtoupper_utf8($this->request->getPost("EnvGuia")),"detalle"=>$this->funciones->strtoupper_utf8($this->request->getPost("EnvDetalle")), "monto"=>$this->request->getPost("EnvMontoSalida"), "correo"=>$this->request->getPost("corElectronico") ), [$this->funciones->normaliza($this->request->getPost("corElectronico"))], "RUNDO: SOPORTE DE ENVIO");
                

                if($send_cliente !== true)
                    $errores[] = $send_cliente;

                }


            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //contabilizo las veces del envio
                $envio->EnvCorreoEstatus="ENVIADO";
                $envio->setEnvEstatus(1);
                $envio->update();
                //guardo accion para auditoria.
                $this->saveAction("Registro de Envio Guia: " . $this->request->getPost("EnvGuia") . "");
                $parametros["text"] = "El ENVIO se registró correctamente.";
                $parametros["type"] = "success";
                $parametros["id"] = $envio->getId();
                }

            echo json_encode($parametros);
        }
    }

    // Vista de envios listads
    public function listaAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css")
            ->addCss("assets/plugins/datatables/dataTables.themify.css");

        $this->assets
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("js/envios/lista.js");
    }

    // Obtención de la lista de envios
    public function getenviosAction() {
        $this->view->disable();
        $envios = Envios::find(array("order" => "id desc"));
        $arrEnvios = array();

        foreach($envios as $clave => $envio) {
            $label = $envio->EnvCorreoEstatus == "ENVIADO" ? "label-success" : "label-danger";
            $arrEnvios["aaData"][$clave]["codigo"] = $this->funciones->str_pad($envio->id);
            $arrEnvios["aaData"][$clave]["rif"] = $envio->EnvRif;
            $arrEnvios["aaData"][$clave]["EnvNombre"] = $envio->EnvNombre;
            $arrEnvios["aaData"][$clave]["EnvFecha"] = $this->funciones->cambiaf_a_normal($envio->EnvFecha);
            $arrEnvios["aaData"][$clave]["EnvEmpresa"] = $envio->EnvEmpresa;
            $arrEnvios["aaData"][$clave]["EnvGuia"] = $envio->EnvGuia;
            // Datos del usuario
            $usuario = Usuarios::findFirstById($envio->UsuId);
            $arrEnvios["aaData"][$clave]["UsuId"] = $usuario->UsuNombre;
            $arrEnvios["aaData"][$clave]["estatus"] = "<span class='label " . $label . "'>" . $envio->EnvCorreoEstatus . "(" . $envio->EnvEstatus . " veces)</span>";
        }

        $arrEnvios["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 12);

        return json_encode($arrEnvios);
    }

        // Vista de actualización de envios
    public function actualizacionAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        if($this->request->isPost()) {
            $this->assets
                ->addCss("assets/plugins/form-select2/select2.css")
                ->addJs("assets/js/number.format.js")
                ->addJs("assets/js/jquery.price_format.min.js")
                ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
                ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
                ->addJs("assets/plugins/form-select2/select2.min.js")
                ->addJs("assets/plugins/bootbox/bootbox.js")
                ->addJs("assets/plugins/form-inputmask/jquery.inputmask.bundle.js")
                ->addJs("assets/js/jquery.redirect.js")
                ->addJs("assets/plugins/form-parsley/parsley.js")
                ->addJs("js/envios/actualizacion.js");

            // Datos del envio
            $envio = Envios::findFirstById($this->request->getPost("ide"));
            $envio ? $this->view->setVar("envio", $envio) : $this->response->redirect("envios/lista");
        } else {
            $this->response->redirect("envios/lista");
            return false;
        }
    }

    // Actualización de envios en la base de datos y reenvio de correo.
    public function actualizacionbdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $envio = Envios::findFirstById($this->request->get("id"));
            $errores = array();

            if($envio) {

                    // Datos del cliente
                    $envio->EnvFecha=$this->funciones->cambiaf_a_sql($this->request->getPost("EnvFecha"));
                    $envio->EnvRif=$this->funciones->strtoupper_utf8($this->request->getPost("codCliente"));
                    $envio->EnvNombre=$this->funciones->strtoupper_utf8($this->request->getPost("nombre"));
                    $envio->EnvEmpresa=$this->funciones->strtoupper_utf8($this->request->getPost("EnvEmpresa"));
                    $envio->EnvMonto=$this->funciones->cambiam_a_numeric($this->request->getPost("EnvMonto"));
                    $envio->EnvMontoSalida=$this->request->getPost("EnvMontoSalida");
                    $envio->EnvGuia=$this->funciones->strtoupper_utf8($this->request->getPost("EnvGuia"));
                    $envio->EnvEmail=$this->funciones->normaliza($this->request->getPost("corElectronico"));
                    $envio->EnvDetalle=$this->funciones->strtoupper_utf8($this->request->getPost("EnvDetalle"));

                    // Datos del cliente
                    $cliente = Clientes::findFirstByCliCodigo($this->funciones->strtoupper_utf8($this->request->getPost("codCliente")));
                    
                    // ******************************** //

                    // Se envía el correo
                    if(count($errores) == 0) { 
                     // $send = $this->mailService->send("envio", array("cuerpo" => $html), ["adiaz02385@gmail.com", "adiaz02385@gmail.com"], "Nuevo Paquete Enviado");

                     $send = $this->mailService->send("envio_interno", array("nombre" => $this->funciones->strtoupper_utf8($this->request->getPost("nombre")), "fecha" => $this->request->getPost("EnvFecha"),"empresa"=>$this->funciones->strtoupper_utf8($this->request->getPost("EnvEmpresa")),"guia"=>$this->funciones->strtoupper_utf8($this->request->getPost("EnvGuia")),"detalle"=>$this->funciones->strtoupper_utf8($this->request->getPost("EnvDetalle")), "monto"=>$this->request->getPost("EnvMontoSalida")), ["oficina.tcm@gmail.com","rundopac@gmail.com","maturin.manuel@gmail.com"], "RUNDO: ENVIO REALIZADO");

                    $send_cliente = $this->mailService->send("envio", array("nombre" => $this->funciones->strtoupper_utf8($this->request->getPost("nombre")), "fecha" => $this->request->getPost("EnvFecha"),"empresa"=>$this->funciones->strtoupper_utf8($this->request->getPost("EnvEmpresa")),"guia"=>$this->funciones->strtoupper_utf8($this->request->getPost("EnvGuia")),"detalle"=>$this->funciones->strtoupper_utf8($this->request->getPost("EnvDetalle")), "monto"=>$this->request->getPost("EnvMontoSalida"), "correo"=>$this->request->getPost("corElectronico") ), [$this->funciones->normaliza($this->request->getPost("corElectronico"))], "RUNDO: SOPORTE DE ENVIO");

                    //contabilizo las veces del envio
                    $envio->EnvEstatus = $envio->EnvEstatus + 1;

                    if($send_cliente !== true)
                      $errores[] = $send_cliente;
                    }


                    if(!$envio->update()) {
                        foreach($envio->getMessages() as $mensaje)
                            $errores[] = $mensaje;
                    }

            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Actualización de Envio Guia: " . $this->request->getPost("EnvGuia") . "");
                $parametros["text"] = "El correo de REENVIO correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    public function eliminacionbdAction() {

        $this->view->disable();

        if($this->request->isPost()) {
            $envio = Envios::findFirstById($this->request->get("id"));
            $errores = array();

            if ($envio !== false) {

                            if ($envio->delete() === false) {
                            $parametros["text"] = implode("</br>", $errores);
                            $parametros["type"] = "error";

                            } else {

                            //guardo accion para auditoria.
                            $this->saveAction("Eliminación de Envio Guia: " . $this->request->get("id") . "");
                            $parametros["text"] = "El envio se eliminó exitosamente.";
                            $parametros["type"] = "success";
                            }


            }

            echo json_encode($parametros);
        }

    }

    //funcion para vista de promociones
    public function promocionesAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
        ->addCss("assets/plugins/form-select2/select2.css")
        ->addCss("assets/plugins/iCheck/skins/flat/blue.css")
        ->addCss("assets/plugins/form-multiselect/css/multi-select.css")
        ->addCss("https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css");

        $this->assets
        ->addJs("assets/js/number.format.js")
        ->addJs("assets/js/jquery.price_format.min.js")
        ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
        ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
        ->addJs("assets/plugins/form-select2/select2.min.js")
        ->addJs("assets/plugins/bootbox/bootbox.js")
        ->addJs("assets/plugins/form-jasnyupload/fileinput.min.js")
        ->addJs("assets/plugins/form-inputmask/jquery.inputmask.bundle.js")
        ->addJs("assets/js/jquery.redirect.js")
        ->addJs("assets/plugins/form-parsley/parsley.js")
        ->addJs("assets/plugins/quicksearch-master/jquery.quicksearch.js")
        ->addJs("assets/plugins/form-multiselect/js/jquery.multi-select.min.js")
        ->addJs("assets/plugins/iCheck/icheck.min.js")
        ->addJs("https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js")

                //Fix since CKEditor
//        ->addJs("assets/plugins/form-ckeditor/")
//        ->addJs("assets/plugins/form-ckeditor/ckeditor.js")
        ->addJs("js/envios/promo.js?V=1.2");

        //verifico que sea un reenvio de promocion
        if($this->request->get("id"))
        {   
            $promo = Promociones::findFirstById($this->request->get("id"));
            $this->view->setVar("cuerpo", $promo->pro_cuerpo);
            $this->view->setVar("asunto", $promo->pro_asunto);

            
            if($this->request->get("continuar")==1)
            {
            
                $cli = Clientes::find(array("order" => "cli_cor_electronico"));
                $clienticos = array(); // creo el array
                
                foreach ($cli as $client) {

                $query = "SELECT c.cli_cor_electronico
                        FROM
                        Clientes as c,
                        PromocionCliente as p
                        WHERE
                        c.id = p.cli_id
                        AND
                        c.cli_cor_electronico = '$client->cli_cor_electronico'
                        AND
                        p.pro_id = '".$this->request->get('id')."'";


                $cliente_faltantes = (new Query($query, $this->getDI()))->execute();

                if(count($cliente_faltantes) < 1)
                    array_push($clienticos, $client->cli_cor_electronico);

                }

                $this->view->setVar("continuar", $this->request->get("continuar"));
                $this->view->setVar("id", $this->request->get("id"));

            }
            
        }
        

        // Lista de clientes
        if($this->request->get("continuar")!=1)
            $clientes = Clientes::find(array("order" => "cli_cor_electronico"));
        else
            $clientes = $clienticos;
            //$clientes = $clienticos;

        $totCliente = count($clientes);

        $this->view->setVar("clientes", $clientes);
        $this->view->setVar("totCliente", $totCliente);

    }

    // Almacenamiento y envio en la base de datos de las promociones
    public function promocionbdAction() {

        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $asunto = $this->funciones->strtoupper_utf8($this->request->getPost("asunto"));
            $cuerpo = $this->request->getPost("cuerpo");
            $emails =json_decode($this->request->getPost("emails"));

            // Datos de la promocion
            $promociones = new Promociones();
            $promociones->setProAsunto($asunto);
            $promociones->setProCuerpo($cuerpo);
            $promociones->setProEstatus("PENDIENTE");
            $promociones->setProFecha(date('Y-m-d H:i:s'));
            if(!$promociones->save()) {
                foreach($promociones->getMessages() as $mensaje)
                    $errores[] = $mensaje;
            }

            foreach ($emails as $correo) {

                $cliente = Clientes::findFirstByCliCorElectronico($correo);

                // cliente promocion
                $promocioncliente = new PromocionCliente();
                $promocioncliente->setCliId($cliente->getId());
                $promocioncliente->setProEstatus("PENDIENTE");
                $promocioncliente->setProId($promociones->getId());
                if(!$promocioncliente->save()) {
                    foreach($promocioncliente->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }

            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Registro de Promoción: " . $promociones->getProAsunto() . "");
                $parametros["text"] = "La Promocion " . $promociones->getProAsunto() . " se registró correctamente.";
                $parametros["type"] = "success";
                $parametros["id"] = $promociones->getId();
                //$parametros["articulo"] = $promociones->getArtCodigo() . " (" . $promociones->getArtDescripcion() . ")";
            }

            echo json_encode($parametros);
        }
    }

            // Almacenamiento y envio en la base de datos de las promociones
    public function continuar_promocionbdAction() {

        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $asunto = $this->funciones->strtoupper_utf8($this->request->getPost("asunto"));
            $cuerpo = $this->request->getPost("cuerpo");
            $emails =json_decode($this->request->getPost("emails"));

            // Actualizo de la promocion

            $promocion = Promociones::findFirstById($this->request->getPost("id"));

            if($promocion) {
                $promocion->pro_asunto = $asunto;
                $promocion->pro_cuerpo = $cuerpo;
                $promocion->pro_estatus = "PENDIENTE";
                $promocion->pro_fecha = date('Y-m-d H:i:s');
                $promocion->pro_fecha_fin = date('Y-m-d H:i:s');

                if(!$promocion->update()) {
                    foreach($promocion->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
                else
                    {

                    foreach ($emails as $correo) {

                    $cliente = Clientes::findFirstByCliCorElectronico($correo);

                    // cliente promocion
                    $promocioncliente = new PromocionCliente();
                    $promocioncliente->setCliId($cliente->getId());
                    $promocioncliente->setProEstatus("PENDIENTE");
                    $promocioncliente->setProId($this->request->getPost("id"));
                        if(!$promocioncliente->save()) {
                            foreach($promocioncliente->getMessages() as $mensaje)
                                $errores[] = $mensaje;
                        }

                    }

                }
            }


            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Continuar Envio de Promoción: " . $promociones->getProAsunto() . "");
                $parametros["text"] = "La Promocion " . $asunto . " se registró correctamente.";
                $parametros["type"] = "success";
                $parametros["id"] = $this->request->getPost("id");
                //$parametros["articulo"] = $promociones->getArtCodigo() . " (" . $promociones->getArtDescripcion() . ")";
            }

            echo json_encode($parametros);
        }
    }
    // Vista de las promociones registrados
    public function promocionesListaAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css")
            ->addCss("assets/plugins/datatables/dataTables.themify.css");

        $this->assets
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("js/envios/promocionesLista.js?V=2.0");
    }

    // Obtención de la lista de promociones
    public function getpromocionesAction() {
        $this->view->disable();
        $promociones = Promociones::find(array("order" => "id desc"));
        $arrPromociones = array();
        
        foreach($promociones as $clave => $promocion) {
            $promocionesEnviadas = PromocionCliente::find(array("pro_id = '" . $promocion->id . "' and pro_estatus = 'ENVIADO'"));
            $promo = PromocionCliente::findByProId($promocion->id);
            $totales = count($promo);
            $tEnviadas = count($promocionesEnviadas);
            if($promocion->pro_estatus == "PENDIENTE")
            {
                $label = "label-danger";
                $date2 = new DateTime("now");
            }
            else
            {
                $label = "label-success";
                $date2 = new DateTime($promocion->pro_fecha_fin);
            }
            $arrPromociones["aaData"][$clave]["codigo"] = $promocion->id;
            $arrPromociones["aaData"][$clave]["asunto"] = "<a class='detalle' data-promo-id='". $promocion->id ."'>". $promocion->pro_asunto . "<a>";
            $arrPromociones["aaData"][$clave]["fecha"] = date("d/m/Y", strtotime($promocion->pro_fecha));
            $date1 = new DateTime($promocion->pro_fecha);
            //$date2 = new DateTime($promocion->pro_fecha_fin);
            $diff = $date1->diff($date2);

            ($promocion->id > 65) ? $timer = $this->funciones->get_format($diff):$timer = 'NO CALCULADO';

            $arrPromociones["aaData"][$clave]["timer"] = "<span style='font-size:12px; color: green'>".$timer."</span>";
            $arrPromociones["aaData"][$clave]["totales"] = $tEnviadas." de ".$totales;
            $arrPromociones["aaData"][$clave]["estatus"] = "<span class='label " . $label . "'>" . $promocion->pro_estatus . "</span>";
        }

        $arrPromociones["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 2);

        return json_encode($arrPromociones);
    }

    // Método para obtener detalle de promo en listado
    function listPromoAction(){
        $this->view->disable();

        if($this->request->isPost()) {
            // Datos de la salida de mercancía
            $promo = Promociones::findFirstById($this->request->get("id"));        

            return json_encode($promo);

                // ******************************** //
        } 
    }
    // Desactivación de promociones
    public function desPromocionAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $promocion = Promociones::findFirstById($this->request->getPost("codigo"));

            if($promocion) {
                $promocion->pro_estatus = "DESACTIVADA";

                if(!$promocion->update()) {
                    foreach($promocion->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "No existe esa Promocion.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Desactivación de Promoción: " . $promocion->pro_asunto . "");
                $parametros["text"] = "La Promocion " . $promocion->pro_asunto . " se desactivó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Activación de PROMOCIONES
    public function activaPromocionAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $promocion = Promociones::findFirstById($this->request->getPost("codigo"));

            if($promocion) {
                $promocion->pro_estatus = "PENDIENTE";

                if(!$promocion->update()) {
                    foreach($promocion->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
             } else
                $errores[] = "No existe esa Promocion.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Activación de Promoción: " . $promocion->pro_asunto . "");
                $parametros["text"] = "La Promocion " . $promocion->pro_asunto . " se activó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }


    public function select_lotesAction() {
        $this->view->disable();

        if( $this->request->get("lote")) {
            $errores = array();
            $lote = $this->request->get("lote");   
            
            if($this->request->get('ide')>0)
                {               
                    $cli = Clientes::find(array("order" => "cli_cor_electronico"));
                    $clienticos = array(); // creo el array

                    foreach ($cli as $client) {

                        $query = "SELECT c.cli_cor_electronico
                                FROM
                                Clientes as c,
                                PromocionCliente as p
                                WHERE
                                c.id = p.cli_id
                                AND
                                c.cli_cor_electronico = '$client->cli_cor_electronico'
                                AND
                                p.pro_id = '".$this->request->get('ide')."'";


                        $cliente_faltantes = (new Query($query, $this->getDI()))->execute();

                        if(count($cliente_faltantes) < 1)
                            array_push($clienticos, $client->cli_cor_electronico);

                    }
                    $clienticos = array_slice($clienticos, 0, $lote);
                }
            else
                {
                    $clienticos = Clientes::find(
                    [
                    'columns' => 'cli_cor_electronico',
                    'order' => 'cli_cor_electronico',
                    'limit' => $lote,
                    ]
                    );
                }
            //$return["json"] = json_encode($clientes);

            echo json_encode($clienticos);

        }
    }

}