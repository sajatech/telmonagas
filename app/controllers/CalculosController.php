<?php

class CalculosController extends ControllerBase {

    // Obtención de los valores en porcentaje para cálculos de precio
    public function getcalculosAction() {
        $this->view->disable();
        $calculos = Calculos::findFirst();
        $dolares = new DolaresController();
        $dolar = $dolares->getdolar();
        $arrCalculos["valores"] = $calculos;
        $arrCalculos["dolar"] = $dolar;

        return json_encode($arrCalculos);
    }

}