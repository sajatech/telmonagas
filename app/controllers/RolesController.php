<?php

use Phalcon\Mvc\Model\Query;

class RolesController extends ControllerBase {

    // Obtención de la lista de roles
    public function getrolesAction() {
        $this->view->disable();
        
        $query = "SELECT
            r.id,
            r.rol_nombre,
            r.rol_estatus,
            u.usu_nombre,
            u.usu_codigo
            FROM 
            Roles AS r,
            Usuarios AS u
            WHERE 
            r.usu_id = u.id";
        
        $roles = new Query($query, $this->getDI());
        $roles = $roles->execute();
        $arrRoles = array();

        foreach($roles as $clave => $rol) {
            $label = $rol->rol_estatus == "ACTIVO" ? "label-success" : "label-danger";
            $arrRoles["aaData"][$clave]["codigo"] = $this->funciones->str_pad($rol->id);
            $arrRoles["aaData"][$clave]["nombre"] = $rol->rol_nombre;
            $arrRoles["aaData"][$clave]["usuario"] = $rol->usu_nombre . " (" . $rol->usu_codigo . ")";
            $arrRoles["aaData"][$clave]["estatus"] = "<span class='label " . $label . "'>" . $rol->rol_estatus . "</span>";
        }

        $arrRoles["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 8);

        return json_encode($arrRoles);
    }

    // Vista de registro de privilegios
    public function privilegiosAction() {
        if($this->request->isPost()) {
            header("Cache-Control: post-chek=0");
            $this->view->setTemplateAfter("main");
            $this->getassets();

            $this->assets
                ->addCss("assets/plugins/iCheck/skins/flat/blue.css");            

            $this->assets
                ->addJs("assets/plugins/iCheck/icheck.min.js")    
                ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
                ->addJs("js/roles/privilegios.js");

            // Lista de privilegios de usuario (sección de artículos)
            $priArticulos = Privilegios::find(array("sec_id = 1", "order" => "pri_nombre"));
            $this->view->setVar("priArticulos", $priArticulos);

            // Lista de privilegios de usuario (sección de proveedores)
            $priProveedores = Privilegios::find(array("sec_id = 2", "order" => "pri_nombre"));
            $this->view->setVar("priProveedores", $priProveedores);

            // Lista de privilegios de usuario (sección de clientes)
            $priClientes = Privilegios::find(array("sec_id = 3", "order" => "pri_nombre"));
            $this->view->setVar("priClientes", $priClientes);

            // Lista de privilegios de usuario (sección de ingresos)
            $priIngresos = Privilegios::find(array("sec_id = 4", "order" => "pri_nombre"));
            $this->view->setVar("priIngresos", $priIngresos);

            // Lista de privilegios de usuario (sección de salidas)
            $priSalidas = Privilegios::find(array("sec_id = 5", "order" => "pri_nombre"));
            $this->view->setVar("priSalidas", $priSalidas);

            // Lista de privilegios de usuario (sección de usuarios)
            $priUsuarios = Privilegios::find(array("sec_id = 6", "order" => "pri_nombre"));
            $this->view->setVar("priUsuarios", $priUsuarios);

            // Lista de privilegios de usuario (precios del dólar)
            $priDolares = Privilegios::find(array("sec_id = 7", "order" => "pri_nombre"));
            $this->view->setVar("priDolares", $priDolares);

            // Lista de privilegios de usuario (roles de usuario)
            $priRoles = Privilegios::find(array("sec_id = 8", "order" => "pri_nombre"));
            $this->view->setVar("priRoles", $priRoles);

            // Lista de privilegios de usuario (calculos de precio)
            $priCalculos = Privilegios::find(array("sec_id = 9", "order" => "pri_nombre"));
            $this->view->setVar("priCalculos", $priCalculos);

            // Lista de privilegios de usuario (sección de reportes)
            $priReportes = Privilegios::find(array("sec_id = 10", "order" => "pri_nombre"));
            $this->view->setVar("priReportes", $priReportes);

            // Lista de privilegios de usuario (sección de inventario)
            $priInventario = Privilegios::find(array("sec_id = 11", "order" => "pri_nombre"));
            $this->view->setVar("priInventario", $priInventario);

            // Lista de privilegios de usuario (sección de envios)
            $priEnvio = Privilegios::find(array("sec_id = 12", "order" => "pri_nombre"));
            $this->view->setVar("priEnvio", $priEnvio);

            // Lista de privilegios de usuario (sección de bancos)
            $priBanco = Privilegios::find(array("sec_id = 13", "order" => "pri_nombre"));
            $this->view->setVar("priBanco", $priBanco);

            // Lista de privilegios de usuario (conceptos de gastos)
            $priCatGastos = Privilegios::find(array("sec_id = 14", "order" => "pri_nombre"));
            $this->view->setVar("priCatGastos", $priCatGastos);

            // Lista de privilegios de usuario (sección de gastos)
            $priGastos = Privilegios::find(array("sec_id = 15", "order" => "pri_nombre"));
            $this->view->setVar("priGastos", $priGastos);

            // Lista de privilegios de importacion (sección de importacion)
            $priImportacion = Privilegios::find(array("sec_id = 16", "order" => "pri_nombre"));
            $this->view->setVar("priImportacion", $priImportacion);

            // Lista de privilegios de estadísticas (sección de estadísticas)
            $priEstadisticas = Privilegios::find(array("sec_id = 17", "order" => "pri_nombre"));
            $this->view->setVar("priEstadisticas", $priEstadisticas);

            // Lista de privilegios asignados al rol
            $this->view->setVar("privilegios", $this->getprivilegios($this->request->getPost("rol")));

            // Id del rol al cual se le asignarán privilegios
            $this->view->setVar("idRol", $this->request->getPost("rol"));

            // Nombre del rol
            $nomRol = Roles::findFirstById($this->request->get("rol"));
            $this->view->setVar("nomRol", $nomRol->rol_nombre);
        } else {
            $this->response->redirect("configuracion/roles");
            return false;
        }
    }

    // Almacenamiento de los privilegios en la base de datos
    public function privilegiosbdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $privilegios = PrivilegiosRoles::find("rol_id = '" . $this->request->get("rol") . "'");

            if($privilegios) {
                if(!$privilegios->delete()) {
                    foreach($privilegios->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            }

            if(count($errores) == 0) {
                foreach($this->request->getPost("privilegios") as $privilegio) {
                    // Privilegios asignados
                    $privilegios = new PrivilegiosRoles();

                    $privilegios->setPriId($privilegio);
                    $privilegios->setRolId($this->request->get("rol"));

                    if(!$privilegios->save()) {
                        foreach($privilegios->getMessages() as $mensaje)
                            $errores[] = $mensaje;
                    }

                    // ******************************** //
                }
            }

            // Nombre del rol
            $rol = Roles::findFirstById($this->request->get("rol"));

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Cambios de Privilegios para el rol: ". $rol->rol_nombre ."");
                $parametros["text"] = "Los privilegios para el rol " . $rol->rol_nombre . " se asignaron correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

}