<?php

use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\View;

class ArticulosController extends ControllerBase {

    // Vista de registro de artículos
    public function registroAction() {
        header("Cache-Control: post-chek=0");
    	$this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addJs("assets/js/jquery.price_format.min.js")
            ->addJs("assets/js/number.format.js")
            ->addJs("assets/plugins/form-jasnyupload/fileinput.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/bootbox/bootbox.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("js/articulos/registro.js");

        // Datos de los porcentajes
        $calculo = Calculos::findFirst();
        
        if($calculo)
            $this->view->setVar("calculo", $calculo);

        // ******************************** //
    }

    // Almacenamiento del artículo en la base de datos
    public function registrobdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $codigo = Articulos::findFirst("upper(art_codigo) = '" . $this->funciones->strtoupper_utf8($this->funciones->fun_eliminarDobleEspacios($this->request->getPost("codArticulo"))) . "'");

            if(!$codigo) {
                // Datos del artículo
                $articulo = new Articulos();

                $articulo->setArtSerial($this->request->getPost("serial"));
                $articulo->setArtCodigo($this->funciones->fun_eliminarDobleEspacios($this->request->getPost("codArticulo")));
                $articulo->setArtDescripcion($this->funciones->strtoupper_utf8($this->request->getPost("descripcion")));
                $articulo->setArtCosUnitario($this->request->get("getCosUnitario"));
                $articulo->setArtPreUnitario1($this->request->get("getPreUnitario1"));
                $articulo->setArtPreUnitario2($this->request->get("getPreUnitario2"));
                $articulo->setArtPreUnitario3($this->request->get("getPreUnitario3"));
                $articulo->setArtPreMerLibre($this->request->get("getPreMerLibre"));
                $articulo->setArtInvInicial($this->request->getPost("invInicial"));
                $articulo->setArtStoMinimo($this->request->getPost("stoMinimo"));
                $articulo->setArtUbicacion($this->request->getPost("ubicacion"));
                $articulo->setUsuId($this->funciones->getUsuario());

                // ******************************** //

                // Foto referencial
                if($this->request->hasFiles()) {
                    foreach($this->request->getUploadedFiles() as $file) {
                        if($file->getExtension()) {
                            if($file->getExtension() == "jpg" || $file->getExtension() == "jpeg" || $file->getExtension() == "png") {
                                if($file->getSize() <= 1025027) {
                                    $ruta = "files/FOT_" . date("Ymd") . "_" . $this->funciones->str_replace(".", "", $this->funciones->str_replace(" ", "", $this->request->getPost("codArticulo"))) . "-2." . $file->getExtension();

                                    if($file->moveTo($ruta) == true) {
                                        rename($ruta, "files/FOT_" . date("Ymd") . "_" . $this->funciones->str_replace(".", "", $this->funciones->str_replace(" ", "", $this->request->getPost("codArticulo"))) . "." . $file->getExtension());

                                        $ruta = "files/FOT_" . date("Ymd") . "_" . $this->funciones->str_replace(".", "", $this->funciones->str_replace(" ", "", $this->request->getPost("codArticulo"))) . "." . $file->getExtension();
                                        $articulo->setArtFotReferencial($ruta);
                                    }
                                } else
                                    $errores[] = "El peso máximo de las imágenes es de 1 Mb.";
                            } else
                                $errores[] = "Sólo puede subir fotos con extensión jpg o png.";
                        }
                    }
                }
                
                // ******************************** //

                if(!$articulo->save()) {
                    foreach($articulo->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "Ya existe un artículo registrado con ese código.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Registro de articulo código: " . $articulo->getArtCodigo() . "");
                $parametros["text"] = "El artículo " . $articulo->getArtCodigo() . " se registró correctamente.";
                $parametros["type"] = "success";
                $parametros["id"] = $articulo->getId();
                $parametros["articulo"] = $articulo->getArtCodigo() . " (" . $articulo->getArtDescripcion() . ")";
                $parametros["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 3);
            }

            echo json_encode($parametros);
        }
    }

    // Vista de artículos registrados
    public function listaAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets->addCss("assets/plugins/datatables/dataTables.responsive.css")
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css")
            ->addCss("assets/plugins/datatables/dataTables.searchHighlight.css")
            ->addCss("assets/plugins/datatables/fixedHeader.bootstrap.min.css")
            ->addCss("assets/plugins/iCheck/skins/flat/blue.css");

        $this->assets
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/jquery.highlight.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.responsive.js")
            ->addJs("assets/plugins/datatables/dataTables.fixedHeader.min.js")  
            ->addJs("assets/plugins/datatables/dataTables.searchHighlight.min.js")
            ->addJs("assets/plugins/iCheck/icheck.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/bootbox/bootbox.js")
            ->addJs("assets/plugins/wijets/wijets.js")
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("js/articulos/lista.js");
    }

    // Obtención de la lista de artículos
    public function getarticulosAction() {
        $this->view->disable();
        $articulos = Articulos::find(array("art_estatus = '" . $this->request->get("estatus") . "'", "order" => "id desc"));
        $dolares = new DolaresController();
        $dolar = $dolares->getdolar();
        $arrArticulos = array();

        foreach($articulos as $clave => $articulo) {
            $label = $articulo->art_estatus == "ACTIVO" ? "label-success" : "label-danger";
            $arrArticulos["aaData"][$clave]["codigo"] = $articulo->art_codigo;
            $arrArticulos["aaData"][$clave]["descripcion"] = "<span class='artDescripcion'>" . $articulo->art_descripcion . "</span>";
            $arrArticulos["aaData"][$clave]["invInicial"] = $articulo->art_inv_inicial;
            $arrArticulos["aaData"][$clave]["stoMinimo"] = $articulo->art_sto_minimo;
            $arrArticulos["aaData"][$clave]["artPreMayor"] = "<span class='fs-16'>$ " . $this->funciones->number_format($articulo->art_pre_unitario2) . " / Bs. " . $this->funciones->number_format($articulo->art_pre_unitario2 * $dolar) . "</span>";
            $arrArticulos["aaData"][$clave]["artPreDetal"] = "<span class='fs-16'>$ " . $this->funciones->number_format($articulo->art_pre_unitario3) . " / Bs. " . $this->funciones->number_format($articulo->art_pre_unitario3 * $dolar) . "</span>";
            $arrArticulos["aaData"][$clave]["estatus"] = "<span class='label " . $label . "'>" . $articulo->art_estatus . "</span>";
        }

        $arrArticulos["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 1);

        return json_encode($arrArticulos);
    }

    // Vista de actualización de artículos
    public function actualizacionAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        if($this->request->isPost()) {
            $this->assets
                ->addCss("assets/plugins/datatables/dataTables.bootstrap.css");

            $this->assets
                ->addJs("assets/js/jquery.price_format.min.js")
                ->addJs("assets/js/number.format.js")
                ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
                ->addJs("assets/plugins/form-jasnyupload/fileinput.min.js")
                ->addJs("assets/plugins/bootbox/bootbox.js")
                ->addJs("assets/plugins/form-parsley/parsley.js")
                ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
                ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
                ->addJs("assets/js/jquery.redirect.js")
                ->addJs("js/articulos/actualizacion.js");

            // Datos del artículo
            $articulo = Articulos::findFirstByArtCodigo($this->request->get("codigo"));
            //guardo accion para auditoria.
            $this->saveAction("Detalle de articulo codigo: " . $this->request->getPost("codigo") . ""); 
            if($articulo) {
                $this->view->setVar("articulo", $articulo);
                $this->view->setVar("test", $_POST);

                // Precio del dólar
                $dolares = new DolaresController();
                $dolar = $dolares->getdolar();
                $this->view->setVar("dolar", $dolar);
            } else
                $this->response->redirect("articulos/lista");

            // ******************************** //

            // Datos de los porcentajes
            $calculo = Calculos::findFirst();
            
            if($calculo)
                $this->view->setVar("calculo", $calculo);

            // ******************************** //

            if($this->request->getPost("anulado"))
                $this->view->setVar("anulado", true);            
        } else {
            $this->response->redirect("articulos/lista");
            return false;
        }
    }

    public function getarticulo($tipo, $articulo) {
        return $this->_getarticulobd($tipo, $articulo);
    }

    // Obtención del detalle del artículo
    private function _getarticulobd($tipo, $articulo) {
        $articulo = $tipo == "id" ? Articulos::findFirstById($articulo) : Articulos::findFirst("upper(art_codigo) = '" . $this->funciones->strtoupper_utf8($articulo) . "'");

        if($articulo) {
            $dolares = new DolaresController();
            $dolar = $dolares->getdolar();
            $totArticulos = 0;
            $totArticulos = $totArticulos + $articulo->art_inv_inicial;
            $movimientos = Movimientos::find("art_id = '" . $articulo->id . "' and mov_estatus = 'ACTIVO'");

            if(count($movimientos) > 0) {
                foreach($movimientos as $movimiento)
                    $totArticulos = $movimiento->mov_tipo == "CARGA" ? $totArticulos + $movimiento->mov_cantidad : $totArticulos - $movimiento->mov_cantidad;
            }

            // Primera entrada de artículo
            $desEntrada = Movimientos::findFirst("art_id = '" . $articulo->id . "' and mov_tipo = 'CARGA'");
            $desEntrada = $desEntrada ? $desEntrada->ing_id != null ? $this->funciones->cambiaf_a_normal(Ingresos::findFirstById($desEntrada->ing_id)->ing_fec_creacion) : $this->funciones->cambiaf_a_normal(Ajustes::findFirstById($desEntrada->aju_id)->aju_fec_creacion) : "--";

            // Última entrada de artículo
            $hasEntrada = Movimientos::findFirst(array("art_id = '" . $articulo->id . "' and mov_tipo = 'CARGA'", "order" => "id desc"));
            $hasEntrada = $hasEntrada ? $hasEntrada->ing_id != null ? $this->funciones->cambiaf_a_normal(Ingresos::findFirstById($hasEntrada->ing_id)->ing_fec_creacion) : $this->funciones->cambiaf_a_normal(Ajustes::findFirstById($hasEntrada->aju_id)->aju_fec_creacion) : "--";

            // Primera salida de artículo
            $desSalida = Movimientos::findFirst("art_id = '" . $articulo->id . "' and mov_tipo = 'DESCARGA' and mov_estatus='ACTIVO'");
            $desSalida = $desSalida ? $desSalida->sal_id != null ? $this->funciones->cambiaf_a_normal(Salidas::findFirstById($desSalida->sal_id)->sal_fec_creacion) : $this->funciones->cambiaf_a_normal(Ajustes::findFirstById($desSalida->aju_id)->aju_fec_creacion) : "--";
            
            // Última salida de artículo
            $hasSalida = Movimientos::findFirst(array("art_id = '" . $articulo->id . "' and mov_tipo = 'DESCARGA' and mov_estatus='ACTIVO'", "order" => "id desc"));
            $hasSalida = $hasSalida ? $hasSalida->sal_id != null ? $this->funciones->cambiaf_a_normal(Salidas::findFirstById($hasSalida->sal_id)->sal_fec_creacion) : $this->funciones->cambiaf_a_normal(Ajustes::findFirstById($hasSalida->aju_id)->aju_fec_creacion) : "--";

            // Sumatoria de entradas y salidas
            $entradas = Movimientos::sum(array("column" => "mov_cantidad", "conditions" => "art_id= '" . $articulo->id . "' and mov_tipo = 'CARGA'"));
            $salidas = Movimientos::sum(array("column" => "mov_cantidad", "conditions" => "art_id= '" . $articulo->id . "' and mov_tipo = 'DESCARGA' and mov_estatus='ACTIVO'"));

            // Valores en porcentaje para cálculos de precio
            $calculos = Calculos::findFirst();

            // Seriales del artículo
            $seriales = Seriales::find("art_id = '" . $articulo->id . "'");

            $parametro["articulo"] = $articulo;
            $parametro["dolar"] = $dolar;
            $parametro["existencia"] = $totArticulos;
            $parametro["desEntrada"] = $desEntrada;
            $parametro["hasEntrada"] = $hasEntrada;
            $parametro["desSalida"] = $desSalida;
            $parametro["hasSalida"] = $hasSalida;
            $parametro["entradas"] = $entradas;
            $parametro["salidas"] = $salidas;
            $parametro["calculos"] = $calculos;
            $parametro["seriales"] = count($seriales) > 0 ? "true" : "false";
        } else
            $parametro["false"] = true;

        return json_encode($parametro);
    }

    //Vista de Inventario Actual de Productos
    public function inventarioAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css");

        $this->assets
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")  
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("js/articulos/inventario.js");

            // Lista de productos
            $art = Articulos::find(array("art_estatus" => "ACTIVO"));
            
            $arrArticulos = array();
            $totalExistencia = 0;
            foreach ($art as $index => $artículos) {

               $arrArticulos[] = json_decode($this->getarticulo("id", $artículos->id));

               $totalExistencia += $arrArticulos[$index]->existencia;
               $totalPrecio += $arrArticulos[$index]->existencia * $arrArticulos[$index]->articulo->art_pre_unitario1;
            }
            $dolares = new DolaresController();
            $dolar = $dolares->getdolar();

            //guardo accion para auditoria.
            $this->saveAction("Consulta de Reporte 7 Inventario Total");
            $this->view->setVar("productos", $arrArticulos);
            $this->view->setVar("totalExistencia", $totalExistencia);
            $this->view->setVar("totalPrecio", $totalPrecio);
            $this->view->setVar("dolar", $dolar);

    }

    public function urlgetarticuloAction() {
        $this->view->disable();

        if($this->request->isPost())
            return $this->_getarticulobd($this->request->getPost("tipo"), $this->request->getPost("articulo"));
    }

    // Actualización de artículos en la base de datos
    public function actualizacionbdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $articulo = Articulos::findFirstById($this->request->get("id"));
            $errores = array();

            if($articulo) {
                $codigo = Articulos::findFirst("upper(art_codigo) = '" . $this->funciones->strtoupper_utf8($this->funciones->fun_eliminarDobleEspacios($this->request->getPost("codArticulo"))) . "' and id != '" . $this->request->get("id") . "'");

                if(!$codigo) {
                    // Datos del artículo
                    $articulo->art_serial = $this->request->getPost("serial");
                    $articulo->art_codigo = $this->funciones->fun_eliminarDobleEspacios($this->request->getPost("codArticulo"));
                    $articulo->art_descripcion = $this->funciones->strtoupper_utf8($this->request->getPost("descripcion"));
                    $articulo->art_cos_unitario = $this->request->get("getCosUnitario");
                    $articulo->art_pre_unitario1 = $this->request->get("getPreUnitario1");
                    $articulo->art_pre_unitario2 = $this->request->get("getPreUnitario2");
                    $articulo->art_pre_unitario3 = $this->request->get("getPreUnitario3");
                    $articulo->art_pre_mer_libre = $this->request->get("getPreMerLibre");
                    $articulo->art_inv_inicial = $this->request->getPost("invInicial");
                    $articulo->art_sto_minimo = $this->request->getPost("stoMinimo");
                    $articulo->art_ubicacion = $this->request->getPost("ubicacion");

                    // ******************************** //

                    // Foto referencial
                    if($this->request->hasFiles()) {
                        foreach($this->request->getUploadedFiles() as $file) {
                            if($file->getExtension()) {
                                if($file->getExtension() == "jpg" || $file->getExtension() == "jpeg" || $file->getExtension() == "png") {
                                    if($file->getSize() <= 1025027) {
                                        $ruta = "files/FOT_" . date("Ymd") . "_" . $this->funciones->str_replace(".", "", $this->funciones->str_replace(" ", "", $this->request->getPost("codArticulo"))) . "-2." . $file->getExtension();

                                        if($file->moveTo($ruta) == true) {
                                            rename($ruta, "files/FOT_" . date("Ymd") . "_" . $this->funciones->str_replace(".", "", $this->funciones->str_replace(" ", "", $this->request->getPost("codArticulo"))) . "." . $file->getExtension());

                                            $ruta = "files/FOT_" . date("Ymd") . "_" . $this->funciones->str_replace(".", "", $this->funciones->str_replace(" ", "", $this->request->getPost("codArticulo"))) . "." . $file->getExtension();
                                            $articulo->art_fot_referencial = $ruta;
                                        }
                                    } else
                                        $errores[] = "El peso máximo de las imágenes es de 1 Mb.";
                                } else
                                    $errores[] = "Sólo puede subir fotos con extensión jpg o png.";
                            }
                        }
                    }
                    
                    // ******************************** //
                                            
                    if(!$articulo->update()) {
                        foreach($articulo->getMessages() as $mensaje)
                            $errores[] = $mensaje;
                    }
                } else
                    $errores[] = "Ya existe un artículo registrado con ese código.";
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Actualización de articulo código: " . $articulo->art_codigo . "");
                $parametros["text"] = "El artículo " . $articulo->art_codigo . " se actualizó correctamente.";
                $parametros["type"] = "success";
                $parametros["image"] = $this->funciones->gethostname() . $articulo->art_fot_referencial;
            }

            echo json_encode($parametros);
        }
    }

        // Desactivación de artículos
    public function desarticuloAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $articulo = Articulos::findFirstByArtCodigo($this->request->getPost("codigo"));

            if($articulo) {
                $articulo->art_estatus = "INACTIVO";

                if(!$articulo->update()) {
                    foreach($articulo->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "No existe ese artículo.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Desactivación de articulo código: " . $articulo->art_codigo . "");
                $parametros["text"] = "El artículo " . $articulo->art_codigo . " se desactivó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Activación de artículos
    public function activarticuloAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $articulo = Articulos::findFirstByArtCodigo($this->request->getPost("codigo"));

            if($articulo) {
                $articulo->art_estatus = "ACTIVO";

                if(!$articulo->update()) {
                    foreach($articulo->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "No existe ese artículo.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Activación de articulo código: " . $articulo->art_codigo . "");
                $parametros["text"] = "El artículo " . $articulo->art_codigo . " se activó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Búsqueda rápida de artículos
    public function busquedaAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->setVar("articulo", json_decode($this->getarticulo("id", $this->request->getPost("articulo"))));
        //guardo accion para auditoria.
        $this->saveAction("Búsqueda de articulo: " . $this->request->getPost("articulo") . "");
    }


    // Eliminacion de artículos en la base de datos
    public function eliminacionbdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $articulo = Articulos::findFirstById($this->request->get("id"));
            $errores = array();

            //verifico movimientos
            $movimiento = Movimientos::findByArtId($this->request->get("id"));

            if ($articulo !== false) {

                //verifico que no tengo movimientos asociados al articulo
                if(count($movimiento)<1)
                    {
                            if ($articulo->delete() === false) {
                            $parametros["text"] = implode("</br>", $errores);
                            $parametros["type"] = "error";

                            } else {

                            //guardo accion para auditoria.
                            $this->saveAction("Eliminacion de articulo: " . $articulo->art_codigo . "");
                            $parametros["text"] = "El artículo " . $articulo->art_codigo . " se eliminó exitosamente.";
                            $parametros["type"] = "success";

                            }

                    } else {

                            $parametros["text"] = "Imposible Eliminar!.<br>Existen movimientos relacionadas con este articulo.";
                            $parametros["type"] = "error";
                    }

            }

            echo json_encode($parametros);
        }
    }
 
    // Desactivación de artículo
    public function desactivacionbdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $articulo = Articulos::findFirstById($this->request->getPost("id"));
            $errores = array();

            if($articulo) {
                $articulo->art_estatus = "INACTIVO";

                if(!$articulo->update()) {
                    foreach($articulo->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "No existe ese artículo.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {

                //guardo accion para auditoria.
                $this->saveAction("Desactivación de articulo código: " . $articulo->art_codigo . "");
                $parametros["text"] = "El artículo " . $articulo->art_codigo . " se desactivó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Activación de artículos
    public function activacionbdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $articulo = Articulos::findFirstById($this->request->getPost("id"));

            if($articulo) {
                $articulo->art_estatus = "ACTIVO";

                if(!$articulo->update()) {
                    foreach($articulo->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "No existe ese artículo.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Activación de articulo código: " . $articulo->art_codigo . "");
                $parametros["text"] = "El artículo " . $articulo->art_codigo . " se activó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Vista de artículos inactivos
    public function inactivosAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets->addCss("assets/plugins/datatables/dataTables.responsive.css")
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css")
            ->addCss("assets/plugins/datatables/dataTables.themify.css")
            ->addCss("assets/plugins/datatables/fixedHeader.bootstrap.min.css");

        $this->assets
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.responsive.js")
            ->addJs("assets/plugins/datatables/dataTables.fixedHeader.min.js")   
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("js/articulos/inactivos.js?v=21.06.01");
    }

}