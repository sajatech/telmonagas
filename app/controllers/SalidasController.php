<?php

use Phalcon\Mvc\Model\Query;

class SalidasController extends ControllerBase { 

    // Salida de mercancía por venta a cliente
    public function ventaAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets->addCss("assets/plugins/form-select2/select2.css")
            ->addCss("assets/plugins/iCheck/skins/flat/blue.css")
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css");

        $this->assets
            ->addJs("assets/js/jquery.price_format.min.js")
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("assets/js/number.format.js")
            ->addJs("assets/plugins/bootbox/bootbox.js")
            ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
            ->addJs("assets/plugins/bootstrap-switch/bootstrap-switch.min.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/form-inputmask/jquery.inputmask.bundle.js")
            ->addJs("assets/plugins/form-jasnyupload/fileinput.min.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("assets/plugins/form-select2/select2.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/iCheck/icheck.min.js")
            ->addJs("js/salidas/venta.js");

        // Lista de clientes
        $clientes = Clientes::find(array("cli_estatus = 'ACTIVO'", "order" => "cli_nombre, cli_codigo"));
        $this->view->setVar("clientes", $clientes);

        // Lista de artículos
        //$articulos = Articulos::find(array("art_estatus = 'ACTIVO'", "order" => "art_codigo, art_descripcion"));
        //$this->view->setVar("articulos", $articulos);

        // Valores en porcentaje para cálculos de precio
        $calculos = Calculos::findFirst();
        $this->view->setVar("calculos", $calculos);

        // Lista de cuentas
        $query = "SELECT
            b.ban_nombre,
            c.id,
            c.cue_numero
            FROM 
            Bancos AS b,
            Cuentas AS c
            WHERE
            c.ban_id = b.id
            ORDER BY
            b.ban_nombre";

        $cuentas = (new Query($query, $this->getDI()))->execute();
        $this->view->setVar("cuentas", $cuentas);

        // ******************************** //

        if($this->request->isPost()) {
            // Datos de la salida de mercancía
            $salida = Salidas::findFirstById($this->request->getPost("codigo"));
            //guardo accion para auditoria.
            $this->saveAction("Consulta de Salida de mercancía: " . $this->request->getPost("codigo") . ""); 
            if($salida) { 
                $this->view->setVar("salida", $salida);

                if($salida->sal_estatus == "ANULADO") {
                    $anulacion = Anulaciones::findFirstBySalId($salida->id);
                    $this->view->setVar("anulacion", $anulacion->anu_motivo);
                }

                // Lista de artículos de la salida
                $query = "SELECT
                    m.mov_cantidad,
                    m.mov_pre_venta,
                    m.mov_importe,
                    a.id,
                    a.art_codigo,
                    a.art_descripcion,
                    a.art_pre_unitario2,
                    a.art_pre_unitario3,
                    a.art_inv_inicial,
                    a.art_fec_creacion,
                    a.art_sto_minimo,
                    a.art_ubicacion,
                    a.art_fot_referencial
                    FROM 
                    Movimientos AS m,
                    Articulos AS a,
                    Salidas AS s
                    WHERE 
                    m.art_id = a.id
                    AND
                    m.sal_id = s.id
                    AND
                    m.mov_tipo = 'DESCARGA'
                    AND
                    s.id = '" . $salida->id . "'";
                
                $lisSalidas = new Query($query, $this->getDI());
                $lisSalidas = $lisSalidas->execute();
                $this->view->setVar("lisSalidas", $lisSalidas);

                // ******************************** //

                // Datos del cliente
                $cliSalida = Clientes::findFirstById($salida->cli_id);
                $this->view->setVar("cliSalida", $cliSalida);

                // Precio del dolar
                $dolar = "SELECT CASE 
                    WHEN d.dol_fec_creacion < to_date('2018-08-23', 'yyyy-mm-dd') THEN d.dol_monto / 100000
                    ELSE d.dol_monto 
                    END
                    FROM
                    tel_dolares AS d
                    WHERE
                    d.id = '". $salida->dol_id ."'";

                $di = \Phalcon\DI::getDefault();
                $db = $di["db"];
                $dolar = $db->query($dolar);
                $dolar->setFetchMode(\Phalcon\Db::FETCH_OBJ);
                $dolar = $dolar->fetchAll();

                $this->view->setVar("dolar", $dolar[0]->dol_monto);

                // ******************************** //

                // Lista de pagos de la salida
                $query = "SELECT
                    p.id,
                    p.pag_fecha,
                    p.pag_num_referencia,
                    p.pag_importe,
                    p.pag_moneda,
                    p.pag_verificado,
                    b.ban_nombre,
                    c.cue_numero
                    FROM 
                    Pagos AS p,
                    Bancos AS b,
                    Cuentas AS c
                    WHERE
                    p.cue_id = c.id
                    AND
                    c.ban_id = b.id
                    AND
                    p.sal_id = '" . $salida->id . "'";
                
                $lisPagos = new Query($query, $this->getDI());
                $lisPagos = $lisPagos->execute();
                $this->view->setVar("lisPagos", $lisPagos);

                // ******************************** //

                if($this->request->getPost("anulada"))
                    $this->view->setVar("anulada", true);
            } else

                $this->response->redirect("salidas/registro");

        } else if($this->request->get("token")) {
            $this->response->redirect("salidas/registro");

            // Precio del dolar
            $dolar = "SELECT CASE 
                WHEN d.dol_fec_creacion < to_date('2018-08-23', 'yyyy-mm-dd') THEN d.dol_monto / 100000
                ELSE d.dol_monto 
                END
                FROM
                tel_dolares AS d
                WHERE
                d.id = '". $salida->dol_id ."'";

            $di = \Phalcon\DI::getDefault();
            $db = $di["db"];
            $dolar = $db->query($dolar);
            $dolar->setFetchMode(\Phalcon\Db::FETCH_OBJ);
            $dolar = $dolar->fetchAll();
            
            $this->view->setVar("dolar", $dolar[0]->dol_monto);

            // ******************************** //

            return false;
        }
    }

    // Almacenamiento de la salida de mercancía en la base de datos
    public function registrobdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            // Precio del dólar
            $dolares = new DolaresController();
            $dolar = $dolares->getdolarid();

            // Datos de la salida de mercancía
            $salida = new Salidas();

            $salida->setSalObservaciones($this->funciones->strtoupper_utf8($this->request->getPost("observaciones")));
            $salida->setCliId($this->request->getPost("cliente"));
            $salida->setUsuId($this->funciones->getUsuario());
            $salida->setDolId($dolar);
            if($this->request->getPost("garantia")==1)
                $salida->setSalGarantia(1);
            $salida->setSalIva($this->funciones->cambiam_a_numeric($this->funciones->str_replace("Bs. ", "", $this->request->get("iva"))));

            // ******************************** //

            if(!$salida->save()) {
                foreach($salida->getMessages() as $mensaje)
                    $errores[] = $mensaje;
            } else {
                $html = "";
                $sumCosTotal = 0;
                $i = 0;
                $flag = false;
                
                foreach(json_decode($this->request->get("articulos")) as $index => $articulo) {
                    // Precio del artículo
                    $datArticulo = Articulos::findFirstById($articulo);
                    $preArticulo = $this->request->getPost("preVenta")[$i] == "PRECIO AL DETAL" ? $datArticulo->art_pre_unitario3 : $datArticulo->art_pre_unitario2;
                    // Datos de la lista de salidas
                    $movimiento = new Movimientos();

                    $movimiento->setMovTipo("DESCARGA");
                    $movimiento->setMovCantidad($this->request->getPost("cantidad")[$i]);
                    if($this->request->getPost("garantia")!=1)
                        $movimiento->setMovPreVenta($this->request->getPost("preVenta")[$i]);
                    else
                        $movimiento->setMovPreVenta("GARANTIA");
                    $movimiento->setArtId($articulo);
                    $movimiento->setMotId(4);
                    $movimiento->setSalId($salida->getId());
                    $movimiento->setMovImporte($preArticulo);
                    $movimiento->setMovCosto($datArticulo->art_cos_unitario);
                    
                    // ******************************** //

                    if(!$movimiento->save()) {
                        foreach($movimiento->getMessages() as $mensaje)
                            $errores[] = $mensaje;
                    } else {
                        $flag = true;

                        // Datos del artículo
                        $datArticulo = Articulos::findFirstById($articulo);
                        $precio = $movimiento->getMovPreVenta() == "PRECIO AL DETAL" ? $datArticulo->art_pre_unitario3 : $datArticulo->art_pre_unitario2;

                        // Costo total de los artículos
                        $cosTotal = $precio * $movimiento->getMovCantidad();

                        // Precio del dolar
                        $dolares = new DolaresController();
                        $dolar = $dolares->getdolar();

                        // Datos del cliente
                        $cliente = Clientes::findFirstById($salida->getCliId());

                        $html .= "<br/>";
                        $html .= "<b>Código de Artículo:</b> " . $datArticulo->art_codigo ."(". $datArticulo->art_descripcion. ")". "<br/>";
                        $html .= "<b>Cantidad:</b> " . $movimiento->getMovCantidad() . "<br/>";
                        if($this->request->getPost("garantia")==1)
                            $html .= "<br><STRONG> :::SALIDA POR GARANTIA::: </STRONG>";

                        /*$html .= "<b>Precio de Venta: </b>" . $movimiento->getMovPreVenta() . "<br/>";
                        $html .= "<b>Precio Unitario: </b> Bs. " . $this->funciones->number_format($precio * $dolar) . "<br/>";
                        $html .= "<b>Precio Total: </b> Bs. " . $this->funciones->number_format($cosTotal * $dolar) . "<br/>";*/

                        // Sumatoria de los costos totales
                        $sumCosTotal = $sumCosTotal + $cosTotal;
                    }

                    $i++; 
                }

                if($flag) {
                    foreach(json_decode($this->request->get("pagos")) as $index => $objPago) {
                        // Datos de la lista de pagos
                        $pago = new Pagos();

                        $pago->setPagFecha($this->funciones->cambiaf_a_sql($objPago->fecha));
                        $pago->setPagNumReferencia($objPago->numReferencia);
                        $pago->setPagImporte($this->funciones->cambiam_a_numeric($objPago->importe));
                        $pago->setPagMoneda($objPago->moneda == "Bs." ? "BOLÍVARES" : "DÓLARES");
                        $pago->setPagVerificado($objPago->verificado == "SÍ" ? true : false);
                        $pago->setPagForma($objPago->forma);
                        $pago->setCueId($objPago->cuenta);
                        $pago->setSalId($salida->getId());

                        if(!$pago->save()) {
                            foreach($pago->getMessages() as $mensaje)
                                $errores[] = $mensaje;
                        }
                    }
                }     
            }

                // ******************************** //

                // Lista de pagos de la salida
                $query = "SELECT
                    p.id,
                    p.pag_fecha,
                    p.pag_num_referencia,
                    p.pag_importe,
                    p.pag_moneda,
                    p.pag_verificado,
                    b.ban_nombre,
                    c.cue_numero
                    FROM 
                    Pagos AS p,
                    Bancos AS b,
                    Cuentas AS c
                    WHERE
                    p.cue_id = c.id
                    AND
                    c.ban_id = b.id
                    AND
                    p.sal_id = '" . $salida->id . "'";
                
                $lisPagos = new Query($query, $this->getDI());
                $lisPagos = $lisPagos->execute();
                $this->view->setVar("lisPagos", $lisPagos);

                // ******************************** //

            if(count($errores) == 0) { // Se envía el correo de movimientos de inventario
               $send = $this->mailService->send("salida", array("salMercancia" => $html, "total" => $this->funciones->number_format($sumCosTotal * $dolar), "lisPagos" => $lisPagos), [$this->funciones->normaliza($cliente->cli_cor_electronico), "oficina.tcm@gmail.com", "rundopac@gmail.com","maturin.manuel@gmail.com"], "RUNDO: pedido de productos");

                if($send !== true)
                  $errores[] = $send;
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Registro de Salida de mercancía: " . $this->funciones->str_pad($salida->getId()) . "");
                $parametros["text"] = "La salida de mercancía con código " . $this->funciones->str_pad($salida->getId()) . " se registró correctamente. ¿Desea imprimir el soporte en PDF?";
                $parametros["salida"] = $salida->getId();
            }

            echo json_encode($parametros);
        }
    }

    // Soporte de salida en formato pdf
    public function salida_mercanciaAction($salida) {
        $this->view->disable();

        // Datos de la salida de mercancía
        $query = "SELECT
            s.id,
            s.sal_observaciones,
            s.sal_fec_creacion,
            s.dol_id,
            c.cli_codigo,
            c.cli_nombre,
            c.cli_tel_local,
            c.cli_tel_celular,
            c.cli_cor_electronico,
            c.cli_direccion,
            u.usu_nombre
            FROM 
            Salidas AS s,
            Clientes AS c,
            Usuarios AS u
            WHERE 
            s.cli_id = c.id
            AND
            s.usu_id = u.id
            AND
            s.id = '" . $salida . "'";
        
        $salMercancia = new Query($query, $this->getDI());
        $salMercancia = $salMercancia->execute();

        // ******************************** //

        // Lista de artículos de la salida
        $query = "SELECT
            m.mov_cantidad,
            m.mov_pre_venta,
            m.mov_importe,
            m.mov_costo,
            a.art_codigo,
            a.art_pre_unitario2,
            a.art_pre_unitario3
            FROM 
            Movimientos AS m,
            Articulos AS a,
            Salidas AS s
            WHERE 
            m.art_id = a.id
            AND
            m.sal_id = s.id
            AND
            s.id = '" . $salida . "'";
        
        $lisSalidas = new Query($query, $this->getDI());
        $lisSalidas = $lisSalidas->execute();

        // ******************************** //

        // Precio del dolar
        $dolar = "SELECT CASE 
            WHEN d.dol_fec_creacion < to_date('2018-08-23', 'yyyy-mm-dd') THEN d.dol_monto / 100000
            ELSE d.dol_monto 
            END
            FROM
            tel_dolares AS d
            WHERE
            d.id = '". $salMercancia[0]->dol_id ."'";

        $di = \Phalcon\DI::getDefault();
        $db = $di["db"];
        $dolar = $db->query($dolar);
        $dolar->setFetchMode(\Phalcon\Db::FETCH_OBJ);
        $dolar = $dolar->fetchAll();

        // ******************************** //

        $html = $this->view->getRender("salidas", "salida_mercancia", array(
            "salMercancia" => $salMercancia,
            "lisSalidas" => $lisSalidas,
            "dolar" => $dolar[0]->dol_monto
        )); 

        $this->mpdf->SetTopMargin(8);
        $this->mpdf->WriteHTML($html);
        $this->mpdf->Output();
        
        exit;
    }

    // Nota de entrega en formato pdf
    public function nota_entregaAction() {
        $this->view->disable();

        // Datos de la salida de mercancía
        $query = "SELECT
            s.id,
            s.sal_observaciones,
            s.sal_fec_creacion,
            c.cli_codigo,
            c.cli_nombre,
            c.cli_tel_local,
            c.cli_tel_celular,
            c.cli_cor_electronico,
            c.cli_direccion,
            u.usu_nombre
            FROM 
            Salidas AS s,
            Clientes AS c,
            Usuarios AS u
            WHERE 
            s.cli_id = c.id
            AND
            s.usu_id = u.id
            AND
            s.id = '" . $this->request->get("salida-id") . "'";
        
        $salMercancia = new Query($query, $this->getDI());
        $salMercancia = $salMercancia->execute();

        // ******************************** //

        // Datos de Salidas por Mes

        $salFecha = $salMercancia[0]->sal_fec_creacion;
        $fecha = $this->funciones->explode($salFecha,'-');

        $fechas = new DateTime($salFecha);
        $fechas->modify('last day of this month');
        $lday = $fechas->format('Y-m-d');

        //echo "El último día del mes es: {$last_day}";

        $query = "SELECT
            s.sal_fec_creacion
            FROM 
            Salidas AS s
            WHERE 
            s.sal_fec_creacion
            BETWEEN 
            '$fecha[0]-$fecha[1]-01' 
            AND 
            '$lday'";

        $salMes = new Query($query, $this->getDI());
        $salMes = $salMes->execute();

        $totalSalMes = count($salMes);

        // ******************************** //

        // Lista de artículos de la salida
        $query = "SELECT
            m.mov_cantidad,
            a.art_codigo,
            a.art_descripcion
            FROM 
            Movimientos AS m,
            Articulos AS a,
            Salidas AS s
            WHERE 
            m.art_id = a.id
            AND
            m.sal_id = s.id
            AND
            s.id = '" . $this->request->get("salida-id") . "'";
        
        $lisSalidas = new Query($query, $this->getDI());
        $lisSalidas = $lisSalidas->execute();

        // ******************************** //

        // Se obtienen los datos de envío y recibo del pedido
        // $clave = "lHk954di_-\\";
        // $cadDescodificada = $this->funciones->dencrypt($envio, $clave);
        //var_dump($envio);
        //$explode = explode("??", $envio);

        $html = $this->view->getRender("salidas", "nota_entrega", array(
            "salMercancia" => $salMercancia,
            "lisSalidas" => $lisSalidas,
            "totalSalMes" => $totalSalMes,
            "nombre1" => $this->request->get("nombre1"),
            "cedIdentidad1" => $this->request->get("cedIdentidad1"),
            "direccion1" => $this->request->get("direccion1"),
            "nombre2" => $this->request->get("nombre2"),
            "cedIdentidad2" => $this->request->get("cedIdentidad2"),
            "direccion2" => $this->request->get("direccion2")
        )); 

        $this->mpdf->SetTopMargin(8);
        $this->mpdf->WriteHTML($html);
        $this->mpdf->Output();
        
        exit;
    }

    // Vista de salidas registradas
    public function listaAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets->addCss("assets/plugins/datatables/dataTables.responsive.css")
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css")
            ->addCss("assets/plugins/datatables/dataTables.themify.css")
            ->addCss("assets/plugins/datatables/fixedHeader.bootstrap.min.css");

        $this->assets
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.responsive.js")
            ->addJs("assets/plugins/datatables/dataTables.fixedHeader.min.js")   
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("js/salidas/lista.js");
    }

    // Obtención de la lista de salidas
    public function getsalidasAction() {
        $this->view->disable();

        // Lista de salidas de mercancía
        $salidas = "SELECT
            s.id,
            s.sal_fec_creacion,
            c.cli_codigo,
            c.cli_nombre,
            u.usu_nombre,
            u.usu_codigo
            FROM 
            Salidas AS s,
            Clientes AS c,
            Usuarios AS u
            WHERE 
            s.cli_id = c.id
            AND
            s.usu_id = u.id
            AND
            s.sal_estatus = '". $this->request->get("estatus") ."'
            ORDER BY
            s.id 
            DESC";
        
        $salidas = (new Query($salidas, $this->getDI()))->execute();

        // ******************************** //

        $arrSalidas = array();

        foreach($salidas as $clave => $salida) {
            $usuario = $salida->usu_codigo . " (" . $salida->usu_nombre . ")";

            if($this->request->get("estatus") == "ANULADO") {
                $anulacion = Anulaciones::findFirstBySalId($salida->id);
                $usuario = Usuarios::findFirstById($anulacion->usu_id);
                $usuario = $usuario->usu_codigo . " (" . $usuario->usu_nombre . ")";
            }

            $arrSalidas["aaData"][$clave]["codSalida"] = $this->funciones->str_pad($salida->id);
            $arrSalidas["aaData"][$clave]["cliente"] = $salida->cli_codigo . " (" . $salida->cli_nombre . ")";
            $arrSalidas["aaData"][$clave]["fecha"] = $this->funciones->cambiaf_a_normal($salida->sal_fec_creacion);
            $arrSalidas["aaData"][$clave]["usuario"] = $usuario;
        }

        $arrSalidas["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 5);

        return json_encode($arrSalidas);
    }

    // Actualización de salidas en la base de datos
    public function actualizacionbdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $salida = Salidas::findFirstById($this->request->get("id"));
            $errores = array();

            if($salida) {
                // Datos de la salida de mercancía
                $salida->sal_observaciones = $this->funciones->strtoupper_utf8($this->request->getPost("observaciones"));
                $salida->cli_id = $this->request->getPost("cliente");

                // ******************************** //

                if(!$salida->update()) {
                    foreach($salida->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Actualización de Salida de mercancía: " . $this->funciones->str_pad($salida->id) . "");
                $parametros["text"] = "La salida de mercancía con código " . $this->funciones->str_pad($salida->id) . " se actualizó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Encriptación de datos del envío antes de enviarlos por la url
    public function envioAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $separador = "??";
            $cadModificar = $this->request->getPost("nombre1") . $separador . $this->request->getPost("cedIdentidad1") . $separador . $this->request->getPost("direccion1") . $separador . $this->request->getPost("nombre2") . $separador . $this->request->getPost("cedIdentidad2") . $separador . $this->request->getPost("direccion2");
            $clave = "lHk954di_-\\";
            $cadModificada = $this->funciones->encrypt($cadModificar, $clave);

            return $cadModificar;
        }
    }

    // Anulación de salida de mercancía
    public function anulacionbdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $salida = Salidas::findFirstById($this->request->getPost("id"));
            $errores = array();

            if($salida) {
                $salida->sal_estatus = "ANULADO";

                if(!$salida->update()) {
                    foreach($salida->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                } else {
                    // Se anulan también los movimientos de la salida
                    $movimientos = Movimientos::find("sal_id = '". $salida->id ."'");

                    if($movimientos) {
                        foreach($movimientos as $movimiento) {
                            $movimiento->mov_estatus = "ANULADO";

                            if(!$movimiento->update()) {
                                foreach($movimiento->getMessages() as $mensaje)
                                    $errores[] = $mensaje;
                            }
                        }
                    }

                    // ******************************** //

                    // Se anulan los pagos de la salida
                    $pagos = Pagos::find("sal_id = '". $salida->id ."'");

                    if($pagos) {
                        foreach($pagos as $pago) {
                            $pago->pag_estatus = "ANULADO";

                            if(!$pago->update()) {
                                foreach($pago->getMessages() as $mensaje)
                                    $errores[] = $mensaje;
                            }
                        }
                    }

                    // ******************************** //

                    // Motivo de anulación
                    $anulacion = new Anulaciones();

                    $anulacion->setAnuMotivo($this->funciones->strtoupper_utf8($this->request->getPost("motAnulacion")));
                    $anulacion->setSalId($salida->id);
                    $anulacion->setUsuId($this->funciones->getUsuario());

                    if(!$anulacion->save()) {
                        foreach($anulacion->getMessages() as $mensaje)
                            $errores[] = $mensaje;
                    }

                    // ******************************** //
                }
            } else
                $errores[] = "No existe esa salida.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Anulación de Salida de mercancía: " . $this->funciones->str_pad($salida->id) . "");
                
                $parametros["text"] = "La salida con código #" . $this->funciones->str_pad($salida->id) . " se anuló correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }

    }

    // Verificacion de pagos
    public function verpagoAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $pago = Pagos::findFirstById($this->request->getPost("id"));

            if($pago) {
                $pago->pag_verificado = $this->request->getPost("state");

                if(!$pago->update()) {
                    foreach($pago->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "No existe ese pago.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Verificacion de pagos de Salida de mercancía: " . $this->funciones->str_pad($salida->id) . "");
                $parametros["text"] = "El pago para la salida con código " . $this->funciones->str_pad($pago->sal_id) . " se " . ($pago->pag_verificado == "true" ? "verificó" : "dejó de verificar") . " correctamente.";
                $parametros["type"] = "success";
                $parametros["verificado"] = $pago->pag_verificado;
            }

            echo json_encode($parametros);
        }
    }

    // Vista de salidas anuladas
    public function anuladasAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets->addCss("assets/plugins/datatables/dataTables.responsive.css")
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css")
            ->addCss("assets/plugins/datatables/dataTables.themify.css")
            ->addCss("assets/plugins/datatables/fixedHeader.bootstrap.min.css");

        $this->assets
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.responsive.js")
            ->addJs("assets/plugins/datatables/dataTables.fixedHeader.min.js")   
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("js/salidas/anuladas.js");
    }

}