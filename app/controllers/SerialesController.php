<?php

use Phalcon\Mvc\View;

class SerialesController extends ControllerBase {

    // Almacenamiento del serial en la base de datos
    public function registrobdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();

            foreach(json_decode($this->request->getPost("seriales")) as $index => $numSerial) {
                // Datos del serial
                $serial = new Seriales();

                $serial->setSerNumero($numSerial);
                $serial->setArtId($this->request->get("id"));
                $serial->setUsuId($this->funciones->getUsuario());

                // ******************************** //

                if(!$serial->save()) {
                    foreach($serial->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "Los seriales se registraron correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Obtención de la lista de seriales
    public function getserialesAction() {
        $this->view->disable();

        $seriales = Seriales::find(array("art_id = '" . $this->request->get("serial") . "'", "order" => "id desc"));
        $arrSeriales = array();
        $index = 1;

        $counter = count($seriales);
        foreach($seriales as $clave => $serial) {
            $label = $serial->ser_estatus == "ACTIVO" ? "label-success" : "label-danger";
            $arrSeriales["aaData"][$clave]["codigo"] = $counter;
            $arrSeriales["aaData"][$clave]["serial"] = $serial->ser_numero;
            $arrSeriales["aaData"][$clave]["estatus"] = "<span class='label " . $label . "'>" . $serial->ser_estatus . "</span>";
            $arrSeriales["aaData"][$clave]["accion"] = "<span><a class='btn btn-inverse-alt btn-label mr-xs desSerial tooltips' id='serial" . $serial->id . "' onclick='desSerial($serial->id, true)' data-trigger='hover' data-original-title='Activar/Desactivar'><i class='ti ti-exchange-vertical btn-label-custom'></i></a>";
            $arrSeriales["aaData"][$clave]["accion"] .= "<a class='btn btn-inverse-alt btn-label mr-xs actSerial tooltips' onclick='actSerial($serial->id, \"$serial->ser_numero\", false)' data-trigger='hover' data-original-title='Actualizar'><i class='ti ti-pencil btn-label-custom'></i></a>";
            $arrSeriales["aaData"][$clave]["accion"] .= "<a class='btn btn-inverse-alt btn-label eliSerial tooltips' onclick='eliSerial($serial->id, true)' data-trigger='hover' data-original-title='Eliminar'><i class='ti ti-trash btn-label-custom'></i></a></span>";
            $index++;
            $counter--;
        }

        return json_encode($arrSeriales);
    }

    // Activación/Desactivación de seriales de artículos
    public function desserialAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $serial = Seriales::findFirstById($this->request->getPost("codigo"));

            if($serial) {
                if($serial->ser_estatus == "INACTIVO") {
                    $serial->ser_estatus = "ACTIVO";
                    $message = "activó";
                    $label = "label-success";
                } else {
                    $serial->ser_estatus = "INACTIVO";
                    $message = "desactivó";
                    $label = "label-danger";
                }

                if(!$serial->update()) {
                    foreach($serial->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "No existe ese serial.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "El serial " . $serial->ser_numero . " se " . $message . " correctamente.";
                $parametros["type"] = "success";
                $parametros["estatus"] = "<span class='label " . $label . "'>" . $serial->ser_estatus . "</span>";
            }

            echo json_encode($parametros);
        }
    }

    // Actualización de seriales en la base de datos
    public function actserialAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            if($this->request->getPost("codigo") != "false")
                $serial = Seriales::findFirstById($this->request->getPost("codigo"));
            
            $errores = array();

            if(isset($serial)) {
                $codigo = Seriales::findFirst("upper(ser_numero) = '" . $this->funciones->strtoupper_utf8($this->funciones->fun_eliminarDobleEspacios($this->request->getPost("numero"))) . "' and id != '" . $this->request->getPost("codigo") . "'");

                if(!$codigo) {
                    // Número de serial
                    $serial->ser_numero = $this->funciones->fun_eliminarDobleEspacios($this->request->getPost("numero"));

                    if(!$serial->update()) {
                        foreach($serial->getMessages() as $mensaje)
                            $errores[] = $mensaje;
                    }

                    // ******************************** //
                } else
                    $errores[] = "Ya existe un serial registrado con ese número.";
            } else {
                $codigo = Seriales::findFirst("upper(ser_numero) = '" . $this->funciones->strtoupper_utf8($this->funciones->fun_eliminarDobleEspacios($this->request->getPost("numero"))) . "'");

                if(!$codigo) {
                    $serial = new Seriales(); 

                    $serial->setSerNumero($this->funciones->fun_eliminarDobleEspacios($this->request->getPost("numero")));
                    $serial->setArtId($this->request->getPost("articulo"));
                    $serial->setUsuId($this->funciones->getUsuario());

                    if(!$serial->save()) {
                        foreach($serial->getMessages() as $mensaje)
                            $errores[] = $mensaje;
                    }
                } else
                    $errores[] = "Ya existe un serial registrado con ese número.";
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "El serial " . $serial->ser_numero . " se actualizó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Eliminación de seriales en la base de datos
    public function eliserialAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $serial = Seriales::findFirstById($this->request->getPost("codigo"));

            if($serial) {
                if(!$serial->delete()) {
                    foreach($serial->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "No existe ese serial.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "El serial " . $serial->ser_numero . " se eliminó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

}