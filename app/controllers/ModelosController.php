<?php

class ModelosController extends ControllerBase { 

    public function getmodelosAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $modelos = Modelos::find(array(
                "columns" => "id, mod_nombre",
                "mar_id = '" . $this->request->getPost("id") ."'",
                "order" => "mod_nombre"
            ));

            if(count($modelos) > 0) {
                echo "<option value=''>-- SELECCIONE --</option>";

                foreach($modelos as $modelo)
                    echo "<option value='". $modelo->id . "'>" . $modelo->mod_nombre ."</option>";
            }
        }
    }
    
}