<?php

use Phalcon\Mvc\Model\Query;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\Uniqueness;

class ClientesController extends ControllerBase { 

    // Vista de registro de clientes
    public function registroAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addJs("assets/plugins/form-inputmask/jquery.inputmask.bundle.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("js/clientes/registro.js");
    }

    // Almacenamiento del cliente en la base de datos
    public function registrobdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $validacion = new Validation();

            $validacion->add(
                "codCliente",
                new Regex(
                    [
                        "pattern" => "/^[vVeE][0-9]{6,8}|[jJgG][0-9]{8}[0-9]{1}$/",
                        "message" => "El campo R.I.F. o Cédula de Identidad no cumple con el formato.",
                        "cancelOnFail" => true
                    ]
                )
            );

            $validacion->add(
                "codCliente",
                new Uniqueness(
                    [
                        "model" => new Clientes(),
                        "attribute" => "cli_codigo",
                        "message" => "Ya existe un cliente registrado con ese R.I.F. o Cédula de Identidad.",
                        "cancelOnFail" => true
                    ]
                )
            );

            $validacion->add(
                "corElectronico",
                new Uniqueness(
                    [
                        "model" => new Clientes(),
                        "attribute" => "cli_cor_electronico",
                        "message" => "El Correo Electrónico ya se encuentra registrado.",
                        "cancelOnFail" => true
                    ]
                )
            );
            // Filtros de validación
            $validacion->setFilters("codCliente", "upper");
            $validacion->setFilters("corElectronico", "lower");

            $mensajes = $validacion->validate($_POST);

            if(count($mensajes)) {
                foreach($mensajes as $mensaje)
                    $errores[] = $mensaje . "<br/>";
            } else {
                // Datos del cliente
                $cliente = new Clientes();
                $nombre = $this->funciones->strtoupper_utf8($this->request->getPost("nombre"));

                $cliente->setCliCodigo($this->request->getPost("codCliente"));
                $cliente->setCliNombre($nombre);
                $cliente->setCliTelLocal($this->request->getPost("telLocal"));
                $cliente->setCliTelCelular($this->request->getPost("telCelular"));
                $cliente->setCliDireccion($this->funciones->strtoupper_utf8($this->request->getPost("direccion")));
                $cliente->setCliCorElectronico($this->funciones->normaliza($this->request->getPost("corElectronico")));
                $cliente->setUsuId($this->funciones->getUsuario());

                // ******************************** //

                if(!$cliente->save()) {
                    foreach($cliente->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                } else {
                    $contacto = new Contactos();

                    $contacto->con_nombre = $nombre;
                    $contacto->con_telefono1 = $this->request->getPost("telCelular");
                    $contacto->con_telefono2 = $this->request->getPost("telLocal");
                    $contacto->usu_id = $this->funciones->getUsuario();

                    if(!$contacto->save()) {
                        foreach($contacto->getMessages() as $mensaje)
                            $errores[] = $mensaje;
                    }                        
                }
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {

                //guardo accion para auditoria.
                $this->saveAction("Registro de Cliente: " . $cliente->getCliNombre() . ""); 
                $parametros["text"] = "El cliente " . $cliente->getCliNombre() . " se registró correctamente.";
                $parametros["type"] = "success";
                $parametros["id"] = $cliente->getId();
                $parametros["cliente"] = $cliente->getCliNombre() . " (" . $cliente->getCliCodigo() . ")";
                $parametros["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 3);
            }

            echo json_encode($parametros);
        }
    }

    // Vista de clientes registrados
    public function listaAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets->addCss("assets/plugins/datatables/dataTables.responsive.css")
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css")
            ->addCss("assets/plugins/datatables/dataTables.searchHighlight.css")
            ->addCss("assets/plugins/datatables/fixedHeader.bootstrap.min.css");

        $this->assets->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/jquery.highlight.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.responsive.js")
            ->addJs("assets/plugins/datatables/dataTables.fixedHeader.min.js")
            ->addJs("assets/plugins/datatables/dataTables.searchHighlight.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("js/clientes/lista.js");
    }

    // Obtención de la lista de clientes
    public function getclientesAction() {
        $this->view->disable();
        $clientes = Clientes::find("solo_promo = '0' order by id desc");
        $arrClientes = array();

        foreach($clientes as $clave => $cliente) {
            $label = $cliente->cli_estatus == "ACTIVO" ? "label-success" : "label-danger";
            $arrClientes["aaData"][$clave]["codigo"] = $this->funciones->str_pad($cliente->id);
            $arrClientes["aaData"][$clave]["rif"] = $cliente->cli_codigo;
            $arrClientes["aaData"][$clave]["nombre"] = $cliente->cli_nombre;
            $arrClientes["aaData"][$clave]["direccion"] = "<span class='cliDireccion'>" . $cliente->cli_direccion . "</span>";
            $arrClientes["aaData"][$clave]["telCelular"] = $cliente->cli_tel_celular;
            $arrClientes["aaData"][$clave]["corElectronico"] = $cliente->cli_cor_electronico;
            $arrClientes["aaData"][$clave]["estatus"] = "<span class='label " . $label . "'>" . $cliente->cli_estatus . "</span>";
        }

        $arrClientes["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 3);

        return json_encode($arrClientes);
    }

    // Vista de actualización de clientes
    public function actualizacionAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        if($this->request->isPost()) {
            $this->assets
                ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
                ->addJs("assets/plugins/form-inputmask/jquery.inputmask.bundle.js")
                ->addJs("assets/plugins/form-parsley/parsley.js")
                ->addJs("js/clientes/actualizacion.js");

            //guardo accion para auditoria.
            $this->saveAction("Detalle del Cliente: " . $this->request->getPost("codigo") . ""); 
            // Datos del cliente
            $cliente = Clientes::findFirstByCliCodigo($this->request->getPost("codigo"));
            $cliente ? $this->view->setVar("cliente", $cliente) : $this->response->redirect("clientes/lista");
        } else {
            $this->response->redirect("clientes/lista");
            return false;
        }
    }

    // Obtención del detalle del cliente
    public function getclienteAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $cliente = Clientes::findFirstById($this->request->getPost("cliente"));

            if(count($cliente) > 0) 
                return json_encode($cliente);
            else {
                $parametro["false"] = true;
                return json_encode($parametro);
            }
        }
    }

    // Actualización de clientes en la base de datos
    public function actualizacionbdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $cliente = Clientes::findFirstById($this->request->get("id"));
            $errores = array();

            if($cliente) {                
                $validacion = new Validation();

                $validacion->add(
                    "codCliente",
                    new Regex(
                        [
                            "pattern" => "/^[vVeE][0-9]{6,8}|[jJgG][0-9]{8}[0-9]{1}$/",
                            "message" => "El campo R.I.F. o Cédula de Identidad no cumple con el formato.",
                            "cancelOnFail" => true
                        ]
                    )
                );

                $mensajes = $validacion->validate($_POST);

                if(count($mensajes)) {
                    foreach($mensajes as $mensaje)
                        $errores[] = $mensaje . "<br/>";
                } else {
                    $codigo = Clientes::findFirst("cli_codigo = '" . strtoupper($this->request->getPost("codCliente")) . "' and id != '" . $this->request->get("id") . "'");

                    if(!$codigo) {
                        // Datos del cliente
                        $cliente->cli_codigo = strtoupper($this->request->getPost("codCliente"));
                        $cliente->cli_nombre = $this->funciones->strtoupper_utf8($this->request->getPost("nombre"));
                        $cliente->cli_tel_local = $this->request->getPost("telLocal");
                        $cliente->cli_tel_celular = $this->request->getPost("telCelular");
                        $cliente->cli_direccion = $this->funciones->strtoupper_utf8($this->request->getPost("direccion"));
                        $cliente->cli_cor_electronico = $this->funciones->normaliza($this->request->getPost("corElectronico"));

                        // ******************************** //

                        if(!$cliente->update()) {
                            foreach($cliente->getMessages() as $mensaje)
                                $errores[] = $mensaje;
                        }
                    } else
                        $errores[] = "Ya existe un cliente registrado con ese R.I.F. o Cédula de Identidad.";
                }
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Actualización del Cliente: " . $cliente->cli_nombre . ""); 
                $parametros["text"] = "El cliente " . $cliente->cli_nombre . " se actualizó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Desactivación de clientes
    public function desclienteAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $cliente = Clientes::findFirstByCliCodigo($this->request->getPost("codigo"));

            if($cliente) {
                $cliente->cli_estatus = "INACTIVO";

                if(!$cliente->update()) {
                    foreach($cliente->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "No existe ese cliente.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Desactivación del Cliente: " . $cliente->cli_nombre . ""); 
                $parametros["text"] = "El cliente " . $cliente->cli_nombre . " se desactivó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Activación de clientes
    public function activclienteAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $cliente = Clientes::findFirstByCliCodigo($this->request->getPost("codigo"));

            if($cliente) {
                $cliente->cli_estatus = "ACTIVO";

                if(!$cliente->update()) {
                    foreach($cliente->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "No existe ese cliente.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Activación del Cliente: " . $cliente->cli_nombre . ""); 
                $parametros["text"] = "El cliente " . $cliente->cli_nombre . " se activó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Obtención de la lista de compras
    public function getcomprasAction() {
        $this->view->disable();
        
        $salidas = Salidas::find(array("cli_id = '" . $this->request->getPost("cliente") . "' and sal_estatus='ACTIVO'", "order" => "id desc"));
        $arrSalidas = array();
        $totBolivares = 0;
        $totDolares = 0;

        foreach($salidas as $clave => $salida) {
            $pagos = "SELECT
                SUM(CASE p.pag_moneda 
                    WHEN 'BOLÍVARES' THEN (SELECT CASE 
                    WHEN p.pag_fecha < to_date('2018-08-23', 'yyyy-mm-dd') THEN p.pag_importe / 100000
                    ELSE p.pag_importe 
                    END) ELSE 0 END) AS pag_bolivares,
                SUM(CASE p.pag_moneda 
                    WHEN 'DÓLARES' THEN p.pag_importe
                    ELSE 0 END) AS pag_dolares
                FROM 
                tel_pagos AS p
                WHERE 
                p.sal_id = '" . $salida->id . "'";

            $di = \Phalcon\DI::getDefault();
            $db = $di['db'];
            $data = $db->query($pagos);
            $data->setFetchMode(\Phalcon\Db::FETCH_OBJ);
            $pagos = $data->fetchAll();

            $articulos = "SELECT
                a.art_descripcion,
                m.mov_cantidad
                FROM
                Articulos AS a,
                Movimientos AS m
                WHERE
                a.id = m.art_id
                AND
                m.sal_id = '" . $salida->id . "'";

            $articulos = (new Query($articulos, $this->getDI()))->execute()->toArray();

            $arrSalidas[$clave]["codigo"] = $this->funciones->str_pad($salida->id);
            $arrSalidas[$clave]["fecha"] = $this->funciones->cambiaf_a_normal($salida->sal_fec_creacion);
            $arrSalidas[$clave]["prePagado"] = "Bs. " . $this->funciones->number_format($pagos[0]->pag_bolivares) . " / $ " . $this->funciones->number_format($pagos[0]->pag_dolares);
            $arrSalidas[$clave]["articulos"] = "<center><a class='btn btn-inverse-alt detalle' data-salida-id='" . $salida->id . "' data-toggle='popover' title='Artículos de la Salida' data-content='<ul>" . implode("<br/>", array_map(function($entry) { return "<li>" . addslashes($entry["art_descripcion"]) . "<br/><b>Cantidad:</b> " . $entry["mov_cantidad"] . "<br/>"; } , $articulos)) . "</ul>' data-container='#modCompras'>Detalle</a></center>";
            $totBolivares = $totBolivares + $pagos[0]->pag_bolivares;
            $totDolares = $totDolares + $pagos[0]->pag_dolares;

            //echo var_dump(addslashes($articulos[0]["art_descripcion"])) . "<br/>";
        }

        $parametros["salidas"] = $arrSalidas;
        $parametros["totCompras"] = count($salidas);
        $parametros["cliente"] = Clientes::findFirstById($this->request->getPost("cliente"))->cli_nombre;
        $parametros["totBolivares"] = $this->funciones->number_format($totBolivares);
        $parametros["totDolares"] = $this->funciones->number_format($totDolares);

        return json_encode($parametros);
    }

}