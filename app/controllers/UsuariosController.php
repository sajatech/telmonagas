<?php

use Phalcon\Mvc\Model\Query;

class UsuariosController extends ControllerBase { 

    // Vista de registro de usuarios
    public function registroAction() {
        header("Cache-Control: post-chek=0");
    	$this->view->setTemplateAfter("main");
        $this->assets->addCss("assets/plugins/form-select2/select2.css");
        $this->getassets();

        $this->assets
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/form-select2/select2.min.js")
            ->addJs("assets/plugins/form-inputmask/jquery.inputmask.bundle.js")
            ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("js/usuarios/registro.js");

        // Lista de roles de usuario
        $roles = Roles::find(array("order" => "rol_nombre"));
        $this->view->setVar("roles", $roles);
    }

    // Registro de usuarios
    public function registrobdAction() {
    	$this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $codigo = Usuarios::findFirstByUsuCodigo($this->funciones->strtoupper_utf8($this->request->getPost("codUsuario")));
            $nomUsuario = Usuarios::findFirstByUsuNomUsuario($this->request->getPost("nomUsuario"));

            if(!$codigo && !$nomUsuario) {
            	// Datos del usuario
                $usuario = new Usuarios();

            	$usuario->setUsuCodigo($this->funciones->strtoupper_utf8($this->request->getPost("codUsuario")));
                $usuario->setUsuNombre($this->funciones->strtoupper_utf8($this->request->getPost("nombre")));
            	$usuario->setUsuCorElectronico($this->funciones->normaliza($this->request->getPost("corElectronico")));
            	$usuario->setUsuTelefono($this->request->getPost("telefono"));
                $usuario->setUsuFecNacimiento($this->funciones->cambiaf_a_sql($this->request->getPost("fecNacimiento")));
                $usuario->setUsuCargo($this->funciones->strtoupper_utf8($this->request->getPost("cargo")));
            	$usuario->setUsuNomUsuario($this->funciones->normaliza($this->request->getPost("nomUsuario")));
            	$usuario->setUsuContrasena(md5($this->request->getPost("contrasena")));
                $usuario->setRolId($this->request->getPost("tipUsuario"));

            	if(!$usuario->save()) {
                    foreach($usuario->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else {
                if($codigo)
                    $errores[] = "Ya existe un usuario registrado con esa Cédula de Identidad.";
                else if($nomUsuario)
                    $errores[] = "Ya existe un usuario registrado con ese Nombre de Usuario.";
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Registro de usuario " . $usuario->getUsuNombre() . "");
                $parametros["text"] = "El usuario " . $usuario->getUsuNombre() . " se registró correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Vista de usuarios registrados
    public function listaAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets->addCss("assets/plugins/datatables/dataTables.responsive.css")
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css")
            ->addCss("assets/plugins/datatables/dataTables.themify.css")
            ->addCss("assets/plugins/datatables/fixedHeader.bootstrap.min.css");

        $this->assets
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.responsive.js")
            ->addJs("assets/plugins/datatables/dataTables.fixedHeader.min.js")   
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("js/usuarios/lista.js");
    }

    // Obtención de la lista de usuarios
    public function getusuariosAction() {
        $this->view->disable();

        $usuarios = new Query("SELECT 
            u.id,
            u.usu_codigo,
            u.usu_nombre,
            u.usu_nom_usuario,
            u.usu_estatus,
            r.rol_nombre
            FROM
            Usuarios u,
            Roles r
            WHERE
            u.rol_id = r.id
            AND
            u.id != 10
            ORDER BY
            u.id 
            DESC", $this->getDI());

        $usuarios = $usuarios->execute();
        $arrUsuarios = array();

        foreach($usuarios as $clave => $usuario) {
            $label = $usuario->usu_estatus == "ACTIVO" ? "label-success" : "label-danger";
            $arrUsuarios["aaData"][$clave]["codigo"] = $this->funciones->str_pad($usuario->id);
            $arrUsuarios["aaData"][$clave]["cedIdentidad"] = $usuario->usu_codigo;
            $arrUsuarios["aaData"][$clave]["nombre"] = $usuario->usu_nombre;
            $arrUsuarios["aaData"][$clave]["nomUsuario"] = $usuario->usu_nom_usuario;
            $arrUsuarios["aaData"][$clave]["tipUsuario"] = $usuario->rol_nombre;
            $arrUsuarios["aaData"][$clave]["estatus"] = "<span class='label " . $label . "'>" . $usuario->usu_estatus . "</span>";
        }

        $arrUsuarios["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 6);

        return json_encode($arrUsuarios);
    }

    // Vista de actualización de usuarios
    public function actualizacionAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        if($this->request->isPost()) {
            $this->assets->addCss("assets/plugins/form-select2/select2.css");
            
            $this->assets
                ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
                ->addJs("assets/plugins/form-select2/select2.min.js")
                ->addJs("assets/plugins/form-inputmask/jquery.inputmask.bundle.js")
                ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
                ->addJs("assets/plugins/form-parsley/parsley.js")
                ->addJs("js/usuarios/actualizacion.js");

            // Datos del usuario
            $usuario = Usuarios::findFirstByUsuCodigo($this->request->getPost("codigo"));

            if($usuario) {
                $this->view->setVar("usuario", $usuario);
                
                // Lista de roles de usuario
                $roles = Roles::find(array("order" => "rol_nombre"));
                $this->view->setVar("roles", $roles);
            } else
                $this->response->redirect("usuarios/lista");
        } else {
            $this->response->redirect("usuarios/lista");
            return false;
        }
    }

    // Actualización de usuarios en la base de datos
    public function actualizacionbdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $usuario = Usuarios::findFirstById($this->request->get("id"));
            $errores = array();

            if($usuario) {
                $codigo = Usuarios::findFirst("usu_codigo = '" . strtoupper($this->request->getPost("codUsuario")) . "' and id != '" . $this->request->get("id") . "'");
                $nomUsuario = Usuarios::findFirst("usu_nom_usuario = '" . $this->request->getPost("nomUsuario") . "' and id != '" . $this->request->get("id") . "'");

                if(!$codigo && !$nomUsuario) {
                    // Datos del usuario
                    $usuario->usu_codigo = strtoupper($this->request->getPost("codUsuario"));
                    $usuario->usu_nombre = $this->funciones->strtoupper_utf8($this->request->getPost("nombre"));
                    $usuario->usu_cor_electronico = $this->funciones->normaliza($this->request->getPost("corElectronico"));
                    $usuario->usu_telefono = $this->request->getPost("telefono");
                    $usuario->usu_fec_nacimiento = $this->funciones->cambiaf_a_sql($this->request->getPost("fecNacimiento"));
                    $usuario->usu_cargo = $this->funciones->strtoupper_utf8($this->request->getPost("cargo"));
                    $usuario->usu_nom_usuario = $this->funciones->normaliza($this->request->getPost("nomUsuario"));
                    $usuario->rol_id = $this->request->getPost("tipUsuario");

                    if(!empty($this->request->getPost("contrasena")))
                        $usuario->usu_contrasena = md5($this->request->getPost("contrasena"));

                    // ******************************** //

                    if(!$usuario->update()) {
                        foreach($usuario->getMessages() as $mensaje)
                            $errores[] = $mensaje;
                    }
                } else {
                    if($codigo)
                        $errores[] = "Ya existe un usuario registrado con esa Cédula de Identidad.";
                    else if($nomUsuario)
                        $errores[] = "Ya existe un usuario registrado con ese Nombre de Usuario.";
                }
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Actualización de usuario " . $usuario->usu_nombre . "");
                $parametros["text"] = "El usuario " . $usuario->usu_nombre . " se actualizó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Desactivación de usuarios
    public function desusuarioAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $usuario = Usuarios::findFirstByUsuCodigo($this->request->getPost("codigo"));

            if($usuario) {
                $usuario->usu_estatus = "INACTIVO";

                if(!$usuario->update()) {
                    foreach($usuario->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "No existe ese usuario.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Desactivación de usuario " . $usuario->usu_nombre . "");
                $parametros["text"] = "El usuario " . $usuario->usu_nombre . " se desactivó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Activación de usuarios
    public function activusuarioAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $usuario = Usuarios::findFirstByUsuCodigo($this->request->getPost("codigo"));

            if($usuario) {
                $usuario->usu_estatus = "ACTIVO";

                if(!$usuario->update()) {
                    foreach($usuario->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "No existe ese usuario.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Activación de usuario " . $usuario->usu_nombre . "");
                $parametros["text"] = "El usuario " . $usuario->usu_nombre . " se activó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Obtención de datos de usuario determinado
    public function getusuarioAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $modelos = Modelos::find(array(
                "columns" => "id, mod_nombre",
                "mar_id = '" . $this->request->getPost("id") ."'",
                "order" => "mod_nombre"
            ));

            if(count($modelos) > 0) {
                echo "<option value=''>-- SELECCIONE --</option>";

                foreach($modelos as $modelo)
                    echo "<option value='". $modelo->id . "'>" . $modelo->mod_nombre ."</option>";
            }
        }
    }

}