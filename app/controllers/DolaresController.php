<?php

use Phalcon\Mvc\Model\Query;

class DolaresController extends ControllerBase {

    public function getdolarbyid($codigo) {

        header("Cache-Control: post-chek=0");

        $dolar = Dolares::findFirstById($codigo);

        return $dolar->dol_monto;
    }

    public function getdolar() {
        return $this->_getdolarbd();
    }

    private function _getdolarbd() {
        $dolar = Dolares::find(array("order" => "id"))->getLast();

        if($dolar)
            return $dolar->dol_monto;
    }

    public function getdolarid() {
        header("Cache-Control: post-chek=0");
        $dolar = Dolares::find(array("order" => "id"))->getLast();

        if($dolar)
            return $dolar->id;
    }

    public function urlgetdolarAction() {
        $this->view->disable();
        return $this->_getdolarbd();
    }

    // Obtención de la lista de dólares
    public function getdolaresAction() {
        $this->view->disable();

        $query = "SELECT
            d.id,
            d.dol_fec_creacion,
            d.dol_monto,
            u.usu_nombre,
            u.usu_codigo
            FROM 
            Dolares AS d,
            Usuarios AS u
            WHERE 
            d.usu_id = u.id";
        
        $dolares = new Query($query, $this->getDI());
        $dolares = $dolares->execute();
        $arrDolares = array();

        foreach($dolares as $clave => $dolar) {
            $arrDolares["aaData"][$clave]["codigo"] = $this->funciones->str_pad($dolar->id);
            $arrDolares["aaData"][$clave]["fecha"] = $this->funciones->cambiaf_a_normal($dolar->dol_fec_creacion);
            $arrDolares["aaData"][$clave]["monto"] = $this->funciones->number_format($dolar->dol_monto);
            $arrDolares["aaData"][$clave]["usuario"] = $dolar->usu_nombre . " (" . $dolar->usu_codigo . ")";
        }

        $arrDolares["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 7);

        return json_encode($arrDolares);
    }

}