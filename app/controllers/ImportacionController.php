<?php

use Phalcon\Mvc\Model\Query;

class ImportacionController extends ControllerBase {

    //funcion para vista de registrar facturas de importacion
    public function preciosAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addJs("assets/js/number.format.js")
            ->addJs("assets/js/jquery.price_format.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
            ->addJs("assets/plugins/form-select2/select2.min.js")
            ->addJs("assets/plugins/bootbox/bootbox.js")
            ->addJs("assets/plugins/form-jasnyupload/fileinput.min.js")
            ->addJs("assets/plugins/form-inputmask/jquery.inputmask.bundle.js")
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("assets/js/number.format.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("js/importacion/registro.js?V=5");


    //tasa de dolar al dia
    $dolares = new DolaresController();
    $dolar = $dolares->getdolar();
    $this->view->setVar("dolar",$dolar);

    if($this->request->isPost()) {
            // Datos de la salida de mercancía
        $factura = FacturasInt::findFirstById($this->request->getPost("factura"));

        if($factura) { 
            $this->view->setVar("factura", $factura);


            // Lista de items de la factura

            $lisItems = FacturasItems::findByFacId($factura->getId());

            $this->view->setVar("lisItems", $lisItems);


                // ******************************** //
        } else
            $this->response->redirect("importacion/precios");

    }

    }

    // Almacenamiento de la factura en la base de datos
    public function registrobdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();

            // Datos de la factura

            $factura = new FacturasInt();

            $factura->setFacMonto($this->funciones->cambiam_a_numeric($this->request->getPost("fac_monto")));
            $factura->setFacMontoFlete($this->funciones->cambiam_a_numeric($this->request->getPost("fac_monto_flete")));
            $factura->setFacNumero($this->request->getPost("fac_numero"));
            $factura->setFacPeso($this->funciones->cambiam_a_numeric($this->request->getPost("fac_peso")));
            $factura->setFacPesoReal($this->funciones->cambiam_a_numeric($this->request->getPost("fac_peso_porcentaje")));
            $factura->setFacBultos($this->request->getPost("fac_bultos"));

            $factura->setFacAduanaBs($this->funciones->cambiam_a_numeric($this->request->getPost("fac_aduana_bs")));
            $factura->setFacTransporteNac($this->funciones->cambiam_a_numeric($this->request->getPost("fac_transporte_nac")));
            $factura->setFacTasaDolar($this->funciones->cambiam_a_numeric($this->request->getPost("fac_tasa_dolar")));

            $factura->setFacFecha($this->funciones->cambiaf_a_sql($this->request->getPost("fac_fecha")));
            $factura->setFacObservacion(strtoupper($this->request->getPost("fac_observacion")));
            $factura->setFacGastosOpe($this->funciones->cambiam_a_numeric($this->request->getPost("fac_gastos_ope")));

            $factura->setFacImp1($this->funciones->cambiam_a_numeric($this->request->getPost("fac_imp1")));
            $factura->setFacImp2($this->funciones->cambiam_a_numeric($this->request->getPost("fac_imp2")));
            $factura->setFacImp3($this->funciones->cambiam_a_numeric($this->request->getPost("fac_imp3")));

            // ******************************** //

            if(!$factura->save()) {

                        foreach($factura->getMessages() as $mensaje)
                            $errores[] = $mensaje;

                    } else {
                      //var_dump($this->request->getPost("items"));
                    foreach(json_decode($this->request->getPost("items")) as $index => $objItem) {
                        // Datos de la lista de pagos
                        $item = new FacturasItems();

                        $item->setItemCodigo($objItem->item_codigo);
                        $item->setItemDescripcion(strtoupper($objItem->item_descripcion));
                        $item->setItemCantidad($objItem->item_cantidad);
                        $item->setItemPeso($objItem->item_peso);
                        $item->setItemCosto($objItem->item_costo);
                        $item->setItemGpg($objItem->costo_gpg);
                        $item->setItemPrecio($this->funciones->cambiam_a_numeric($objItem->precio_real));
                        $item->setFacId($factura->getId());
                         
                        if(!$item->save()) {
                            foreach($item->getMessages() as $mensaje)
                                $errores[] = $mensaje;
                        }
                    }
                }     
            }


            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Registro de Factura de Importación: ". $factura->getId() ."");
                $parametros["text"] = "La factura se registró correctamente. ¿Desea imprimir el soporte en PDF?";
                $parametros["factura"] = $factura->getId();
            }

            echo json_encode($parametros);
    }

    // Calculo de precios en formato pdf
    public function factura_pdfAction($factura) {
        $this->view->disable();

        // Datos de la factura
        $dat_factura = FacturasInt::findFirstById($factura);

        // Lista de items de la factura
        if($dat_factura) 
            $lisItems = FacturasItems::findByFacId($factura);

        // ******************************** //

        $html = $this->view->getRender("importacion", "factura_pdf", array(
            "factura" => $dat_factura,
            "lisItems" => $lisItems
        )); 

        $this->mpdf->SetTopMargin(8);
        $this->mpdf->WriteHTML($html);
        $this->mpdf->Output();
        
        exit;
    }

    // Vista de facturas registradas
    public function listaAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css")
            ->addCss("assets/plugins/datatables/dataTables.themify.css");

        $this->assets
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/bootbox/bootbox.js")
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("js/importacion/lista.js");
    }

    // Obtención de la lista de salidas
    public function getfacturasAction() {
        $this->view->disable();

        $facturas = FacturasInt::find(array("order" => "id desc"));

        // ******************************** //

        $arrFacturas = array();

        foreach($facturas as $clave => $factura) {
            $arrFacturas["aaData"][$clave]["ide"] = $factura->id;
            $arrFacturas["aaData"][$clave]["codigo"] = $factura->fac_numero;
            $arrFacturas["aaData"][$clave]["monto"] = $this->funciones->number_format($factura->fac_monto_flete);
            $arrFacturas["aaData"][$clave]["fecha"] = $this->funciones->cambiaf_a_normal($factura->fac_fecha);
            $arrFacturas["aaData"][$clave]["observacion"] = $factura->fac_observacion;
            $arrFacturas["aaData"][$clave]["peso"] = $this->funciones->number_format($factura->fac_peso);
        }

        $arrFacturas["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 5);

        return json_encode($arrFacturas);
    }

    public function eliminacionbdAction() {

        $this->view->disable();

        if($this->request->isPost()) {
            $factura = FacturasInt::findFirstById($this->request->get("id"));
            $errores = array();

            if ($factura !== false) {

                if ($factura->delete() === false) {

                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";

                } else {

                        //elimino los itema de la factura
                        $query = "DELETE
                        FROM FacturasItems
                        WHERE fac_id = ".$this->request->get('id')."";

                        $borrar = new Query($query, $this->getDI());
                        $borrar = $borrar->execute();

                        $parametros["text"] = "La factura se eliminó exitosamente.";
                        $parametros["type"] = "success";
                }


            }

            echo json_encode($parametros);
        }

    }

}