<?php

use Phalcon\Mvc\Controller,
    Phalcon\Http\Request as Request;;

class ControllerBase extends Controller {

    public function initialize() {
        //Total Clientes
        $totClientes = Clientes::find("solo_promo = '0'");
        $this->view->setVar("totClientes", count($totClientes));

        // Total ítems
        $totItems = Articulos::find();
        $this->view->setVar("totItems", count($totItems));

        // Total artículos
        $articulos = Articulos::find();
        $totArticulos = 0;
        
        foreach($articulos as $articulo) {
            $totArticulos = $totArticulos + $articulo->art_inv_inicial;
            $movimientos = Movimientos::find("art_id = '" . $articulo->id . "'");

            if(count($movimientos) > 0) {
                foreach($movimientos as $movimiento)
                    $totArticulos = $movimiento->mov_tipo == "CARGA" ? $totArticulos + $movimiento->mov_cantidad : $totArticulos - $movimiento->mov_cantidad;
            }
        }

        $this->view->setVar("totArticulos", $totArticulos);

        // ******************************** //

        // Privilegios de rol
        if(!empty($this->funciones->getRolUsuario()))          
            $this->view->setVar("priRol", $this->getprivilegios($this->funciones->getRolUsuario(), 1));

        // Lista de artículos
        $articulos = Articulos::find(array("art_estatus = 'ACTIVO'", "order" => "art_codigo, art_descripcion"));
        $this->view->setVar("articulos", $articulos);
    }

    // Obtención de los assets principales
    public function getassets() {
        return $this->assets
            ->addJs("assets/js/jquery-1.10.2.min.js")
            ->addJs("assets/js/bootstrap.min.js")
            ->addJs("assets/js/enquire.min.js") 
            ->addJs("assets/js/application.js")
            ->addJs("assets/plugins/velocityjs/velocity.min.js")
            ->addJs("assets/plugins/nanoScroller/js/jquery.nanoscroller.min.js")
            ->addJs("assets/plugins/smartmenus/jquery.smartmenus.js")
            ->addJs("assets/plugins/smartmenus/addons/bootstrap/jquery.smartmenus.bootstrap.js")
            ->addJs("assets/plugins/mousetrap/mousetrap.min.js");
    }

    // Lista de privilegios asignados al rol
    public function getprivilegios($rol, $seccion) {
        $privilegios = PrivilegiosRoles::find("rol_id = '" . $rol . "'");
        $arrPrivilegios = array();

        foreach($privilegios as $privilegio)
            $arrPrivilegios[] = (int) $privilegio->pri_id;

        return $arrPrivilegios;
    }

     // Guardo accion ejecutada por usuario para auditoria.
    public function saveAction($accion) {

        $request = new Request();
        $acceso = new Accesos();

        if (isset($_SERVER['HTTP_CLIENT_IP']) && $_SERVER['HTTP_CLIENT_IP'] != '')
            $Ip = $_SERVER['HTTP_CLIENT_IP'];
        elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != '')
            $Ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        elseif (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] != '')
            $Ip = $_SERVER['REMOTE_ADDR'];

        $acceso->setAccDetalles($request->getUserAgent());
        $acceso->setUsuId($this->funciones->getUsuario());
        $acceso->setAccIp($Ip);
        $acceso->setAccAccion($accion);

        if($this->funciones->getUsuario() > 1)
            $acceso->save();
        
    }

}