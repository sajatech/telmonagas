<?php

use Phalcon\Mvc\Model\Query;

class InventarioController extends ControllerBase { 

    // Vista de ajustes de inventario
    public function ajusteAction() {
        header("Cache-Control: post-chek=0");
    	$this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets->addCss("assets/plugins/form-select2/select2.css")
            ->addCss("assets/plugins/iCheck/skins/flat/blue.css");

        $this->assets
            ->addJs("assets/js/jquery.price_format.min.js")
            ->addJs("assets/js/number.format.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/form-select2/select2.min.js")
            ->addJs("assets/plugins/form-inputmask/jquery.inputmask.bundle.js")
            ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("assets/plugins/iCheck/icheck.min.js")
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("js/inventario/ajuste.js");

        // Lista de artículos
        $articulos = Articulos::find(array("art_estatus = 'ACTIVO'", "order" => "art_codigo, art_descripcion"));
        $this->view->setVar("articulos", $articulos);

        // Precio del dólar
        $dolares = new DolaresController();
        $dolar = $dolares->getdolar();
        $this->view->setVar("dolar", $dolar);

        if($this->request->isPost()) {
            // Datos del ajuste de inventario
            $ajuste = Ajustes::findFirstById($this->request->getPost("codigo"));

            // Motivo de ajuste
            $motAjuste = Movimientos::findFirstByAjuId($ajuste->id);
            $this->view->setVar("motAjuste", $motAjuste->mot_id);

            // Tipo de movimiento
            $this->view->setVar("tipMovimiento", $motAjuste->mov_tipo);

            if($ajuste) { 
                $this->view->setVar("ajuste", $ajuste);

                // Lista de artículos del ajuste
                $query = "SELECT
                    m.mov_cantidad,
                    a.id,
                    a.art_codigo,
                    a.art_descripcion,
                    a.art_cos_unitario,
                    a.art_pre_unitario2,
                    a.art_pre_unitario3,
                    a.art_inv_inicial,
                    a.art_fec_creacion,
                    a.art_sto_minimo,
                    a.art_ubicacion,
                    a.art_fot_referencial
                    FROM 
                    Movimientos AS m,
                    Articulos AS a,
                    Ajustes AS j
                    WHERE 
                    m.art_id = a.id
                    AND
                    m.aju_id = j.id
                    AND
                    j.id = '" . $ajuste->id . "'";
                
                $lisAjustes = new Query($query, $this->getDI());
                $lisAjustes = $lisAjustes->execute();
                $this->view->setVar("lisAjustes", $lisAjustes);

                // ******************************** //
            } else
                $this->response->redirect("inventario/ajustes");
        } else if($this->request->get("token")) {
            $this->response->redirect("inventario/ajustes");
            return false;
        }
    }

    // Almacenamiento del ajuste por inventario en la base de datos
    public function ajustebdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();

            // Datos del ajuste por inventario
            $ajuste = new Ajustes();

            $ajuste->setAjuObservaciones($this->funciones->strtoupper_utf8($this->request->getPost("observaciones")));
            $ajuste->setUsuId($this->funciones->getUsuario());

            // ******************************** //

            if(!$ajuste->save()) {
                foreach($ajuste->getMessages() as $mensaje)
                    $errores[] = $mensaje;
            } else {
                $i = 0; 
                foreach(json_decode($this->request->get("articulos")) as $articulo) {
                    // Datos de la lista de ajustes
                    $movimiento = new Movimientos();

                    $movimiento->setMovTipo($this->request->getPost("tipMovimiento"));
                    $movimiento->setMovCantidad($this->request->getPost("cantidad")[$i]);
                    $movimiento->setArtId($articulo);
                    $movimiento->setMotId($this->request->getPost("motAjuste"));
                    $movimiento->setAjuId($ajuste->getId());

                    // ******************************** //

                    if(!$movimiento->save()) {
                        foreach($movimiento->getMessages() as $mensaje)
                            $errores[] = $mensaje;
                    }

                    $i++; 
                }
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Registro de ajuste de Inventario: " . $this->funciones->str_pad($ajuste->getId()) . "");
                $parametros["text"] = "El ajuste de inventario con código " . $this->funciones->str_pad($ajuste->getId()) . " se registró correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Vista de ajustes registrados
    public function ajustesAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css")
            ->addCss("assets/plugins/datatables/dataTables.themify.css");

        $this->assets
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("js/inventario/ajustes.js");
    }

    public function eliminacionbdAction() {

        $this->view->disable();

        if($this->request->isPost()) {
            $ajuste = Ajustes::findFirstById($this->request->get("id"));
            $errores = array();

            if ($ajuste !== false) {

                if ($ajuste->delete() === false) {

                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";

                } else {

                //elimino los movimientos del ajuste
                $query = "DELETE
                        FROM Movimientos
                        WHERE aju_id = ".$this->request->get('id')."";

                        $borrar = new Query($query, $this->getDI());
                        $borrar = $borrar->execute();
                        //guardo accion para auditoria.
                        $this->saveAction("Eliminación de ajuste de Inventario: " . $this->funciones->str_pad($ajuste->getId()) . "");
                        $parametros["text"] = "El ajuste se eliminó exitosamente.";
                        $parametros["type"] = "success";
                }


            }

            echo json_encode($parametros);
        }

    }
}