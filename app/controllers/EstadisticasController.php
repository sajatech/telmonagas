<?php

use Phalcon\Mvc\Model\Query;

class EstadisticasController extends ControllerBase { 

    // Artículos con valor % de venta
    public function valventaAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets->addCss("assets/plugins/iCheck/skins/flat/blue.css")
            ->addCss("assets/plugins/pikaday/pikaday.css");

        $this->assets
            ->addJs("assets/plugins/amcharts4/core.js")
            ->addJs("assets/plugins/amcharts4/charts.js")
            ->addJs("assets/plugins/amcharts4/themes/kelly.js")
            ->addJs("assets/plugins/amcharts4/themes/animated.js")
            ->addJs("assets/plugins/iCheck/icheck.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/pikaday/moment.js")
            ->addJs("assets/plugins/pikaday/pikaday.js")
            ->addJs("js/estadisticas/valventa.js");
    }

    // Data para artículos con más valor % de venta
    public function getdata1Action() {
        $this->view->disable(); 
        $fecFin = date("Y-m-d");

        if(!empty($this->request->getPost("fecInicio"))) {
            $fecInicio = $this->request->getPost("fecInicio");
            $fecFin = !empty($this->request->getPost("fecFin")) ? $this->funciones->cambiaf_a_sql($this->request->getPost("fecFin")) : $fecFin;
        } else
            $fecInicio = "2018-01-01";

        $data = "SELECT
            a.id,
            a.art_codigo,
            a.art_descripcion,
            a.art_inv_inicial,
            get_cant_mov('CARGA', a.id, '2018-01-01', '" . date("Y-m-d") . "') as mov_carga,
            get_cant_mov('DESCARGA', a.id, '" . $fecInicio . "', '" . $fecFin . "') as total_ventas,";

        $data .= " get_porc_ven(a.id, '" . $fecInicio . "', '" . $fecFin . "', '" . $this->request->getPost("seleccione") . "', '" . date("Y-m-d") . "') as valor";

        $data .= " FROM
            Articulos AS a,
            Movimientos AS m
            WHERE
            m.art_id = a.id";

        if(!empty($this->request->getPost("fecInicio"))) {
            $fecFin = !empty($this->request->getPost("fecFin")) ? $this->funciones->cambiaf_a_sql($this->request->getPost("fecFin")) : $fecFin;

            $data .= " AND
                m.mov_fec_creacion
                BETWEEN 
                '". $this->funciones->cambiaf_a_sql($this->request->getPost("fecInicio")) ."' 
                AND 
                '". $fecFin ."'";
        }

        $data .= " GROUP BY
            a.id, 
            a.art_codigo, 
            a.art_descripcion";

        if($this->request->getPost("seleccione") == "true") {
            $data .= " ORDER BY
            valor DESC 
            LIMIT 10 OFFSET " . $this->request->getPost("offset") . "";
        } else {
            $data .= " ORDER BY
                valor
                LIMIT 10 OFFSET " . $this->request->getPost("offset") . "";
        }

        $data = (new Query($data, $this->getDI()))->execute();
        $data = $data->toArray();

        $data = array_map(function($v, $k) {
            if($v["valor"] == 100)
                return false;

            $arrData["valor"] = $v["valor"];
            $arrData["articulo"] = $v["art_codigo"];
            $arrData["compras"] = $v["mov_carga"] + $v["art_inv_inicial"];
            $arrData["ventas"] = $v["total_ventas"];
            $arrData["descripcion"] = $v["art_descripcion"];

            if($k == 0) {
                $config = new stdClass();
                $config->isActive = true;
                $arrData["config"] = $config;
            }

            return $arrData;
        }, $data, array_keys($data));

        return json_encode($data);
    }

    public function movstockAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets->addCss("assets/plugins/iCheck/skins/flat/blue.css")
            ->addCss("assets/plugins/pikaday/pikaday.css");

        $this->assets
            ->addJs("assets/plugins/amcharts4/core.js")
            ->addJs("assets/plugins/amcharts4/charts.js")
            ->addJs("assets/plugins/amcharts4/themes/kelly.js")
            ->addJs("assets/plugins/amcharts4/themes/animated.js")
            ->addJs("assets/plugins/iCheck/icheck.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/pikaday/moment.js")
            ->addJs("assets/plugins/pikaday/pikaday.js")
            ->addJs("js/estadisticas/movstock.js");
    }

    public function getdata2Action() {
        $this->view->disable();
        $fecInicio = "2018-01-01";
        $fecFin = date("Y-m-d");
        $datPart = "";

        if(!empty($this->request->getPost("fecInicio"))) {
            $fecInicio = $this->funciones->cambiaf_a_sql($this->request->getPost("fecInicio"));
            $fecFin = !empty($this->request->getPost("fecFin")) ? $this->funciones->cambiaf_a_sql($this->request->getPost("fecFin")) : $fecFin;

            $datPart = " AND
                m.mov_fec_creacion
                BETWEEN 
                '". $fecInicio ."' 
                AND 
                '". $fecFin ."'";
        }

        $data = "SELECT
            a.id,
            a.art_codigo,
            a.art_descripcion,
            a.art_inv_inicial,
            get_cant_mov('DESCARGA', a.id, '" . $fecInicio . "', '" . $fecFin . "') as total_ventas,
            get_cant_exi(a.id, '" . $fecInicio . "', '" . $fecFin . "', '" . date("Y-m-d") . "') as existencia
            FROM
            Articulos AS a,
            Movimientos AS m
            WHERE
            m.art_id = a.id 
            AND 
            m.mov_tipo = 'DESCARGA'";

        $data .= $datPart;

        $data .= "GROUP BY
            a.id, 
            a.art_codigo, 
            a.art_descripcion";

        if($this->request->getPost("seleccione") == "true") {
            $data .= " ORDER BY
            total_ventas DESC,
            existencia";
        } else {
            $data .= " ORDER BY
                total_ventas,
                existencia DESC";
        }

        $data .= " LIMIT 10 OFFSET " . $this->request->getPost("offset") . "";

        $data = (new Query($data, $this->getDI()))->execute();
        $data = $data->toArray();

        $data = array_map(function($v) {
            $arrData["valor"] = (int) $v["total_ventas"];
            $arrData["articulo"] = $v["art_codigo"];
            $arrData["descripcion"] = $v["art_descripcion"];
            $arrData["existencia"] = $v["existencia"];

            return $arrData;
        }, $data);

        return json_encode($data);
    }

}