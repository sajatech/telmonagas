<?php

use Phalcon\Mvc\Model\Query;

class EgresosController extends ControllerBase {

    // Almacenamiento del egreso en la base de datos
    public function registrobdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            // Datos del egreso
            $egreso = new Egresos();

            $egreso->setEgrImporte($this->funciones->cambiam_a_numeric($this->request->getPost("importe")));
            $egreso->setEgrMoneda($this->request->getPost("moneda"));
            $egreso->setEgrFecha($this->funciones->cambiaf_a_sql($this->request->getPost("fecEgreso")));
            $egreso->setEgrDescripcion($this->funciones->strtoupper_utf8($this->request->getPost("descripcion")));
            $egreso->setCueId($this->request->getPost("modCuenta"));
            $egreso->setUsuId($this->funciones->getUsuario());

            // ******************************** //

            if(!$egreso->save()) {
                foreach($egreso->getMessages() as $mensaje)
                    $errores[] = $mensaje;
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "El egreso de " . ($this->request->getPost("moneda") == "BOLÍVARES" ? "Bs. " : "$ ") . $this->request->getPost("importe") . " se registró correctamente.";
                $parametros["type"] = "success";
                $parametros["cuenta"] = $egreso->getCueId();
            }

            echo json_encode($parametros);
        }
    }

}