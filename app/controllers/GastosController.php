<?php

use Phalcon\Mvc\Model\Query;

class GastosController extends ControllerBase {

    // Obtención de la lista de categorías de gastos
    public function getcategoriasAction() {
        $this->view->disable();
        $categorias = CategoriasGastos::find(array("order" => "id desc"));
        $arrCategorias = array();

        foreach($categorias as $clave => $categoria) {
            $usuario = Usuarios::findFirstById($categoria->usu_id);
            $arrCategorias["aaData"][$clave]["codigo"] = $this->funciones->str_pad($categoria->id);
            $arrCategorias["aaData"][$clave]["fecha"] = $this->funciones->cambiaf_a_normal($categoria->cat_fec_creacion);
            $arrCategorias["aaData"][$clave]["nombre"] = $categoria->cat_nombre;
            $arrCategorias["aaData"][$clave]["usuario"] = $usuario->usu_nombre . " (" . $usuario->usu_codigo . ")";
            if ($categoria->cat_estatus == 'TOTALIZADO' )
                $cat = "SUMAR A GASTOS";
            else
                $cat = "SUMAR SEPARADAMENTE";
            $arrCategorias["aaData"][$clave]["estatus"] = "<span class='label " . ($categoria->cat_estatus == "TOTALIZADO" ? "label-success" : "label-danger") . "'>" . $cat . "</span>";
        }

        $arrCategorias["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 14);

        return json_encode($arrCategorias);
    }

    // Vista de registro de gastos
    public function registroAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();
        
        $this->assets->addCss("assets/plugins/form-select2/select2.css");
            //->addCss("assets/plugins/bootstrap-datepicker/datepicker.css");

        $this->assets->addJs("assets/js/jquery.price_format.min.js")
            ->addJs("assets/js/number.format.js")
            ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("assets/plugins/form-select2/select2.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("js/gastos/registro.js");

        // Lista de categorías
        $catGatos = CategoriasGastos::find(array("order" => "cat_nombre"));
        $this->view->setVar("catGatos", $catGatos);
    }

    // Almacenamiento del gasto en la base de datos
    public function registrobdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $fecGasto = $this->funciones->cambiaf_a_sql($this->request->getPost("fecGasto"));

            // Precio del dólar
            $dolar = Dolares::findFirst(array("dol_fec_creacion <= '" . $fecGasto . "'", "order" => "id desc"))->id;

            if(!$dolar)
                $dolar = 1;

            // ******************************** //

            // Datos del gasto
            $gasto = new Gastos();

            $gasto->setGasDescripcion($this->funciones->strtoupper_utf8($this->request->getPost("descripcion")));
            $gasto->setGasFecha($fecGasto);
            $gasto->setGasCantidad(1);
            $gasto->setGasCosto($this->funciones->cambiam_a_numeric($this->request->getPost("costo")));
            $gasto->setGasEstatus($this->request->getPost("estatus"));
            $gasto->setGasDivisas($this->request->getPost("divisa"));
            $gasto->setCatId($this->request->getPost("categoria"));
            $gasto->setUsuId($this->funciones->getUsuario());
            $gasto->setDolId($dolar);

            // ******************************** //

            if(!$gasto->save()) {
                foreach($gasto->getMessages() as $mensaje)
                    $errores[] = $mensaje;
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Registro de Gastos : " . CategoriasGastos::findFirstById($this->request->getPost("categoria"))->cat_nombre . "");
                $parametros["text"] = "El gasto para la categoría " . CategoriasGastos::findFirstById($this->request->getPost("categoria"))->cat_nombre . " se registró correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Vista de gastos registrados
    public function listaAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets->addCss("assets/plugins/datatables/dataTables.responsive.css")
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css")
            ->addCss("assets/plugins/datatables/dataTables.themify.css")
            ->addCss("assets/plugins/datatables/fixedHeader.bootstrap.min.css");

        $this->assets
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.responsive.js")
            ->addJs("assets/plugins/datatables/dataTables.fixedHeader.min.js")        
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("js/gastos/lista.js");
    }

    // Obtención de la lista de gastos
    public function getgastosAction() {
        $this->view->disable();
        $gastos = Gastos::find(array("gas_estatus = 'PAGADO'", "order" => "id desc"));
        $arrGastos = array();

        foreach($gastos as $clave => $gasto) {
            $importe = $gasto->gas_cantidad * $gasto->gas_costo;

                $tipCambio = "--";
                $importe = $this->funciones->number_format($importe);
    

            $arrGastos["aaData"][$clave]["codigo"] = $this->funciones->str_pad($gasto->id);
            $arrGastos["aaData"][$clave]["descripcion"] = $gasto->gas_descripcion;
            $arrGastos["aaData"][$clave]["fecha"] = $this->funciones->cambiaf_a_normal($gasto->gas_fecha);
            $arrGastos["aaData"][$clave]["importe"] = $importe;
            $arrGastos["aaData"][$clave]["tipCambio"] = $gasto->gas_divisas;
            $arrGastos["aaData"][$clave]["categoria"] = CategoriasGastos::findFirstById($gasto->cat_id)->cat_nombre;
            $arrGastos["aaData"][$clave]["estatus"] = "<span class='label " . ($gasto->gas_estatus == "PAGADO" ? "label-success" : "label-danger") . "'>" . $gasto->gas_estatus . "</span>";
        }

        $arrGastos["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 15);

        return json_encode($arrGastos);
    }

    // Vista de actualización de gastos
    public function actualizacionAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        if($this->request->isPost()) {
            $this->assets->addCss("assets/plugins/form-select2/select2.css");

            $this->assets
                ->addJs("assets/js/jquery.price_format.min.js")
                ->addJs("assets/js/number.format.js")
                ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
                ->addJs("assets/plugins/form-parsley/parsley.js")
                ->addJs("assets/plugins/form-select2/select2.min.js")
                ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
                ->addJs("js/gastos/actualizacion.js");

            // Datos del gasto
            $gasto = Gastos::findFirstById($this->request->getPost("codigo"));
            $gasto ? $this->view->setVar("gasto", $gasto) : $this->response->redirect("gastos/lista");

            // Valor del estatus del gasto
            $this->tag->setDefault("estatus", $gasto->gas_estatus);

            // Lista de categorías
            $catGatos = CategoriasGastos::find(array("order" => "cat_nombre"));
            $this->view->setVar("catGatos", $catGatos);
        } else {
            $this->response->redirect("gastos/lista");
            return false;
        }
    }

    // Actualización de gastos en la base de datos
    public function actualizacionbdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $gasto = Gastos::findFirstById($this->request->get("id"));
            $errores = array();

            if($gasto) {
                $fecGasto = $this->funciones->cambiaf_a_sql($this->request->getPost("fecGasto"));

                // Precio del dólar
                $dolar = Dolares::findFirst(array("dol_fec_creacion <= '" . $fecGasto . "'", "order" => "id desc"))->id;

                if(!$dolar)
                    $dolar = 1;

                // ******************************** //

                // Datos del gasto
                $gasto->gas_estatus = $this->request->getPost("estatus");
                $gasto->gas_descripcion = $this->funciones->strtoupper_utf8($this->request->getPost("descripcion"));
                $gasto->gas_fecha = $fecGasto;
                $gasto->gas_cantidad = $this->request->getPost("cantidad");
                $gasto->gas_costo = $this->funciones->cambiam_a_numeric($this->request->getPost("costo"));
                $gasto->gas_divisas = $this->funciones->cambiam_a_numeric($this->request->getPost("divisas"));
                $gasto->cat_id = $this->request->getPost("categoria");
                $gasto->dol_id = $dolar;

                // ******************************** //

                if(!$gasto->update()) {
                    foreach($gasto->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
        }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Actualización de Gasto : " . $this->funciones->str_pad($gasto->id) . "");
                $parametros["text"] = "El gasto con código #" . $this->funciones->str_pad($gasto->id) . " se actualizó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Cancelación de gastos
    public function cangastoAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $gasto = Gastos::findFirstById($this->request->getPost("codigo"));

            if($gasto) {
                $gasto->gas_estatus = "CANCELADO";

                if(!$gasto->update()) {
                    foreach($gasto->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "No existe ese gasto.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Cancelación de Gasto : " . $this->funciones->str_pad($gasto->id) . "");
                $parametros["text"] = "El gasto con código #" . $this->funciones->str_pad($gasto->id) . " se canceló correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

}