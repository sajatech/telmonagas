<?php

use Phalcon\Mvc\Model\Query,
Phalcon\Http\Request as Request;

class InicioController extends ControllerBase {

    public function indexAction() {
        $this->assets
        ->addJs("assets/js/jquery-1.10.2.min.js")
        ->addJs("assets/js/jqueryui-1.10.3.min.js")
        ->addJs("assets/js/bootstrap.min.js")
        ->addJs("assets/js/enquire.min.js")
        ->addJs("assets/js/application.js")
        ->addJs("assets/js/jquery.redirect.js")
        ->addJs("assets/plugins/velocityjs/velocity.min.js")
        ->addJs("assets/plugins/velocityjs/velocity.ui.min.js")
        ->addJs("assets/plugins/nanoScroller/js/jquery.nanoscroller.min.js")
        ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
        ->addJs("assets/plugins/form-parsley/parsley.js")
        ->addJs("js/index/index.js?V=2");
    }

    private function _registerSession($usuario) {
        $request = new Request();
        $acceso = new Accesos();

        if (isset($_SERVER['HTTP_CLIENT_IP']) && $_SERVER['HTTP_CLIENT_IP'] != '')
            $Ip = $_SERVER['HTTP_CLIENT_IP'];
        elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != '')
            $Ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        elseif (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] != '')
            $Ip = $_SERVER['REMOTE_ADDR'];

        $acceso->setAccDetalles($request->getUserAgent());
        $acceso->setUsuId($usuario[0]->id);
        $acceso->setAccIp($Ip);
        $acceso->setAccAccion('Ingreso a Sistema');
        if($acceso->save()) {

                        // Se crea la sesión para el usuario
            $this->session->set(strtolower($usuario[0]->rol_nombre), array(
                "id" => $usuario[0]->id,
                "nombre" => $usuario[0]->usu_nombre,
                "rol" => $usuario[0]->rol_id
                        //"nombre" => ucwords($funciones->strtolower_utf8($nombre[0]))
            ));
        }
    }

    public function loginAction() {
       $this->view->disable();

       if($this->request->isPost()) {
        $query = new Query("SELECT
            u.id,
            u.usu_nombre,
            u.rol_id,
            r.rol_nombre
            FROM
            Usuarios u,
            Roles r
            WHERE
            u.usu_nom_usuario = '" . $this->request->getPost("nombreUsuario"). "'
            AND
            u.usu_contrasena = '" . md5($this->request->getPost("contrasena")) . "'
            AND
            u.usu_estatus = 'ACTIVO'
            AND
            u.rol_id = r.id", $this->getDI());

        $usuario = $query->execute();
            //echo count($usuario);
        if(count($usuario) > 0) {
           $this->_registerSession($usuario);
           echo 0;
       } else
       echo "Nombre de Usuario o Contraseña incorrectos";
   }
}

public function cerrarAction() {

    //guardo accion para auditoria.
    $this->saveAction("Cierre de Sesion");
    // Se eliminan las sesiones abiertas
    $this->session->destroy();

    return $this->dispatcher->forward(array(
        "action" => "index"
    ));
}

}