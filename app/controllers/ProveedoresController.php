<?php

class ProveedoresController extends ControllerBase { 

    // Vista de registro de proveedores
    public function registroAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addJs("assets/plugins/form-inputmask/jquery.inputmask.bundle.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("js/proveedores/registro.js");
    }

    // Almacenamiento del proveedor en la base de datos
    public function registrobdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $codigo = Proveedores::findFirstByProCodigo($this->request->getPost("codProveedor"));

            if(!$codigo) {
                // Datos del proveedor
                $proveedor = new Proveedores();

                $proveedor->setProCodigo($this->request->getPost("codProveedor"));
                $proveedor->setProNombre($this->funciones->strtoupper_utf8($this->request->getPost("nombre")));
                $proveedor->setProPerContacto($this->funciones->strtoupper_utf8($this->request->getPost("perContacto")));
                $proveedor->setProTelLocal($this->request->getPost("telLocal"));
                $proveedor->setProTelCelular($this->request->getPost("telCelular"));
                $proveedor->setProDireccion($this->funciones->strtoupper_utf8($this->request->getPost("direccion")));
                $proveedor->setProCorElectronico($this->funciones->normaliza($this->request->getPost("corElectronico")));
                $proveedor->setUsuId($this->funciones->getUsuario());

                // ******************************** //

                if(!$proveedor->save()) {
                    foreach($proveedor->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "Ya existe un proveedor registrado con ese código.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Registro de proveedor: " . $proveedor->getProNombre() . "");
                $parametros["text"] = "El proveedor " . $proveedor->getProNombre() . " se registró correctamente.";
                $parametros["type"] = "success";
                $parametros["id"] = $proveedor->getId();
                $parametros["proveedor"] = $proveedor->getProNombre() . " (" . $proveedor->getProCodigo() . ")";
                $parametros["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 3);
            }

            echo json_encode($parametros);
        }
    }

    // Vista de proveedores registrados
    public function listaAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css")
            ->addCss("assets/plugins/datatables/dataTables.themify.css");

        $this->assets
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("js/proveedores/lista.js");
    }

    // Obtención de la lista de proveedores
    public function getproveedoresAction() {
        $this->view->disable();
        $proveedores = Proveedores::find(array("order" => "id desc"));
        $arrProveedores = array();

        foreach($proveedores as $clave => $proveedor) {
            $label = $proveedor->pro_estatus == "ACTIVO" ? "label-success" : "label-danger";
            $arrProveedores["aaData"][$clave]["codigo"] = $proveedor->pro_codigo;
            $arrProveedores["aaData"][$clave]["nombre"] = $proveedor->pro_nombre;
            $arrProveedores["aaData"][$clave]["perContacto"] = $proveedor->pro_per_contacto;
            $arrProveedores["aaData"][$clave]["telCelular"] = $proveedor->pro_tel_celular;
            $arrProveedores["aaData"][$clave]["corElectronico"] = $proveedor->pro_cor_electronico;
            $arrProveedores["aaData"][$clave]["estatus"] = "<span class='label " . $label . "'>" . $proveedor->pro_estatus . "</span>";
        }

        $arrProveedores["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 2);

        return json_encode($arrProveedores);
    }

    // Vista de actualización de proveedores
    public function actualizacionAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        if($this->request->isPost()) {
            $this->assets
                ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
                ->addJs("assets/plugins/form-parsley/parsley.js")
                ->addJs("js/proveedores/actualizacion.js");

            //guardo accion para auditoria.
            $this->saveAction("Detalle de proveedor codigo: " . $this->request->getPost("codigo") . ""); 
            // Datos del proveedor
            $proveedor = Proveedores::findFirstByProCodigo($this->request->getPost("codigo"));
            $proveedor ? $this->view->setVar("proveedor", $proveedor) : $this->response->redirect("proveedores/lista");
        } else {
            $this->response->redirect("proveedores/lista");
            return false;
        }
    }

    // Obtención del detalle del proveedor
    public function getproveedorAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $proveedor = Proveedores::findFirstById($this->request->getPost("proveedor"));

            if(count($proveedor) > 0) 
                return json_encode($proveedor);
            else {
                $parametro["false"] = true;
                return json_encode($parametro);
            }
        }
    }

    // Actualización de proveedores en la base de datos
    public function actualizacionbdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $proveedor = Proveedores::findFirstById($this->request->get("id"));
            $errores = array();

            if($proveedor) {
                $codigo = Proveedores::findFirst("pro_codigo = '" . $this->request->getPost("codProveedor") . "' and id != '" . $this->request->get("id") . "'");

                if(!$codigo) {
                    // Datos del proveedor
                    $proveedor->pro_codigo = $this->request->getPost("codProveedor");
                    $proveedor->pro_nombre = $this->funciones->strtoupper_utf8($this->request->getPost("nombre"));
                    $proveedor->pro_per_contacto = $this->funciones->strtoupper_utf8($this->request->getPost("perContacto"));
                    $proveedor->pro_tel_local = $this->request->getPost("telLocal");
                    $proveedor->pro_tel_celular = $this->request->getPost("telCelular");
                    $proveedor->pro_direccion = $this->funciones->strtoupper_utf8($this->request->getPost("direccion"));
                    $proveedor->pro_cor_electronico = $this->funciones->normaliza($this->request->getPost("corElectronico"));

                    // ******************************** //

                    if(!$proveedor->update()) {
                        foreach($proveedor->getMessages() as $mensaje)
                            $errores[] = $mensaje;
                    }
                } else
                    $errores[] = "Ya existe un proveedor registrado con ese código.";
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Actualización de proveedor: " . $proveedor->pro_nombre . "");
                $parametros["text"] = "El proveedor " . $proveedor->pro_nombre . " se actualizó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Desactivación de proveedores
    public function desproveedorAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $proveedor = Proveedores::findFirstByProCodigo($this->request->getPost("codigo"));

            if($proveedor) {
                $proveedor->pro_estatus = "INACTIVO";

                if(!$proveedor->update()) {
                    foreach($proveedor->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "No existe ese proveedor.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Desactivación de proveedor: " . $proveedor->pro_nombre . "");
                $parametros["text"] = "El proveedor " . $proveedor->pro_nombre . " se desactivó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Activación de proveedores
    public function activproveedorAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $proveedor = Proveedores::findFirstByProCodigo($this->request->getPost("codigo"));

            if($proveedor) {
                $proveedor->pro_estatus = "ACTIVO";

                if(!$proveedor->update()) {
                    foreach($proveedor->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "No existe ese proveedor.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Activación de proveedor: " . $proveedor->pro_nombre . "");
                $parametros["text"] = "El proveedor " . $proveedor->pro_nombre . " se activó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

}