<?php

use Phalcon\Mvc\View;

class DirectorioController extends ControllerBase { 

    // Vista de notas registradas
    public function notasAction() {
        $this->view->setTemplateAfter("main");
        $this->getassets();
        
        $this->assets
            ->addCss("assets/plugins/pikaday/pikaday.css")
            ->addCss("assets/plugins/summernote/summernote.css");

        $this->assets
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/pikaday/pikaday.js")
            ->addJs("assets/plugins/summernote/summernote.js")
            ->addJs("assets/plugins/summernote/lang/summernote-es-ES.js")
            ->addJs("js/directorio/notas.js");
    }

    // Almacenamiento de la nota en la base de datos
    public function regnotaAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $nota = Notas::findFirst("not_fecha = '". $this->request->getPost("fecha") ."' and usu_id = '". $this->funciones->getUsuario() ."'");

            // Datos de la nota
            if(!$nota) {
                $nota = new Notas();

                $nota->usu_id = $this->funciones->getUsuario();
                $nota->not_fecha = $this->request->getPost("fecha");
            }
            
            $nota->not_detalle = $this->request->getPost("summernote");

            // ******************************** //

            if(!$nota->save()) {
                foreach($nota->getMessages() as $mensaje)
                    $errores[] = $mensaje;
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Registro de Nota  de fecha: " . $this->funciones->cambiaf_a_normal($this->request->getPost("fecha")) . "");
                $parametros["text"] = "La nota para la fecha " . $this->funciones->cambiaf_a_normal($this->request->getPost("fecha")) . " se registró correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Obtención de la nota del día
    public function getnotaAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $nota = Notas::findFirst("not_fecha = '". $this->request->getPost("fecha") ."' and usu_id = '". $this->funciones->getUsuario() ."'");
            return $nota ? $nota->not_detalle : false;
        }
    }

    // Obtención de todas las notas del usuario
    public function getnotasAction() {
        $this->view->disable();

        $notas = array_map(function($v) {
            return date("D M d Y", strtotime($v["not_fecha"]));
        }, Notas::find("usu_id = '". $this->funciones->getUsuario() ."'")->toArray());

        return json_encode($notas);
    }

    // Eliminación de nota
    public function elinotaAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $nota = Notas::findFirst("not_fecha = '". $this->request->getPost("fecha") ."' and usu_id = '". $this->funciones->getUsuario() ."'");

            if(!$nota->delete()) {
                foreach($nota->getMessages() as $mensaje)
                    $errores[] = $mensaje;
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Eliminación de Nota  de fecha: " . $this->funciones->cambiaf_a_normal($this->request->getPost("fecha")) . "");
                $parametros["text"] = "La nota para la fecha " . $this->funciones->cambiaf_a_normal($this->request->getPost("fecha")) . " se eliminó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Vista de contactos registrados
    public function contactosAction() {
        $this->view->setTemplateAfter("main");
        $this->getassets();
        
        $this->assets
            ->addCss("assets/plugins/datatables/dataTables.responsive.css")
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css")
            ->addCss("assets/plugins/datatables/dataTables.searchHighlight.css")
            ->addCss("assets/plugins/datatables/fixedHeader.bootstrap.min.css")
            ->addJs("assets/plugins/form-parsley/parsley.js");

        $this->assets
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/jquery.highlight.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.responsive.js")
            ->addJs("assets/plugins/datatables/dataTables.fixedHeader.min.js")
            ->addJs("assets/plugins/datatables/dataTables.searchHighlight.min.js")
            ->addJs("assets/plugins/form-inputmask/jquery.inputmask.bundle.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("js/directorio/contactos.js");
    }

    // Obtención de la lista de contactos
    public function getcontactosAction() {
        $this->view->disable();
        $contactos = Contactos::find(array("order" => "con_nombre", "columns" => array("id", "trim(con_nombre) as con_nombre", "con_telefono1", "con_telefono2")));
        $arrContactos = array();

        foreach($contactos as $clave => $contacto) {
            $arrContactos["aaData"][$clave]["codigo"] = $contacto->id;
            $arrContactos["aaData"][$clave]["nombre"] = $contacto->con_nombre;
            $arrContactos["aaData"][$clave]["telefono1"] = $contacto->con_telefono1;
            $arrContactos["aaData"][$clave]["telefono2"] = $contacto->con_telefono2;
        }

        //$arrContactos["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 3);

        return json_encode($arrContactos);
    }

    // Almacenamiento del contacto en la base de datos
    public function regcontactoAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $add = false;

            if($this->request->getPost("accion") != "actualizar") {
                $contacto = new Contactos();
                $contacto->usu_id = $this->funciones->getUsuario();
                $add = true;
            } else
                $contacto = Contactos::findFirstById($this->request->getPost("codigo"));

            $contacto->con_nombre = $this->funciones->strtoupper_utf8($this->request->getPost("nombre"));
            $contacto->con_telefono1 = $this->request->getPost("telefono1");
            $contacto->con_telefono2 = $this->request->getPost("telefono2");

            if(!$contacto->save()) {
                foreach($contacto->getMessages() as $mensaje)
                    $errores[] = $mensaje;
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Registro de contacto: " . $contacto->getConNombre() . "");
                $parametros["text"] = "El contacto ". $contacto->getConNombre() ." se registró correctamente.";
                $parametros["type"] = "success";
                $parametros["codigo"] = $contacto->getId();
                $parametros["nombre"] = $contacto->getConNombre();
                $parametros["telefono1"] = $contacto->getConTelefono1();
                $parametros["telefono2"] = $contacto->getConTelefono2();
                $parametros["add"] = $add;
            }

            echo json_encode($parametros);
        }
    }

    // Contactos registrados por usuario
    public function masinformacionAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $usuarios = array_map(function($v) {
            $arrContactos["usuario"] = $v["usu_nombre"];
            $arrContactos["contactos"] = Contactos::count("usu_id = '". $v["id"] ."'");

            return $arrContactos;
        }, Usuarios::find(array("order" => "usu_nombre"))->toArray());

        $this->view->setVar("usuarios", $usuarios);
    }

    // Eliminación de contacto en la bd
    public function elicontactoAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $contacto = Contactos::findFirstById($this->request->getPost("codigo"));

            if($contacto) {
                if(!$contacto->delete()) {
                    foreach($contacto->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "No existe ese contacto en el directorio.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Eliminación de contacto: " . $contacto->con_nombre . "");
                $parametros["text"] = "El contacto " . $contacto->con_nombre . " se eliminó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

}