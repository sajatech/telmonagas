<?php

use Phalcon\Mvc\Model\Query;

class AjustesController extends ControllerBase {

    // Obtención de la lista de ajustes
    public function getajustesAction() {
        $this->view->disable();
        
        $query = "SELECT DISTINCT
            a.id,
            a.aju_fec_creacion,
            m.mov_tipo,
            t.mot_nombre,
            u.usu_nombre,
            u.usu_codigo
            FROM 
            Ajustes AS a,
            Movimientos AS m,
            Motivos AS t,
            Usuarios AS u
            WHERE 
            m.aju_id = a.id
            AND
            m.mot_id = t.id
            AND
            a.usu_id = u.id
            ORDER BY
            a.id 
            DESC";
        
        $ajustes = new Query($query, $this->getDI());
        $ajustes = $ajustes->execute();
        $arrAjustes = array();

        foreach($ajustes as $clave => $ajuste) {
            $arrAjustes["aaData"][$clave]["codigo"] = $this->funciones->str_pad($ajuste->id);
            $arrAjustes["aaData"][$clave]["motAjuste"] = $ajuste->mot_nombre;
            $arrAjustes["aaData"][$clave]["tipMovimiento"] = $ajuste->mov_tipo == "CARGA" ? "INGRESO O ENTRADA" : "SALIDA O DESPACHO";
            $arrAjustes["aaData"][$clave]["fecha"] = $this->funciones->cambiaf_a_normal($ajuste->aju_fec_creacion);
            $arrAjustes["aaData"][$clave]["usuario"] = $ajuste->usu_nombre . " (" . $ajuste->usu_codigo . ")";
        }

        $arrAjustes["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 11);

        return json_encode($arrAjustes);
    }

}