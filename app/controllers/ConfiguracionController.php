<?php

use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\View;
use Phalcon\Db\Adapter\Pdo\Postgresql;

class ConfiguracionController extends ControllerBase { 

    // ********************************  INICIO DE PRECIOS DEL DÓLAR  ******************************** //

    // Vista de precios del dólar registrados
    public function dolarAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css")
            ->addCss("assets/plugins/datatables/dataTables.themify.css");

        $this->assets
            ->addJs("assets/js/jquery.price_format.min.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("js/configuracion/dolar.js");
    }

    // Almacenamiento del precio en la base de datos
    public function regdolarAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $fecha = Dolares::findFirstByDolFecCreacion(date("Y-m-d"));

            if(!$fecha) {
                // Datos del precio del dólar
                $dolar = new Dolares();
                $dolar->setDolMonto($this->funciones->cambiam_a_numeric($this->request->getPost("monto")));
                $dolar->setUsuId($this->funciones->getUsuario());

                if(!$dolar->save()) {
                    foreach($dolar->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }

                // ******************************** //
            } else
                $errores[] = "Ya existe un precio del dólar registrado con la fecha actual.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Registro de Tasa de Dolar");
                $parametros["text"] = "El precio del dólar se registró correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Actualización de precios del dólar en la base de datos
    public function actdolarAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $dolar = Dolares::findFirstById($this->request->get("codigo"));
            $errores = array();

            if($dolar) {
                // Datos del precio del dólar
                $dolar->dol_monto = $this->funciones->cambiam_a_numeric($this->request->getPost("monto"));

                if(!$dolar->update()) {
                    foreach($dolar->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }

                // ******************************** //
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Actualización de Tasa de Dolar");
                $parametros["text"] = "El precio del dólar se actualizó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // ********************************  FIN DE PRECIOS DEL DÓLAR  ******************************** //


    // Vista de roles de usuario registrados
    public function rolesAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css")
            ->addCss("assets/plugins/datatables/dataTables.themify.css");

        $this->assets
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("js/configuracion/roles.js");
    }


    // ********************************  INICIO DE PORCENTAJES PARA CÁLCULOS  ******************************** //

    // Vista de registro de porcentajes para cálculos de precio
    public function calculosAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addJs("assets/js/jquery.price_format.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("js/configuracion/calculos.js?V=1.0");

        // Datos de los porcentajes
        $calculo = Calculos::findFirst();
        
        if($calculo)
            $this->view->setVar("calculo", $calculo);

        // ******************************** //
    }

    // Almacenamiento de los porcentajes en la base de datos
    public function regcalculoAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $calculo = Calculos::findFirst();

            if($calculo) {
                // Datos de los porcentajes
                $calculo->cal_pre_unitario1 = $this->funciones->cambiam_a_numeric($this->request->getPost("preCifVenezuela"));
                $calculo->cal_pre_unitario2 = $this->funciones->cambiam_a_numeric($this->request->getPost("preMayor"));
                $calculo->cal_pre_unitario3 = $this->funciones->cambiam_a_numeric($this->request->getPost("preDetal"));
                $calculo->cal_mer_libre = $this->funciones->cambiam_a_numeric($this->request->getPost("merLibre"));

                if(!$calculo->update()) {
                    foreach($calculo->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }

                // ******************************** //
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Actualización de % en calculo de precios");
                $parametros["text"] = "Los valores en porcentaje se actualizaron correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // ********************************  FIN DE PORCENTAJES PARA CÁLCULOS  ******************************** //


    // ********************************  INICIO DE CUENTAS BANCARIAS  ******************************** //

    // Vista de cuentas bancarias registradas
    public function cuentasAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css")
            ->addCss("assets/plugins/datatables/dataTables.themify.css")
            ->addCss("assets/plugins/form-select2/select2.css");

        $this->assets
            ->addJs("assets/js/jquery.price_format.min.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/form-select2/select2.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("js/configuracion/cuentas.js");

        // Lista de bancos
        $bancos = Bancos::find(array("ban_estatus = 'ACTIVO'", "order" => "ban_nombre"));
        count($bancos) > 0 ? $this->view->setVar("bancos", $bancos) : null;
    }

    // Almacenamiento de la cuenta en la base de datos
    public function regcuentaAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $cuenta = Cuentas::findFirstByCueNumero($this->request->getPost("numCuenta"));

            if(!$cuenta) {
                // Datos de la cuenta bancaria
                $cuenta = new Cuentas();
                $cuenta->setCueTipo($this->request->getPost("tipCuenta"));
                $cuenta->setCueNumero($this->request->getPost("numCuenta"));
                $cuenta->setBanId($this->request->getPost("banco"));
                $cuenta->setUsuId($this->funciones->getUsuario());

                if(!$cuenta->save()) {
                    foreach($cuenta->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }

                // ******************************** //
            } else
                $errores[] = "Ya existe una cuenta bancaria registrada con ese número.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Registro de Cuenta bancaria: ". $cuenta->getCueNumero() ."");
                $parametros["text"] = "La cuenta bancaria " . $cuenta->getCueNumero() .  " se registró correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Actualización de cuentas bancarias en la base de datos
    public function actcuentaAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $cuenta = Cuentas::findFirstById($this->request->get("codigo"));
            $errores = array();

            if($cuenta) {
                $numero = Cuentas::findFirst("cue_numero = '" . $this->request->getPost("numCuenta") . "' and id != '" . $this->request->get("codigo") . "'");

                if(!$numero) {
                    // Datos de la cuenta bancaria
                    $cuenta->cue_tipo = $this->request->getPost("tipCuenta");
                    $cuenta->cue_numero = $this->request->getPost("numCuenta");
                    $cuenta->ban_id = $this->request->getPost("banco");

                    if(!$cuenta->update()) {
                        foreach($cuenta->getMessages() as $mensaje)
                            $errores[] = $mensaje;
                    }

                    // ******************************** //
                } else
                    $errores[] = "Ya existe una cuenta bancaria registrada con ese número.";
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Actualización de Cuenta bancaria: ". $cuenta->cue_numero ."");
                $parametros["text"] = "La cuenta bancaria " . $cuenta->cue_numero . " se actualizó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // ********************************  FIN DE CUENTAS BANCARIAS  ******************************** //


    // ********************************  INICIO DE CATEGORÍAS DE GASTOS  ******************************** //

    // Vista de categorías de gastos registradas
    public function gastosAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css")
            ->addCss("assets/plugins/datatables/dataTables.themify.css")
            ->addCss("assets/plugins/form-select2/select2.css");

        $this->assets
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("js/configuracion/gastos.js?V=1");
    }

    // Almacenamiento de la categoría en la base de datos
    public function reggastoAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $catGastos = CategoriasGastos::findFirst("upper(cat_nombre) = '" . $this->funciones->strtoupper_utf8($this->funciones->fun_eliminarDobleEspacios($this->request->getPost("nombre"))) . "'");

            if(!$catGastos) {
                // Datos de la categoría de gastos
                $catGastos = new CategoriasGastos();
                $catGastos->setCatNombre($this->funciones->strtoupper_utf8($this->funciones->fun_eliminarDobleEspacios($this->request->getPost("nombre"))));
                $catGastos->setUsuId($this->funciones->getUsuario());

                if(!$catGastos->save()) {
                    foreach($catGastos->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }

                // ******************************** //
            } else
                $errores[] = "Ya existe un concepto de gastos registrado con ese nombre.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Registro de concepto de gastos: ". $catGastos->getCatNombre() ."");
                $parametros["text"] = "El concepto de gastos " . $catGastos->getCatNombre() .  " se registró correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Actualización de categorías de gastos en la base de datos
    public function actgastoAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $catGastos = CategoriasGastos::findFirstById($this->request->get("codigo"));
            $errores = array();

            if($catGastos) {
                $nombre = CategoriasGastos::findFirst("upper(cat_nombre) = '" . $this->funciones->strtoupper_utf8($this->funciones->fun_eliminarDobleEspacios($this->request->getPost("nombre"))) . "' and id != '" . $this->request->get("codigo") . "'");

                if(!$nombre) {
                    // Datos de la categoría
                    $catGastos->cat_nombre = $this->funciones->strtoupper_utf8($this->funciones->fun_eliminarDobleEspacios($this->request->getPost("nombre")));
                    $catGastos->usu_id = $this->funciones->getUsuario();

                    if(!$catGastos->update()) {
                        foreach($catGastos->getMessages() as $mensaje)
                            $errores[] = $mensaje;
                    }

                    // ******************************** //
                } else
                    $errores[] = "Ya existe un concepto de gastos registrado con ese número.";
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Activacion de concepto de gastos: ". $catGastos->cat_nombre ."");
                $parametros["text"] = "El concepto de gastos " . $catGastos->cat_nombre . " se actualizó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Descontar categoría de gasto
    public function desgastoAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $catGastos = CategoriasGastos::findFirstById($this->request->getPost("codigo"));

            if($catGastos) {
                $catGastos->cat_estatus = "DESCONTADO";

                if(!$catGastos->update()) {
                    foreach($catGastos->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "No existe ese concepto de gastos.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Desactivación de concepto de gastos: ". $catGastos->cat_nombre ."");
                $parametros["text"] = "El concepto de gastos " . $catGastos->cat_nombre . " se descontó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Totalizar categoría
    public function totgastoAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $catGastos = CategoriasGastos::findFirstById($this->request->getPost("codigo"));

            if($catGastos) {
                $catGastos->cat_estatus = "TOTALIZADO";

                if(!$catGastos->update()) {
                    foreach($catGastos->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "No existe ese concepto de gastos.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "El concepto de gastos " . $catGastos->cat_nombre . " se totalizó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // ********************************  FIN DE CATEGORÍAS DE GASTOS  ******************************** //

    // ********************************  ACCESOS AL SISTEMA  ******************************** //
    public function accesosAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets->addCss("assets/plugins/datatables/dataTables.responsive.css")
            ->addCss("assets/plugins/form-select2/select2.css")
            ->addCss("assets/plugins/iCheck/skins/flat/blue.css")
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css")
            ->addCss("assets/plugins/datatables/dataTables.themify.css")
            ->addCss("assets/plugins/datatables/fixedHeader.bootstrap.min.css");

        $this->assets
            ->addJs("assets/plugins/form-select2/select2.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
            ->addJs("assets/plugins/iCheck/icheck.min.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")  
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.responsive.js")
            ->addJs("assets/plugins/datatables/dataTables.fixedHeader.min.js")   
            ->addJs("js/configuracion/accesos.js?V=1.1");

        // Lista de usuarios
        $usuarios = Usuarios::find(array("id != 10", "order" => "usu_nombre"));
        $this->view->setVar("usuarios", $usuarios);
    }

        // Obtención de la lista de envios
    public function getaccesosAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        if($this->request->isPost()) {

        // Lista de accesos
            $query = "SELECT
            a.acc_fecha as fecha,
            a.acc_ip,
            a.usu_id,
            a.acc_detalles,
            u.usu_nombre,
            a.acc_accion,
            a.id
            FROM
            Accesos as a,
            Usuarios AS u
            WHERE a.usu_id = u.id";
        
        //completo el query si se recibe un rango de fechas
        if(!empty($this->request->getPost("desde")) && !empty($this->request->getPost("hasta"))) {
            $query .= " AND
            DATE(a.acc_fecha)
            BETWEEN 
            '". $this->funciones->cambiaf_a_sql($this->request->getPost("desde")) ."' 
            AND 
            '". $this->funciones->cambiaf_a_sql($this->request->getPost("hasta")) ."'";

        }

        //completo el query si recibo un usuario
        if(!empty($this->request->getPost("usuario"))) {
            $query .= " AND
            a.usu_id = '" . $this->request->getPost("usuario") . "'";
           
        }

        $query .= " ORDER BY
            a.id desc";

        $accesos = new Query($query, $this->getDI());
        $accesos = $accesos->execute();

        //$this->view->setVar("accesos", $accesos);
        //echo var_dump($accesos);
        count($accesos) > 0 ? $this->view->setVar("accesos", $accesos) : $this->view->disable();

        }
 
    }

}