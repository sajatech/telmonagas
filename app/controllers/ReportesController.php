<?php

use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\View;
use Phalcon\Db\Adapter\Pdo\Postgresql;

class ReportesController extends ControllerBase {

    // ********************************  INICIO DE REPORTE DE MOVIMIENTOS  ******************************** //

    // Vista de ingreso de parámetros para reporte de mov. de artículos
    public function movimientosAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets->addCss("assets/plugins/form-select2/select2.css")
            ->addCss("assets/plugins/iCheck/skins/flat/blue.css")
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css");

        $this->assets
            ->addJs("assets/plugins/form-select2/select2.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
            ->addJs("assets/plugins/iCheck/icheck.min.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")  
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("js/reportes/movimientos.js");

        // Lista de artículos
        $articulos = Articulos::find(array("art_estatus = 'ACTIVO'", "order" => "art_codigo, art_descripcion"));
        $this->view->setVar("articulos", $articulos);

        // Lista de usuarios
        $usuarios = Usuarios::find(array("id != 10", "order" => "usu_nombre"));
        $this->view->setVar("usuarios", $usuarios);

        //guardo accion para auditoria.
        $this->saveAction("Consulta de Reporte 6 Entradas / Salidas");
        
    }

    // Obtención de la lista de movimientos
    public function getmovimientosAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        if($this->request->isPost()) {
            $ingresos = Ingresos::find();
            $salidas = Salidas::find();
            $ajustes = Ajustes::find();

            $flaIngreso = false;
            $flaSalida = false;

            // Lista de ingresos o entradas
            $query = "SELECT DISTINCT
                t.mot_nombre,
                u.usu_nombre,
                u.usu_codigo,
                a.art_codigo,";

            if(count($ingresos) > 0)
                $query .= " m.ing_id,";

            if(count($salidas) > 0)
                $query .= " m.sal_id,";

            if(count($ajustes) > 0)
                $query .= " m.aju_id,";

            $query .= " m.id,
                m.mov_cantidad,
                m.mov_tipo,
                m.mov_fec_creacion,
                m.art_id,
                m.mov_pre_venta
                FROM";

            if(count($ingresos) > 0)
                $query .= " Ingresos AS i,";

            if(count($salidas) > 0)
                $query .= " Salidas AS s,";

            if(count($ajustes) > 0)
                $query .= " Ajustes AS j,";

            $query .= " Articulos AS a,
                Movimientos AS m,
                Usuarios AS u,
                Motivos AS t
                WHERE
                m.art_id = a.id
                AND
                m.mot_id = t.id
                AND
                m.mov_estatus = 'ACTIVO'
                AND
                a.usu_id = u.id
                AND (";

            if(count($ingresos) > 0) {
                $flaIngreso = true;
                $query .= "m.ing_id = i.id";
            }

            if(count($salidas) > 0) {
                $flaSalida = true;

                if($flaIngreso)
                    $query .= " OR";

                $query .= " m.sal_id = s.id";
            }

            if(count($ajustes) > 0) {
                if($flaIngreso || $flaSalida)
                    $query .= " OR";
                
                $query .= " m.aju_id = j.id";
            }

            $query .= ")";

            if(!empty($this->request->getPost("tipMovimiento"))) {
                $query .= " AND (";

                foreach($this->request->getPost("tipMovimiento") as $index => $tipMovimiento) {
                    $query .= " m.mov_tipo = '" . $this->request->getPost("tipMovimiento")[$index] . "'";

                    if(count($this->request->getPost("tipMovimiento")) - 1 > $index)
                        $query .= " OR ";
                }

                $query .= ")";
            }

            if(!empty($this->request->getPost("desde")) && !empty($this->request->getPost("hasta"))) {
                $query .= " AND
                m.mov_fec_creacion
                BETWEEN 
                '". $this->funciones->cambiaf_a_sql($this->request->getPost("desde")) ."' 
                AND 
                '". $this->funciones->cambiaf_a_sql($this->request->getPost("hasta")) ."'";
            }

            if(!empty($this->request->getPost("usuario"))) {
                $query .= " AND
                u.id = '" . $this->request->getPost("usuario") . "'";
            }

            if(!empty($this->request->getPost("articulo"))) {
                $query .= " AND
                a.id = '" . $this->request->getPost("articulo") . "'";
            }

            $query .= " ORDER BY
                m.mov_fec_creacion DESC";

            $movimientos = new Query($query, $this->getDI());
            $movimientos = $movimientos->execute();

            // ******************************** //            

            $articulo = "";
            foreach($movimientos as $movimiento) {
                $artActual = $movimiento->art_codigo;

                if($articulo != $artActual && $this->request->getPost("tipMovimiento")[0] != "DESCARGA") {
                    $invInicial = Articulos::findFirstById($movimiento->art_id);
                    $usuario = Usuarios::findFirstById($invInicial->usu_id);

                    $obj = (object) array(
                        "mov_fec_creacion" => $invInicial->art_fec_creacion,
                        "mov_tipo" => "CARGA",
                        "usu_nombre" => $usuario->usu_nombre,
                        "usu_codigo" => $usuario->usu_codigo,
                        "art_codigo" => $invInicial->art_codigo,
                        "mov_cantidad" => $invInicial->art_inv_inicial,
                        "mot_nombre" => "INVENTARIO INICIAL",
                        "mov_pre_venta" => "INVENTARIO INICIAL"
                    );

                    $arrMovimiento[] = $obj;
                }
                
                $arrMovimiento[] = $movimiento;
                $articulo = $movimiento->art_codigo;
            }

            count($movimientos) > 0 ? $this->view->setVar("movimientos", $arrMovimiento) : $this->view->disable();
        }
    }

    // Reporte de movimientos de artículos en formato pdf
    public function movimientos_articulosAction() {
        $this->view->disable();
        
        $ingresos = Ingresos::find();
        $salidas = Salidas::find();
        $ajustes = Ajustes::find();

        $flaIngreso = false;
        $flaSalida = false;

        // Lista de ingresos o entradas
        $query = "SELECT DISTINCT
            t.mot_nombre,
            u.usu_nombre,
            u.usu_codigo,
            a.art_codigo,";

        if(count($ingresos) > 0)
            $query .= " m.ing_id,";

        if(count($salidas) > 0)
            $query .= " m.sal_id,";

        if(count($ajustes) > 0)
            $query .= " m.aju_id,";

        $query .= " m.id,
            m.mov_cantidad,
            m.mov_tipo,
            m.mov_fec_creacion,
            m.art_id,
            m.mov_pre_venta
            FROM";

        if(count($ingresos) > 0)
            $query .= " Ingresos AS i,";

        if(count($salidas) > 0)
            $query .= " Salidas AS s,";

        if(count($ajustes) > 0)
            $query .= " Ajustes AS j,";

        $query .= " Articulos AS a,
            Movimientos AS m,
            Usuarios AS u,
            Motivos AS t
            WHERE
            m.art_id = a.id
            AND
            m.mot_id = t.id
            AND
            m.mov_estatus = 'ACTIVO'
            AND
            a.usu_id = u.id
            AND (";

        if(count($ingresos) > 0) {
            $flaIngreso = true;
            $query .= "m.ing_id = i.id";
        }

        if(count($salidas) > 0) {
            $flaSalida = true;

            if($flaIngreso)
                $query .= " OR";

            $query .= " m.sal_id = s.id";
        }

        if(count($ajustes) > 0) {
            if($flaIngreso || $flaSalida)
                $query .= " OR";
            
            $query .= " m.aju_id = j.id";
        }

        $query .= ")";

        if(!empty($this->request->getPost("tipMovimiento"))) {
            $query .= " AND (";

            foreach($this->request->getPost("tipMovimiento") as $index => $tipMovimiento) {
                $query .= " m.mov_tipo = '" . $this->request->getPost("tipMovimiento")[$index] . "'";

                if(count($this->request->getPost("tipMovimiento")) - 1 > $index)
                    $query .= " OR ";
            }

            $query .= ")";
        }

        if(!empty($this->request->getPost("desde")) && !empty($this->request->getPost("hasta"))) {
            $query .= " AND
            m.mov_fec_creacion
            BETWEEN 
            '". $this->funciones->cambiaf_a_sql($this->request->getPost("desde")) ."' 
            AND 
            '". $this->funciones->cambiaf_a_sql($this->request->getPost("hasta")) ."'";
        }

        if(!empty($this->request->getPost("usuario"))) {
            $query .= " AND
            u.id = '" . $this->request->getPost("usuario") . "'";
        }

        if(!empty($this->request->getPost("articulo"))) {
            $query .= " AND
            a.id = '" . $this->request->getPost("articulo") . "'";
        }

        $query .= " ORDER BY
            m.mov_fec_creacion DESC";

        $movimientos = new Query($query, $this->getDI());
        $movimientos = $movimientos->execute();

        // ******************************** //

        $articulo = "";
        foreach($movimientos as $movimiento) {
            $artActual = $movimiento->art_codigo;

            if($articulo != $artActual) {
                $invInicial = Articulos::findFirstById($movimiento->art_id);
                $usuario = Usuarios::findFirstById($invInicial->usu_id);

                $obj = (object) array(
                    "mov_fec_creacion" => $invInicial->art_fec_creacion,
                    "mov_tipo" => "CARGA",
                    "usu_nombre" => $usuario->usu_nombre,
                    "usu_codigo" => $usuario->usu_codigo,
                    "art_codigo" => $invInicial->art_codigo,
                    "mov_cantidad" => $invInicial->art_inv_inicial,
                    "mot_nombre" => "INVENTARIO INICIAL"
                );

                $arrMovimiento[] = $obj;
            }
            
            $arrMovimiento[] = $movimiento;
            $articulo = $movimiento->art_codigo;
        }

        foreach($arrMovimiento as $index => $mov)
            $tmp[$index] = $mov->mov_fec_creacion;

        array_multisort($tmp, SORT_ASC, $arrMovimiento);

        $html = $this->view->getRender("reportes", "movimientos_articulos", array(
            "movimientos" => (object) $arrMovimiento
        )); 

        //$this->mpdf->SetHTMLFooter("<table width='100%'><tr><td colspan='2'><hr/></td></tr><tr><td>{DATE d/m/Y} - Página {PAGENO} de {nb} | Telecomunicaciones Monagas | R.I.F. J-08020670-3. Copyright {DATE Y}.</td></tr></table>");
        $this->mpdf->SetTopMargin(8);
        $this->mpdf->WriteHTML($html);
        $this->mpdf->Output();
        
        exit;
    }

    // ********************************  FIN DE REPORTE DE MOVIMIENTOS  ******************************** //


    // ********************************  INICIO DE REPORTE DE ENVÍOS  ******************************** //

    // Vista de ingreso de parámetros para reporte de envios
    public function enviosAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets->addCss("assets/plugins/datatables/dataTables.responsive.css")
            ->addCss("assets/plugins/form-select2/select2.css")
            ->addCss("assets/plugins/iCheck/skins/flat/blue.css")
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css")
            ->addCss("assets/plugins/datatables/dataTables.themify.css")
            ->addCss("assets/plugins/datatables/fixedHeader.bootstrap.min.css");

        $this->assets
            ->addJs("assets/plugins/form-select2/select2.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
            ->addJs("assets/plugins/iCheck/icheck.min.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")  
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.responsive.js")
            ->addJs("assets/plugins/datatables/dataTables.fixedHeader.min.js")   
            ->addJs("js/reportes/envios.js");

        //guardo accion para auditoria.
        $this->saveAction("Consulta de Reporte de Envios");
        // Lista de usuarios
        $usuarios = Usuarios::find(array("id != 10", "order" => "usu_nombre"));
        $this->view->setVar("usuarios", $usuarios);
    }

    // Obtención de la lista de envios
    public function getenviosAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        if($this->request->isPost()) {

        // Lista de ingresos o entradas
        $query = "SELECT
            e.env_detalle,
            e.env_fecha,
            e.env_nombre,
            e.env_monto_salida,
            e.usu_id,
            e.env_correo_estatus,
            e.env_empresa,
            e.env_guia,
            u.usu_nombre
            FROM
            Envios AS e,
            Usuarios AS u
            WHERE e.usu_id = u.id";

        //total monto por ventas
        $queryT = "SELECT 
                SUM(env_monto_salida)
                as valor_suma
                FROM Envios as e
                WHERE e.usu_id != 0";

        //completo el query si se recibe un rango de fechas
        if(!empty($this->request->getPost("desde")) && !empty($this->request->getPost("hasta"))) {
            $query .= " AND
            e.env_fecha
            BETWEEN 
            '". $this->funciones->cambiaf_a_sql($this->request->getPost("desde")) ."' 
            AND 
            '". $this->funciones->cambiaf_a_sql($this->request->getPost("hasta")) ."'";

            $queryT .= " AND
            e.env_fecha
            BETWEEN 
            '". $this->funciones->cambiaf_a_sql($this->request->getPost("desde")) ."' 
            AND 
            '". $this->funciones->cambiaf_a_sql($this->request->getPost("hasta")) ."'";
        }

        //completo el query si recibo un usuario
        if(!empty($this->request->getPost("usuario"))) {
            $query .= " AND
            e.usu_id = '" . $this->request->getPost("usuario") . "'";

            $queryT .= " AND
            e.usu_id = '" . $this->request->getPost("usuario") . "'";
        }

        $query .= " ORDER BY
            e.id";

        $envios = new Query($query, $this->getDI());
        $envios = $envios->execute();

        $totalVentas = new Query($queryT, $this->getDI());
        $totalVentas = $totalVentas->execute();

        $this->view->setVar("totalVentas", $totalVentas[0]->valor_suma);

        //total de envios
        $totEnvios = count($envios);
        $this->view->setVar("totEnvios", $totEnvios);

        //TOP 5 CLIENTES CON MAS ENVIOS
        $queryTop = "SELECT e.env_nombre, COUNT( e.env_nombre ) AS total
            FROM  Envios AS e
            GROUP BY e.env_nombre
            ORDER BY total DESC 
            LIMIT 3";

        $topEnvios = new Query($queryTop, $this->getDI());
        $topEnvios = $topEnvios->execute();
        $this->view->setVar("topEnvios", $topEnvios);

        count($envios) > 0 ? $this->view->setVar("envios", $envios) : $this->view->disable();

        }
    }

        // Reporte de envios en formato pdf
    public function pdf_enviosAction() {
        $this->view->disable();


        // Lista de ingresos o entradas
        $query = "SELECT
            e.env_detalle,
            e.env_fecha,
            e.env_nombre,
            e.env_monto_salida,
            e.usu_id,
            e.env_correo_estatus,
            e.env_empresa,
            e.env_guia,
            u.usu_nombre
            FROM
            Envios AS e,
            Usuarios AS u
            WHERE e.usu_id = u.id";

        //total monto por ventas
        $queryT = "SELECT 
                SUM(env_monto_salida)
                as valor_suma
                FROM Envios as e
                WHERE e.usu_id != 0";

        //completo el query si se recibe un rango de fechas
        if(!empty($this->request->getPost("desde")) && !empty($this->request->getPost("hasta"))) {
            $query .= " AND
            e.env_fecha
            BETWEEN 
            '". $this->funciones->cambiaf_a_sql($this->request->getPost("desde")) ."' 
            AND 
            '". $this->funciones->cambiaf_a_sql($this->request->getPost("hasta")) ."'";

            $queryT .= " AND
            e.env_fecha
            BETWEEN 
            '". $this->funciones->cambiaf_a_sql($this->request->getPost("desde")) ."' 
            AND 
            '". $this->funciones->cambiaf_a_sql($this->request->getPost("hasta")) ."'";
        }

        //completo el query si recibo un usuario
        if(!empty($this->request->getPost("usuario"))) {
            $query .= " AND
            e.usu_id = '" . $this->request->getPost("usuario") . "'";

            $queryT .= " AND
            e.usu_id = '" . $this->request->getPost("usuario") . "'";
        }

        $query .= " ORDER BY
            e.id";

        $envios = new Query($query, $this->getDI());
        $envios = $envios->execute();

        $totalVentas = new Query($queryT, $this->getDI());
        $totalVentas = $totalVentas->execute();

        //total de envios
        $totEnvios = count($envios);

        //TOP 5 CLIENTES CON MAS ENVIOS
        $queryTop = "SELECT e.env_nombre, COUNT( e.env_nombre ) AS total
            FROM  Envios AS e
            GROUP BY e.env_nombre
            ORDER BY total DESC 
            LIMIT 3";

        $topEnvios = new Query($queryTop, $this->getDI());
        $topEnvios = $topEnvios->execute();
        $this->view->setVar("topEnvios", $topEnvios);

        $html = $this->view->getRender("reportes", "pdf_envios", array(
            "envios" => $envios,
            "totEnvios" => $totEnvios,
            "topEnvios" => $topEnvios,
            "totalVentas" => $totalVentas[0]->valor_suma
        )); 

        $this->mpdf->SetHTMLFooter("<table width='100%'><tr><td colspan='2'><hr/></td></tr><tr><td>{DATE d/m/Y} - Página {PAGENO} de {nb} | Telecomunicaciones Monagas | R.I.F. J-08020670-3. Copyright {DATE Y}.</td></tr></table>");
        $this->mpdf->SetTopMargin(8);
        $this->mpdf->WriteHTML($html);
        $this->mpdf->Output();
        
        exit;
    }

    // ********************************  FIN DE REPORTE DE ENVÍOS  ******************************** //


    // ********************************  INICIO DE REPORTE DE SALIDAS  ******************************** //
    
    // Vista de ingreso de parámetros para reporte de salidas
    public function salidasAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets->addCss("assets/plugins/datatables/dataTables.responsive.css")
            ->addCss("assets/plugins/form-select2/select2.css")
            ->addCss("assets/plugins/iCheck/skins/flat/blue.css")
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css")
            ->addCss("assets/plugins/datatables/dataTables.themify.css")
            ->addCss("assets/plugins/pikaday/pikaday.css")
            ->addCss("assets/plugins/datatables/fixedHeader.bootstrap.min.css");

        $this->assets
            ->addJs("assets/plugins/form-select2/select2.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            //->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
            ->addJs("assets/plugins/pikaday/moment.js")
            ->addJs("assets/plugins/pikaday/pikaday.js")
            ->addJs("assets/plugins/iCheck/icheck.min.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")  
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.responsive.js")
            ->addJs("assets/plugins/datatables/dataTables.fixedHeader.min.js")   
            ->addJs("js/reportes/salidas.js");

        //guardo accion para auditoria.
        $this->saveAction("Consulta de Reporte de Salidas");
        
        // Lista de usuarios
        $usuarios = Usuarios::find(array("id != 10", "order" => "usu_nombre"));
        $this->view->setVar("usuarios", $usuarios);

        // Lista de artículos
        $articulos = Articulos::find(array("art_estatus = 'ACTIVO'", "order" => "art_codigo, art_descripcion"));
        $this->view->setVar("articulos", $articulos);
    }

    // Obtención de la lista de salidas
    public function getsalidasAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        if($this->request->isPost()) {
            // Lista de salidas
            $salidas = "SELECT DISTINCT 
                s.id,
                s.sal_fec_creacion,
                s.usu_id,
                s.sal_garantia,
                s.sal_iva,
                c.cli_codigo,
                c.cli_nombre,
                u.usu_nombre,
                d.dol_monto,
                SUM(m.mov_importe * (SELECT CASE 
                    WHEN s.sal_fec_creacion < to_date('2018-08-23', 'yyyy-mm-dd') THEN d.dol_monto / 100000
                    ELSE d.dol_monto 
                    END) * m.mov_cantidad) AS mon_venta,
                SUM(m.mov_importe * m.mov_cantidad) + (s.sal_iva / (SELECT CASE 
                    WHEN s.sal_fec_creacion < to_date('2018-08-23', 'yyyy-mm-dd') THEN d.dol_monto / 100000
                    ELSE d.dol_monto 
                    END)) AS mon_ven_dolares,
                SUM(m.mov_costo * (SELECT CASE 
                    WHEN s.sal_fec_creacion < to_date('2018-08-23', 'yyyy-mm-dd') THEN d.dol_monto / 100000
                    ELSE d.dol_monto 
                    END) * m.mov_cantidad) AS mon_costo,
                SUM(m.mov_costo * m.mov_cantidad) AS mon_cos_dolares,
                SUM(CASE p.pag_moneda 
                    WHEN 'BOLÍVARES' THEN (SELECT CASE 
                    WHEN p.pag_fecha < to_date('2018-08-23', 'yyyy-mm-dd') THEN p.pag_importe / 100000
                    ELSE p.pag_importe 
                    END) ELSE 0 END) AS pag_bolivares,
                SUM(CASE p.pag_moneda 
                    WHEN 'BOLÍVARES' THEN (SELECT CASE 
                    WHEN p.pag_fecha < to_date('2018-08-23', 'yyyy-mm-dd') THEN p.pag_importe / 100000
                    ELSE p.pag_importe 
                    END) ELSE 0 END / (SELECT CASE 
                    WHEN d.dol_fec_creacion < to_date('2018-08-23', 'yyyy-mm-dd') THEN d.dol_monto / 100000
                    ELSE d.dol_monto 
                    END)) AS pag_bol_dolares,
                COUNT(CASE p.pag_moneda 
                    WHEN 'BOLÍVARES' THEN p.pag_importe
                    ELSE 0 END) AS pag_bol_count,
                SUM(CASE p.pag_moneda 
                    WHEN 'DÓLARES' THEN p.pag_importe
                    ELSE 0 END) AS pag_dolares,
                COUNT(CASE p.pag_moneda 
                    WHEN 'DÓLARES' THEN p.pag_importe
                    ELSE 0 END) AS pag_dol_count
                FROM 
                tel_salidas AS s,
                tel_clientes AS c,
                tel_usuarios AS u,
                tel_movimientos AS m,
                tel_dolares AS d,
                tel_pagos AS p
                WHERE 
                s.cli_id = c.id
                AND
                s.usu_id = u.id
                AND
                s.id = m.sal_id
                AND
                s.dol_id = d.id
                AND
                p.sal_id = s.id
                AND
                s.sal_estatus = 'ACTIVO'";

            // Total de gastos
            $gastos = "SELECT
                SUM((SELECT CASE 
                    WHEN g.gas_fecha < to_date('2018-08-23', 'yyyy-mm-dd') THEN g.gas_costo / 100000
                    ELSE g.gas_costo 
                    END) * g.gas_cantidad) AS tot_gastos,
                SUM((SELECT CASE 
                    WHEN g.gas_fecha < to_date('2018-08-23', 'yyyy-mm-dd') THEN g.gas_costo / 100000
                    ELSE g.gas_costo 
                    END) / (SELECT CASE 
                    WHEN d.dol_fec_creacion < to_date('2018-08-23', 'yyyy-mm-dd') THEN d.dol_monto / 100000
                    ELSE d.dol_monto END)) AS tot_gas_dolares
                FROM
                tel_gastos AS g,
                tel_categorias_gastos as c,
                tel_dolares as d
                WHERE
                g.cat_id = c.id
                AND
                c.cat_estatus = 'TOTALIZADO'
                AND
                g.gas_estatus = 'PAGADO' 
                AND 
                g.dol_id = d.id";
        
            // Se completa el query si se recibe un rango de fechas
            if(!empty($this->request->getPost("desde"))) {
                $hasta = !empty($this->request->getPost("hasta")) ? $this->funciones->cambiaf_a_sql($this->request->getPost("hasta")) : date("Y-m-d");

                $salidas .= " AND
                s.sal_fec_creacion
                BETWEEN 
                '". $this->funciones->cambiaf_a_sql($this->request->getPost("desde")) ."' 
                AND 
                '". $hasta ."'";

                $gastos .= " AND
                g.gas_fecha
                BETWEEN 
                '". $this->funciones->cambiaf_a_sql($this->request->getPost("desde")) ."' 
                AND 
                '". $hasta ."'";
            }

            // Se completa el query si recibe un usuario
            if(!empty($this->request->getPost("usuario"))) {
                $salidas .= " AND
                    s.usu_id = '" . $this->request->getPost("usuario") . "'";
            }

            // Se completa el query si recibe un artículo
            if(!empty($this->request->getPost("codigo"))) {
                $salidas .= " AND
                    m.art_id = '" . $this->request->getPost("codigo") . "'";
            }

            $salidas .= "GROUP BY
                s.id, 
                c.cli_codigo,
                c.cli_nombre,
                u.usu_nombre, 
                d.dol_monto
                ORDER BY
                s.id DESC";

            $di = \Phalcon\DI::getDefault();
            $db = $di['db'];
            $data = $db->query($salidas);
            $data->setFetchMode(\Phalcon\Db::FETCH_OBJ);
            $salidas = $data->fetchAll();

            if(count($salidas) > 0) {
                $this->view->setVar("salidas", $salidas);

                if(!empty($this->request->getPost("codigo")) && empty($this->request->getPost("desde"))) {
                    foreach($salidas as $salida) {
                        $fechas[] = new DateTime($salida->fecha);
                    }

                    $gastos .= " AND
                    g.gas_fecha
                    BETWEEN 
                    '". min($fechas)->format("Y-m-d") ."' 
                    AND 
                    '". max($fechas)->format("Y-m-d") ."'";

                }
            } else
                $this->view->disable();

            $di = \Phalcon\DI::getDefault();
            $db = $di['db'];
            $data = $db->query($gastos);
            $data->setFetchMode(\Phalcon\Db::FETCH_OBJ);
            $gastos = $data->fetchAll();

            isset($gastos[0]) ? $this->view->setVar("gastos", $gastos) : $this->view->setVar("gastos", 0);

            // Precio del dólar
            $dolares = new DolaresController();
            $dolar = $dolares->getdolar();
            $this->view->setVar("dolar",$dolar);

            $totalVentasAntes = 23566081561.38;

            if(empty($this->request->getPost("desde")) || $this->funciones->cambiaf_a_sql($this->request->getPost("desde")) <= "2018-07-27")
                $this->view->setVar("totalVentasAntes", $totalVentasAntes);

            // Total de salidas
            $this->view->setVar("totSalidas", count($salidas));
        }
    }

    // Método para obtener artículos en detalles
    function listArticulosAction(){
        $this->view->disable();

        if($this->request->isPost()) {
            // Datos de la salida de mercancía
            $salida = Salidas::findFirstById($this->request->get("id"));
            
            if($salida) { 
                // Lista de artículos de la salida
                $query = "SELECT
                    m.mov_cantidad,
                    a.art_codigo,
                    a.art_descripcion,
                    a.art_pre_unitario3,
                    d.dol_monto
                    FROM 
                    Movimientos AS m,
                    Articulos AS a,
                    Salidas AS s,
                    Dolares AS d
                    WHERE 
                    m.art_id = a.id
                    AND
                    m.sal_id = s.id
                    AND
                    s.dol_id = d.id
                    AND
                    m.mov_tipo = 'DESCARGA'
                    AND
                    m.mov_estatus = 'ACTIVO'
                    AND
                    s.id = '" . $salida->id . "'";
                
                $lisSalidas = new Query($query, $this->getDI());
                $lisSalidas = $lisSalidas->execute();
                //$this->view->setVar("lisSalidas", $lisSalidas);

                 return json_encode($lisSalidas);

                // ******************************** //

   
            }
        } 
    }

    // Reporte de  en formato pdf
    public function pdf_salidasAction() {
        $this->view->disable();

        // Lista de salidas
        $query = "SELECT DISTINCT s.id,
            m.mov_pre_venta,
            SUM (       
                CASE m.mov_pre_venta 
                WHEN 'PRECIO AL DETAL' THEN 
                a.art_pre_unitario3
                WHEN 'PRECIO AL MAYOR' THEN 
                a.art_pre_unitario2
                ELSE 
                0 END * d.dol_monto * m.mov_cantidad) 
            as valor_suma,
            SUM (
                CASE m.mov_pre_venta
                WHEN 'GARANTIA' THEN
                0
                ELSE
                a.art_cos_unitario END * d.dol_monto * m.mov_cantidad) 
            as suma_costo,
            s.sal_fec_creacion,
            c.cli_codigo,
            c.cli_nombre,
            u.usu_nombre,
            s.usu_id,
            s.sal_garantia,
            s.sal_iva
            FROM 
            Salidas AS s,
            Articulos AS a,
            Clientes AS c,
            Dolares AS d,
            Usuarios AS u,
            Movimientos AS m
            WHERE 
            s.cli_id = c.id
            AND
            s.usu_id = u.id
            AND
            s.id = m.sal_id
            AND
            m.art_id = a.id
            AND
            s.dol_id = d.id
            AND
            s.sal_estatus = 'ACTIVO' ";

        //total monto por ventas
        $queryT = "SELECT 
                m.mov_pre_venta,
                SUM (       
                    CASE m.mov_pre_venta 
                    WHEN 'PRECIO AL DETAL' THEN 
                    a.art_pre_unitario3
                    WHEN 'PRECIO AL MAYOR' THEN 
                    a.art_pre_unitario2
                    ELSE 
                    0 END * d.dol_monto * m.mov_cantidad) 
                as valor_suma,
                SUM (
                    CASE m.mov_pre_venta
                    WHEN 'GARANTIA' THEN
                    0
                    ELSE
                    a.art_cos_unitario END * d.dol_monto * m.mov_cantidad) 
                as suma_costo, 
                SUM (s.sal_iva) as total_iva
                FROM 
                Dolares AS d,
                Salidas AS s,
                Movimientos AS m,
                Articulos AS a
                WHERE
                s.id = m.sal_id
                AND
                m.art_id = a.id
                AND
                s.dol_id = d.id
                AND
                s.sal_estatus = 'ACTIVO'";

        //TOTAL DE GASTOS POR RANGO DE FECHAS
        $queryGastos = "SELECT
            Sum(gas_costo*gas_cantidad) AS totalG
            FROM
            Gastos AS g,
            CategoriasGastos as c
            WHERE
            g.cat_id = c.id
            AND
            c.cat_estatus = 'TOTALIZADO'
            AND
            g.gas_estatus = 'PAGADO'";

        //completo el query si se recibe un rango de fechas
        if(!empty($this->request->get("desde")) && !empty($this->request->get("hasta"))) {
            $query .= " AND
            s.sal_fec_creacion
            BETWEEN 
            '". $this->funciones->cambiaf_a_sql($this->request->get("desde")) ."' 
            AND 
            '". $this->funciones->cambiaf_a_sql($this->request->get("hasta")) ."'";

            $queryT .= " AND
            s.sal_fec_creacion
            BETWEEN 
            '". $this->funciones->cambiaf_a_sql($this->request->get("desde")) ."' 
            AND 
            '". $this->funciones->cambiaf_a_sql($this->request->get("hasta")) ."'";

            $queryGastos .= " AND
            g.gas_fecha
            BETWEEN 
            '". $this->funciones->cambiaf_a_sql($this->request->get("desde")) ."' 
            AND 
            '". $this->funciones->cambiaf_a_sql($this->request->get("hasta")) ."'";
        }

        //completo el query si recibo un usuario
        if(!empty($this->request->get("usuario"))) {
            $query .= " AND
            s.usu_id = '" . $this->request->get("usuario") . "'";

            $queryT .= " AND
            s.usu_id = '" . $this->request->get("usuario") . "'";
        }

        $queryT .= "GROUP BY m.mov_pre_venta";

        $query .= "GROUP BY
            s.id,c.cli_codigo,c.cli_nombre,u.usu_nombre, m.mov_pre_venta
            ORDER BY
            s.id DESC";

        $salidas = new Query($query, $this->getDI());
        $salidas = $salidas->execute();

        $totalVentas = new Query($queryT, $this->getDI());
        $totalVentas = $totalVentas->execute();

        $totalGastos = new Query($queryGastos, $this->getDI());
        $totalGastos = $totalGastos->execute();
        $dolares = new DolaresController();
        $dolar = $dolares->getdolar();

        if(isset($totalGastos[0]))
            $totalGastos=$totalGastos[0]->totalG;
        else
            $totalGastos=0;

        $this->view->setVar("dolar",$dolar);

        //total de envios
        $totSalidas = count($salidas);

/*      //TOP 5 CLIENTES CON MAS SALIDAS
        $queryTop = "SELECT s.cli_id, COUNT( s.cli_id ) AS total, c.cli_nombre
            FROM  Salidas AS s, Clientes AS c
            WHERE s.cli_id = c.id
            GROUP BY s.cli_id, c.cli_nombre
            ORDER BY total DESC 
            LIMIT 3";

        $topSalidas = new Query($queryTop, $this->getDI());
        $topSalidas = $topSalidas->execute(); */

        //total_ventas
        if(isset($totalVentas[0]))
            $totalv0 = $totalVentas[0]->valor_suma + $totalVentas[0]->total_iva;
        else
            $totalv0 = 0;

        if(isset($totalVentas[1]))
            $totalv1 = $totalVentas[1]->valor_suma + $totalVentas[1]->total_iva;
        else
            $totalv1 = 0;

        if(isset($totalVentas[2]))
            $totalv2 = $totalVentas[2]->valor_suma + $totalVentas[2]->total_iva;
        else
            $totalv2 = 0;

        $tventa = $totalv0 + $totalv1 + $totalv2;

        //total costo
        if(isset($totalVentas[0]))
            $totalc0 = $totalVentas[0]->suma_costo;
        else
            $totalc0 = 0;

        if(isset($totalVentas[1]))
            $totalc1 = $totalVentas[1]->suma_costo;
        else
            $totalc1 = 0;

        if(isset($totalVentas[2]))
            $totalc2 = $totalVentas[2]->suma_costo;
        else
            $totalc2 = 0;

        $tcosto = $totalc0 + $totalc1 + $totalc2;

/*        if(count($salGarantia)>0){
            //envio total de ventas en costo
            $totalCosto = $totalVentas[0]->suma_costo + $totalVentas[1]->suma_costo + $totalVentas[2]->suma_costo;

            $totalVentas = $totalVentas[0]->valor_suma + $totalVentas[1]->valor_suma + $totalVentas[2]->valor_suma;
            }
        else{
            //envio total de ventas en costo
            $totalCosto = $totalVentas[0]->suma_costo + $totalVentas[1]->suma_costo;
            $totalVentas = $totalVentas[0]->valor_suma + $totalVentas[1]->valor_suma;        
            }*/

        $html = $this->view->getRender("reportes", "pdf_salidas", array(
            "salidas" => $salidas,
            "totSalidas" => $totSalidas,
            "totalGastos" => $totalGastos,
            "totalVentas" => $tventa,
            "totalCosto" => $tcosto

        )); 

        $this->mpdf->SetHTMLFooter("<table width='100%'><tr><td colspan='2'><hr/></td></tr><tr><td>{DATE d/m/Y} - Página {PAGENO} de {nb} | Telecomunicaciones Monagas | R.I.F. J-08020670-3. Copyright {DATE Y}.</td></tr></table>");
        $this->mpdf->SetTopMargin(8);
        $this->mpdf->WriteHTML($html);
        $this->mpdf->Output();
        
        exit;

    }

    // ********************************  FIN DE REPORTE DE SALIDAS  ******************************** //


    // ********************************  INICIO DE REPORTE DE PAGOS  ******************************** //

    // Vista de reporte de pagos bancarios
    public function pagosAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets->addCss("assets/plugins/datatables/dataTables.responsive.css")
            ->addCss("assets/plugins/form-select2/select2.css")
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css")
            ->addCss("assets/plugins/datatables/dataTables.themify.css")
            ->addCss("assets/plugins/datatables/fixedHeader.bootstrap.min.css")
            ->addCss("assets/plugins/iCheck/skins/flat/blue.css");

        $this->assets
            ->addJs("assets/js/jquery.price_format.min.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("assets/plugins/form-select2/select2.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")  
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.responsive.js")
            ->addJs("assets/plugins/datatables/dataTables.fixedHeader.min.js")   
            ->addJs("js/reportes/pagos.js");

        // Lista de cuentas
        $query = "SELECT
            b.ban_nombre,
            c.id,
            c.cue_numero
            FROM 
            Bancos AS b,
            Cuentas AS c
            WHERE
            c.ban_id = b.id
            ORDER BY
            b.ban_nombre";

        $cuentas = (new Query($query, $this->getDI()))->execute();
        $this->view->setVar("cuentas", $cuentas);

        //guardo accion para auditoria.
        $this->saveAction("Consulta de Reporte de Pagos/Bancos");

        // ******************************** //    
    }

    // Obtención de la lista de pagos
    public function getpagosAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        if($this->request->isPost()) {
            // Lista de depósitos bancarios
            $query = "SELECT
                p.pag_fecha,
                p.pag_num_referencia,
                p.pag_importe,
                p.pag_moneda,
                b.ban_nombre,
                u.cue_numero,
                c.cli_nombre,
                c.cli_codigo
                FROM
                Pagos AS p,
                Bancos AS b,
                Clientes AS c,
                Salidas AS s,
                Cuentas AS u
                WHERE
                p.cue_id = u.id
                AND
                p.sal_id = s.id
                AND
                u.ban_id = b.id
                AND
                s.cli_id = c.id";

            if(!empty($this->request->getPost("desde")) && !empty($this->request->getPost("hasta"))) {
                $query .= " AND
                p.pag_fecha
                BETWEEN 
                '". $this->funciones->cambiaf_a_sql($this->request->getPost("desde")) ."' 
                AND 
                '". $this->funciones->cambiaf_a_sql($this->request->getPost("hasta")) ."'";
            }

            if(!empty($this->request->getPost("cuenta"))) {
                $query .= " AND
                p.cue_id = '" . $this->request->getPost("cuenta") . "'";
            }

            if(!empty($this->request->getPost("descripcion"))) {
                $query .= " AND
                UPPER(c.cli_nombre) LIKE UPPER('%" . $this->request->getPost("descripcion") . "%')";
            }

            $query .= " ORDER BY
                p.id DESC";

            $pagos = (new Query($query, $this->getDI()))->execute()->toArray();

            // ******************************** //

            // Lista de egresos
            $query = "SELECT
                e.egr_importe,
                e.egr_moneda,
                e.egr_fecha,
                e.egr_descripcion,
                c.cue_numero,
                b.ban_nombre
                FROM
                Egresos AS e,
                Cuentas AS c,
                Bancos As b
                WHERE
                e.cue_id = c.id
                AND
                c.ban_id = b.id";

            if(!empty($this->request->getPost("desde")) && !empty($this->request->getPost("hasta"))) {
                $query .= " AND
                e.egr_fecha
                BETWEEN 
                '". $this->funciones->cambiaf_a_sql($this->request->getPost("desde")) ."' 
                AND 
                '". $this->funciones->cambiaf_a_sql($this->request->getPost("hasta")) ."'";
            }

            if(!empty($this->request->getPost("cuenta"))) {
                $query .= " AND
                    e.cue_id = '" . $this->request->getPost("cuenta") . "'";
            }

            if(!empty($this->request->getPost("descripcion"))) {
                $query .= " AND
                UPPER(e.egr_descripcion) LIKE UPPER('%" . $this->request->getPost("descripcion") . "%')";
            }

            $query .= " ORDER BY 
                e.id DESC";

            $egresos = (new Query($query, $this->getDI()))->execute()->toArray();

            // ******************************** //

            $pagos = array_merge($pagos, $egresos);

            count($pagos) > 0 ? $this->view->setVar("pagos", $pagos) : $this->view->disable();
        }
    }

    // Reporte de pagos bancarios en formato pdf
    public function pagos_bancariosAction() {
        $this->view->disable();
        
        // Lista de depósitos bancarios
        $query = "SELECT
            p.pag_fecha AS fecha,
            p.pag_num_referencia,
            p.pag_importe,
            p.pag_moneda,
            b.ban_nombre,
            u.cue_numero,
            c.cli_nombre,
            c.cli_codigo
            FROM
            Pagos AS p,
            Bancos AS b,
            Clientes AS c,
            Salidas AS s,
            Cuentas AS u
            WHERE
            p.cue_id = u.id
            AND
            p.sal_id = s.id
            AND
            u.ban_id = b.id
            AND
            s.cli_id = c.id";

        if(!empty($this->request->get("desde")) && !empty($this->request->get("hasta"))) {
            $query .= " AND
                p.pag_fecha
                BETWEEN 
                '". $this->funciones->cambiaf_a_sql($this->request->get("desde")) ."' 
                AND 
                '". $this->funciones->cambiaf_a_sql($this->request->get("hasta")) ."'";
        }

        if(!empty($this->request->get("cuenta"))) {
            $query .= " AND
                p.cue_id = '" . $this->request->get("cuenta") . "'";
        }

        if(!empty($this->request->get("descripcion"))) {
            $query .= " AND
                UPPER(c.cli_nombre) LIKE UPPER('%" . $this->request->get("descripcion") . "%')";
        }

        $query .= " ORDER BY
            p.id";

        $pagos = (new Query($query, $this->getDI()))->execute()->toArray();

        // ******************************** //

        // Lista de egresos
        $query = "SELECT
            e.egr_importe,
            e.egr_moneda,
            e.egr_fecha AS fecha,
            e.egr_descripcion,
            c.cue_numero,
            b.ban_nombre
            FROM
            Egresos AS e,
            Cuentas AS c,
            Bancos As b
            WHERE
            e.cue_id = c.id
            AND
            c.ban_id = b.id";

        if(!empty($this->request->get("desde")) && !empty($this->request->get("hasta"))) {
            $query .= " AND
                e.egr_fecha
                BETWEEN 
                '". $this->funciones->cambiaf_a_sql($this->request->get("desde")) ."' 
                AND 
                '". $this->funciones->cambiaf_a_sql($this->request->get("hasta")) ."'";
        }

        if(!empty($this->request->get("cuenta"))) {
            $query .= " AND
                e.cue_id = '" . $this->request->get("cuenta") . "'";
        }

        if(!empty($this->request->get("descripcion"))) {
            $query .= " AND
                UPPER(e.egr_descripcion) LIKE UPPER('%" . $this->request->get("descripcion") . "%')";
        }

        $query .= " ORDER BY 
            e.id DESC";

        $egresos = (new Query($query, $this->getDI()))->execute()->toArray();

        // ******************************** //

        $pagos = array_merge($pagos, $egresos);

        foreach($pagos as $key => $pago)
            $fechas[$key] = $pago["fecha"];

        array_multisort($fechas, SORT_ASC, $pagos); 

        $html = $this->view->getRender("reportes", "pagos_bancarios", array(
            "pagos" => $pagos
        )); 

        $this->mpdf->SetTopMargin(8);
        $this->mpdf->WriteHTML($html);
        $this->mpdf->Output();
        
        exit;
    }

    // ********************************  FIN DE REPORTE DE PAGOS  ******************************** //


    // ********************************  INICIO DE PRODUCTOS MÁS VENDIDOS  ******************************** //

    // Vista de reporte de productos mas vendios
    public function productos_ventasAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets->addCss("assets/plugins/form-select2/select2.css")
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css");

        $this->assets
            ->addJs("assets/plugins/form-select2/select2.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")  
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("js/reportes/productos_ventas.js?V=1.7");

        //guardo accion para auditoria.
        $this->saveAction("Consulta de Reporte 4 C/S Movimientos");        

    }

    // Obtención de la lista de productos
    public function getproductosAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        if($this->request->isPost()) {

            // Lista de productos

            if( $this->request->getPost("ventas")==1 ) {

                // verifico que la fecha no venga vacia
                if(!empty($this->request->getPost("desde")) && !empty($this->request->getPost("hasta")))
                    {
                        $desde = $this->funciones->cambiaf_a_sql($this->request->getPost("desde"));
                        $hasta = $this->funciones->cambiaf_a_sql($this->request->getPost("hasta"));    
                    }
                else
                    {
                        $desde = '2018-01-01';
                        $hasta = date('Y-m-d');
                    }
                // consulta para total salidas

                $query = "SELECT
                        a.id,
                        a.art_codigo,
                        a.art_descripcion,
                        a.art_inv_inicial,
                        get_cant_mov('DESCARGA', a.id, '".$desde."', '".$hasta."') as total_ventas,
                        get_cant_mov('CARGA', a.id, '2018-01-01', '".date('Y-m-d')."') as mov_carga
                        FROM
                        Articulos AS a,
                        Movimientos AS m
                        WHERE
                        m.art_id = a.id
                        ";
                
                if(!empty($this->request->getPost("desde")) && !empty($this->request->getPost("hasta"))) {
                    $query .= " AND
                        m.mov_fec_creacion
                        BETWEEN 
                        '". $this->funciones->cambiaf_a_sql($this->request->getPost("desde")) ."' 
                        AND 
                        '". $this->funciones->cambiaf_a_sql($this->request->getPost("hasta")) ."'";
                    }

                $query .= " GROUP BY
                        a.id, a.art_codigo, a.art_inv_inicial ORDER BY total_ventas DESC";


                
            } else
            {

                $query = "SELECT a.art_codigo,
                a.art_descripcion,
                a.art_inv_inicial
                FROM Articulos AS a
                WHERE a.id NOT IN (SELECT m.art_id
                FROM Movimientos as m ";

                if(!empty($this->request->getPost("desde")) && !empty($this->request->getPost("hasta"))) {
                    $query .= " WHERE
                        m.mov_fec_creacion
                        BETWEEN 
                        '". $this->funciones->cambiaf_a_sql($this->request->getPost("desde")) ."' 
                        AND 
                        '". $this->funciones->cambiaf_a_sql($this->request->getPost("hasta")) ."')";
                }
                else
                {
                    $query .= " )";
                }

                $query .= " ORDER BY a.art_descripcion ASC";
                               
            }

            $product = new Query($query, $this->getDI());
            $product = $product->execute();

            $this->view->setVar("total", count($product));

            // ******************************** //

            count($product) > 0 ? $this->view->setVar("product", $product) : $this->view->disable();
        }
    }

    // ********************************  FIN DE PRODUCTOS MÁS VENDIDOS  ******************************** //


    // ********************************  INICIO DE REPORTE DE GASTOS  ******************************** //

    // Vista de reporte de gastos
    public function gastosAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets->addCss("assets/plugins/form-select2/select2.css")
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css")
            ->addCss("assets/plugins/datatables/dataTables.themify.css");

        $this->assets
            ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")  
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/form-select2/select2.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("js/reportes/gastos.js?v=29.03.01");

        // Lista de conceptos
        $catGastos = CategoriasGastos::find(array("order" => "cat_nombre"));
        $this->view->setVar("catGastos", $catGastos);

        // ******************************** //    
    }

    // Obtención de la lista de gastos
    public function getgastosAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        if($this->request->isPost()) {
            // Lista de gastos emitidos
            $query = "SELECT
                g.gas_descripcion,
                g.gas_fecha,
                g.gas_cantidad,
                g.gas_costo,
                g.gas_estatus,
                g.gas_divisas,
                g.cat_id,
                c.cat_nombre
                FROM
                Gastos AS g,
                CategoriasGastos AS c
                WHERE
                g.cat_id = c.id";

            if(!empty($this->request->getPost("desde")) && !empty($this->request->getPost("hasta"))) {
                $query .= " AND
                    g.gas_fecha
                    BETWEEN 
                    '". $this->funciones->cambiaf_a_sql($this->request->getPost("desde")) ."' 
                    AND 
                    '". $this->funciones->cambiaf_a_sql($this->request->getPost("hasta")) ."'";
            }

            if(!empty($this->request->getPost("concepto"))) {
                $query .= " AND
                    g.cat_id = '" . $this->request->getPost("concepto") . "'";
            }

            $query .= " ORDER BY
                g.id";

            $gastos = (new Query($query, $this->getDI()))->execute();

            // ******************************** //            

            count($gastos) > 0 ? $this->view->setVar("gastos", $gastos) : $this->view->disable();
        }
    }

    // Reporte de gastos emitidos en formato pdf
    public function gastos_emitidosAction() {
        $this->view->disable();
        
        // Lista de gastos emitidos
        $query = "SELECT
            g.gas_descripcion,
            g.gas_fecha,
            g.gas_cantidad,
            g.gas_costo,
            g.gas_estatus,
            c.cat_nombre
            FROM
            Gastos AS g,
            CategoriasGastos AS c
            WHERE
            g.cat_id = c.id";

        if(!empty($this->request->get("desde")) && !empty($this->request->get("hasta"))) {
            $query .= " AND
                g.gas_fecha
                BETWEEN 
                '". $this->funciones->cambiaf_a_sql($this->request->get("desde")) ."' 
                AND 
                '". $this->funciones->cambiaf_a_sql($this->request->get("hasta")) ."'";
        }

        if(!empty($this->request->get("concepto"))) {
            $query .= " AND
                g.cat_id = '" . $this->request->get("concepto") . "'";
        }

        $query .= " ORDER BY
            g.id";

        $gastos = (new Query($query, $this->getDI()))->execute();

        // ******************************** //

        $html = $this->view->getRender("reportes", "gastos_emitidos", array(
            "gastos" => $gastos
        ));

        $this->mpdf->SetHTMLFooter("<div width='100%' style='font-size: 14px; text-align: center'><hr/>{DATE d/m/Y} - Página {PAGENO} de {nb} | Telecomunicaciones Monagas | R.I.F. J-08020670-3. Copyright {DATE Y}.</div>");
        $this->mpdf->SetTopMargin(8);
        $this->mpdf->WriteHTML($html);
        $this->mpdf->Output();
        
        exit;
    }

    // Obtención de la lista de productos EN PDF
    public function pdf_productosAction() {
        $this->view->disable();

                // Lista de productos

            if( $this->request->get("ventas")==1 ) {

                // verifico que la fecha no venga vacia
                if(!empty($this->request->get("desde")) && !empty($this->request->get("hasta")))
                    {
                        $desde = $this->funciones->cambiaf_a_sql($this->request->get("desde"));
                        $hasta = $this->funciones->cambiaf_a_sql($this->request->get("hasta"));    
                    }
                else
                    {
                        $desde = '2018-01-01';
                        $hasta = date('Y-m-d');
                    }
                // consulta para total salidas

                $query = "SELECT
                        a.id,
                        a.art_codigo,
                        a.art_descripcion,
                        a.art_inv_inicial,
                        get_cant_mov('DESCARGA', a.id, '".$desde."', '".$hasta."') as total_ventas,
                        get_cant_mov('CARGA', a.id, '".$desde."', '".$hasta."') as mov_carga
                        FROM
                        Articulos AS a,
                        Movimientos AS m
                        WHERE
                        m.art_id = a.id
                        ";
                
                if(!empty($this->request->get("desde")) && !empty($this->request->get("hasta"))) {
                    $query .= " AND
                        m.mov_fec_creacion
                        BETWEEN 
                        '". $this->funciones->cambiaf_a_sql($this->request->get("desde")) ."' 
                        AND 
                        '". $this->funciones->cambiaf_a_sql($this->request->get("hasta")) ."'";
                    }

                $query .= " GROUP BY
                        a.id, a.art_codigo, a.art_inv_inicial ORDER BY total_ventas DESC";


                
            } else
            {

                $query = "SELECT a.art_codigo,
                a.art_descripcion,
                a.art_inv_inicial
                FROM Articulos AS a
                WHERE a.id NOT IN (SELECT m.art_id
                FROM Movimientos as m ";

                if(!empty($this->request->get("desde")) && !empty($this->request->get("hasta"))) {
                    $query .= " WHERE
                        m.mov_fec_creacion
                        BETWEEN 
                        '". $this->funciones->cambiaf_a_sql($this->request->get("desde")) ."' 
                        AND 
                        '". $this->funciones->cambiaf_a_sql($this->request->get("hasta")) ."')";
                }
                else
                {
                    $query .= " )";
                }

                $query .= " ORDER BY a.art_descripcion ASC";
                               
            }

        $product = new Query($query, $this->getDI());
        $product = $product->execute();


                // ******************************** //            

        $html = $this->view->getRender("reportes", "pdf_productos", array(
            "product" => $product,
            "total" => count($product)
        )); 

        $this->mpdf->SetHTMLFooter("<div width='100%' style='font-size: 14px; text-align: center'><hr/>{DATE d/m/Y} - Página {PAGENO} de {nb} | Telecomunicaciones Monagas | R.I.F. J-08020670-3. Copyright {DATE Y}.</div>");
        $this->mpdf->SetTopMargin(8);
        $this->mpdf->WriteHTML($html);
        $this->mpdf->Output();
        
        exit;
    }

}