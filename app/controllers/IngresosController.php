<?php

use Phalcon\Mvc\Model\Query;

class IngresosController extends ControllerBase { 

    // Ingreso de mercancía por compra a proveedor
    public function compraAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets->addCss("assets/plugins/form-select2/select2.css");

        $this->assets
            ->addJs("assets/js/number.format.js")
            ->addJs("assets/js/jquery.price_format.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/form-select2/select2.min.js")
            ->addJs("assets/plugins/bootbox/bootbox.js")
            ->addJs("assets/plugins/form-jasnyupload/fileinput.min.js")
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("js/ingresos/compra.js");

        // Lista de proveedores
        $proveedores = Proveedores::find(array("pro_estatus = 'ACTIVO'", "order" => "pro_nombre, pro_codigo"));
        $this->view->setVar("proveedores", $proveedores);

        // Lista de artículos
        $articulos = Articulos::find(array("art_estatus = 'ACTIVO'", "order" => "art_codigo, art_descripcion"));
        $this->view->setVar("articulos", $articulos);

        // Precio del dólar
        $dolares = new DolaresController();
        $dolar = $dolares->getdolar();
        $this->view->setVar("dolar", $dolar);

        // Valores en porcentaje para cálculos de precio
        $calculos = Calculos::findFirst();
        $this->view->setVar("calculos", $calculos);

        if($this->request->isPost()) {
            // Datos del ingreso de mercancía
            $ingreso = Ingresos::findFirstById($this->request->getPost("codigo"));
            
            if($ingreso) { 
                $this->view->setVar("ingreso", $ingreso);

                // Lista de artículos del ingreso
                $query = "SELECT
                    m.mov_cantidad,
                    a.id,
                    a.art_codigo,
                    a.art_descripcion,
                    a.art_cos_unitario,
                    a.art_pre_unitario1,
                    a.art_inv_inicial,
                    a.art_fec_creacion,
                    a.art_sto_minimo,
                    a.art_ubicacion,
                    a.art_fot_referencial
                    FROM 
                    Movimientos AS m,
                    Articulos AS a,
                    Ingresos AS i
                    WHERE 
                    m.art_id = a.id
                    AND
                    m.ing_id = i.id
                    AND
                    m.mov_tipo = 'CARGA'
                    AND
                    i.id = '" . $ingreso->id . "'";
                
                $lisIngresos = new Query($query, $this->getDI());
                $lisIngresos = $lisIngresos->execute();
                $this->view->setVar("lisIngresos", $lisIngresos);

                // ******************************** //

                // Datos del proveedor
                $proIngreso = Proveedores::findFirstById($ingreso->pro_id);
                $this->view->setVar("proIngreso", $proIngreso);
            } else
                $this->response->redirect("ingresos/lista");
        } else if($this->request->get("token")) {
            $this->response->redirect("ingresos/lista");
            return false;
        }
    }

    // Almacenamiento del ingreso de mercancía en la base de datos
    public function registrobdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();

            // Datos del ingreso de mercancía
            $ingreso = new Ingresos();

            $ingreso->setIngObservaciones($this->funciones->strtoupper_utf8($this->request->getPost("observaciones")));
            $ingreso->setProId($this->request->getPost("proveedor"));
            $ingreso->setUsuId($this->funciones->getUsuario());

            // ******************************** //

            if(!$ingreso->save()) {
                foreach($ingreso->getMessages() as $mensaje)
                    $errores[] = $mensaje;
            } else {
                foreach(json_decode($this->request->get("articulos")) as $index => $articulo) {
                    // Datos de la lista de ingresos
                    $movimiento = new Movimientos();

                    $movimiento->setMovTipo("CARGA");
                    $movimiento->setMovCantidad($this->request->getPost("cantidad")[$index]);
                    $movimiento->setArtId($articulo);
                    $movimiento->setMotId(3);
                    $movimiento->setIngId($ingreso->getId());

                    // ******************************** //

                    if(!$movimiento->save()) {
                        foreach($movimiento->getMessages() as $mensaje)
                            $errores[] = $mensaje;
                    }
                }
            }

            if(count($errores) > 0)
                $parametros["text"] = implode("</br>", $errores);
            else {
                //guardo accion para auditoria.
                $this->saveAction("Ingreso de Mercancia: " . $this->funciones->str_pad($ingreso->getId()) . ""); 
                $parametros["text"] = "El ingreso de mercancía con código " . $this->funciones->str_pad($ingreso->getId()) . " se registró correctamente. ¿Desea imprimir el soporte en PDF?";
                $parametros["ingreso"] = $ingreso->getId();
            }

            echo json_encode($parametros);
        }
    }

    // Soporte de ingreso en formato pdf
    public function ingreso_mercanciaAction($ingreso) {
        $this->view->disable();
        
        // Datos del ingreso de mercancía
        $query = "SELECT
            i.id,
            i.ing_observaciones,
            i.ing_fec_creacion,
            p.pro_codigo,
            p.pro_nombre,
            p.pro_per_contacto,
            p.pro_tel_celular,
            p.pro_cor_electronico,
            u.usu_nombre
            FROM 
            Ingresos AS i,
            Proveedores AS p,
            Usuarios AS u
            WHERE 
            i.pro_id = p.id
            AND
            i.usu_id = u.id
            AND
            i.id = '" . $ingreso . "'";
        
        $ingMercancia = new Query($query, $this->getDI());
        $ingMercancia = $ingMercancia->execute();

        // ******************************** //

        // Lista de artículos del ingreso
        $query = "SELECT
            m.mov_cantidad,
            a.art_codigo,
            a.art_cos_unitario
            FROM 
            Movimientos AS m,
            Articulos AS a,
            Ingresos AS i
            WHERE 
            m.art_id = a.id
            AND
            m.ing_id = i.id
            AND
            i.id = '" . $ingreso . "'";
        
        $lisIngresos = new Query($query, $this->getDI());
        $lisIngresos = $lisIngresos->execute();

        // ******************************** //

        // Precio del dolar
        $dolares = new DolaresController();
        $dolar = $dolares->getdolar();

        $html = $this->view->getRender("ingresos", "ingreso_mercancia", array(
            "ingMercancia" => $ingMercancia,
            "lisIngresos" => $lisIngresos,
            "dolar" => $dolar
        )); 

        $this->mpdf->SetHTMLFooter("<table width='100%'><tr><td colspan='2'><hr/></td></tr><tr><td>{DATE d/m/Y} - Página {PAGENO} de {nb} | Telecomunicaciones Monagas | R.I.F. J-40458220-7. Copyright {DATE Y}.</td></tr></table>");
        $this->mpdf->SetTopMargin(8);
        $this->mpdf->WriteHTML($html);
        $this->mpdf->Output();
        
        exit;
    }

    // Vista de ingresos registrados
    public function listaAction() {
        header("Cache-Control: post-chek=0");
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css")
            ->addCss("assets/plugins/datatables/dataTables.themify.css");

        $this->assets
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("js/ingresos/lista.js");
    }

    // Obtención de la lista de ingresos
    public function getingresosAction() {
        $this->view->disable();

        // Lista de ingresos de mercancía
        $query = "SELECT
            i.id,
            i.ing_fec_creacion,
            p.pro_codigo,
            p.pro_nombre,
            u.usu_nombre,
            u.usu_codigo
            FROM 
            Ingresos AS i,
            Proveedores AS p,
            Usuarios AS u
            WHERE 
            i.pro_id = p.id
            AND
            i.usu_id = u.id
            ORDER BY
            i.id desc";
        
        $ingresos = new Query($query, $this->getDI());
        $ingresos = $ingresos->execute();

        // ******************************** //

        $arrIngresos = array();

        foreach($ingresos as $clave => $ingreso) {
            $arrIngresos["aaData"][$clave]["codIngreso"] = $this->funciones->str_pad($ingreso->id);
            $arrIngresos["aaData"][$clave]["proveedor"] = $ingreso->pro_codigo . " (" . $ingreso->pro_nombre . ")";
            $arrIngresos["aaData"][$clave]["fecha"] = $this->funciones->cambiaf_a_normal($ingreso->ing_fec_creacion);
            $arrIngresos["aaData"][$clave]["usuario"] = $ingreso->usu_codigo . " (" . $ingreso->usu_nombre . ")";
        }

        $arrIngresos["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 4);

        return json_encode($arrIngresos);
    }

    // Actualización de ingresos en la base de datos
    public function actualizacionbdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $ingreso = Ingresos::findFirstById($this->request->get("id"));
            $errores = array();

            if($ingreso) {
                // Datos del ingreso de mercancía
                $ingreso->ing_observaciones = $this->funciones->strtoupper_utf8($this->request->getPost("observaciones"));
                $ingreso->pro_id = $this->request->getPost("proveedor");

                // ******************************** //

                if(!$ingreso->update()) {
                    foreach($ingreso->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                //guardo accion para auditoria.
                $this->saveAction("Actualización de Ingreso de Mercancia: " . $this->funciones->str_pad($ingreso->id) . ""); 
                $parametros["text"] = "El ingreso de mercancía con código " . $this->funciones->str_pad($ingreso->id) . " se actualizó correctamente.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    public function eliminacionbdAction() {

        $this->view->disable();

        if($this->request->isPost()) {
            $ingreso = Ingresos::findFirstById($this->request->get("id"));
            $errores = array();

            if ($ingreso !== false) {

                if ($ingreso->delete() === false) {

                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";

                } else {

                //elimino los movimientos de la ingreso
                $query = "DELETE
                        FROM Movimientos
                        WHERE ing_id = ".$this->request->get('id')."";

                        $borrar = new Query($query, $this->getDI());
                        $borrar = $borrar->execute();
                        //guardo accion para auditoria.
                        $this->saveAction("Eliminación de Ingreso de Mercancia: " . $this->request->get("id") . ""); 
                        $parametros["text"] = "El ingreso se eliminó exitosamente.";
                        $parametros["type"] = "success";
                }


            }

            echo json_encode($parametros);
        }

    }

}