<?php

class Estados extends \Phalcon\Mvc\Model {

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $est_nombre;

    /**
     *
     * @var string
     */
    protected $est_estatus;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field est_nombre
     *
     * @param string $est_nombre
     * @return $this
     */
    public function setEstNombre($est_nombre)
    {
        $this->est_nombre = $est_nombre;

        return $this;
    }

    /**
     * Method to set the value of field est_estatus
     *
     * @param string $est_estatus
     * @return $this
     */
    public function setEstEstatus($est_estatus)
    {
        $this->est_estatus = $est_estatus;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field est_nombre
     *
     * @return string
     */
    public function getEstNombre()
    {
        return $this->est_nombre;
    }

    /**
     * Returns the value of field est_estatus
     *
     * @return string
     */
    public function getEstEstatus()
    {
        return $this->est_estatus;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_estados';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'est_nombre' => 'est_nombre', 
            'est_estatus' => 'est_estatus'
        );
    }

}
