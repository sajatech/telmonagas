<?php

class FacturasItems extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $item_codigo;

    /**
     *
     * @var string
     */
    protected $item_descripcion;

    /**
     *
     * @var string
     */
    protected $item_cantidad;

    /**
     *
     * @var string
     */
    protected $item_gpg;

    /**
     *
     * @var string
     */
    protected $item_peso;

    /**
     *
     * @var string
     */
    protected $item_costo;

    /**
     *
     * @var string
     */
    protected $item_precio;

    /**
     *
     * @var integer
     */
    protected $fac_id;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field item_codigo
     *
     * @param string $item_codigo
     * @return $this
     */
    public function setItemCodigo($item_codigo)
    {
        $this->item_codigo = $item_codigo;

        return $this;
    }

    /**
     * Method to set the value of field tem_descripcion
     *
     * @param string $tem_descripcion
     * @return $this
     */
    public function setItemDescripcion($item_descripcion)
    {
        $this->item_descripcion = $item_descripcion;

        return $this;
    }

    /**
     * Method to set the value of field item_cantidad
     *
     * @param string $item_cantidad
     * @return $this
     */
    public function setItemCantidad($item_cantidad)
    {
        $this->item_cantidad = $item_cantidad;

        return $this;
    }

    public function setItemGpg($item_gpg)
    {
        $this->item_gpg = $item_gpg;

        return $this;
    }

    /**
     * Method to set the value of field item_peso
     *
     * @param string $item_peso
     * @return $this
     */
    public function setItemPeso($item_peso)
    {
        $this->item_peso = $item_peso;

        return $this;
    }

    /**
     * Method to set the value of field item_costo
     *
     * @param string $item_costo
     * @return $this
     */
    public function setItemCosto($item_costo)
    {
        $this->item_costo = $item_costo;

        return $this;
    }

    /**
     * Method to set the value of field item_precio
     *
     * @param string $item_precio
     * @return $this
     */
    public function setItemPrecio($item_precio)
    {
        $this->item_precio = $item_precio;

        return $this;
    }

    /**
     * Method to set the value of field fac_id
     *
     * @param integer $fac_id
     * @return $this
     */
    public function setFacId($fac_id)
    {
        $this->fac_id = $fac_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field item_codigo
     *
     * @return string
     */
    public function getItemCodigo()
    {
        return $this->item_codigo;
    }

    /**
     * Returns the value of field tem_descripcion
     *
     * @return string
     */
    public function getItemDescripcion()
    {
        return $this->item_descripcion;
    }

    /**
     * Returns the value of field item_cantidad
     *
     * @return string
     */
    public function getItemCantidad()
    {
        return $this->item_cantidad;
    }

    public function getItemGpg()
    {
        return $this->item_gpg;
    }

    /**
     * Returns the value of field item_peso
     *
     * @return string
     */
    public function getItemPeso()
    {
        return $this->item_peso;
    }

    /**
     * Returns the value of field item_costo
     *
     * @return string
     */
    public function getItemCosto()
    {
        return $this->item_costo;
    }

    /**
     * Returns the value of field item_precio
     *
     * @return string
     */
    public function getItemPrecio()
    {
        return $this->item_precio;
    }

    /**
     * Returns the value of field fac_id
     *
     * @return integer
     */
    public function getFacId()
    {
        return $this->fac_id;
    }

        /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_facturas_items';
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->belongsTo('fac_id', 'Tel_facturas_items', 'id', array('alias' => 'FacturasInt'));
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'item_codigo' => 'item_codigo', 
            'item_descripcion' => 'item_descripcion', 
            'item_cantidad' => 'item_cantidad', 
            'item_peso' => 'item_peso', 
            'item_costo' => 'item_costo', 
            'fac_id' => 'fac_id',
            'item_precio' => 'item_precio',
            'item_gpg' => 'item_gpg'
        );
    }

}
