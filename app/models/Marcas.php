<?php

class Marcas extends \Phalcon\Mvc\Model {

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $mar_nombre;

    /**
     *
     * @var string
     */
    protected $mar_estatus;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field mar_nombre
     *
     * @param string $mar_nombre
     * @return $this
     */
    public function setMarNombre($mar_nombre)
    {
        $this->mar_nombre = $mar_nombre;

        return $this;
    }

    /**
     * Method to set the value of field mar_estatus
     *
     * @param string $mar_estatus
     * @return $this
     */
    public function setMarEstatus($mar_estatus)
    {
        $this->mar_estatus = $mar_estatus;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field mar_nombre
     *
     * @return string
     */
    public function getMarNombre()
    {
        return $this->mar_nombre;
    }

    /**
     * Returns the value of field mar_estatus
     *
     * @return string
     */
    public function getMarEstatus()
    {
        return $this->mar_estatus;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_marcas';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'mar_nombre' => 'mar_nombre', 
            'mar_estatus' => 'mar_estatus'
        );
    }

}
