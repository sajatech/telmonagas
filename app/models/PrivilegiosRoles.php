<?php

class PrivilegiosRoles extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $pri_id;

    /**
     *
     * @var string
     */
    protected $rol_id;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field pri_id
     *
     * @param string $pri_id
     * @return $this
     */
    public function setPriId($pri_id)
    {
        $this->pri_id = $pri_id;

        return $this;
    }

    /**
     * Method to set the value of field rol_id
     *
     * @param string $rol_id
     * @return $this
     */
    public function setRolId($rol_id)
    {
        $this->rol_id = $rol_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field pri_id
     *
     * @return string
     */
    public function getPriId()
    {
        return $this->pri_id;
    }

    /**
     * Returns the value of field rol_id
     *
     * @return string
     */
    public function getRolId()
    {
        return $this->rol_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->belongsTo('pri_id', 'Tel_privilegios', 'id', array('alias' => 'Tel_privilegios'));
        $this->belongsTo('rol_id', 'Tel_roles', 'id', array('alias' => 'Tel_roles'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_privilegios_roles';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'pri_id' => 'pri_id', 
            'rol_id' => 'rol_id'
        );
    }

}
