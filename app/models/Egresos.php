<?php

class Egresos extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $egr_importe;

    /**
     *
     * @var string
     */
    protected $egr_moneda;

    /**
     *
     * @var string
     */
    protected $egr_fecha;

    /**
     *
     * @var string
     */
    protected $egr_descripcion;

    /**
     *
     * @var string
     */
    protected $egr_fec_creacion;

    /**
     *
     * @var string
     */
    protected $cue_id;

    /**
     *
     * @var string
     */
    protected $usu_id;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field egr_importe
     *
     * @param string $egr_importe
     * @return $this
     */
    public function setEgrImporte($egr_importe)
    {
        $this->egr_importe = $egr_importe;

        return $this;
    }

    /**
     * Method to set the value of field egr_moneda
     *
     * @param string $egr_moneda
     * @return $this
     */
    public function setEgrMoneda($egr_moneda)
    {
        $this->egr_moneda = $egr_moneda;

        return $this;
    }

    /**
     * Method to set the value of field egr_fecha
     *
     * @param string $egr_fecha
     * @return $this
     */
    public function setEgrFecha($egr_fecha)
    {
        $this->egr_fecha = $egr_fecha;

        return $this;
    }

    /**
     * Method to set the value of field egr_descripcion
     *
     * @param string $egr_descripcion
     * @return $this
     */
    public function setEgrDescripcion($egr_descripcion)
    {
        $this->egr_descripcion = $egr_descripcion;

        return $this;
    }

    /**
     * Method to set the value of field egr_fec_creacion
     *
     * @param string $egr_fec_creacion
     * @return $this
     */
    public function setEgrFecCreacion($egr_fec_creacion)
    {
        $this->egr_fec_creacion = $egr_fec_creacion;

        return $this;
    }

    /**
     * Method to set the value of field cue_id
     *
     * @param string $cue_id
     * @return $this
     */
    public function setCueId($cue_id)
    {
        $this->cue_id = $cue_id;

        return $this;
    }

    /**
     * Method to set the value of field usu_id
     *
     * @param string $usu_id
     * @return $this
     */
    public function setUsuId($usu_id)
    {
        $this->usu_id = $usu_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field egr_importe
     *
     * @return string
     */
    public function getEgrImporte()
    {
        return $this->egr_importe;
    }

    /**
     * Returns the value of field egr_moneda
     *
     * @return string
     */
    public function getEgrMoneda()
    {
        return $this->egr_moneda;
    }

    /**
     * Returns the value of field egr_fecha
     *
     * @return string
     */
    public function getEgrFecha()
    {
        return $this->egr_fecha;
    }

    /**
     * Returns the value of field egr_descripcion
     *
     * @return string
     */
    public function getEgrDescripcion()
    {
        return $this->egr_descripcion;
    }

    /**
     * Returns the value of field egr_fec_creacion
     *
     * @return string
     */
    public function getEgrFecCreacion()
    {
        return $this->egr_fec_creacion;
    }

    /**
     * Returns the value of field cue_id
     *
     * @return string
     */
    public function getCueId()
    {
        return $this->cue_id;
    }

    /**
     * Returns the value of field usu_id
     *
     * @return string
     */
    public function getUsuId()
    {
        return $this->usu_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->belongsTo('cue_id', 'Tel_cuentas', 'id', array('alias' => 'Tel_cuentas'));
        $this->belongsTo('usu_id', 'Tel_usuarios', 'id', array('alias' => 'Tel_usuarios'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_egresos';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'egr_importe' => 'egr_importe', 
            'egr_moneda' => 'egr_moneda', 
            'egr_fecha' => 'egr_fecha', 
            'egr_descripcion' => 'egr_descripcion', 
            'egr_fec_creacion' => 'egr_fec_creacion', 
            'cue_id' => 'cue_id', 
            'usu_id' => 'usu_id'
        );
    }

}
