<?php

class Gastos extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $gas_descripcion;

    /**
     *
     * @var string
     */
    protected $gas_fecha;

    /**
     *
     * @var integer
     */
    protected $gas_cantidad;

    /**
     *
     * @var string
     */
    protected $gas_costo;

    /**
     *
     * @var string
     */
    protected $gas_estatus;

    /**
     *
     * @var string
     */
    protected $gas_fec_creacion;

    /**
     *
     * @var string
     */
    protected $cat_id;

    /**
     *
     * @var string
     */
    protected $usu_id;

    /**
     *
     * @var string
     */
    protected $dol_id;

    /**
     *
     * @var string
     */
    protected $gas_divisas;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field gas_descripcion
     *
     * @param string $gas_descripcion
     * @return $this
     */
    public function setGasDescripcion($gas_descripcion)
    {
        $this->gas_descripcion = $gas_descripcion;

        return $this;
    }

    /**
     * Method to set the value of field gas_fecha
     *
     * @param string $gas_fecha
     * @return $this
     */
    public function setGasFecha($gas_fecha)
    {
        $this->gas_fecha = $gas_fecha;

        return $this;
    }

    /**
     * Method to set the value of field gas_cantidad
     *
     * @param integer $gas_cantidad
     * @return $this
     */
    public function setGasCantidad($gas_cantidad)
    {
        $this->gas_cantidad = $gas_cantidad;

        return $this;
    }

    /**
     * Method to set the value of field gas_costo
     *
     * @param string $gas_costo
     * @return $this
     */
    public function setGasCosto($gas_costo)
    {
        $this->gas_costo = $gas_costo;

        return $this;
    }

    /**
     * Method to set the value of field gas_estatus
     *
     * @param string $gas_estatus
     * @return $this
     */
    public function setGasEstatus($gas_estatus)
    {
        $this->gas_estatus = $gas_estatus;

        return $this;
    }

    /**
     * Method to set the value of field gas_fec_creacion
     *
     * @param string $gas_fec_creacion
     * @return $this
     */
    public function setGasFecCreacion($gas_fec_creacion)
    {
        $this->gas_fec_creacion = $gas_fec_creacion;

        return $this;
    }

    /**
     * Method to set the value of field cat_id
     *
     * @param string $cat_id
     * @return $this
     */
    public function setCatId($cat_id)
    {
        $this->cat_id = $cat_id;

        return $this;
    }

    /**
     * Method to set the value of field usu_id
     *
     * @param string $usu_id
     * @return $this
     */
    public function setUsuId($usu_id)
    {
        $this->usu_id = $usu_id;

        return $this;
    }

    /**
     * Method to set the value of field dol_id
     *
     * @param string $dol_id
     * @return $this
     */
    public function setDolId($dol_id)
    {
        $this->dol_id = $dol_id;

        return $this;
    }

    /**
     * Method to set the value of field gas_divisas
     *
     * @param string $gas_divisas
     * @return $this
     */
    public function setGasDivisas($gas_divisas)
    {
        $this->gas_divisas = $gas_divisas;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field gas_descripcion
     *
     * @return string
     */
    public function getGasDescripcion()
    {
        return $this->gas_descripcion;
    }

    /**
     * Returns the value of field gas_fecha
     *
     * @return string
     */
    public function getGasFecha()
    {
        return $this->gas_fecha;
    }

    /**
     * Returns the value of field gas_cantidad
     *
     * @return integer
     */
    public function getGasCantidad()
    {
        return $this->gas_cantidad;
    }

    /**
     * Returns the value of field gas_costo
     *
     * @return string
     */
    public function getGasCosto()
    {
        return $this->gas_costo;
    }

    /**
     * Returns the value of field gas_estatus
     *
     * @return string
     */
    public function getGasEstatus()
    {
        return $this->gas_estatus;
    }

    /**
     * Returns the value of field gas_fec_creacion
     *
     * @return string
     */
    public function getGasFecCreacion()
    {
        return $this->gas_fec_creacion;
    }

    /**
     * Returns the value of field cat_id
     *
     * @return string
     */
    public function getCatId()
    {
        return $this->cat_id;
    }

    /**
     * Returns the value of field usu_id
     *
     * @return string
     */
    public function getUsuId()
    {
        return $this->usu_id;
    }

    /**
     * Returns the value of field dol_id
     *
     * @return string
     */
    public function getDolId()
    {
        return $this->dol_id;
    }

    /**
     * Returns the value of field gas_divisas
     *
     * @return string
     */
    public function getGasDivisas()
    {
        return $this->gas_divisas;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->belongsTo('cat_id', 'Tel_categorias_gastos', 'id', array('alias' => 'Tel_categorias_gastos'));
        $this->belongsTo('usu_id', 'Tel_usuarios', 'id', array('alias' => 'Tel_usuarios'));
        $this->belongsTo('dol_id', 'Tel_dolares', 'id', array('alias' => 'Tel_dolares'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_gastos';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'gas_descripcion' => 'gas_descripcion', 
            'gas_fecha' => 'gas_fecha', 
            'gas_cantidad' => 'gas_cantidad', 
            'gas_costo' => 'gas_costo', 
            'gas_estatus' => 'gas_estatus', 
            'gas_fec_creacion' => 'gas_fec_creacion', 
            'cat_id' => 'cat_id', 
            'usu_id' => 'usu_id', 
            'dol_id' => 'dol_id', 
            'gas_divisas' => 'gas_divisas'
        );
    }

}
