<?php

class Roles extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $rol_nombre;

    /**
     *
     * @var string
     */
    protected $rol_estatus;

    /**
     *
     * @var string
     */
    protected $usu_id;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field rol_nombre
     *
     * @param string $rol_nombre
     * @return $this
     */
    public function setRolNombre($rol_nombre)
    {
        $this->rol_nombre = $rol_nombre;

        return $this;
    }

    /**
     * Method to set the value of field rol_estatus
     *
     * @param string $rol_estatus
     * @return $this
     */
    public function setRolEstatus($rol_estatus)
    {
        $this->rol_estatus = $rol_estatus;

        return $this;
    }

    /**
     * Method to set the value of field usu_id
     *
     * @param string $usu_id
     * @return $this
     */
    public function setUsuId($usu_id)
    {
        $this->usu_id = $usu_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field rol_nombre
     *
     * @return string
     */
    public function getRolNombre()
    {
        return $this->rol_nombre;
    }

    /**
     * Returns the value of field rol_estatus
     *
     * @return string
     */
    public function getRolEstatus()
    {
        return $this->rol_estatus;
    }

    /**
     * Returns the value of field usu_id
     *
     * @return string
     */
    public function getUsuId()
    {
        return $this->usu_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->hasMany('id', 'Tel_privilegios_roles', 'rol_id', array('alias' => 'Tel_privilegios_roles'));
        $this->hasMany('id', 'Tel_usuarios', 'rol_id', array('alias' => 'Tel_usuarios'));
        $this->belongsTo('usu_id', 'Tel_usuarios', 'id', array('alias' => 'Tel_usuarios'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_roles';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'rol_nombre' => 'rol_nombre', 
            'rol_estatus' => 'rol_estatus', 
            'usu_id' => 'usu_id'
        );
    }

}
