<?php

class Contactos extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $con_nombre;

    /**
     *
     * @var string
     */
    protected $con_telefono1;

    /**
     *
     * @var string
     */
    protected $usu_id;

    /**
     *
     * @var string
     */
    protected $con_telefono2;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field con_nombre
     *
     * @param string $con_nombre
     * @return $this
     */
    public function setConNombre($con_nombre)
    {
        $this->con_nombre = $con_nombre;

        return $this;
    }

    /**
     * Method to set the value of field con_telefono1
     *
     * @param string $con_telefono1
     * @return $this
     */
    public function setConTelefono1($con_telefono1)
    {
        $this->con_telefono1 = $con_telefono1;

        return $this;
    }

    /**
     * Method to set the value of field usu_id
     *
     * @param string $usu_id
     * @return $this
     */
    public function setUsuId($usu_id)
    {
        $this->usu_id = $usu_id;

        return $this;
    }

    /**
     * Method to set the value of field con_telefono2
     *
     * @param string $con_telefono2
     * @return $this
     */
    public function setConTelefono2($con_telefono2)
    {
        $this->con_telefono2 = $con_telefono2;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field con_nombre
     *
     * @return string
     */
    public function getConNombre()
    {
        return $this->con_nombre;
    }

    /**
     * Returns the value of field con_telefono1
     *
     * @return string
     */
    public function getConTelefono1()
    {
        return $this->con_telefono1;
    }

    /**
     * Returns the value of field usu_id
     *
     * @return string
     */
    public function getUsuId()
    {
        return $this->usu_id;
    }

    /**
     * Returns the value of field con_telefono2
     *
     * @return string
     */
    public function getConTelefono2()
    {
        return $this->con_telefono2;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->belongsTo('usu_id', 'Tel_usuarios', 'id', array('alias' => 'Tel_usuarios'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_contactos';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'con_nombre' => 'con_nombre', 
            'con_telefono1' => 'con_telefono1', 
            'usu_id' => 'usu_id', 
            'con_telefono2' => 'con_telefono2'
        );
    }

}
