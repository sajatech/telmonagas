<?php

class Seriales extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $ser_numero;

    /**
     *
     * @var string
     */
    protected $ser_fec_creacion;

    /**
     *
     * @var string
     */
    protected $ser_estatus;

    /**
     *
     * @var string
     */
    protected $art_id;

    /**
     *
     * @var string
     */
    protected $usu_id;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field ser_numero
     *
     * @param string $ser_numero
     * @return $this
     */
    public function setSerNumero($ser_numero)
    {
        $this->ser_numero = $ser_numero;

        return $this;
    }

    /**
     * Method to set the value of field ser_fec_creacion
     *
     * @param string $ser_fec_creacion
     * @return $this
     */
    public function setSerFecCreacion($ser_fec_creacion)
    {
        $this->ser_fec_creacion = $ser_fec_creacion;

        return $this;
    }

    /**
     * Method to set the value of field ser_estatus
     *
     * @param string $ser_estatus
     * @return $this
     */
    public function setSerEstatus($ser_estatus)
    {
        $this->ser_estatus = $ser_estatus;

        return $this;
    }

    /**
     * Method to set the value of field art_id
     *
     * @param string $art_id
     * @return $this
     */
    public function setArtId($art_id)
    {
        $this->art_id = $art_id;

        return $this;
    }

    /**
     * Method to set the value of field usu_id
     *
     * @param string $usu_id
     * @return $this
     */
    public function setUsuId($usu_id)
    {
        $this->usu_id = $usu_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field ser_numero
     *
     * @return string
     */
    public function getSerNumero()
    {
        return $this->ser_numero;
    }

    /**
     * Returns the value of field ser_fec_creacion
     *
     * @return string
     */
    public function getSerFecCreacion()
    {
        return $this->ser_fec_creacion;
    }

    /**
     * Returns the value of field ser_estatus
     *
     * @return string
     */
    public function getSerEstatus()
    {
        return $this->ser_estatus;
    }

    /**
     * Returns the value of field art_id
     *
     * @return string
     */
    public function getArtId()
    {
        return $this->art_id;
    }

    /**
     * Returns the value of field usu_id
     *
     * @return string
     */
    public function getUsuId()
    {
        return $this->usu_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->belongsTo('art_id', 'Tel_articulos', 'id', array('alias' => 'Tel_articulos'));
        $this->belongsTo('usu_id', 'Tel_usuarios', 'id', array('alias' => 'Tel_usuarios'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_seriales';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'ser_numero' => 'ser_numero', 
            'ser_fec_creacion' => 'ser_fec_creacion', 
            'ser_estatus' => 'ser_estatus', 
            'art_id' => 'art_id', 
            'usu_id' => 'usu_id'
        );
    }

}
