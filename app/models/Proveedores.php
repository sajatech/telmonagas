<?php

class Proveedores extends \Phalcon\Mvc\Model {

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $pro_codigo;

    /**
     *
     * @var string
     */
    protected $pro_nombre;

    /**
     *
     * @var string
     */
    protected $pro_per_contacto;

    /**
     *
     * @var string
     */
    protected $pro_tel_local;

    /**
     *
     * @var string
     */
    protected $pro_tel_celular;

    /**
     *
     * @var string
     */
    protected $pro_direccion;

    /**
     *
     * @var string
     */
    protected $pro_cor_electronico;

    /**
     *
     * @var string
     */
    protected $pro_estatus;

    /**
     *
     * @var string
     */
    protected $usu_id;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field pro_codigo
     *
     * @param string $pro_codigo
     * @return $this
     */
    public function setProCodigo($pro_codigo)
    {
        $this->pro_codigo = $pro_codigo;

        return $this;
    }

    /**
     * Method to set the value of field pro_nombre
     *
     * @param string $pro_nombre
     * @return $this
     */
    public function setProNombre($pro_nombre)
    {
        $this->pro_nombre = $pro_nombre;

        return $this;
    }

    /**
     * Method to set the value of field pro_per_contacto
     *
     * @param string $pro_per_contacto
     * @return $this
     */
    public function setProPerContacto($pro_per_contacto)
    {
        $this->pro_per_contacto = $pro_per_contacto;

        return $this;
    }

    /**
     * Method to set the value of field pro_tel_local
     *
     * @param string $pro_tel_local
     * @return $this
     */
    public function setProTelLocal($pro_tel_local)
    {
        $this->pro_tel_local = $pro_tel_local;

        return $this;
    }

    /**
     * Method to set the value of field pro_tel_celular
     *
     * @param string $pro_tel_celular
     * @return $this
     */
    public function setProTelCelular($pro_tel_celular)
    {
        $this->pro_tel_celular = $pro_tel_celular;

        return $this;
    }

    /**
     * Method to set the value of field pro_direccion
     *
     * @param string $pro_direccion
     * @return $this
     */
    public function setProDireccion($pro_direccion)
    {
        $this->pro_direccion = $pro_direccion;

        return $this;
    }

    /**
     * Method to set the value of field pro_cor_electronico
     *
     * @param string $pro_cor_electronico
     * @return $this
     */
    public function setProCorElectronico($pro_cor_electronico)
    {
        $this->pro_cor_electronico = $pro_cor_electronico;

        return $this;
    }

    /**
     * Method to set the value of field pro_estatus
     *
     * @param string $pro_estatus
     * @return $this
     */
    public function setProEstatus($pro_estatus)
    {
        $this->pro_estatus = $pro_estatus;

        return $this;
    }

    /**
     * Method to set the value of field usu_id
     *
     * @param string $usu_id
     * @return $this
     */
    public function setUsuId($usu_id)
    {
        $this->usu_id = $usu_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field pro_codigo
     *
     * @return string
     */
    public function getProCodigo()
    {
        return $this->pro_codigo;
    }

    /**
     * Returns the value of field pro_nombre
     *
     * @return string
     */
    public function getProNombre()
    {
        return $this->pro_nombre;
    }

    /**
     * Returns the value of field pro_per_contacto
     *
     * @return string
     */
    public function getProPerContacto()
    {
        return $this->pro_per_contacto;
    }

    /**
     * Returns the value of field pro_tel_local
     *
     * @return string
     */
    public function getProTelLocal()
    {
        return $this->pro_tel_local;
    }

    /**
     * Returns the value of field pro_tel_celular
     *
     * @return string
     */
    public function getProTelCelular()
    {
        return $this->pro_tel_celular;
    }

    /**
     * Returns the value of field pro_direccion
     *
     * @return string
     */
    public function getProDireccion()
    {
        return $this->pro_direccion;
    }

    /**
     * Returns the value of field pro_cor_electronico
     *
     * @return string
     */
    public function getProCorElectronico()
    {
        return $this->pro_cor_electronico;
    }

    /**
     * Returns the value of field pro_estatus
     *
     * @return string
     */
    public function getProEstatus()
    {
        return $this->pro_estatus;
    }

    /**
     * Returns the value of field usu_id
     *
     * @return string
     */
    public function getUsuId()
    {
        return $this->usu_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->belongsTo('usu_id', 'Tel_usuarios', 'id', array('alias' => 'Tel_usuarios'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_proveedores';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'pro_codigo' => 'pro_codigo', 
            'pro_nombre' => 'pro_nombre', 
            'pro_per_contacto' => 'pro_per_contacto', 
            'pro_tel_local' => 'pro_tel_local', 
            'pro_tel_celular' => 'pro_tel_celular', 
            'pro_direccion' => 'pro_direccion', 
            'pro_cor_electronico' => 'pro_cor_electronico', 
            'pro_estatus' => 'pro_estatus', 
            'usu_id' => 'usu_id'
        );
    }

}
