<?php

class Dolares extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $dol_fec_creacion;

    /**
     *
     * @var string
     */
    protected $dol_monto;

    /**
     *
     * @var string
     */
    protected $dol_estatus;

    /**
     *
     * @var string
     */
    protected $usu_id;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field dol_fec_creacion
     *
     * @param string $dol_fec_creacion
     * @return $this
     */
    public function setDolFecCreacion($dol_fec_creacion)
    {
        $this->dol_fec_creacion = $dol_fec_creacion;

        return $this;
    }

    /**
     * Method to set the value of field dol_monto
     *
     * @param string $dol_monto
     * @return $this
     */
    public function setDolMonto($dol_monto)
    {
        $this->dol_monto = $dol_monto;

        return $this;
    }

    /**
     * Method to set the value of field dol_estatus
     *
     * @param string $dol_estatus
     * @return $this
     */
    public function setDolEstatus($dol_estatus)
    {
        $this->dol_estatus = $dol_estatus;

        return $this;
    }

    /**
     * Method to set the value of field usu_id
     *
     * @param string $usu_id
     * @return $this
     */
    public function setUsuId($usu_id)
    {
        $this->usu_id = $usu_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field dol_fec_creacion
     *
     * @return string
     */
    public function getDolFecCreacion()
    {
        return $this->dol_fec_creacion;
    }

    /**
     * Returns the value of field dol_monto
     *
     * @return string
     */
    public function getDolMonto()
    {
        return $this->dol_monto;
    }

    /**
     * Returns the value of field dol_estatus
     *
     * @return string
     */
    public function getDolEstatus()
    {
        return $this->dol_estatus;
    }

    /**
     * Returns the value of field usu_id
     *
     * @return string
     */
    public function getUsuId()
    {
        return $this->usu_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->hasMany('id', 'Tel_gastos', 'dol_id', array('alias' => 'Tel_gastos'));
        $this->hasMany('id', 'Tel_salidas', 'dol_id', array('alias' => 'Tel_salidas'));
        $this->belongsTo('usu_id', 'Tel_usuarios', 'id', array('alias' => 'Tel_usuarios'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_dolares';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'dol_fec_creacion' => 'dol_fec_creacion', 
            'dol_monto' => 'dol_monto', 
            'dol_estatus' => 'dol_estatus', 
            'usu_id' => 'usu_id'
        );
    }

}
