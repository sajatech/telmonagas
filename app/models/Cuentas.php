<?php

class Cuentas extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $cue_tipo;

    /**
     *
     * @var string
     */
    protected $cue_numero;

    /**
     *
     * @var string
     */
    protected $cue_fec_creacion;

    /**
     *
     * @var string
     */
    protected $cue_estatus;

    /**
     *
     * @var string
     */
    protected $ban_id;

    /**
     *
     * @var string
     */
    protected $usu_id;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field cue_tipo
     *
     * @param string $cue_tipo
     * @return $this
     */
    public function setCueTipo($cue_tipo)
    {
        $this->cue_tipo = $cue_tipo;

        return $this;
    }

    /**
     * Method to set the value of field cue_numero
     *
     * @param string $cue_numero
     * @return $this
     */
    public function setCueNumero($cue_numero)
    {
        $this->cue_numero = $cue_numero;

        return $this;
    }

    /**
     * Method to set the value of field cue_fec_creacion
     *
     * @param string $cue_fec_creacion
     * @return $this
     */
    public function setCueFecCreacion($cue_fec_creacion)
    {
        $this->cue_fec_creacion = $cue_fec_creacion;

        return $this;
    }

    /**
     * Method to set the value of field cue_estatus
     *
     * @param string $cue_estatus
     * @return $this
     */
    public function setCueEstatus($cue_estatus)
    {
        $this->cue_estatus = $cue_estatus;

        return $this;
    }

    /**
     * Method to set the value of field ban_id
     *
     * @param string $ban_id
     * @return $this
     */
    public function setBanId($ban_id)
    {
        $this->ban_id = $ban_id;

        return $this;
    }

    /**
     * Method to set the value of field usu_id
     *
     * @param string $usu_id
     * @return $this
     */
    public function setUsuId($usu_id)
    {
        $this->usu_id = $usu_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field cue_tipo
     *
     * @return string
     */
    public function getCueTipo()
    {
        return $this->cue_tipo;
    }

    /**
     * Returns the value of field cue_numero
     *
     * @return string
     */
    public function getCueNumero()
    {
        return $this->cue_numero;
    }

    /**
     * Returns the value of field cue_fec_creacion
     *
     * @return string
     */
    public function getCueFecCreacion()
    {
        return $this->cue_fec_creacion;
    }

    /**
     * Returns the value of field cue_estatus
     *
     * @return string
     */
    public function getCueEstatus()
    {
        return $this->cue_estatus;
    }

    /**
     * Returns the value of field ban_id
     *
     * @return string
     */
    public function getBanId()
    {
        return $this->ban_id;
    }

    /**
     * Returns the value of field usu_id
     *
     * @return string
     */
    public function getUsuId()
    {
        return $this->usu_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->hasMany('id', 'Tel_egresos', 'cue_id', array('alias' => 'Tel_egresos'));
        $this->hasMany('id', 'Tel_pagos', 'cue_id', array('alias' => 'Tel_pagos'));
        $this->belongsTo('usu_id', 'Tel_usuarios', 'id', array('alias' => 'Tel_usuarios'));
        $this->belongsTo('ban_id', 'Tel_bancos', 'id', array('alias' => 'Tel_bancos'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_cuentas';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'cue_tipo' => 'cue_tipo', 
            'cue_numero' => 'cue_numero', 
            'cue_fec_creacion' => 'cue_fec_creacion', 
            'cue_estatus' => 'cue_estatus', 
            'ban_id' => 'ban_id', 
            'usu_id' => 'usu_id'
        );
    }

}
