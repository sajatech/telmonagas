<?php

class Envios extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false)
     */
    protected $id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $env_fecha;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $env_rif;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $env_nombre;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $env_empresa;

    /**
     *
     * @var string
     * @Column(type="string", length=53, nullable=true)
     */
    protected $env_monto;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $env_guia;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $env_email;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $env_detalle;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $env_estatus;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $env_correo_estatus;

    /**
     *
     * @var string
     */
    protected $usu_id;

    /**
     *
     * @var string
     */
    protected $env_monto_salida;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field usu_id
     *
     * @param string $usu_id
     * @return $this
     */
    public function setUsuId($usu_id)
    {
        $this->usu_id = $usu_id;

        return $this;
    }

    /**
     * Method to set the value of field env_fecha
     *
     * @param string $env_fecha
     * @return $this
     */
    public function setEnvFecha($env_fecha)
    {
        $this->env_fecha = $env_fecha;

        return $this;
    }

    /**
     * Method to set the value of field env_rif
     *
     * @param string $env_rif
     * @return $this
     */
    public function setEnvRif($env_rif)
    {
        $this->env_rif = $env_rif;

        return $this;
    }

    /**
     * Method to set the value of field env_nombre
     *
     * @param string $env_nombre
     * @return $this
     */
    public function setEnvNombre($env_nombre)
    {
        $this->env_nombre = $env_nombre;

        return $this;
    }

    /**
     * Method to set the value of field env_empresa
     *
     * @param string $env_empresa
     * @return $this
     */
    public function setEnvEmpresa($env_empresa)
    {
        $this->env_empresa = $env_empresa;

        return $this;
    }

    /**
     * Method to set the value of field env_monto
     *
     * @param string $env_monto
     * @return $this
     */
    public function setEnvMonto($env_monto)
    {
        $this->env_monto = $env_monto;

        return $this;
    }

    /**
     * Method to set the value of field env_guia
     *
     * @param string $env_guia
     * @return $this
     */
    public function setEnvGuia($env_guia)
    {
        $this->env_guia = $env_guia;

        return $this;
    }

    /**
     * Method to set the value of field env_email
     *
     * @param string $env_email
     * @return $this
     */
    public function setEnvEmail($env_email)
    {
        $this->env_email = $env_email;

        return $this;
    }

    /**
     * Method to set the value of field env_detalle
     *
     * @param string $env_detalle
     * @return $this
     */
    public function setEnvDetalle($env_detalle)
    {
        $this->env_detalle = $env_detalle;

        return $this;
    }

    /**
     * Method to set the value of field env_estatus
     *
     * @param string $env_estatus
     * @return $this
     */
    public function setEnvEstatus($env_estatus)
    {
        $this->env_estatus = $env_estatus;

        return $this;
    }

    /**
     * Method to set the value of field env_correo_estatus
     *
     * @param string $env_correo_estatus
     * @return $this
     */
    public function setEnvCorreoEstatus($env_correo_estatus)
    {
        $this->env_correo_estatus = $env_correo_estatus;

        return $this;
    }

     /**
     * Method to set the value of field env_monto_salida
     *
     * @param string $env_monto_salida
     * @return $this
     */
    public function setEnvMontoSalida($env_monto_salida)
    {
        $this->env_monto_salida = $env_monto_salida;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field env_fecha
     *
     * @return string
     */
    public function getEnvFecha()
    {
        return $this->env_fecha;
    }

    /**
     * Returns the value of field env_rif
     *
     * @return string
     */
    public function getEnvRif()
    {
        return $this->env_rif;
    }

    /**
     * Returns the value of field env_nombre
     *
     * @return string
     */
    public function getEnvNombre()
    {
        return $this->env_nombre;
    }

    /**
     * Returns the value of field env_empresa
     *
     * @return string
     */
    public function getEnvEmpresa()
    {
        return $this->env_empresa;
    }

    /**
     * Returns the value of field env_monto
     *
     * @return string
     */
    public function getEnvMonto()
    {
        return $this->env_monto;
    }

    /**
     * Returns the value of field env_guia
     *
     * @return string
     */
    public function getEnvGuia()
    {
        return $this->env_guia;
    }

    /**
     * Returns the value of field env_email
     *
     * @return string
     */
    public function getEnvEmail()
    {
        return $this->env_email;
    }

    /**
     * Returns the value of field env_detalle
     *
     * @return string
     */
    public function getEnvDetalle()
    {
        return $this->env_detalle;
    }

    /**
     * Returns the value of field env_estatus
     *
     * @return string
     */
    public function getEnvEstatus()
    {
        return $this->env_estatus;
    }

    /**
     * Returns the value of field env_correo_estatus
     *
     * @return string
     */
    public function getEnvCorreoEstatus()
    {
        return $this->env_correo_estatus;
    }

    /**
     * Returns the value of field env_monto_salida
     *
     * @return string
     */
    public function getEnvMontoSalida()
    {
        return $this->env_monto_salida;
    }

    /**
     * Returns the value of field usu_id
     *
     * @return string
     */
    public function getUsuId()
    {
        return $this->usu_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->belongsTo('usu_id', 'Tel_usuarios', 'id', array('alias' => 'Tel_usuarios'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_envios';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return TelEnvios[]|TelEnvios
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return TelEnvios
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
