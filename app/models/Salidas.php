<?php

class Salidas extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $sal_observaciones;

    /**
     *
     * @var string
     */
    protected $sal_fec_creacion;

    /**
     *
     * @var string
     */
    protected $sal_estatus;

    /**
     *
     * @var string
     */
    protected $cli_id;

    /**
     *
     * @var string
     */
    protected $usu_id;

    /**
     *
     * @var string
     */
    protected $dol_id;

    /**
     *
     * @var string
     */
    protected $sal_garantia;

    /**
     *
     * @var string
     */
    protected $sal_iva;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field sal_observaciones
     *
     * @param string $sal_observaciones
     * @return $this
     */
    public function setSalObservaciones($sal_observaciones)
    {
        $this->sal_observaciones = $sal_observaciones;

        return $this;
    }

    /**
     * Method to set the value of field sal_fec_creacion
     *
     * @param string $sal_fec_creacion
     * @return $this
     */
    public function setSalFecCreacion($sal_fec_creacion)
    {
        $this->sal_fec_creacion = $sal_fec_creacion;

        return $this;
    }

    /**
     * Method to set the value of field sal_estatus
     *
     * @param string $sal_estatus
     * @return $this
     */
    public function setSalEstatus($sal_estatus)
    {
        $this->sal_estatus = $sal_estatus;

        return $this;
    }

    /**
     * Method to set the value of field cli_id
     *
     * @param string $cli_id
     * @return $this
     */
    public function setCliId($cli_id)
    {
        $this->cli_id = $cli_id;

        return $this;
    }

    /**
     * Method to set the value of field usu_id
     *
     * @param string $usu_id
     * @return $this
     */
    public function setUsuId($usu_id)
    {
        $this->usu_id = $usu_id;

        return $this;
    }

    /**
     * Method to set the value of field dol_id
     *
     * @param string $dol_id
     * @return $this
     */
    public function setDolId($dol_id)
    {
        $this->dol_id = $dol_id;

        return $this;
    }

    /**
     * Method to set the value of field sal_garantia
     *
     * @param string $sal_garantia
     * @return $this
     */
    public function setSalGarantia($sal_garantia)
    {
        $this->sal_garantia = $sal_garantia;

        return $this;
    }

    /**
     * Method to set the value of field sal_iva
     *
     * @param string $sal_iva
     * @return $this
     */
    public function setSalIva($sal_iva)
    {
        $this->sal_iva = $sal_iva;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field sal_observaciones
     *
     * @return string
     */
    public function getSalObservaciones()
    {
        return $this->sal_observaciones;
    }

    /**
     * Returns the value of field sal_fec_creacion
     *
     * @return string
     */
    public function getSalFecCreacion()
    {
        return $this->sal_fec_creacion;
    }

    /**
     * Returns the value of field sal_estatus
     *
     * @return string
     */
    public function getSalEstatus()
    {
        return $this->sal_estatus;
    }

    /**
     * Returns the value of field cli_id
     *
     * @return string
     */
    public function getCliId()
    {
        return $this->cli_id;
    }

    /**
     * Returns the value of field usu_id
     *
     * @return string
     */
    public function getUsuId()
    {
        return $this->usu_id;
    }

    /**
     * Returns the value of field dol_id
     *
     * @return string
     */
    public function getDolId()
    {
        return $this->dol_id;
    }

    /**
     * Returns the value of field sal_garantia
     *
     * @return string
     */
    public function getSalGarantia()
    {
        return $this->sal_garantia;
    }

    /**
     * Returns the value of field sal_iva
     *
     * @return string
     */
    public function getSalIva()
    {
        return $this->sal_iva;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->hasMany('id', 'Tel_anulaciones', 'sal_id', array('alias' => 'Tel_anulaciones'));
        $this->hasMany('id', 'Tel_movimientos', 'sal_id', array('alias' => 'Tel_movimientos'));
        $this->hasMany('id', 'Tel_pagos', 'sal_id', array('alias' => 'Tel_pagos'));
        $this->belongsTo('cli_id', 'Tel_clientes', 'id', array('alias' => 'Tel_clientes'));
        $this->belongsTo('usu_id', 'Tel_usuarios', 'id', array('alias' => 'Tel_usuarios'));
        $this->belongsTo('dol_id', 'Tel_dolares', 'id', array('alias' => 'Tel_dolares'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_salidas';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'sal_observaciones' => 'sal_observaciones', 
            'sal_fec_creacion' => 'sal_fec_creacion', 
            'sal_estatus' => 'sal_estatus', 
            'cli_id' => 'cli_id', 
            'usu_id' => 'usu_id', 
            'dol_id' => 'dol_id', 
            'sal_garantia' => 'sal_garantia', 
            'sal_iva' => 'sal_iva'
        );
    }

}
