<?php

class Notas extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $not_detalle;

    /**
     *
     * @var string
     */
    protected $usu_id;

    /**
     *
     * @var string
     */
    protected $not_fec_creacion;

    /**
     *
     * @var string
     */
    protected $not_fecha;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field not_detalle
     *
     * @param string $not_detalle
     * @return $this
     */
    public function setNotDetalle($not_detalle)
    {
        $this->not_detalle = $not_detalle;

        return $this;
    }

    /**
     * Method to set the value of field usu_id
     *
     * @param string $usu_id
     * @return $this
     */
    public function setUsuId($usu_id)
    {
        $this->usu_id = $usu_id;

        return $this;
    }

    /**
     * Method to set the value of field not_fec_creacion
     *
     * @param string $not_fec_creacion
     * @return $this
     */
    public function setNotFecCreacion($not_fec_creacion)
    {
        $this->not_fec_creacion = $not_fec_creacion;

        return $this;
    }

    /**
     * Method to set the value of field not_fecha
     *
     * @param string $not_fecha
     * @return $this
     */
    public function setNotFecha($not_fecha)
    {
        $this->not_fecha = $not_fecha;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field not_detalle
     *
     * @return string
     */
    public function getNotDetalle()
    {
        return $this->not_detalle;
    }

    /**
     * Returns the value of field usu_id
     *
     * @return string
     */
    public function getUsuId()
    {
        return $this->usu_id;
    }

    /**
     * Returns the value of field not_fec_creacion
     *
     * @return string
     */
    public function getNotFecCreacion()
    {
        return $this->not_fec_creacion;
    }

    /**
     * Returns the value of field not_fecha
     *
     * @return string
     */
    public function getNotFecha()
    {
        return $this->not_fecha;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->belongsTo('usu_id', 'Tel_usuarios', 'id', array('alias' => 'Tel_usuarios'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_notas';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'not_detalle' => 'not_detalle', 
            'usu_id' => 'usu_id', 
            'not_fec_creacion' => 'not_fec_creacion', 
            'not_fecha' => 'not_fecha'
        );
    }

}
