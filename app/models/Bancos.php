<?php

class Bancos extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $ban_codigo;

    /**
     *
     * @var string
     */
    protected $ban_nombre;

    /**
     *
     * @var string
     */
    protected $ban_estatus;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field ban_codigo
     *
     * @param string $ban_codigo
     * @return $this
     */
    public function setBanCodigo($ban_codigo)
    {
        $this->ban_codigo = $ban_codigo;

        return $this;
    }

    /**
     * Method to set the value of field ban_nombre
     *
     * @param string $ban_nombre
     * @return $this
     */
    public function setBanNombre($ban_nombre)
    {
        $this->ban_nombre = $ban_nombre;

        return $this;
    }

    /**
     * Method to set the value of field ban_estatus
     *
     * @param string $ban_estatus
     * @return $this
     */
    public function setBanEstatus($ban_estatus)
    {
        $this->ban_estatus = $ban_estatus;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field ban_codigo
     *
     * @return string
     */
    public function getBanCodigo()
    {
        return $this->ban_codigo;
    }

    /**
     * Returns the value of field ban_nombre
     *
     * @return string
     */
    public function getBanNombre()
    {
        return $this->ban_nombre;
    }

    /**
     * Returns the value of field ban_estatus
     *
     * @return string
     */
    public function getBanEstatus()
    {
        return $this->ban_estatus;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->hasMany('id', 'Tel_cuentas', 'ban_id', array('alias' => 'Tel_cuentas'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_bancos';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'ban_codigo' => 'ban_codigo', 
            'ban_nombre' => 'ban_nombre', 
            'ban_estatus' => 'ban_estatus'
        );
    }

}
