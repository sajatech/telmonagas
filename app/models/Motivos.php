<?php

class Motivos extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $mot_nombre;

    /**
     *
     * @var string
     */
    protected $mot_fec_creacion;

    /**
     *
     * @var string
     */
    protected $mot_estatus;

    /**
     *
     * @var string
     */
    protected $usu_id;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field mot_nombre
     *
     * @param string $mot_nombre
     * @return $this
     */
    public function setMotNombre($mot_nombre)
    {
        $this->mot_nombre = $mot_nombre;

        return $this;
    }

    /**
     * Method to set the value of field mot_fec_creacion
     *
     * @param string $mot_fec_creacion
     * @return $this
     */
    public function setMotFecCreacion($mot_fec_creacion)
    {
        $this->mot_fec_creacion = $mot_fec_creacion;

        return $this;
    }

    /**
     * Method to set the value of field mot_estatus
     *
     * @param string $mot_estatus
     * @return $this
     */
    public function setMotEstatus($mot_estatus)
    {
        $this->mot_estatus = $mot_estatus;

        return $this;
    }

    /**
     * Method to set the value of field usu_id
     *
     * @param string $usu_id
     * @return $this
     */
    public function setUsuId($usu_id)
    {
        $this->usu_id = $usu_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field mot_nombre
     *
     * @return string
     */
    public function getMotNombre()
    {
        return $this->mot_nombre;
    }

    /**
     * Returns the value of field mot_fec_creacion
     *
     * @return string
     */
    public function getMotFecCreacion()
    {
        return $this->mot_fec_creacion;
    }

    /**
     * Returns the value of field mot_estatus
     *
     * @return string
     */
    public function getMotEstatus()
    {
        return $this->mot_estatus;
    }

    /**
     * Returns the value of field usu_id
     *
     * @return string
     */
    public function getUsuId()
    {
        return $this->usu_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->hasMany('id', 'Tel_movimientos', 'mot_id', array('alias' => 'Tel_movimientos'));
        $this->belongsTo('usu_id', 'Tel_usuarios', 'id', array('alias' => 'Tel_usuarios'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_motivos';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'mot_nombre' => 'mot_nombre', 
            'mot_fec_creacion' => 'mot_fec_creacion', 
            'mot_estatus' => 'mot_estatus', 
            'usu_id' => 'usu_id'
        );
    }

}
