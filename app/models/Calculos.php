<?php

class Calculos extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $cal_pre_unitario1;

    /**
     *
     * @var string
     */
    protected $cal_pre_unitario2;

    /**
     *
     * @var string
     */
    protected $cal_pre_unitario3;

    /**
     *
     * @var string
     */
    protected $cal_mer_libre;

    /**
     *
     * @var string
     */
    protected $cal_fec_creacion;

    /**
     *
     * @var string
     */
    protected $usu_id;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field cal_pre_unitario1
     *
     * @param string $cal_pre_unitario1
     * @return $this
     */
    public function setCalPreUnitario1($cal_pre_unitario1)
    {
        $this->cal_pre_unitario1 = $cal_pre_unitario1;

        return $this;
    }

    /**
     * Method to set the value of field cal_pre_unitario2
     *
     * @param string $cal_pre_unitario2
     * @return $this
     */
    public function setCalPreUnitario2($cal_pre_unitario2)
    {
        $this->cal_pre_unitario2 = $cal_pre_unitario2;

        return $this;
    }

    /**
     * Method to set the value of field cal_pre_unitario3
     *
     * @param string $cal_pre_unitario3
     * @return $this
     */
    public function setCalPreUnitario3($cal_pre_unitario3)
    {
        $this->cal_pre_unitario3 = $cal_pre_unitario3;

        return $this;
    }

    /**
     * Method to set the value of field cal_pre_unitario3
     *
     * @param string $cal_pre_unitario3
     * @return $this
     */
    public function setCalMerLibre($cal_mer_libre)
    {
        $this->cal_mer_libre = $cal_mer_libre;

        return $this;
    }

    /**
     * Method to set the value of field cal_fec_creacion
     *
     * @param string $cal_fec_creacion
     * @return $this
     */
    public function setCalFecCreacion($cal_fec_creacion)
    {
        $this->cal_fec_creacion = $cal_fec_creacion;

        return $this;
    }

    /**
     * Method to set the value of field usu_id
     *
     * @param string $usu_id
     * @return $this
     */
    public function setUsuId($usu_id)
    {
        $this->usu_id = $usu_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field cal_pre_unitario1
     *
     * @return string
     */
    public function getCalPreUnitario1()
    {
        return $this->cal_pre_unitario1;
    }

    /**
     * Returns the value of field cal_pre_unitario2
     *
     * @return string
     */
    public function getCalPreUnitario2()
    {
        return $this->cal_pre_unitario2;
    }

    /**
     * Returns the value of field cal_pre_unitario3
     *
     * @return string
     */
    public function getCalPreUnitario3()
    {
        return $this->cal_pre_unitario3;
    }

    /**
     * Returns the value of field cal_mer_libre
     *
     * @return string
     */
    public function getCalMerLibre()
    {
        return $this->cal_mer_libre;
    }

    /**
     * Returns the value of field cal_fec_creacion
     *
     * @return string
     */
    public function getCalFecCreacion()
    {
        return $this->cal_fec_creacion;
    }

    /**
     * Returns the value of field usu_id
     *
     * @return string
     */
    public function getUsuId()
    {
        return $this->usu_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->belongsTo('usu_id', 'Tel_usuarios', 'id', array('alias' => 'Tel_usuarios'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_calculos';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'cal_pre_unitario1' => 'cal_pre_unitario1', 
            'cal_pre_unitario2' => 'cal_pre_unitario2', 
            'cal_pre_unitario3' => 'cal_pre_unitario3', 
            'cal_mer_libre' => 'cal_mer_libre', 
            'cal_fec_creacion' => 'cal_fec_creacion', 
            'usu_id' => 'usu_id'
        );
    }

}
