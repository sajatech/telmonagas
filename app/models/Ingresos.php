<?php

class Ingresos extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $ing_observaciones;

    /**
     *
     * @var string
     */
    protected $ing_fec_creacion;

    /**
     *
     * @var string
     */
    protected $ing_estatus;

    /**
     *
     * @var string
     */
    protected $pro_id;

    /**
     *
     * @var string
     */
    protected $usu_id;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field ing_observaciones
     *
     * @param string $ing_observaciones
     * @return $this
     */
    public function setIngObservaciones($ing_observaciones)
    {
        $this->ing_observaciones = $ing_observaciones;

        return $this;
    }

    /**
     * Method to set the value of field ing_fec_creacion
     *
     * @param string $ing_fec_creacion
     * @return $this
     */
    public function setIngFecCreacion($ing_fec_creacion)
    {
        $this->ing_fec_creacion = $ing_fec_creacion;

        return $this;
    }

    /**
     * Method to set the value of field ing_estatus
     *
     * @param string $ing_estatus
     * @return $this
     */
    public function setIngEstatus($ing_estatus)
    {
        $this->ing_estatus = $ing_estatus;

        return $this;
    }

    /**
     * Method to set the value of field pro_id
     *
     * @param string $pro_id
     * @return $this
     */
    public function setProId($pro_id)
    {
        $this->pro_id = $pro_id;

        return $this;
    }

    /**
     * Method to set the value of field usu_id
     *
     * @param string $usu_id
     * @return $this
     */
    public function setUsuId($usu_id)
    {
        $this->usu_id = $usu_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field ing_observaciones
     *
     * @return string
     */
    public function getIngObservaciones()
    {
        return $this->ing_observaciones;
    }

    /**
     * Returns the value of field ing_fec_creacion
     *
     * @return string
     */
    public function getIngFecCreacion()
    {
        return $this->ing_fec_creacion;
    }

    /**
     * Returns the value of field ing_estatus
     *
     * @return string
     */
    public function getIngEstatus()
    {
        return $this->ing_estatus;
    }

    /**
     * Returns the value of field pro_id
     *
     * @return string
     */
    public function getProId()
    {
        return $this->pro_id;
    }

    /**
     * Returns the value of field usu_id
     *
     * @return string
     */
    public function getUsuId()
    {
        return $this->usu_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->hasMany('id', 'Tel_movimientos', 'ing_id', array('alias' => 'Tel_movimientos'));
        $this->belongsTo('pro_id', 'Tel_proveedores', 'id', array('alias' => 'Tel_proveedores'));
        $this->belongsTo('usu_id', 'Tel_usuarios', 'id', array('alias' => 'Tel_usuarios'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_ingresos';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'ing_observaciones' => 'ing_observaciones', 
            'ing_fec_creacion' => 'ing_fec_creacion', 
            'ing_estatus' => 'ing_estatus', 
            'pro_id' => 'pro_id', 
            'usu_id' => 'usu_id'
        );
    }

}
