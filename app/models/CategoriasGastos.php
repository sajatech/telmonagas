<?php

class CategoriasGastos extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     *
     * @var string
     * @Column(column="cat_nombre", type="string", nullable=false)
     */
    protected $cat_nombre;

    /**
     *
     * @var string
     * @Column(column="cat_estatus", type="string", nullable=false)
     */
    protected $cat_estatus;

    /**
     *
     * @var string
     * @Column(column="cat_fec_creacion", type="string", nullable=false)
     */
    protected $cat_fec_creacion;

    /**
     *
     * @var integer
     * @Column(column="usu_id", type="integer", nullable=false)
     */
    protected $usu_id;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field cat_nombre
     *
     * @param string $cat_nombre
     * @return $this
     */
    public function setCatNombre($cat_nombre)
    {
        $this->cat_nombre = $cat_nombre;

        return $this;
    }

    /**
     * Method to set the value of field cat_estatus
     *
     * @param string $cat_estatus
     * @return $this
     */
    public function setCatEstatus($cat_estatus)
    {
        $this->cat_estatus = $cat_estatus;

        return $this;
    }

    /**
     * Method to set the value of field cat_fec_creacion
     *
     * @param string $cat_fec_creacion
     * @return $this
     */
    public function setCatFecCreacion($cat_fec_creacion)
    {
        $this->cat_fec_creacion = $cat_fec_creacion;

        return $this;
    }

    /**
     * Method to set the value of field usu_id
     *
     * @param integer $usu_id
     * @return $this
     */
    public function setUsuId($usu_id)
    {
        $this->usu_id = $usu_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field cat_nombre
     *
     * @return string
     */
    public function getCatNombre()
    {
        return $this->cat_nombre;
    }

    /**
     * Returns the value of field cat_estatus
     *
     * @return string
     */
    public function getCatEstatus()
    {
        return $this->cat_estatus;
    }

    /**
     * Returns the value of field cat_fec_creacion
     *
     * @return string
     */
    public function getCatFecCreacion()
    {
        return $this->cat_fec_creacion;
    }

    /**
     * Returns the value of field usu_id
     *
     * @return integer
     */
    public function getUsuId()
    {
        return $this->usu_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->setSource("tel_categorias_gastos");
        $this->belongsTo('usu_id', '\TelUsuarios', 'id', ['alias' => 'TelUsuarios']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_categorias_gastos';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return TelCategoriasGastos[]|TelCategoriasGastos|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return TelCategoriasGastos|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
