<?php

class Articulos extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $art_serial;

    /**
     *
     * @var string
     */
    protected $art_codigo;

    /**
     *
     * @var string
     */
    protected $art_descripcion;

    /**
     *
     * @var string
     */
    protected $art_cos_unitario;

    /**
     *
     * @var string
     */
    protected $art_pre_unitario1;

    /**
     *
     * @var string
     */
    protected $art_pre_unitario2;

    /**
     *
     * @var string
     */
    protected $art_pre_unitario3;

    /**
     *
     * @var string
     */
    protected $art_pre_mer_libre;


    /**
     *
     * @var string
     */
    protected $art_inv_inicial;

    /**
     *
     * @var string
     */
    protected $art_fec_creacion;

    /**
     *
     * @var string
     */
    protected $art_sto_minimo;

    /**
     *
     * @var string
     */
    protected $art_ubicacion;

    /**
     *
     * @var string
     */
    protected $art_fot_referencial;

    /**
     *
     * @var string
     */
    protected $art_estatus;

    /**
     *
     * @var string
     */
    protected $usu_id;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field art_serial
     *
     * @param string $art_serial
     * @return $this
     */
    public function setArtSerial($art_serial)
    {
        $this->art_serial = $art_serial;

        return $this;
    }

    /**
     * Method to set the value of field art_codigo
     *
     * @param string $art_codigo
     * @return $this
     */
    public function setArtCodigo($art_codigo)
    {
        $this->art_codigo = $art_codigo;

        return $this;
    }

    /**
     * Method to set the value of field art_descripcion
     *
     * @param string $art_descripcion
     * @return $this
     */
    public function setArtDescripcion($art_descripcion)
    {
        $this->art_descripcion = $art_descripcion;

        return $this;
    }

    /**
     * Method to set the value of field art_cos_unitario
     *
     * @param string $art_cos_unitario
     * @return $this
     */
    public function setArtCosUnitario($art_cos_unitario)
    {
        $this->art_cos_unitario = $art_cos_unitario;

        return $this;
    }

    /**
     * Method to set the value of field art_pre_unitario1
     *
     * @param string $art_pre_unitario1
     * @return $this
     */
    public function setArtPreUnitario1($art_pre_unitario1)
    {
        $this->art_pre_unitario1 = $art_pre_unitario1;

        return $this;
    }

    /**
     * Method to set the value of field art_pre_unitario2
     *
     * @param string $art_pre_unitario2
     * @return $this
     */
    public function setArtPreUnitario2($art_pre_unitario2)
    {
        $this->art_pre_unitario2 = $art_pre_unitario2;

        return $this;
    }

    /**
     * Method to set the value of field art_pre_unitario3
     *
     * @param string $art_pre_unitario3
     * @return $this
     */
    public function setArtPreUnitario3($art_pre_unitario3)
    {
        $this->art_pre_unitario3 = $art_pre_unitario3;

        return $this;
    }

    /**
     * Method to set the value of field   art_pre_mer_libre
     *
     * @param string $art_pre_mer_libre
     * @return $this
     */
    public function setArtPreMerLibre($art_pre_mer_libre)
    {
        $this->art_pre_mer_libre = $art_pre_mer_libre;

        return $this;
    }

    /**
     * Method to set the value of field art_inv_inicial
     *
     * @param string $art_inv_inicial
     * @return $this
     */
    public function setArtInvInicial($art_inv_inicial)
    {
        $this->art_inv_inicial = empty($art_inv_inicial) ? null : $art_inv_inicial;
        
        return $this;
    }

    /**
     * Method to set the value of field art_fec_creacion
     *
     * @param string $art_fec_creacion
     * @return $this
     */
    public function setArtFecCreacion($art_fec_creacion)
    {
        $this->art_fec_creacion = $art_fec_creacion;

        return $this;
    }

    /**
     * Method to set the value of field art_sto_minimo
     *
     * @param string $art_sto_minimo
     * @return $this
     */
    public function setArtStoMinimo($art_sto_minimo)
    {
        $this->art_sto_minimo = empty($art_sto_minimo) ? null : $art_sto_minimo;

        return $this;
    }

    /**
     * Method to set the value of field art_ubicacion
     *
     * @param string $art_ubicacion
     * @return $this
     */
    public function setArtUbicacion($art_ubicacion)
    {
        $this->art_ubicacion = $art_ubicacion;

        return $this;
    }

    /**
     * Method to set the value of field art_fot_referencial
     *
     * @param string $art_fot_referencial
     * @return $this
     */
    public function setArtFotReferencial($art_fot_referencial)
    {
        $this->art_fot_referencial = $art_fot_referencial;

        return $this;
    }

    /**
     * Method to set the value of field art_estatus
     *
     * @param string $art_estatus
     * @return $this
     */
    public function setArtEstatus($art_estatus)
    {
        $this->art_estatus = $art_estatus;

        return $this;
    }

    /**
     * Method to set the value of field usu_id
     *
     * @param string $usu_id
     * @return $this
     */
    public function setUsuId($usu_id)
    {
        $this->usu_id = $usu_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field art_serial
     *
     * @return string
     */
    public function getArtSerial()
    {
        return $this->art_serial;
    }

    /**
     * Returns the value of field art_codigo
     *
     * @return string
     */
    public function getArtCodigo()
    {
        return $this->art_codigo;
    }

    /**
     * Returns the value of field art_descripcion
     *
     * @return string
     */
    public function getArtDescripcion()
    {
        return $this->art_descripcion;
    }

    /**
     * Returns the value of field art_cos_unitario
     *
     * @return string
     */
    public function getArtCosUnitario()
    {
        return $this->art_cos_unitario;
    }

    /**
     * Returns the value of field art_pre_unitario1
     *
     * @return string
     */
    public function getArtPreUnitario1()
    {
        return $this->art_pre_unitario1;
    }

    /**
     * Returns the value of field art_pre_unitario2
     *
     * @return string
     */
    public function getArtPreUnitario2()
    {
        return $this->art_pre_unitario2;
    }

    /**
     * Returns the value of field art_pre_unitario3
     *
     * @return string
     */
    public function getArtPreUnitario3()
    {
        return $this->art_pre_unitario3;
    }

    /**
     * Returns the value of field art_pre_mer_libre
     *
     * @return string
     */
    public function getArtPreMerLibre()
    {
        return $this->art_pre_mer_libre;
    }

    /**
     * Returns the value of field art_inv_inicial
     *
     * @return string
     */
    public function getArtInvInicial()
    {
        return $this->art_inv_inicial;
    }

    /**
     * Returns the value of field art_fec_creacion
     *
     * @return string
     */
    public function getArtFecCreacion()
    {
        return $this->art_fec_creacion;
    }

    /**
     * Returns the value of field art_sto_minimo
     *
     * @return string
     */
    public function getArtStoMinimo()
    {
        return $this->art_sto_minimo;
    }

    /**
     * Returns the value of field art_ubicacion
     *
     * @return string
     */
    public function getArtUbicacion()
    {
        return $this->art_ubicacion;
    }

    /**
     * Returns the value of field art_fot_referencial
     *
     * @return string
     */
    public function getArtFotReferencial()
    {
        return $this->art_fot_referencial;
    }

    /**
     * Returns the value of field art_estatus
     *
     * @return string
     */
    public function getArtEstatus()
    {
        return $this->art_estatus;
    }

    /**
     * Returns the value of field usu_id
     *
     * @return string
     */
    public function getUsuId()
    {
        return $this->usu_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->hasMany('id', 'Tel_movimientos', 'art_id', array('alias' => 'Tel_movimientos'));
        $this->belongsTo('usu_id', 'Tel_usuarios', 'id', array('alias' => 'Tel_usuarios'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_articulos';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'art_serial' => 'art_serial', 
            'art_codigo' => 'art_codigo', 
            'art_descripcion' => 'art_descripcion', 
            'art_cos_unitario' => 'art_cos_unitario', 
            'art_pre_unitario1' => 'art_pre_unitario1', 
            'art_pre_unitario2' => 'art_pre_unitario2', 
            'art_pre_unitario3' => 'art_pre_unitario3', 
            'art_pre_mer_libre' => 'art_pre_mer_libre', 
            'art_inv_inicial' => 'art_inv_inicial', 
            'art_fec_creacion' => 'art_fec_creacion', 
            'art_sto_minimo' => 'art_sto_minimo', 
            'art_ubicacion' => 'art_ubicacion', 
            'art_fot_referencial' => 'art_fot_referencial', 
            'art_estatus' => 'art_estatus', 
            'usu_id' => 'usu_id'
        );
    }

}
