<?php

class Accesos extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $acc_fecha;

    /**
     *
     * @var string
     */
    protected $acc_ip;

    /**
     *
     * @var string
     */
    protected $usu_id;

    /**
     *
     * @var string
     */
    protected $acc_detalles;

    /**
     *
     * @var string
     */
    protected $acc_accion;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field acc_fecha
     *
     * @param string $acc_fecha
     * @return $this
     */
    public function setAccFecha($acc_fecha)
    {
        $this->acc_fecha = $acc_fecha;

        return $this;
    }

    /**
     * Method to set the value of field acc_ip
     *
     * @param string $acc_ip
     * @return $this
     */
    public function setAccIp($acc_ip)
    {
        $this->acc_ip = $acc_ip;

        return $this;
    }

    /**
     * Method to set the value of field usu_id
     *
     * @param string $usu_id
     * @return $this
     */
    public function setUsuId($usu_id)
    {
        $this->usu_id = $usu_id;

        return $this;
    }

    /**
     * Method to set the value of field acc_detalles
     *
     * @param string $acc_detalles
     * @return $this
     */
    public function setAccDetalles($acc_detalles)
    {
        $this->acc_detalles = $acc_detalles;

        return $this;
    }

        /**
     * Method to set the value of field acc_detalles
     *
     * @param string $acc_detalles
     * @return $this
     */
    public function setAccAccion($acc_accion)
    {
        $this->acc_accion = $acc_accion;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field acc_fecha
     *
     * @return string
     */
    public function getAccFecha()
    {
        return $this->acc_fecha;
    }

    /**
     * Returns the value of field acc_ip
     *
     * @return string
     */
    public function getAccIp()
    {
        return $this->acc_ip;
    }

    /**
     * Returns the value of field usu_id
     *
     * @return string
     */
    public function getUsuId()
    {
        return $this->usu_id;
    }

    /**
     * Returns the value of field acc_detalles
     *
     * @return string
     */
    public function getAccDetalles()
    {
        return $this->acc_detalles;
    }

    /**
     * Returns the value of field acc_detalles
     *
     * @return string
     */
    public function getAccAccion()
    {
        return $this->acc_accion;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->belongsTo('usu_id', 'Usuarios', 'id', array('alias' => 'Usuarios'));
        $this->belongsTo('usu_id', 'Usuarios', 'id', array('foreignKey' => true));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_accesos';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return HimAccesos[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return HimAccesos
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'acc_fecha' => 'acc_fecha',
            'acc_ip' => 'acc_ip',
            'acc_detalles' => 'acc_detalles',
            'usu_id' => 'usu_id',
            'acc_accion' => 'acc_accion',
        );
    }

}
