<?php

class FacturasInt extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $fac_monto;

    /**
     *
     * @var string
     */
    protected $fac_monto_flete;

    /**
     *
     * @var string
     */
    protected $fac_numero;

    /**
     *
     * @var string
     */
    protected $fac_peso;

    /**
     *
     * @var string
     */
    protected $fac_bultos;

    /**
     *
     * @var string
     */
    protected $fac_aduana_bs;

    /**
     *
     * @var string
     */
    protected $fac_transporte_nac;

    /**
     *
     * @var string
     */
    protected $fac_tasa_dolar;

    /**
     *
     * @var string
     */
    protected $fac_fecha;

    /**
     *
     * @var string
     */
    protected $fac_observacion;

    /**
     *
     * @var string
     */
    protected $fac_gastos_ope;

    /**
     *
     * @var string
     */
    protected $fac_imp1;

    /**
     *
     * @var string
     */
    protected $fac_imp2;

    /**
     *
     * @var string
     */
    protected $fac_imp3;

    /**
     *
     * @var string
     */
    protected $fac_peso_real;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field fac_monto
     *
     * @param string $fac_monto
     * @return $this
     */
    public function setFacMonto($fac_monto)
    {
        $this->fac_monto = $fac_monto;

        return $this;
    }

    /**
     * Method to set the value of field fac_monto_flete
     *
     * @param string $fac_monto_flete
     * @return $this
     */
    public function setFacMontoFlete($fac_monto_flete)
    {
        $this->fac_monto_flete = $fac_monto_flete;

        return $this;
    }

    /**
     * Method to set the value of field fac_numero
     *
     * @param string $fac_numero
     * @return $this
     */
    public function setFacNumero($fac_numero)
    {
        $this->fac_numero = $fac_numero;

        return $this;
    }

    /**
     * Method to set the value of field fac_peso
     *
     * @param string $fac_peso
     * @return $this
     */
    public function setFacPeso($fac_peso)
    {
        $this->fac_peso = $fac_peso;

        return $this;
    }

    /**
     * Method to set the value of field fac_bultos
     *
     * @param string $fac_bultos
     * @return $this
     */
    public function setFacBultos($fac_bultos)
    {
        $this->fac_bultos = $fac_bultos;

        return $this;
    }

    /**
     * Method to set the value of field fac_aduana_bs
     *
     * @param string $fac_aduana_bs
     * @return $this
     */
    public function setFacAduanaBs($fac_aduana_bs)
    {
        $this->fac_aduana_bs = $fac_aduana_bs;

        return $this;
    }

    /**
     * Method to set the value of field fac_transporte_nac
     *
     * @param string $fac_transporte_nac
     * @return $this
     */
    public function setFacTransporteNac($fac_transporte_nac)
    {
        $this->fac_transporte_nac = $fac_transporte_nac;

        return $this;
    }

    /**
     * Method to set the value of field fac_tasa_dolar
     *
     * @param string $fac_tasa_dolar
     * @return $this
     */
    public function setFacTasaDolar($fac_tasa_dolar)
    {
        $this->fac_tasa_dolar = $fac_tasa_dolar;

        return $this;
    }

    /**
     * Method to set the value of field fac_fecha
     *
     * @param string $fac_fecha
     * @return $this
     */
    public function setFacFecha($fac_fecha)
    {
        $this->fac_fecha = $fac_fecha;

        return $this;
    }

    /**
     * Method to set the value of field fac_observacion
     *
     * @param string $fac_observacion
     * @return $this
     */
    public function setFacObservacion($fac_observacion)
    {
        $this->fac_observacion = $fac_observacion;

        return $this;
    }

    public function setFacGastosOpe($fac_gastos_ope)
    {
        $this->fac_gastos_ope = $fac_gastos_ope;

        return $this;
    }

    public function setFacImp1($fac_imp1)
    {
        $this->fac_imp1 = $fac_imp1;

        return $this;
    }

    public function setFacImp2($fac_imp2)
    {
        $this->fac_imp2 = $fac_imp2;

        return $this;
    }

    public function setFacImp3($fac_imp3)
    {
        $this->fac_imp3 = $fac_imp3;

        return $this;
    }

    public function setFacPesoReal($fac_peso_real)
    {
        $this->fac_peso_real = $fac_peso_real;

        return $this;
    }

    
    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field fac_monto
     *
     * @return string
     */
    public function getFacMonto()
    {
        return $this->fac_monto;
    }

    /**
     * Returns the value of field fac_monto_flete
     *
     * @return string
     */
    public function getFacMontoFlete()
    {
        return $this->fac_monto_flete;
    }

    /**
     * Returns the value of field fac_numero
     *
     * @return string
     */
    public function getFacNumero()
    {
        return $this->fac_numero;
    }

    /**
     * Returns the value of field fac_peso
     *
     * @return string
     */
    public function getFacPeso()
    {
        return $this->fac_peso;
    }

    /**
     * Returns the value of field fac_bultos
     *
     * @return string
     */
    public function getFacBultos()
    {
        return $this->fac_bultos;
    }

    /**
     * Returns the value of field fac_aduana_bs
     *
     * @return string
     */
    public function getFacAduanaBs()
    {
        return $this->fac_aduana_bs;
    }

    /**
     * Returns the value of field fac_transporte_nac
     *
     * @return string
     */
    public function getFacTransporteNac()
    {
        return $this->fac_transporte_nac;
    }

    /**
     * Returns the value of field fac_tasa_dolar
     *
     * @return string
     */
    public function getFacTasaDolar()
    {
        return $this->fac_tasa_dolar;
    }

    /**
     * Returns the value of field fac_fecha
     *
     * @return string
     */
    public function getFacFecha()
    {
        return $this->fac_fecha;
    }

    /**
     * Returns the value of field fac_observacion
     *
     * @return string
     */
    public function getFacObservacion()
    {
        return $this->fac_observacion;
    }

    /**
     * Returns the value of field fac_observacion
     *
     * @return string
     */
    public function getFacGastosOpe()
    {
        return $this->fac_gastos_ope;
    }

    /**
     * Returns the value of field fac_observacion
     *
     * @return string
     */
    public function getFacImp1()
    {
        return $this->fac_imp1;
    }

    /**
     * Returns the value of field fac_observacion
     *
     * @return string
     */
    public function getFacImp2()
    {
        return $this->fac_imp2;
    }

    /**
     * Returns the value of field fac_observacion
     *
     * @return string
     */
    public function getFacImp3()
    {
        return $this->fac_imp3;
    }

    /**
     * Returns the value of field fac_peso_real
     *
     * @return string
     */
    public function getFacPesoReal()
    {
        return $this->fac_peso_real;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_facturas_int';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'fac_monto' => 'fac_monto', 
            'fac_monto_flete' => 'fac_monto_flete', 
            'fac_numero' => 'fac_numero', 
            'fac_peso' => 'fac_peso', 
            'fac_bultos' => 'fac_bultos', 
            'fac_aduana_bs' => 'fac_aduana_bs', 
            'fac_transporte_nac' => 'fac_transporte_nac', 
            'fac_tasa_dolar' => 'fac_tasa_dolar', 
            'fac_fecha' => 'fac_fecha', 
            'fac_observacion' => 'fac_observacion',
            'fac_gastos_ope' => 'fac_gastos_ope', 
            'fac_imp1' => 'fac_imp1', 
            'fac_imp2' => 'fac_imp2', 
            'fac_imp3' => 'fac_imp3',
            'fac_peso_real' => 'fac_peso_real'
        );
    }

}
