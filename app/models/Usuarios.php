<?php

class Usuarios extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     *
     * @var string
     * @Column(column="usu_codigo", type="string", length=255, nullable=false)
     */
    protected $usu_codigo;

    /**
     *
     * @var string
     * @Column(column="usu_nombre", type="string", length=255, nullable=false)
     */
    protected $usu_nombre;

    /**
     *
     * @var string
     * @Column(column="usu_cor_electronico", type="string", length=255, nullable=true)
     */
    protected $usu_cor_electronico;

    /**
     *
     * @var string
     * @Column(column="usu_telefono", type="string", length=255, nullable=true)
     */
    protected $usu_telefono;

    /**
     *
     * @var string
     * @Column(column="usu_fec_nacimiento", type="string", nullable=true)
     */
    protected $usu_fec_nacimiento;

    /**
     *
     * @var string
     * @Column(column="usu_cargo", type="string", length=255, nullable=true)
     */
    protected $usu_cargo;

    /**
     *
     * @var string
     * @Column(column="usu_nom_usuario", type="string", length=255, nullable=false)
     */
    protected $usu_nom_usuario;

    /**
     *
     * @var string
     * @Column(column="usu_contrasena", type="string", length=255, nullable=false)
     */
    protected $usu_contrasena;

    /**
     *
     * @var string
     * @Column(column="usu_estatus", type="string", length=255, nullable=false)
     */
    protected $usu_estatus;

    /**
     *
     * @var string
     * @Column(column="usu_fec_creacion", type="string", nullable=false)
     */
    protected $usu_fec_creacion;

    /**
     *
     * @var integer
     * @Column(column="rol_id", type="integer", nullable=false)
     */
    protected $rol_id;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field usu_codigo
     *
     * @param string $usu_codigo
     * @return $this
     */
    public function setUsuCodigo($usu_codigo)
    {
        $this->usu_codigo = $usu_codigo;

        return $this;
    }

    /**
     * Method to set the value of field usu_nombre
     *
     * @param string $usu_nombre
     * @return $this
     */
    public function setUsuNombre($usu_nombre)
    {
        $this->usu_nombre = $usu_nombre;

        return $this;
    }

    /**
     * Method to set the value of field usu_cor_electronico
     *
     * @param string $usu_cor_electronico
     * @return $this
     */
    public function setUsuCorElectronico($usu_cor_electronico)
    {
        $this->usu_cor_electronico = $usu_cor_electronico;

        return $this;
    }

    /**
     * Method to set the value of field usu_telefono
     *
     * @param string $usu_telefono
     * @return $this
     */
    public function setUsuTelefono($usu_telefono)
    {
        $this->usu_telefono = $usu_telefono;

        return $this;
    }

    /**
     * Method to set the value of field usu_fec_nacimiento
     *
     * @param string $usu_fec_nacimiento
     * @return $this
     */
    public function setUsuFecNacimiento($usu_fec_nacimiento)
    {
        $this->usu_fec_nacimiento = $usu_fec_nacimiento;

        return $this;
    }

    /**
     * Method to set the value of field usu_cargo
     *
     * @param string $usu_cargo
     * @return $this
     */
    public function setUsuCargo($usu_cargo)
    {
        $this->usu_cargo = $usu_cargo;

        return $this;
    }

    /**
     * Method to set the value of field usu_nom_usuario
     *
     * @param string $usu_nom_usuario
     * @return $this
     */
    public function setUsuNomUsuario($usu_nom_usuario)
    {
        $this->usu_nom_usuario = $usu_nom_usuario;

        return $this;
    }

    /**
     * Method to set the value of field usu_contrasena
     *
     * @param string $usu_contrasena
     * @return $this
     */
    public function setUsuContrasena($usu_contrasena)
    {
        $this->usu_contrasena = $usu_contrasena;

        return $this;
    }

    /**
     * Method to set the value of field usu_estatus
     *
     * @param string $usu_estatus
     * @return $this
     */
    public function setUsuEstatus($usu_estatus)
    {
        $this->usu_estatus = $usu_estatus;

        return $this;
    }

    /**
     * Method to set the value of field usu_fec_creacion
     *
     * @param string $usu_fec_creacion
     * @return $this
     */
    public function setUsuFecCreacion($usu_fec_creacion)
    {
        $this->usu_fec_creacion = $usu_fec_creacion;

        return $this;
    }

    /**
     * Method to set the value of field rol_id
     *
     * @param integer $rol_id
     * @return $this
     */
    public function setRolId($rol_id)
    {
        $this->rol_id = $rol_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field usu_codigo
     *
     * @return string
     */
    public function getUsuCodigo()
    {
        return $this->usu_codigo;
    }

    /**
     * Returns the value of field usu_nombre
     *
     * @return string
     */
    public function getUsuNombre()
    {
        return $this->usu_nombre;
    }

    /**
     * Returns the value of field usu_cor_electronico
     *
     * @return string
     */
    public function getUsuCorElectronico()
    {
        return $this->usu_cor_electronico;
    }

    /**
     * Returns the value of field usu_telefono
     *
     * @return string
     */
    public function getUsuTelefono()
    {
        return $this->usu_telefono;
    }

    /**
     * Returns the value of field usu_fec_nacimiento
     *
     * @return string
     */
    public function getUsuFecNacimiento()
    {
        return $this->usu_fec_nacimiento;
    }

    /**
     * Returns the value of field usu_cargo
     *
     * @return string
     */
    public function getUsuCargo()
    {
        return $this->usu_cargo;
    }

    /**
     * Returns the value of field usu_nom_usuario
     *
     * @return string
     */
    public function getUsuNomUsuario()
    {
        return $this->usu_nom_usuario;
    }

    /**
     * Returns the value of field usu_contrasena
     *
     * @return string
     */
    public function getUsuContrasena()
    {
        return $this->usu_contrasena;
    }

    /**
     * Returns the value of field usu_estatus
     *
     * @return string
     */
    public function getUsuEstatus()
    {
        return $this->usu_estatus;
    }

    /**
     * Returns the value of field usu_fec_creacion
     *
     * @return string
     */
    public function getUsuFecCreacion()
    {
        return $this->usu_fec_creacion;
    }

    /**
     * Returns the value of field rol_id
     *
     * @return integer
     */
    public function getRolId()
    {
        return $this->rol_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->setSource("tel_usuarios");
        $this->hasMany('id', 'TelAjustes', 'usu_id', ['alias' => 'TelAjustes']);
        $this->hasMany('id', 'TelAnulaciones', 'usu_id', ['alias' => 'TelAnulaciones']);
        $this->hasMany('id', 'TelArticulos', 'usu_id', ['alias' => 'TelArticulos']);
        $this->hasMany('id', 'TelCalculos', 'usu_id', ['alias' => 'TelCalculos']);
        $this->hasMany('id', 'TelCategoriasGastos', 'usu_id', ['alias' => 'TelCategoriasGastos']);
        $this->hasMany('id', 'TelClientes', 'usu_id', ['alias' => 'TelClientes']);
        $this->hasMany('id', 'TelContactos', 'usu_id', ['alias' => 'TelContactos']);
        $this->hasMany('id', 'TelCuentas', 'usu_id', ['alias' => 'TelCuentas']);
        $this->hasMany('id', 'TelDolares', 'usu_id', ['alias' => 'TelDolares']);
        $this->hasMany('id', 'TelEgresos', 'usu_id', ['alias' => 'TelEgresos']);
        $this->hasMany('id', 'TelEnvios', 'usu_id', ['alias' => 'TelEnvios']);
        $this->hasMany('id', 'TelGastos', 'usu_id', ['alias' => 'TelGastos']);
        $this->hasMany('id', 'TelIngresos', 'usu_id', ['alias' => 'TelIngresos']);
        $this->hasMany('id', 'TelMotivos', 'usu_id', ['alias' => 'TelMotivos']);
        $this->hasMany('id', 'TelNotas', 'usu_id', ['alias' => 'TelNotas']);
        $this->hasMany('id', 'TelProveedores', 'usu_id', ['alias' => 'TelProveedores']);
        $this->hasMany('id', 'TelRoles', 'usu_id', ['alias' => 'TelRoles']);
        $this->hasMany('id', 'TelSalidas', 'usu_id', ['alias' => 'TelSalidas']);
        $this->hasMany('id', 'TelSeriales', 'usu_id', ['alias' => 'TelSeriales']);
        $this->belongsTo('rol_id', '\TelRoles', 'id', ['alias' => 'TelRoles']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_usuarios';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return TelUsuarios[]|TelUsuarios|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return TelUsuarios|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
