<?php

class Promociones extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $pro_asunto;

    /**
     *
     * @var string
     */
    protected $pro_cuerpo;

    /**
     *
     * @var string
     */
    protected $pro_fecha;

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $pro_estatus;

     /**
     *
     * @var string
     */
    protected $pro_fecha_fin;

    /**
     * Method to set the value of field pro_asunto
     *
     * @param string $pro_asunto
     * @return $this
     */
    public function setProAsunto($pro_asunto)
    {
        $this->pro_asunto = $pro_asunto;

        return $this;
    }

    /**
     * Method to set the value of field pro_cuerpo
     *
     * @param string $pro_cuerpo
     * @return $this
     */
    public function setProCuerpo($pro_cuerpo)
    {
        $this->pro_cuerpo = $pro_cuerpo;

        return $this;
    }

    /**
     * Method to set the value of field pro_fecha
     *
     * @param string $pro_fecha
     * @return $this
     */
    public function setProFecha($pro_fecha)
    {
        $this->pro_fecha = $pro_fecha;

        return $this;
    }

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field pro_estatus
     *
     * @param string $pro_estatus
     * @return $this
     */
    public function setProEstatus($pro_estatus)
    {
        $this->pro_estatus = $pro_estatus;

        return $this;
    }

    /**
     * Method to set the value of field pro_estatus
     *
     * @param string $pro_estatus
     * @return $this
     */
    public function setProFechaFin($pro_fecha_fin)
    {
        $this->pro_fecha_fin = $pro_fecha_fin;

        return $this;
    }

    /**
     * Returns the value of field pro_asunto
     *
     * @return string
     */
    public function getProAsunto()
    {
        return $this->pro_asunto;
    }

    /**
     * Returns the value of field pro_cuerpo
     *
     * @return string
     */
    public function getProCuerpo()
    {
        return $this->pro_cuerpo;
    }

    /**
     * Returns the value of field pro_fecha
     *
     * @return string
     */
    public function getProFecha()
    {
        return $this->pro_fecha;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field pro_estatus
     *
     * @return string
     */
    public function getProEstatus()
    {
        return $this->pro_estatus;
    }

    /**
     * Returns the value of field pro_estatus
     *
     * @return string
     */
    public function getProFechaFin()
    {
        return $this->pro_fecha_fin;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->setSource("tel_promociones");
        $this->hasMany('id', 'PromocionCliente', 'pro_id', ['alias' => 'PromocionCliente']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_promociones';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return TelPromociones[]|TelPromociones|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return TelPromociones|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
