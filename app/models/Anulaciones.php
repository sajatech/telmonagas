<?php

class Anulaciones extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $anu_motivo;

    /**
     *
     * @var string
     */
    protected $sal_id;

    /**
     *
     * @var string
     */
    protected $usu_id;

    /**
     *
     * @var string
     */
    protected $anu_fec_creacion;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field anu_motivo
     *
     * @param string $anu_motivo
     * @return $this
     */
    public function setAnuMotivo($anu_motivo)
    {
        $this->anu_motivo = $anu_motivo;

        return $this;
    }

    /**
     * Method to set the value of field sal_id
     *
     * @param string $sal_id
     * @return $this
     */
    public function setSalId($sal_id)
    {
        $this->sal_id = $sal_id;

        return $this;
    }

    /**
     * Method to set the value of field usu_id
     *
     * @param string $usu_id
     * @return $this
     */
    public function setUsuId($usu_id)
    {
        $this->usu_id = $usu_id;

        return $this;
    }

    /**
     * Method to set the value of field anu_fec_creacion
     *
     * @param string $anu_fec_creacion
     * @return $this
     */
    public function setAnuFecCreacion($anu_fec_creacion)
    {
        $this->anu_fec_creacion = $anu_fec_creacion;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field anu_motivo
     *
     * @return string
     */
    public function getAnuMotivo()
    {
        return $this->anu_motivo;
    }

    /**
     * Returns the value of field sal_id
     *
     * @return string
     */
    public function getSalId()
    {
        return $this->sal_id;
    }

    /**
     * Returns the value of field usu_id
     *
     * @return string
     */
    public function getUsuId()
    {
        return $this->usu_id;
    }

    /**
     * Returns the value of field anu_fec_creacion
     *
     * @return string
     */
    public function getAnuFecCreacion()
    {
        return $this->anu_fec_creacion;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->belongsTo('usu_id', 'Tel_usuarios', 'id', array('alias' => 'Tel_usuarios'));
        $this->belongsTo('sal_id', 'Tel_salidas', 'id', array('alias' => 'Tel_salidas'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_anulaciones';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'anu_motivo' => 'anu_motivo', 
            'sal_id' => 'sal_id', 
            'usu_id' => 'usu_id', 
            'anu_fec_creacion' => 'anu_fec_creacion'
        );
    }

}
