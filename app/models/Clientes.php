<?php

class Clientes extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $cli_codigo;

    /**
     *
     * @var string
     */
    protected $cli_nombre;

    /**
     *
     * @var string
     */
    protected $cli_tel_local;

    /**
     *
     * @var string
     */
    protected $cli_tel_celular;

    /**
     *
     * @var string
     */
    protected $cli_direccion;

    /**
     *
     * @var string
     */
    protected $cli_cor_electronico;

    /**
     *
     * @var string
     */
    protected $cli_estatus;

    /**
     *
     * @var string
     */
    protected $cli_fec_creacion;

    /**
     *
     * @var string
     */
    protected $usu_id;


    /**
     *
     * @var string
     */
    protected $solo_promo;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field cli_codigo
     *
     * @param string $cli_codigo
     * @return $this
     */
    public function setCliCodigo($cli_codigo)
    {
        $this->cli_codigo = $cli_codigo;

        return $this;
    }

    /**
     * Method to set the value of field cli_nombre
     *
     * @param string $cli_nombre
     * @return $this
     */
    public function setCliNombre($cli_nombre)
    {
        $this->cli_nombre = $cli_nombre;

        return $this;
    }

    /**
     * Method to set the value of field cli_tel_local
     *
     * @param string $cli_tel_local
     * @return $this
     */
    public function setCliTelLocal($cli_tel_local)
    {
        $this->cli_tel_local = $cli_tel_local;

        return $this;
    }

    /**
     * Method to set the value of field cli_tel_celular
     *
     * @param string $cli_tel_celular
     * @return $this
     */
    public function setCliTelCelular($cli_tel_celular)
    {
        $this->cli_tel_celular = $cli_tel_celular;

        return $this;
    }

    /**
     * Method to set the value of field cli_direccion
     *
     * @param string $cli_direccion
     * @return $this
     */
    public function setCliDireccion($cli_direccion)
    {
        $this->cli_direccion = $cli_direccion;

        return $this;
    }

    /**
     * Method to set the value of field cli_cor_electronico
     *
     * @param string $cli_cor_electronico
     * @return $this
     */
    public function setCliCorElectronico($cli_cor_electronico)
    {
        $this->cli_cor_electronico = $cli_cor_electronico;

        return $this;
    }

    /**
     * Method to set the value of field cli_estatus
     *
     * @param string $cli_estatus
     * @return $this
     */
    public function setCliEstatus($cli_estatus)
    {
        $this->cli_estatus = $cli_estatus;

        return $this;
    }

    /**
     * Method to set the value of field cli_fec_creacion
     *
     * @param string $cli_fec_creacion
     * @return $this
     */
    public function setCliFecCreacion($cli_fec_creacion)
    {
        $this->cli_fec_creacion = $cli_fec_creacion;

        return $this;
    }

    /**
     * Method to set the value of field usu_id
     *
     * @param string $usu_id
     * @return $this
     */
    public function setUsuId($usu_id)
    {
        $this->usu_id = $usu_id;

        return $this;
    }

    /**
     * Method to set the value of field usu_id
     *
     * @param string $usu_id
     * @return $this
     */
    public function setSoloPromo($solo_promo)
    {
        $this->solo_promo = $solo_promo;

        return $this;
    }
    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field cli_codigo
     *
     * @return string
     */
    public function getCliCodigo()
    {
        return $this->cli_codigo;
    }

    /**
     * Returns the value of field cli_nombre
     *
     * @return string
     */
    public function getCliNombre()
    {
        return $this->cli_nombre;
    }

    /**
     * Returns the value of field cli_tel_local
     *
     * @return string
     */
    public function getCliTelLocal()
    {
        return $this->cli_tel_local;
    }

    /**
     * Returns the value of field cli_tel_celular
     *
     * @return string
     */
    public function getCliTelCelular()
    {
        return $this->cli_tel_celular;
    }

    /**
     * Returns the value of field cli_direccion
     *
     * @return string
     */
    public function getCliDireccion()
    {
        return $this->cli_direccion;
    }

    /**
     * Returns the value of field cli_cor_electronico
     *
     * @return string
     */
    public function getCliCorElectronico()
    {
        return $this->cli_cor_electronico;
    }

    /**
     * Returns the value of field cli_estatus
     *
     * @return string
     */
    public function getCliEstatus()
    {
        return $this->cli_estatus;
    }

    /**
     * Returns the value of field cli_fec_creacion
     *
     * @return string
     */
    public function getCliFecCreacion()
    {
        return $this->cli_fec_creacion;
    }

    /**
     * Returns the value of field usu_id
     *
     * @return string
     */
    public function getUsuId()
    {
        return $this->usu_id;
    }

    /**
     * Returns the value of field usu_id
     *
     * @return string
     */
    public function getSoloPromo()
    {
        return $this->solo_promo;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->belongsTo('usu_id', 'Tel_usuarios', 'id', array('alias' => 'Tel_usuarios'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_clientes';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'cli_codigo' => 'cli_codigo', 
            'cli_nombre' => 'cli_nombre', 
            'cli_tel_local' => 'cli_tel_local', 
            'cli_tel_celular' => 'cli_tel_celular', 
            'cli_direccion' => 'cli_direccion', 
            'cli_cor_electronico' => 'cli_cor_electronico', 
            'cli_estatus' => 'cli_estatus', 
            'cli_fec_creacion' => 'cli_fec_creacion', 
            'usu_id' => 'usu_id',
            'solo_promo' => 'solo_promo'
        );
    }

}
