<?php

class Modelos extends \Phalcon\Mvc\Model {

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $mod_nombre;

    /**
     *
     * @var string
     */
    protected $mod_estatus;

    /**
     *
     * @var string
     */
    protected $mar_id;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field mod_nombre
     *
     * @param string $mod_nombre
     * @return $this
     */
    public function setModNombre($mod_nombre)
    {
        $this->mod_nombre = $mod_nombre;

        return $this;
    }

    /**
     * Method to set the value of field mod_estatus
     *
     * @param string $mod_estatus
     * @return $this
     */
    public function setModEstatus($mod_estatus)
    {
        $this->mod_estatus = $mod_estatus;

        return $this;
    }

    /**
     * Method to set the value of field mar_id
     *
     * @param string $mar_id
     * @return $this
     */
    public function setMarId($mar_id)
    {
        $this->mar_id = $mar_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field mod_nombre
     *
     * @return string
     */
    public function getModNombre()
    {
        return $this->mod_nombre;
    }

    /**
     * Returns the value of field mod_estatus
     *
     * @return string
     */
    public function getModEstatus()
    {
        return $this->mod_estatus;
    }

    /**
     * Returns the value of field mar_id
     *
     * @return string
     */
    public function getMarId()
    {
        return $this->mar_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_modelos';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'mod_nombre' => 'mod_nombre', 
            'mod_estatus' => 'mod_estatus', 
            'mar_id' => 'mar_id'
        );
    }

}
