<?php

class Privilegios extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $pri_nombre;

    /**
     *
     * @var string
     */
    protected $sec_id;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field pri_nombre
     *
     * @param string $pri_nombre
     * @return $this
     */
    public function setPriNombre($pri_nombre)
    {
        $this->pri_nombre = $pri_nombre;

        return $this;
    }

    /**
     * Method to set the value of field sec_id
     *
     * @param string $sec_id
     * @return $this
     */
    public function setSecId($sec_id)
    {
        $this->sec_id = $sec_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field pri_nombre
     *
     * @return string
     */
    public function getPriNombre()
    {
        return $this->pri_nombre;
    }

    /**
     * Returns the value of field sec_id
     *
     * @return string
     */
    public function getSecId()
    {
        return $this->sec_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->hasMany('id', 'Tel_privilegios_roles', 'pri_id', array('alias' => 'Tel_privilegios_roles'));
        $this->belongsTo('sec_id', 'Tel_secciones', 'id', array('alias' => 'Tel_secciones'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_privilegios';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'pri_nombre' => 'pri_nombre', 
            'sec_id' => 'sec_id'
        );
    }

}
