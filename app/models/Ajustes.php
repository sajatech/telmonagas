<?php

class Ajustes extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $aju_observaciones;

    /**
     *
     * @var string
     */
    protected $aju_fec_creacion;

    /**
     *
     * @var string
     */
    protected $aju_estatus;

    /**
     *
     * @var string
     */
    protected $usu_id;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field aju_observaciones
     *
     * @param string $aju_observaciones
     * @return $this
     */
    public function setAjuObservaciones($aju_observaciones)
    {
        $this->aju_observaciones = $aju_observaciones;

        return $this;
    }

    /**
     * Method to set the value of field aju_fec_creacion
     *
     * @param string $aju_fec_creacion
     * @return $this
     */
    public function setAjuFecCreacion($aju_fec_creacion)
    {
        $this->aju_fec_creacion = $aju_fec_creacion;

        return $this;
    }

    /**
     * Method to set the value of field aju_estatus
     *
     * @param string $aju_estatus
     * @return $this
     */
    public function setAjuEstatus($aju_estatus)
    {
        $this->aju_estatus = $aju_estatus;

        return $this;
    }

    /**
     * Method to set the value of field usu_id
     *
     * @param string $usu_id
     * @return $this
     */
    public function setUsuId($usu_id)
    {
        $this->usu_id = $usu_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field aju_observaciones
     *
     * @return string
     */
    public function getAjuObservaciones()
    {
        return $this->aju_observaciones;
    }

    /**
     * Returns the value of field aju_fec_creacion
     *
     * @return string
     */
    public function getAjuFecCreacion()
    {
        return $this->aju_fec_creacion;
    }

    /**
     * Returns the value of field aju_estatus
     *
     * @return string
     */
    public function getAjuEstatus()
    {
        return $this->aju_estatus;
    }

    /**
     * Returns the value of field usu_id
     *
     * @return string
     */
    public function getUsuId()
    {
        return $this->usu_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->hasMany('id', 'Tel_movimientos', 'aju_id', array('alias' => 'Tel_movimientos'));
        $this->belongsTo('usu_id', 'Tel_usuarios', 'id', array('alias' => 'Tel_usuarios'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_ajustes';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'aju_observaciones' => 'aju_observaciones', 
            'aju_fec_creacion' => 'aju_fec_creacion', 
            'aju_estatus' => 'aju_estatus', 
            'usu_id' => 'usu_id'
        );
    }

}
