<?php

class PromocionCliente extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $cli_id;

    /**
     *
     * @var string
     */
    protected $pro_estatus;

    /**
     *
     * @var integer
     */
    protected $pro_id;

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     * Method to set the value of field cli_id
     *
     * @param integer $cli_id
     * @return $this
     */
    public function setCliId($cli_id)
    {
        $this->cli_id = $cli_id;

        return $this;
    }

    /**
     * Method to set the value of field pro_estatus
     *
     * @param string $pro_estatus
     * @return $this
     */
    public function setProEstatus($pro_estatus)
    {
        $this->pro_estatus = $pro_estatus;

        return $this;
    }

    /**
     * Method to set the value of field pro_id
     *
     * @param integer $pro_id
     * @return $this
     */
    public function setProId($pro_id)
    {
        $this->pro_id = $pro_id;

        return $this;
    }

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Returns the value of field cli_id
     *
     * @return integer
     */
    public function getCliId()
    {
        return $this->cli_id;
    }

    /**
     * Returns the value of field pro_estatus
     *
     * @return string
     */
    public function getProEstatus()
    {
        return $this->pro_estatus;
    }

    /**
     * Returns the value of field pro_id
     *
     * @return integer
     */
    public function getProId()
    {
        return $this->pro_id;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->setSource("tel_promocion_cliente");
        $this->belongsTo('cli_id', 'Clientes', 'id', ['alias' => 'Clientes']);
        $this->belongsTo('pro_id', 'Promociones', 'id', ['alias' => 'Promociones']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_promocion_cliente';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return TelPromocionCliente[]|TelPromocionCliente|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return TelPromocionCliente|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
