<?php

class Pagos extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $pag_fecha;

    /**
     *
     * @var string
     */
    protected $pag_num_referencia;

    /**
     *
     * @var string
     */
    protected $pag_importe;

    /**
     *
     * @var string
     */
    protected $pag_moneda;

    /**
     *
     * @var string
     */
    protected $pag_verificado;

    /**
     *
     * @var string
     */
    protected $pag_fec_creacion;

    /**
     *
     * @var string
     */
    protected $cue_id;

    /**
     *
     * @var string
     */
    protected $sal_id;

    /**
     *
     * @var string
     */
    protected $pag_forma;

    /**
     *
     * @var string
     */
    protected $pag_estatus;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field pag_fecha
     *
     * @param string $pag_fecha
     * @return $this
     */
    public function setPagFecha($pag_fecha)
    {
        $this->pag_fecha = $pag_fecha;

        return $this;
    }

    /**
     * Method to set the value of field pag_num_referencia
     *
     * @param string $pag_num_referencia
     * @return $this
     */
    public function setPagNumReferencia($pag_num_referencia)
    {
        $this->pag_num_referencia = $pag_num_referencia;

        return $this;
    }

    /**
     * Method to set the value of field pag_importe
     *
     * @param string $pag_importe
     * @return $this
     */
    public function setPagImporte($pag_importe)
    {
        $this->pag_importe = $pag_importe;

        return $this;
    }

    /**
     * Method to set the value of field pag_moneda
     *
     * @param string $pag_moneda
     * @return $this
     */
    public function setPagMoneda($pag_moneda)
    {
        $this->pag_moneda = $pag_moneda;

        return $this;
    }

    /**
     * Method to set the value of field pag_verificado
     *
     * @param string $pag_verificado
     * @return $this
     */
    public function setPagVerificado($pag_verificado)
    {
        $this->pag_verificado = $pag_verificado;

        return $this;
    }

    /**
     * Method to set the value of field pag_fec_creacion
     *
     * @param string $pag_fec_creacion
     * @return $this
     */
    public function setPagFecCreacion($pag_fec_creacion)
    {
        $this->pag_fec_creacion = $pag_fec_creacion;

        return $this;
    }

    /**
     * Method to set the value of field cue_id
     *
     * @param string $cue_id
     * @return $this
     */
    public function setCueId($cue_id)
    {
        $this->cue_id = $cue_id;

        return $this;
    }

    /**
     * Method to set the value of field sal_id
     *
     * @param string $sal_id
     * @return $this
     */
    public function setSalId($sal_id)
    {
        $this->sal_id = $sal_id;

        return $this;
    }

    /**
     * Method to set the value of field pag_forma
     *
     * @param string $pag_forma
     * @return $this
     */
    public function setPagForma($pag_forma)
    {
        $this->pag_forma = $pag_forma;

        return $this;
    }

    /**
     * Method to set the value of field pag_estatus
     *
     * @param string $pag_estatus
     * @return $this
     */
    public function setPagEstatus($pag_estatus)
    {
        $this->pag_estatus = $pag_estatus;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field pag_fecha
     *
     * @return string
     */
    public function getPagFecha()
    {
        return $this->pag_fecha;
    }

    /**
     * Returns the value of field pag_num_referencia
     *
     * @return string
     */
    public function getPagNumReferencia()
    {
        return $this->pag_num_referencia;
    }

    /**
     * Returns the value of field pag_importe
     *
     * @return string
     */
    public function getPagImporte()
    {
        return $this->pag_importe;
    }

    /**
     * Returns the value of field pag_moneda
     *
     * @return string
     */
    public function getPagMoneda()
    {
        return $this->pag_moneda;
    }

    /**
     * Returns the value of field pag_verificado
     *
     * @return string
     */
    public function getPagVerificado()
    {
        return $this->pag_verificado;
    }

    /**
     * Returns the value of field pag_fec_creacion
     *
     * @return string
     */
    public function getPagFecCreacion()
    {
        return $this->pag_fec_creacion;
    }

    /**
     * Returns the value of field cue_id
     *
     * @return string
     */
    public function getCueId()
    {
        return $this->cue_id;
    }

    /**
     * Returns the value of field sal_id
     *
     * @return string
     */
    public function getSalId()
    {
        return $this->sal_id;
    }

    /**
     * Returns the value of field pag_forma
     *
     * @return string
     */
    public function getPagForma()
    {
        return $this->pag_forma;
    }

    /**
     * Returns the value of field pag_estatus
     *
     * @return string
     */
    public function getPagEstatus()
    {
        return $this->pag_estatus;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->belongsTo('sal_id', 'Tel_salidas', 'id', array('alias' => 'Tel_salidas'));
        $this->belongsTo('cue_id', 'Tel_cuentas', 'id', array('alias' => 'Tel_cuentas'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_pagos';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'pag_fecha' => 'pag_fecha', 
            'pag_num_referencia' => 'pag_num_referencia', 
            'pag_importe' => 'pag_importe', 
            'pag_moneda' => 'pag_moneda', 
            'pag_verificado' => 'pag_verificado', 
            'pag_fec_creacion' => 'pag_fec_creacion', 
            'cue_id' => 'cue_id', 
            'sal_id' => 'sal_id', 
            'pag_forma' => 'pag_forma', 
            'pag_estatus' => 'pag_estatus'
        );
    }

}
