<?php

class Movimientos extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $mov_tipo;

    /**
     *
     * @var string
     */
    protected $mov_cantidad;

    /**
     *
     * @var string
     */
    protected $mov_estatus;

    /**
     *
     * @var string
     */
    protected $mov_pre_venta;

    /**
     *
     * @var string
     */
    protected $art_id;

    /**
     *
     * @var string
     */
    protected $mot_id;

    /**
     *
     * @var string
     */
    protected $ing_id;

    /**
     *
     * @var string
     */
    protected $sal_id;

    /**
     *
     * @var string
     */
    protected $aju_id;

    /**
     *
     * @var string
     */
    protected $mov_fec_creacion;

    /**
     *
     * @var string
     */
    protected $mov_importe;

    /**
     *
     * @var string
     */
    protected $mov_costo;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field mov_tipo
     *
     * @param string $mov_tipo
     * @return $this
     */
    public function setMovTipo($mov_tipo)
    {
        $this->mov_tipo = $mov_tipo;

        return $this;
    }

    /**
     * Method to set the value of field mov_cantidad
     *
     * @param string $mov_cantidad
     * @return $this
     */
    public function setMovCantidad($mov_cantidad)
    {
        $this->mov_cantidad = $mov_cantidad;

        return $this;
    }

    /**
     * Method to set the value of field mov_estatus
     *
     * @param string $mov_estatus
     * @return $this
     */
    public function setMovEstatus($mov_estatus)
    {
        $this->mov_estatus = $mov_estatus;

        return $this;
    }

    /**
     * Method to set the value of field mov_pre_venta
     *
     * @param string $mov_pre_venta
     * @return $this
     */
    public function setMovPreVenta($mov_pre_venta)
    {
        $this->mov_pre_venta = $mov_pre_venta;

        return $this;
    }

    /**
     * Method to set the value of field art_id
     *
     * @param string $art_id
     * @return $this
     */
    public function setArtId($art_id)
    {
        $this->art_id = $art_id;

        return $this;
    }

    /**
     * Method to set the value of field mot_id
     *
     * @param string $mot_id
     * @return $this
     */
    public function setMotId($mot_id)
    {
        $this->mot_id = $mot_id;

        return $this;
    }

    /**
     * Method to set the value of field ing_id
     *
     * @param string $ing_id
     * @return $this
     */
    public function setIngId($ing_id)
    {
        $this->ing_id = $ing_id;

        return $this;
    }

    /**
     * Method to set the value of field sal_id
     *
     * @param string $sal_id
     * @return $this
     */
    public function setSalId($sal_id)
    {
        $this->sal_id = $sal_id;

        return $this;
    }

    /**
     * Method to set the value of field aju_id
     *
     * @param string $aju_id
     * @return $this
     */
    public function setAjuId($aju_id)
    {
        $this->aju_id = $aju_id;

        return $this;
    }

    /**
     * Method to set the value of field mov_fec_creacion
     *
     * @param string $mov_fec_creacion
     * @return $this
     */
    public function setMovFecCreacion($mov_fec_creacion)
    {
        $this->mov_fec_creacion = $mov_fec_creacion;

        return $this;
    }

    /**
     * Method to set the value of field mov_importe
     *
     * @param string $mov_importe
     * @return $this
     */
    public function setMovImporte($mov_importe)
    {
        $this->mov_importe = $mov_importe;

        return $this;
    }

    /**
     * Method to set the value of field mov_costo
     *
     * @param string $mov_costo
     * @return $this
     */
    public function setMovCosto($mov_costo)
    {
        $this->mov_costo = $mov_costo;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field mov_tipo
     *
     * @return string
     */
    public function getMovTipo()
    {
        return $this->mov_tipo;
    }

    /**
     * Returns the value of field mov_cantidad
     *
     * @return string
     */
    public function getMovCantidad()
    {
        return $this->mov_cantidad;
    }

    /**
     * Returns the value of field mov_estatus
     *
     * @return string
     */
    public function getMovEstatus()
    {
        return $this->mov_estatus;
    }

    /**
     * Returns the value of field mov_pre_venta
     *
     * @return string
     */
    public function getMovPreVenta()
    {
        return $this->mov_pre_venta;
    }

    /**
     * Returns the value of field art_id
     *
     * @return string
     */
    public function getArtId()
    {
        return $this->art_id;
    }

    /**
     * Returns the value of field mot_id
     *
     * @return string
     */
    public function getMotId()
    {
        return $this->mot_id;
    }

    /**
     * Returns the value of field ing_id
     *
     * @return string
     */
    public function getIngId()
    {
        return $this->ing_id;
    }

    /**
     * Returns the value of field sal_id
     *
     * @return string
     */
    public function getSalId()
    {
        return $this->sal_id;
    }

    /**
     * Returns the value of field aju_id
     *
     * @return string
     */
    public function getAjuId()
    {
        return $this->aju_id;
    }

    /**
     * Returns the value of field mov_fec_creacion
     *
     * @return string
     */
    public function getMovFecCreacion()
    {
        return $this->mov_fec_creacion;
    }

    /**
     * Returns the value of field mov_importe
     *
     * @return string
     */
    public function getMovImporte()
    {
        return $this->mov_importe;
    }

    /**
     * Returns the value of field mov_costo
     *
     * @return string
     */
    public function getMovCosto()
    {
        return $this->mov_costo;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->belongsTo('aju_id', 'Tel_ajustes', 'id', array('alias' => 'Tel_ajustes'));
        $this->belongsTo('ing_id', 'Tel_ingresos', 'id', array('alias' => 'Tel_ingresos'));
        $this->belongsTo('art_id', 'Tel_articulos', 'id', array('alias' => 'Tel_articulos'));
        $this->belongsTo('mot_id', 'Tel_motivos', 'id', array('alias' => 'Tel_motivos'));
        $this->belongsTo('sal_id', 'Tel_salidas', 'id', array('alias' => 'Tel_salidas'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tel_movimientos';
    }
    
    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'mov_tipo' => 'mov_tipo', 
            'mov_cantidad' => 'mov_cantidad', 
            'mov_estatus' => 'mov_estatus', 
            'mov_pre_venta' => 'mov_pre_venta', 
            'art_id' => 'art_id', 
            'mot_id' => 'mot_id', 
            'ing_id' => 'ing_id', 
            'sal_id' => 'sal_id', 
            'aju_id' => 'aju_id', 
            'mov_fec_creacion' => 'mov_fec_creacion', 
            'mov_importe' => 'mov_importe', 
            'mov_costo' => 'mov_costo'
        );
    }

}